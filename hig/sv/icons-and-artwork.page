<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="icons-and-artwork" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Att välja och skapa ikoner.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Ikoner och grafik</title>

<p>Ikoner är ett grundläggande element i användargränssnitt. De utgör också en grundläggande del av programs identitet. Denna sida tillhandahåller en översikt över ikonanvändning. Den beskriver också vilka ikoner som är tillgängliga via GNOME-plattformen, introducerar resurser för att skapa nya ikoner och inkluderar en del allmänna riktlinjer för användning av ikoner i användargränssnitt.</p>

<section id="icon-styles">
<title>Ikonstilar</title>

<p>Två ikonstilar används i GNOME 3: fullfärgsikoner och symboliska ikoner.</p>

<p>Fullfärgsikoner är färgglada och detaljerade och är optimerade för större storlekar. De är definierade som SVG:er på 128✕128 bildpunkter och är skarpast om de skalas upp i multipler om 128 (så som 256×256 och 512×512). Designen för fullfärgsikoner möjliggör också för dem att renderas skarpt vid 64×64 och 32×32 bildpunkter, men det är inte rekommenderat att göra dem mindre än så.</p>

<media type="image" mime="image/png" src="figures/icons/fullcolor-v-symbolic.svg"/>

<p>Symboliska ikoner är enkla och monokroma och har designats för att fungera väl vid mindre storlekar. De är definierade som SVG:er på 16✕16 bildpunkter och kan skalas till multipler av 16 (så som 32✕32, 64✕64, 128✕128). Symboliska ikoner har generellt en neutral färg, men deras färg kan ändras programmatiskt.</p>

</section>

<section id="icon-uses">
<title>Ikonanvändning</title>


<p>Programikoner är den mest framträdande typen av ikoner. Varje program bör ha en egen unik och vacker programikon: den är programmets ansikte och det första visuella elementet en användare ser när de letar efter nya program.</p>

<p>Programikoner använder fullfärgsikoner. Program rekommenderas också att tillhandahålla symboliska versioner av deras ikoner, vilka används för hjälpmedelsfunktioner med högkontrast, så väl som i sammanhang där en tydlig lågupplöst ikon behövs.</p>

<p>Ikoner i användargränssnittet använder vanligtvis den symboliska stilen och detta är den primära ikonstilen som används för användargränssnittskomponenter. Det vanligaste och mest uppenbara exemplet på användning av symboliska ikoner är knappar.</p>

<p>Förutom programikoner kan ikoner i fullfärgsstil också används i sammanhang där ikoner visas vid stora storlekar och är avsedda att vara i fokus. Fil- och mappikoner i en filhanterare är ett bra exempel på detta.</p>

</section>

<section id="icon-resources">
<title>Standardikoner och skapa dina egna</title>

<p>GNOME tillhandahåller en standarduppsättning av ikoner som kan användas av program. Denna kan kommas åt direkt via GTK. Ikonnamn följer <link href="https://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html">Freedesktops ikonspecifikation</link>. Namnen på symboliska versioner av ikoner slutar med <code>-symbolic</code>, så som <code>open-menu-symbolic</code>.</p>

<p><link xref="icon-design">Riktlinjer för ikondesign</link> tillhandahåller ytterligare detaljer om hur du skapar dina egna ikoner, inklusive programikoner.</p>

</section>

<section id="using-icons">
<title>Att använda ikoner i ditt användargränssnitt</title>

<p>Ikoner är ett vanligt element i användargränssnitt och de har en del praktiska fördelar framför textetiketter (så som att de är mindre). Samtidigt kan överanvändning av ikoner leda till förvirring och en dålig användarupplevelse. På samma sätt kan användning av olämpliga ikoner ofta göra ett gränssnitt svårt att använda.</p>

<p>Använd därför endast ikoner vars betydelse är allmänt erkänd. Om en allmänt erkänd ikon inte är tillgänglig kan det vara bättre att använda en textetikett istället. Konventioner etablerar vanligtvis vilka ikoner som är allmänt erkända. Denna uppsättning av ikoner är i själva verket ganska liten och omfattar standardikoner så som sökning, meny, framåt, bakåt och dela. Om du är osäker, använd ikoner som frekvent används i andra program.</p>

<p>Andra saker att tänka på när ikoner används:</p>

<list>
<item><p>Tänk på vilka ikoner som kommer att vara meningsfulla i ett speciellt sammanhang i ditt program — användare av specialistverktyg kommer ofta att vara bekanta med domänspecifika symboler.</p></item>
<item><p>Kom ihåg att vissa ikoner endast är meningsfulla bredvid andra ikoner av samma typ. En mediaikon för att stoppa är vanligtvis en fyrkant och kan till exempel komma att inte kännas igen som en stoppikon om inte andra mediakontroller (så som uppspelning, pausa eller hoppa över) syns i närheten. På samma sätt kan en ikon för att ta bort ett objekt från en lista, som visas som en subtraktionssymbol (d.v.s ett streck) komma att inte kännas igen utan en motsvarande ”plus”-ikon för tillägg.</p></item>
</list>

</section>

</page>
