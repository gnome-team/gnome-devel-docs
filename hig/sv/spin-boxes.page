<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="spin-boxes" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/spin-boxes.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Stegningsrutor</title>

<p>En stegningsruta är en textruta som accepterar ett intervall av värden. Den har två knappar som låter användaren öka eller minska värdet med en bestämd mängd.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/spin-boxes.svg"/>

<section id="when-to-use">
<title>När ska de användas</title>

<p>Använd stegningsrutor för att låta användare välja numeriska värden, men bara när dessa värden har mening eller är användbara för användare att känna till. Om de inte är det kan ett <link xref="sliders">skjutreglage</link> vara ett bättre val.</p>

<p>Använd stegningsrutor endast för numerisk inmatning. För icke-numeriska uppsättningar alternativ kan istället en <link xref="lists">lista</link> eller <link xref="drop-down-lists">rullgardinslista</link> användas.</p>

</section>

<section id="sliders">
<title>Skjutreglage</title>

<p>I vissa fall är det lämpligt att länka en stegningsruta till ett skjutreglage. Denna kombination tillåter både ungefärlig kontroll samt inmatning av specifika värden. Detta är dock endast användbart om du vill underlätta för personer som kan veta ett specifikt värde som de vill använda. Använd ett skjutreglage när:</p>

<list>
<item><p>Omedelbar återkoppling för ändringar i stegningsrutans värde är möjlig (så som vid bildredigering).</p></item>
<item><p>Det är användbart för användaren att styra ändringens hastighet i realtid. Exempelvis för att övervaka effekterna av en färgändring i ett förhandsgranskningsfönster som uppdateras då de drar i RGB-skjutreglagen.</p></item>
</list>

</section>

<section id="general-guidelines">
<title>Allmänna riktlinjer</title>

<list>
<item><p>Ge stegningsrutan en textetikett och placera den ovanför eller till vänster om stegningsrutan, med stor bokstav i början på mening. Tillhandahåll en snabbtangent i etiketten som låter användaren ge stegningsrutan fokus direkt.</p></item>
<item><p>Högerjustera innehållen i stegningsrutor, om inte konventionen i användarens lokal kräver annat. Detta är användbart i fönster där användaren kan vilja jämföra två numeriska värden i samma kolumn av komponenter. Säkerställ i detta fall även att högerkanterna för de relevanta kolumnerna är uppradade.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-referens</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSpinButton.html">GtkSpinButton</link></p></item>
</list>

</section>

</page>
