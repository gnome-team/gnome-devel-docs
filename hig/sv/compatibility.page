<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="compatibility" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Använda riktlinjerna för program på flera plattformar eller program av GNOME 2-stil.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Kompatibilitet</title>

<p>Dessa riktlinjer för mänskliga gränssnitt riktar sig främst mot nya GTK- och GNOME-program, med betoning på GNOME 3-integration. De är dock även avsedda att vara användbara för plattformsoberoende program, så väl som för äldre program som kan ha designats och implementerats i enlighet med riktlinjerna för GNOME 2.</p>

<section id="cross-platform-compatibility">
<title>Kompatibilitet mellan plattformar</title>

<p>Allmänt kan designmönstren som hittas i riktlinjerna för mänskliga gränssnitt användas på alla skrivbordsplattformar. De flesta konventionerna, så som menyer och menyknappar, vyer och vyväxlare, samt kontextfönster är vanliga och kommer allmänt att bli förstådda.</p>

<p>Det främsta exemplet på ett designmönster som varierar mellan skrivbordsplattformar är <link xref="menu-bars">menyraden</link>, vilken antar olika former på olika plattformar. Se designsidan för detta för mer råd om saker att ha i åtanke för program på flera plattformar.</p>

</section>

<section id="gnome-2-compatibility">
<title>Kompatibilitet med GNOME 2</title>

<p>Dessa riktlinjer för mänskliga gränssnitt är en vidareutveckling av riktlinjerna för GNOME 2. Program som följer den nya versionen av riktlinjerna kommer att använda utrymme mer effektivt, tillhandahåller en mer fokuserad användarupplevelse, och kommer att använda moderna tolkningar av nyckelfunktionalitet, så som sökning, integration med nätet eller aviseringar.</p>

<p>Att följa några av designmönstren i dessa riktlinjer kan implicera storskaliga designändringar för program av GNOME 2-stil, speciellt om dessa program är komplexa. Specifikt skulle att ersätta namnlister och <link xref="menu-bars">menyrader</link> för fönster med en <link xref="header-bars">rubrikrad</link> kunna vara en stor ändring för ditt program.</p>

<p>Många av råden som finns i dessa riktlinjer för mänskliga gränssnitt kan dock införlivas i program av GNOME 2-stil med minimal störning, och kommer att leda till en förbättrad upplevelse för användare. Detta inkluderar:</p>

<list>
<item><p>Att använda nya användargränssnittselement så som kontextfönster.</p></item>
<item><p>Råd om nya GTK-möjligheter, som animationer.</p></item>
<item><p>Moderniserade råd för ämnen så som visuell layout, typografi och ikonanvändning.</p></item>
</list>

<p>Att införliva dessa element ur riktlinjerna kan förbättra ditt program utan att kräva en stor designändring, och alla GTK- och GNOME-program kan dra nytta av dem.</p>

<p>Dessa riktlinjer för mänskliga gränssnitt har designats för att ge dig stöd i att avgöra den bästa designen för ditt program, snarare än att föreslå en enda mall som ska användas i alla program. Därför tillhandahåller de, även om de rekommenderar <link xref="header-bars">rubrikrader</link> över <link xref="menu-bars">menyrader</link>, riktlinjer för båda infallsvinklarna.</p>

</section>

</page>
