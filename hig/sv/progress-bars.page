<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="progress-bars" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/progress-bars.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Förloppsindikator</title>

<p>Förloppsindikatorer indikerar att en uppgift håller på att utföras, samt hur stor del av uppgiften som har utförts.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/progress-bars.svg"/>

<section id="when-to-use">
<title>När ska de användas</title>

<p>Det är vanligtvis nödvändigt att indikera ett händelseförlopp när en åtgärd tar mer än cirka tre sekunder. Detta försäkrar att användare förstår att de måste vänta och att ett fel inte har inträffat.</p>

<p>När man vill indikera ett förlopp är det huvudsakliga valet mellan en förloppsindikator eller en <link xref="progress-spinners">väntesnurror</link>. Förloppsindikatorer indikerar hur stor del av en uppgift som har slutförts. De är därför användbara för uppgifter som tar lång tid. Som en tumregel används en förloppsindikator endast för uppgifter som tar mer än 30 sekunder. För uppgifter som tar kortare tid är <link xref="progress-spinners">väntesnurror</link> ofta ett bättre val.</p>

</section>

<section id="types">
<title>Typer</title>

<p>Det finns tre typer av förloppsindikatorer:</p>

<list>
<item><p>Tid som återstår: dessa indikerar hur mycket tid som återstår av en åtgärd.</p></item>
<item><p>Typisk tid: dessa indikerar hur mycket tid som återstår, baserat på en uppskattning av den förväntade varaktigheten.</p></item>
<item><p>Obestämbar: dessa indikerar endast att en åtgärd pågår, inte hur lång tid den kommer att ta.</p></item>
</list>

<p>Noggrannhet är att föredra för förloppsindikatorer. När det är möjligt, använd en förloppsindikator som visar återstående tid, följt av typisk tid. Försök att undvika att använda obestämbara förloppsindikatorer.</p>

</section>

<section id="progress-text">
<title>Förloppstext</title>

<p>Varje förloppsindikator kan inkludera en textbeskrivning. Denna text bör ge en uppfattning om hur mycket av uppgiften som har slutförts. När du bestämmer dig för en förloppstext:</p>

<list>
<item><p>Betänk alltid vad som är mest relevant och intressant för användaren.</p></item>
<item><p>Det är ofta bättre att tillhandahålla specifik information snarare än en enhetslös procentsats. Använd till exempel <gui>13 av 19 bilder roterade</gui> eller <gui>12,1 av 30 MB hämtade</gui> snarare än <gui>13% färdigt</gui>.</p></item>
<item><p>För uppgifter som tar lång tid kan det vara att föredra att visa en uppskattning av tiden som återstår i förloppstexten. Om annan relevant information inte är tillgänglig kan endast detta visas. Alternativt, kan de visas vid sidan om texten om förloppet; var dock uppmärksam på att inte överväldiga användaren med allt för mycket information när detta görs och använd <link xref="typography">de typografiska konventionerna</link> för att särskilja den mest användbara informationen.</p></item>
<item><p>Om tiden som återstår är en uppskattning använd ordet <gui>ungefär</gui>, till exempel <gui>Ungefär 3 minuter kvar</gui>.</p></item>
</list>

</section>

<section id="task-stages">
<title>Uppgiftsetapper</title>

<p>Några uppgifter kan bestå av en serie av etapper som utförs efter varandra, där var och en har olika alternativ för tidsuppskattning. Det kan exempelvis vara möjligt att bedöma kvarvarande tid för en del av en uppgift, men inte för en annan del. I dessa situationer:</p>

<list>
<item><p>Kommunicera endast de olika etapperna av en uppgift då de är relevanta för användaren. I allmänhet kommer detta inte att behövas, och det är inte nödvändigt eller önskvärt att kommunicera enskilda etapper i en uppgift.</p></item>
<item><p>Om en uppgift innehåller etapper som beräknar kvarvarande tid respektive typisk tid, försök att skapa en sammansatt förloppsindikator som anger typisk tid.</p></item>
<item><p>Om en uppgift innehåller en förloppsetapp av obestämbar längd så kan förloppsindikatorn visa okänt förlopp för en del av uppgiften. Du bör dock undvika att visa obestämda förloppsindikatorer under lång tid, och bör försöka hålla antalet lägesändringar för förloppsindikatorer till ett absolut minimum. Undvik obestämda förlopp närhelst det är möjligt.</p></item>
</list>

</section>

<section id="sub-tasks">
<title>Deluppgifter</title>

<p>Om en uppgift består av flera deluppgifter (så som att hämta flera filer samtidigt) så är det i allmänhet rekommenderat att visa en enda förloppsindikator som indikerar det sammansatta förloppet för hela uppgiften. Det finns dock några situationer där detta kanske inte är fallet:</p>

<list>
<item><p>Om det är genuint användbart för användaren att veta förloppet för varje individuell deluppgift. (Som ett alternativ kan slutförande av varje deluppgift indikeras med en förloppstext.)</p></item>
<item><p>Om det kan vara nödvändigt att pausa eller stoppa en deluppgift (se de <link xref="#general-guidelines">allmänna riktlinjerna</link> för detta nedan).</p></item>
<item><p>Om deluppgifter redan indikeras i ditt programs användargränssnitt. I detta fall kan det vara mindre störande att visa förloppet för varje objekt inbäddat.</p></item>
</list>

<p>Då förloppsindikatorer visas för deluppgifter:</p>

<list>
<item><p>Bör varje deluppgift följa riktlinjerna för användning av förloppsindikatorer (se <link xref="#when-to-use">när ska de användas</link> ovan).</p></item>
<item><p>I allmänhet är det inte nödvändigt att visa en förloppsindikator för totalt förlopp genom uppsättningen av uppgifter.</p></item>
</list>

</section>

<section id="progress-windows">
<title>Förloppsfönster</title>

<p>Tidigare var förloppsfönster ett populärt sätt att presentera förloppsindikatorer. Dessa sekundära fönster visades under tiden en uppgift behandlades och innehöll en eller flera förloppsindikatorer. Generellt rekommenderas inte förloppsfönster eftersom konsekvenserna av att stänga fönstret kan vara oklara och att de kan skymma användbara komponenter och innehåll.</p>

<p>Där det är möjligt bör förloppsindikatorer visas inbäddade och bör ha en tät, visuell relation med innehållsobjekten eller komponenterna som representerar den pågående uppgiften.</p>

</section>

<section id="general-guidelines">
<title>Allmänna riktlinjer</title>

<list>
<item><p>Om åtgärden som pågår är destruktiv eller resursintensiv, överväg att placera en paus- och/eller avbrytknapp nära förloppsindikatorn.</p></item>
<item><p>Säkerställ att förloppsindikatorns återstående tid och typisk tid mäter en åtgärds totala tid eller totala arbete, inte enbart den för ett enstaka steg.</p></item>
<item><p>Uppdatera förloppsindikatorns återstående tid när förändringar inträffar som kan förorsaka att åtgärden avslutas snabbare eller långsammare.</p></item>
<item><p>När en förloppsindikator med typisk tid används kan, om ditt program överskattar mängden färdigt arbete kan längden på indikatorn indikera <gui>nästan färdigt</gui> tills åtgärden är klar. Om ditt program underskattar hur mycket arbete som är färdigt, fyll den återstående delen av indikatorn när åtgärden är klar.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-referens</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkProgressBar.html">GtkProgressBar</link></p></item>
</list>
</section>

</page>
