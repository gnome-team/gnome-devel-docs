<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="text-fields" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/text-fields.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Textfält</title>

<p>Ett textinmatningsfält är ett gränssnittselement för att mata in eller redigera text. Det är ett grundläggande element med ett flertal användningsområden, inklusive sökinmatning, inställningar eller kontokonfigurering och inställning. Ett textinmatningsfält kan förifyllas med text och kan inkludera ytterligare knappar eller ikoner.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/text-fields.svg"/>

<section id="general-guidelines">
<title>Allmänna riktlinjer</title>

<list>
<item><p>Ställ in storleken på textfält efter den troliga storleken på innehållet de kommer att ha. Detta ger en användbar visuell antydan till hur mycket inmatning som förväntas och förhindrar överspill.</p></item>
<item><p>I en <link xref="dialogs#instant-and-explicit-apply">”tillämpas omedelbart”-dialog</link>, validera innehållet i inmatningsfältet när det tappar fokus eller när fönstret stängs, inte efter varje tangentnedslag. Undantag: om fältet endast accepterar ett begränsat antal tecken, så som hexadecimala färgkoder, validera och tillämpa ändringen så snart det antalet tecken har matats in.</p></item>
<item><p>Om du implementerar ett inmatningsfält som endast accepterar vissa tecken, så som siffror, spela upp systemets varningspip när användaren försöker att mata in ett ogiltigt tecken.</p></item>
<item><p>Normalt när användaren trycker <key>Tabb</key> i ett enradsinmatningsfält bör fokus flyttas till nästa komponent och i ett flerradsinmatningsfält bör det infoga ett tabbtecken. Att trycka på <keyseq><key>Ctrl</key><key>Tabb</key></keyseq> i ett flerradsinmatningsfält bör flytta fokus till nästa komponent.</p></item>
<item><p>Om du behöver tillhandahålla en tangentbordsgenväg som infogar ett tabbtecken i ett enradsinmatningsfält, använd <keyseq><key>Ctrl</key><key>Tabb</key></keyseq>. Det är dock osannolikt att du kommer att hitta många situationer där detta är användbart.</p></item>
</list>

</section>

<section id="embedding-info-and-controls">
<title>Bädda in information och komponenter</title>

<p>En mängd av ytterligare information eller komponenter kan infogas i ett textinmatningsfält.</p>

<p>Ikoner eller ikonknappar kan placeras i ett textfält för att tillhandahålla statusinformation eller ytterligare komponenter.</p>

<list>
<item><p>En ikon i början av inmatningsfältet kan användas för att indikera dess syfte — vilket då ersätter behovet av att ha en etikett på inmatningsfältet. Inmatningsfält för sökning är det klassiska exemplet på detta, där en sökikon placeras till vänster i inmatningsfältet.</p></item>
<item><p>Om texten som ska matas in är skiftlägeskänslig kan en varningsikon visas inuti textfältet om caps lock är på. Detta visas vanligen på den högra sidan om inmatningsfältet.</p></item>
<item><p>Om det är vanligt att textfältet rensas så kan en ikonknapp för rensning placeras i fältet, på höger sida.</p></item>
<item><p>Om du placerar en ikon i ett textinmatningsfält (antingen som en indikator eller en knapp), använd dess symboliska variant från GNOME:s symboliska ikontema.</p></item>
</list>

<p>Då en användare skulle ha nytta av ytterligare information för att använda ett textinmatningsfält så kan det förifyllas med en hjälptext. Som med varje beslut om att visa ytterligare information så bör detta endast göras då det är nödvändigt.</p>

</section>

<section id="api-reference">
<title>API-referens</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkEntry.html">GtkEntry</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html">GtkSearchEntry</link></p></item>
</list>

</section>
</page>
