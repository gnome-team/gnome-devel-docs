<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pointer-and-touch-input" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Interaktion med mus, styrplatta och pekskärm.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Pekar- och pekinmatning</title>

<p>Inmatning med pekare och pekinmatning är två av de främsta sätten genom vilka användare kommer att interagera med ditt program.</p>

<section id="pointer-input">
<title>Pekarinmatning</title>

<p>Ett pekdon är en inmatningsenhet som tillåter manipulering av en pekare — typiskt representerad med en pil, och ofta kallad en markör — på skärmen. Även om möss och styrplattor är vanligast, så finns det ett stort antal sådana enheter, inkluderande ritplattor, styrkulor, styrpinnar och styrspakar.</p>

<section id="primary-and-secondary-buttons">
<title>Primär och sekundär knapp</title>

<p>Möss och styrplattor har ofta två huvudknappar. En av dessa agerar som den primära knappen, och den andra som den sekundära knappen. Typiskt används vänsterknappen som primär knapp och den högra knappen som sekundär knapp. Denna ordning går dock att konfigurera av användaren och gäller inte för inmatning på pekskärm. Dessa riktlinjer hänvisar därför till primär och sekundär åtgärd snarare än vänster och höger.</p>

<p>Använd den primära åtgärden för att markera objekt och aktivera komponenter. Den sekundära åtgärden kan användas för att komma åt ytterligare alternativ, vanligen genom en snabbvalsmeny.</p>

<p>Var inte beroende av inmatning från sekundära eller andra ytterligare knappar. Så väl som att dessa är fysiskt svårare att klicka på så stöder eller emulerar vissa pekdon och många hjälpmedelsteknologier bara den primära knappen.</p>

<p>Tryck och håll ner bör användas för att simulera den sekundära knappen för pekdon med en knapp. Använd därför inte tryck och håll ner för andra syften.</p>

</section>

<section id="general-guidelines">
<title>Allmänna riktlinjer</title>

<list>
<item><p>Dubbelklick bör inte användas, då det inte går att upptäcka, och är svårt att översätta till pekinmatning.</p></item>
<item><p>Om det finns ett rullningshjul på musen bör detta rulla fönstret eller komponenten under pekaren, om stöd för rullning finns. Att initiera rullning på detta sätt bör inte flytta tangentbordsfokus till fönstret eller komponenten som rullas.</p></item>
<item><p>Kräv inte användandet av ackord (att trycka ned flera musknappar samtidigt) för några åtgärder.</p></item>
<item><p>Kräv inte användning av flera (tredubbla eller fyrdubbla) klickåtgärder för några operationer, om du inte också tillhandahåller en tillgänglig alternativ metod för att utföra samma åtgärd.</p></item>
<item><p>Tillåt alla musåtgärder att avbrytas innan de slutförts. Att trycka ned <key>Esc</key> bör avbryta alla pågående musåtgärder, så som att dra och släppa en fil i en filhanterare, eller att rita en form i ett ritprogram.</p></item>
<item><p>Hänvisa inte till specifika musknappar i ditt gränssnitt om inte absolut nödvändigt. Alla kommer inte att använda en vanlig mus med vänster-, mitt- och högerknappar, så alla texter eller diagram som hänvisar till dessa kan vara förvirrande.</p></item>
</list>

</section>

<section id="mouse-and-keyboard-equivalents">
<title>Mus- och tangentbordsekvivalenter</title>

<p>Försäkra att varje åtgärd i ditt program som kan genomföras med musen också kan genomföras med tangentbordet. Det enda undantaget är åtgärder där finmotorik är en väsentlig del av uppgiften. Till exempel att kontrollera rörelsen i vissa typer av actionspel eller att rita på frihand i ett bildredigeringsprogram.</p>

<p>Om ditt program tillåter att objekt markeras bör följande ekvivalenta åtgärder finnas tillgängliga.</p>

<table>
<thead>
<tr>
<td><p>Åtgärd</p></td>
<td><p>Mus</p></td>
<td><p>Tangentbord</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Öppna ett objekt</p></td>
<td><p>Primär knapp</p></td>
<td><p><key>Blanksteg</key></p></td>
</tr>
<tr>
<td><p>Lägg till/ta bort objekt från markeringen</p></td>
<td><p><key>Ctrl</key> och primär knapp</p></td>
<td><p><keyseq><key>Ctrl</key><key>Blanksteg</key></keyseq></p></td>
</tr>
<tr>
<td><p>Utöka markering</p></td>
<td><p><key>Skift</key> och primär knapp</p></td>
<td><p><key>Skift</key> i kombination med någon av följande: <key>Blanksteg</key> <key>Home</key> <key>End</key> <key>PageUp</key> <key>PageDown</key></p></td>
</tr>
<tr>
<td><p>Ändra markering</p></td>
<td><p>Primär knapp</p></td>
<td><p>Någon av följande: <key>←</key> <key>↑</key> <key>→</key> <key>↓</key> <key>Home</key> <key>End</key> <key>PageUp</key> <key>PageDown</key></p></td>
</tr>
<tr>
<td><p>Markera alla</p></td>
<td><p>Primär knapp på det första objektet, sedan primär knapp och <key>Skift</key> på det sista objektet</p></td>
<td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
</tr>
<tr>
<td><p>Avmarkera alla</p></td>
<td><p>Primärklick på behållarens bakgrund</p></td>
<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>A</key></keyseq></p></td>
</tr>
</tbody>
</table>

</section>
</section>

<section id="touch-input">
<title>Pekinmatning</title>

<p>Pekskärmar är också en alltmer vanlig del av modern datorhårdvara, och program som skapats med GTK är troliga att användas med hårdvara som innehåller en pekskärm. För att få ut det mesta av den här hårdvaran, och för att följa användares förväntningar så är det därför viktigt att överväga pekinmatning som en del av programdesignen.</p>

<section id="application-touch-conventions">
<title>Pekkonventioner för program</title>

<p>Att använda pekinmatning konsekvent med andra program kommer låta användare att lätt lära sig hur de använder ditt program med en pekskärm. Följande konventioner rekommenderas där de är relevanta.</p>

<table>
<thead>
<tr>
<td><p>Åtgärd</p></td>
<td><p>Beskrivning</p></td>
<td><p>Resultat</p></td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="3"><p><em style="strong">Trycka</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/tap.svg"/></td>
<td><p>Tryck på ett objekt.</p></td>
<td><p>Primär åtgärd. Objekt öppnas — foton visas i fullständig storlek, program startas, låtar börjar spelas.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Tryck och håll ner</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/tap-and-hold.svg"/></td>
<td><p>Tryck och håll ner en sekund eller två.</p></td>
<td><p>Sekundär åtgärd. Markera objektet och lista åtgärder som kan utföras.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Dra</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/drag.svg"/></td>
<td><p>Dra ett finger som rör ytan.</p></td>
<td><p>Rullar området på skärmen.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Nyp ihop eller sträck ut</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/pinch-or-stretch.svg"/></td>
<td><p>Rör ytan med två fingrar medan du drar ihop eller isär dem.</p></td>
<td><p>Ändrar zoomnivån för vyn (t.ex. Kartor, Foton).</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Dubbeltrycka</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/double-tap.svg"/></td>
<td><p>Tryck två gånger i rask följd.</p></td>
<td><p>Stegad inzoomning.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Snärta</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/flick.svg"/></td>
<td><p>Väldigt snabb dragning, förlorar kontakt med ytan utan att sänka hastigheten.</p></td>
<td><p>Tar bort ett objekt.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="system-touch-conventions">
<title>Pekkonventioner för systemet</title>

<p>I GNOME är ett antal pekgester reserverade för användning av systemet. Dessa bör undvikas av program.</p>

<table>
<tr>
<td colspan="3"><p><em style="strong">Kantdrag</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/edge-drag.svg"/></td>
<td><p>Dra ett finger med början från en skärmkant.</p></td>
<td><p>Övre delen av vänstra kanten öppnar programmenyn.</p>
<p>Övre delen av högra kanten öppnar systemstatusmenyn.</p>
<p>Vänsterkanten öppnar översiktsvyn Aktiviteter med programvyn synlig.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Trefingernyp</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/3-finger-pinch.svg"/></td>
<td><p>För ihop tre eller fler fingrar när du rör ytan.</p></td>
<td><p>Öppnar översiktsvyn Aktiviteter.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Fyrfingerdrag</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/4-finger-drag.svg"/></td>
<td><p>Dra uppåt eller nedåt med fyra fingrar som rör ytan.</p></td>
<td><p>Växlar arbetsyta.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Trefingerhållning och tryck</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/3-finger-hold-and-tap.svg"/></td>
<td><p>Håll ner tre fingrar på ytan medan du trycker med det fjärde.</p></td>
<td><p>Växlar program.</p></td>
</tr>
</table>

</section>
</section>
</page>
