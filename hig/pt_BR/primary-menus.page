<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="primary-menus" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>O menu principal do aplicativo, representado por três linhas horizontais empilhadas</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Menus primários</title>

<media type="image" mime="image/svg" src="figures/patterns/primary-menu.svg"/>

<p>Os menus primários são um padrão de design padrão encontrado na maioria dos aplicativos. Eles são rotulados com o ícone de menu (chamado <code>menu-open</code>) e contêm os itens de menu de nível superior para o aplicativo. Isso pode incluir itens padrão como <gui>Preferences</gui>, <gui>Help</gui> e <gui>About Application</gui>, além de outros itens específicos do aplicativo.</p>


<section id="when-to-use">
<title>Quando usar</title>

<p>A maioria dos aplicativos tem menu primário, já que este é o local padrão para <gui>About Application</gui>, o qual é esperado que todo aplicativo tenha.</p>

</section>

<section id="guidance">
<title>Orientação</title>

<p>Os menus primários geralmente são colocados no lado direito da barra de cabeçalho. No entanto, existem duas variações nessa regra:</p>

<list>
<item><p>Se o aplicativo incorporar a navegação na janela, com um local e subpáginas de nível superior, o menu primário deverá ser colocado apenas no nível superior: as subpáginas podem incluir um <link xref="secondary-menus">menu secundário</link>, se um menu for necessário.</p></item>
<item><p>Quando usado em combinação com uma <link xref="sidebar-lists">lista da barra lateral</link>, o menu primário deve ser colocado acima da lista da barra lateral à direita. Se um menu for necessário para os itens mostrados no lado do conteúdo da janela, um <link xref="secondary-menus">menu secundário</link> poderá ser usado.</p></item>
</list>

<p>Outras diretrizes:</p>

<list>
<item><p>Os menus primários podem conter itens para a janela ou exibição atual, bem como o aplicativo como um todo. Isso os diferencia dos <link xref="secondary-menus">menus secundários</link>, que contêm apenas itens de menu relacionados a uma visão ou item específico.</p></item>
<item><p>Siga as <link xref="menus">diretrizes padrão para menus</link> ao decidir sobre o conteúdo e organização do menu primário.</p></item>
<item><p>Um menu de primário está contido dentro de uma <link xref="popovers">janela sobreposta</link>. Como tal, um menu primário pode incluir uma variedade de controles, como grupos de botões.</p></item>
</list>

</section>

<section id="standard-menu-items">
<title>Itens de menu padrão</title>

<p>Os itens a seguir são itens de menu primários padrão e devem ser colocados em um grupo no final do menu:</p>

<table>
<tr>
<td><p><gui>Preferences</gui></p></td>
<td><p>Abre a caixa de diálogo de preferências do aplicativo, se ele tiver uma.</p></td>
</tr>
<tr>
<td><p><gui>Keyboard Shortcuts</gui></p></td>
<td><p>Abre a janela de atalhos de teclado do aplicativo, se ele tiver uma.</p></td>
</tr>
<tr>
<td><p><gui>Help</gui></p></td>
<td><p>Abre a documentação do usuário do aplicativo no aplicativo <app>Ajuda</app>.</p></td>
</tr>
<tr>
<td><p><gui>About Application</gui></p></td>
<td><p>Abre a caixa de diálogo de “sobre” do aplicativo. Este item deve incluir o nome do aplicativo, como <gui>About Photos</gui> ou <gui>About Calculator</gui>. Todo menu primário deve incluir este item.</p></td>
</tr>
</table>

<p>Os menus primários não devem incluir itens de menu para fechar ou sair: as janelas já podem ser fechadas usando o botão Close na barra de cabeçalho e pode ser ambíguo quanto ao item de menu de fechamento. Os usuários não diferenciam prontamente entre sair e fechar e, portanto, podem ser enganosos.</p>

</section>

<section id="api-reference">
<title>Referência de API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuButton.html">GtkMenuButton</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkPopoverMenu.html">GtkPopoverMenu</link></p></item>
</list>

</section>

</page>
