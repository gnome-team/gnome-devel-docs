<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="sliders" xml:lang="pt-BR">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/sliders.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Controles deslizantes</title>

<p>Um controle deslizante permite que o usuário selecione rapidamente um valor de um intervalo.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/sliders.svg"/>

<section id="when-to-use">
<title>Quando usar</title>

<p>Os controles deslizantes podem ser usados para alterar um valor ou para navegação em um item de conteúdo, como vídeo, áudio ou até mesmo documentos. Usos comuns incluem a busca por áudio ou vídeo, a alteração de um nível de zoom ou volume ou a definição de valores em editores de imagem.</p>

<p>Use um controle deslizante quando:</p>

<list>
<item><p>O intervalo de valores é fixo e ordenado, e ao ajustar o valor relativo ao seu valor atual é mais importante do que escolher um valor absoluto.</p></item>
<item><p>É útil para o usuário controlar a taxa de alteração do valor em tempo real.</p></item>
</list>

<p>Se o intervalo de valores não tiver um máximo fixo e/ou mínimo, uma <link xref="spin-boxes">caixa de rotação</link> pode ser usada.</p>

</section>

<section id="guidelines">
<title>Diretrizes</title>

<list>
<item><p>Certifique-se de que o feedback em tempo real seja fornecido, para permitir que o usuário faça ajustes ao vivo. Exemplos disso incluem som de alto-falantes, indicando alterações de volume ou feedback ao vivo em um editor de imagens.</p></item>
<item><p>Tome cuidado para garantir que o objetivo de um controle deslizante seja claramente identificado.</p>
<list>
<item><p>Nos casos em que é comum usar um controle deslizante, siga as convenções de posicionamento. Por exemplo, em reprodutores de vídeo, é comum situar uma barra de busca horizontal ao longo da parte inferior da janela. Simplesmente colocar um controle deslizante nessa posição é suficiente para identificá-lo.</p></item>
<item><p>Em outros casos, rotule o controle deslizante com um rótulo de texto acima ou à sua esquerda, usando <link xref="writing-style#capitalization">maiusculização de frase</link>. Forneça uma <link xref="keyboard-input#access-keys">tecla de acesso</link> no rótulo que permite ao usuário dar foco diretamente ao controle deslizante.</p></item>
</list></item>
<item><p>Mark significant values along the length of the slider with text or tick marks. For example the left, right and center points on an audio balance control.</p></item>
<item><p>Para grandes intervalos de inteiros (mais de 20), e para intervalos de números de ponto flutuante, considere fornecer uma <link xref="text-fields">caixa de texto</link> ou <link xref="spin-boxes">caixa de rotação</link> que está vinculada ao valor do controle deslizante. Isso permite que o usuário defina ou ajuste rapidamente a configuração com mais facilidade do que com o controle deslizante sozinho.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referência de API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkHScale.html">GtkHSCale</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkHScale.html">GtkVSCale</link></p></item>
</list>

</section>

</page>
