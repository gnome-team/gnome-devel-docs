<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="initial-state-placeholder" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Imagem e texto mostrados quando um aplicativo está inicialmente vazio</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Espaços reservados de estado inicial</title>

<media type="image" mime="image/svg" src="figures/patterns/initial-state-placeholder.svg"/>

<p>Uma espaço reservado (ou “placeholder”) de estado inicial é uma imagem e texto que preenche o espaço em um aplicativo que nunca teve qualquer conteúdo.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>Em alguns casos, um aplicativo pode ficar vazio até que um usuário adicione algum conteúdo a ele. Nestes cenários, um estado inicial pode ser usado para fornecer uma experiência rica e convidativa. Isso ajuda a evitar que o primeiro uso do aplicativo seja ineficiente e hostil.</p>

<p>Um espaço reservado de estado inicial só deve ser usado quando um aplicativo ficará inevitavelmente vazio. Em muitos casos, geralmente é melhor pré-preencher o aplicativo.</p>

</section>

<section id="guidelines">
<title>Diretrizes</title>

<list>
<item><p>Siga o layout padrão para o tamanho e a colocação da imagem e rótulos, de modo que seu aplicativo seja consistente com outros aplicativos do GNOME 3.</p></item>
<item><p>As imagens usadas devem ser ricas e coloridas.</p></item>
<item><p>O texto que acompanha a imagem deve ser positivo e otimista. Este é um momento em que você pode vender sua inscrição e estabelecer uma identidade positiva para ela. Também pode ser uma oportunidade de iniciar um relacionamento com o usuário, abordando-o diretamente.</p></item>
<item><p>Se houver controles que permitam adicionar itens, pode ser apropriado destacá-los usando um <link xref="buttons#suggested-and-destructive">estilo sugerido</link> enquanto a lista/grade está vazia.</p></item>
<item><p>Enquanto um aplicativo está inicialmente vazio, alguns controles não servem a um propósito (como aqueles para navegar pelo conteúdo, alterar a exibição ou pesquisar). Tornar esses controles insensíveis ajudará a evitar que o usuário fique desapontado ou tente recursos que não funcionem.</p></item>
<item><p>Um estado inicial deve perseverar até que o conteúdo seja adicionado ao aplicativo, após o qual ele não deve ser visto novamente. Se a aplicação ficar vazia posteriormente, um <link xref="empty-placeholder">estado vazio pode ser usado</link>.</p></item>
</list>

</section>

</page>
