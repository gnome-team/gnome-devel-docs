<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="text-fields" xml:lang="pt-BR">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/text-fields.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Campos de texto</title>

<p>Um campo de entrada de texto é um elemento de interface para inserir ou editar texto. É um elemento básico com vários usos, incluindo entrada de pesquisa, configurações e preferências, ou configuração da conta e da instalação. Um campo de entrada de texto pode ser pré-preenchido com texto e pode incluir botões ou ícones adicionais.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/text-fields.svg"/>

<section id="general-guidelines">
<title>Diretrizes gerais</title>

<list>
<item><p>Dimensione os campos de texto de acordo com o tamanho provável do conteúdo que eles conterão. Isso fornece uma indicação visual útil para a quantidade de entrada esperada e evita transbordar.</p></item>
<item><p>Em uma <link xref="dialogs#instant-and-explicit-apply">caixa de diálogo de aplicação instantânea</link>, valide o conteúdo do campo de entrada quando ele perde o foco ou quando a janela é fechada, não após cada pressionamento de tecla. Exceção: se o campo aceitar apenas um número fixo de caracteres, como um código de cor hexadecimal, valide e aplique a alteração assim que esse número de caracteres for inserido.</p></item>
<item><p>Se você implementar um campo de entrada que aceita apenas determinados caracteres, como dígitos, execute o bipe de aviso do sistema quando o usuário tentar digitar um caractere inválido.</p></item>
<item><p>Normalmente, pressionar a tecla <key>Tab</key> em um campo de entrada de linha única deve mover o foco para o próximo controle e, em um campo de entrada multilinha, ela deve inserir um caractere de tabulação. Pressionar <keyseq><key>Ctrl</key><key>Tab</key></keyseq> em um campo de entrada multilinha deve mover o foco para o próximo controle.</p></item>
<item><p>Se você precisar fornecer um atalho de teclado que insira um caractere de tabulação em um campo de entrada de linha única, use <keyseq><key>Ctrl</key><key>Tab</key></keyseq>. No entanto, é improvável que você encontre muitas situações em que isso seja útil.</p></item>
</list>

</section>

<section id="embedding-info-and-controls">
<title>Embutindo informações e controles</title>

<p>Uma variedade de informações e controles adicionais podem ser inseridos dentro de um campo de entrada de texto.</p>

<p>Ícones ou botões de ícones podem ser colocados dentro de um campo de texto para fornecer informações de status ou controles adicionais.</p>

<list>
<item><p>Um ícone no início da entrada pode ser usado para indicar sua finalidade – substituindo a necessidade de a entrada ser rotulada. Campos de entrada de pesquisa são o exemplo clássico disso, onde um ícone de pesquisa é colocado no lado esquerdo do campo de entrada.</p></item>
<item><p>Se o texto a ser digitado fizer distinção entre maiúsculas e minúsculas, um ícone de aviso poderá ser exibido dentro do campo de texto se o Caps Lock estiver ativado. Isso é normalmente mostrado no lado direito da entrada.</p></item>
<item><p>Se é comum que o campo de texto seja apagado, um botão de ícone claro pode ser colocado dentro do campo, no lado direito.</p></item>
<item><p>Se você colocar um ícone em um campo de entrada de texto (como um indicador ou um botão), use sua variante simbólica do Tema de Ícones Simbólicos do GNOME.</p></item>
</list>

<p>Quando um usuário se beneficiaria de informações adicionais para usar um campo de entrada de texto, ele pode ser pré-preenchido com um texto de dica. Como com qualquer decisão de exibir informações adicionais, isso deve ser feito somente quando for necessário.</p>

</section>

<section id="api-reference">
<title>Referência de API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkEntry.html">GtkEntry</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html">GtkSearchEntry</link></p></item>
</list>

</section>
</page>
