<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="in-app-notifications" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Notificações de eventos do aplicativos</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Notificações no aplicativo</title>

<media type="image" mime="image/svg" src="figures/patterns/in-app-notification.svg"/>

<p>Notificações no aplicativos são janelas instantâneas de informação que podem ser exibidas dentro de um aplicativo. Elas incluem um rótulo que descreve um evento que aconteceu e também pode incluir um botão que permite que o usuário responda. Elas sempre são transitórias e descartáveis pelos usuários.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>Notificações no aplicativo podem ser usadas para informar os usuários sobre eventos que são imediatamente relevantes para sua atividade em um aplicativo, mas que não gera interesse de longo prazo. Um uso comum é fornecer feedback em resposta às ações do usuário, enquanto o aplicativo está sendo usado. Isso contrasta com <link xref="notifications">notificações padrões</link>, que fornecem alertas para todo sistema, e que persiste após a notificação ter sido exibida inicialmente.</p>

<p>Permitir que o usuário desfaça uma ação destrutiva é um exemplo de um bom uso de notificações no aplicativo: a notificação não é mostrada imediatamente após uma ação do usuário e é mostrada no contexto mais relevante.</p>

<p>Notificações no aplicativo não são uma boa solução para comunicar estados em andamento.</p>

</section>

<section id="guidelines">
<title>Diretrizes</title>

<list>
<item><p>Nem sempre é necessário incluir um botão de ação em uma notificação no aplicativo: apenas inclua uma se estiver diretamente relacionada com o evento e se geralmente for útil.</p></item>
<item><p>Não distraia com notificações desnecessárias no aplicativo, e tenha cuidado para não usá-las em excesso: elas podem ser incômodas se surgirem com frequência.</p></item>
<item><p>Apenas uma notificação no aplicativo pode ser exibida por vez, e novas instâncias devem ser substituídas por existentes.</p></item>
</list>

</section>

</page>
