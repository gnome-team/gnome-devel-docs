<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="button-menus" xml:lang="pt-BR">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/button-menu.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Botões de menu</title>

<p>Um menu que é aberto ao pressionar um botão. Podem ser incorporados em uma série de widgets contêineres, como barras de cabeçalho, barras de ação, barras laterais ou barras de ferramentas. <link xref="header-bar-menus">Menus de barra de cabeçalho</link> são o exemplo mais comum.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/button-menu.svg"/>

<section id="when-to-use">
<title>Quando usar</title>

<p>Um botão de menu é uma maneira de apresentar ações ou opções adicionais. São apropriados quando é necessário fornecer ações secundárias ou opções que não são incorporadas confortavelmente em sua interface de usuário primária. Existem dois tipos primários de botão de menu:</p>

<list>
<item><p><em style="strong">Menus de transborde</em>: aqui, o botão de menu serve como uma extensão de um conjunto existente de controles. Isto pode ser visto no exemplo de <link xref="header-bar-menus">menus de barra de cabeçalho</link>, onde o menu age como uma continuação dos controles apresentados na própria barra de cabeçalho. Esta abordagem pode ser aplicada a outros contêineres, como barras de ação. Esses botões de menu genéricos podem conter uma variedade de itens diversos que estão relacionados ao contexto do menu.</p></item>
<item><p><em style="strong">Menus de propósito único</em>: estes fornecem ações e/ou opções para uma área específica de funcionalidade. Da mesma maneira que uma barra de menu apresenta uma série de menus de diferentes tópicos, botões de menu individuais podem apresentar grupos de funcionalidade, como edição, mudança de visão, ou fornecimento de controles de navegação.</p></item>
</list>

<p>Os menus oferecem uma maneira clara e consistente de apresentar diversos conjuntos de ações e configurações. Ao mesmo tempo, uma janela sobreposta com controles embutidos, como botões, controles deslizantes, botões de rotação, listas e entradas de texto, podem fornecer uma interface mais eficaz para muitas tarefas.</p>

<list>
<item><p>Avalie cada função dentro de um menu de botão, para decidir se seria melhor servido por um <link xref="ui-elements">elemento de interface de usuário</link> diferente. Embora ações ou configurações simples possam ser efetivamente representadas por itens de menu, outros não podem. Em particular, controles deslizantes, botões de rotação, interruptores e entradas de texto fornecem funcionalidades que não podem ser facilmente reproduzidas com um menu. Da mesma forma, algumas entradas podem ser melhor representadas como ícones em vez de texto – nesse caso, os botões podem ser mais apropriados do que um menu.</p></item>
<item><p>Se um botão de menu contiver um pequeno número de itens que podem ser representados de forma mais efetiva como um grupo de controles, uma janela sobreposta contendo diferentes elementos de interface pode ser uma interface de usuário mais interessante e eficiente. No entanto, esta abordagem pode facilmente tornar-se excessivamente complexa para menus de botões maiores e mais diversos.</p></item>
<item><p>Um botão de menu pode ser combinado com um pequeno número de outros elementos da interface, como botões, controles deslizantes e interruptores (veja o exemplo abaixo). Isso pode permitir que alguns itens de menu sejam apresentados de forma mais eficiente em termos de espaço, ou para fornecer interações que não são possíveis com um menu padrão. No entanto, tenha cuidado para não misturar muitos tipos de controle ou tornar o menu muito complexo no processo.</p></item>
</list>

<media type="image" mime="image/svg" src="figures/ui-elements/hybrid-button-menu.svg"/>

</section>

<section id="general-guidelines">
<title>Diretrizes gerais</title>

<list>
<item><p>Cada contexto – seja uma visão ou área delineada da sua interface – deve incluir apenas um menu de botão genérico.</p></item>
<item><p>Certifique-se de que os menus dos botões de propósito único estejam efetivamente rotulados. Enquanto um ícone é mais compacto, use-os somente quando eles serão comumente entendidos pelos seus usuários. As <link xref="icons-and-artwork">diretrizes de uso de ícones</link> fornecem mais conselhos sobre isso.</p></item>
<item><p>Os botões de propósito único devem ser definidos de forma clara e consistente. Os itens do menu devem ter uma relação óbvia com o propósito geral do menu.</p></item>
<item><p>Enquanto vários menus de botões podem ser usados simultaneamente, tenha cuidado ao introduzir muitos pontos de divulgação na sua interface de usuário. Quanto mais desses você apresentar, mais difícil será para os usuários encontrarem os controles que precisam e o erro humano será aumentado.</p></item>
</list>

</section>

</page>
