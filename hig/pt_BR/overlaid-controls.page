<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="overlaid-controls" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Controles flutuantes, frequentemente usados para imagens e controles de vídeo</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Controles sobrepostos</title>

<media type="image" mime="image/svg" src="figures/patterns/overlaid-controls.svg"/>

<p>Os controles transitórios que flutuam sobre o conteúdo são um padrão comum para aplicativos que mostram imagens ou vídeos.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>Uma vez que os controles sobrepostos são ocultados quando não estão em uso, eles ajudam a fornecer uma experiência de visualização não organizada. Eles são apropriados quando é desejável apresentar uma visão limpa e livre de distração em um item de conteúdo – isso é particularmente (embora não exclusivamente) apropriado para imagens e vídeos.</p>

<p>Os controles superpostos podem ser inadequados se obscurecer partes relevantes do conteúdo abaixo. Os controles de edição de imagem podem interferir na capacidade de ver seus efeitos, por exemplo. Nesses casos, os controles não devem ser sobrepostos.</p>

</section>

<section id="guidelines">
<title>Diretrizes</title>

<list>
<item><p>Siga as convenções estabelecidas para este tipo de controle, como os botões de navegação esquerda/direita nos visualizadores de imagens e os controles do reprodutor na parte inferior para o vídeo.</p></item>
<item><p>Os controles devem ser exibidos quando o ponteiro é movido sobre o conteúdo, ou quando é tocada (em dispositivos sensíveis a toque).</p></item>
<item><p>Os controles sobrepostos podem ser anexados à borda do conteúdo/janela, ou podem ser flutuantes. As <link xref="action-bars">barras de ação</link> podem ser tratadas como controles superpostos.</p></item>
<item><p>Use o estilo de tema padrão “OSD” para controles sobrepostos.</p></item>
</list>

</section>

</page>
