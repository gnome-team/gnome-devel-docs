<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="application-basics" xml:lang="pt-BR">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Características básicas de um aplicativo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Noções básicas de aplicativos</title>

<p>Essas Diretrizes de Interface Humana fornecem orientação de design sobre a criação de aplicativos GNOME. Dado esse foco nos aplicativos, essa página fornece uma definição de aplicativos a partir de uma perspectiva de experiência do usuário. Ela também fornece orientação sobre o comportamento e as características básicas da aplicação.</p>

<p>O modelo de aplicativo permite que o usuário entenda como o software é distribuído, instalado e removido, bem como a maneira como o software se comporta quando está em uso. Garantir que seu software se comporte de acordo com este modelo irá ajudar a garantir que ele é previsível e conforma-se com as expectativas do usuário. Também garantirá correta integração com as ferramentas do sistema para instalar e remover aplicativos.</p>

<section id="application-definition">
<title>O que é um aplicativo?</title>

<p>A lista de características a seguir fornece uma definição básica de um aplicativo. Um aplicativo é uma peça distinta de software que:</p>

<list>
<item><p>Pode ser instalado e removido individualmente do sistema.</p></item>
<item><p>Não afeta ou interfere no comportamento de outros aplicativos.</p></item>
<item><p>Não depende de outros aplicativos para ser executado.</p></item>
<item><p>Fornece ao menos uma <link xref="primary-windows">janela primária</link>.</p></item>
<item><p>Possui um nome único e <link xref="icons-and-artwork#application-icons">ícone</link>.</p></item>
<item><p>Possui um inicializador (lançador) de aplicativo.</p></item>
</list>

<p>Um tema chave nessa definição é que os aplicativos devem ser independentes de outros aplicativos. Isso garante flexibilidade, previsibilidade e simplicidade de uso.</p>

<p>Naturalmente, há outras características esperadas dos aplicativos, incluindo o comportamento padrão da janela, a integração do sistema e os metadados do aplicativo. Orientações sobre esses aspectos de aplicações podem ser encontradas em outros lugares.</p>

<!-- Add a section on application launchers? -->

</section>

<section id="application-names">
<title>Nomeando seu aplicativo</title>

<p>O nome de um aplicativo é vital. É o que os usuários vão ser expostos primeiramente, e irá ajudá-los a decidir se querem ou não usar o aplicativo. É uma parte importante de face pública de seu aplicativo.</p>

<p>O nome de um aplicativo desempenha uma série de funções:</p>

<list>
<item><p>Deve anunciar seu aplicativo à potenciais usuários.</p></item>
<item><p>Deve servir para reforçar uma identidade positiva e qualidades expressivas.</p></item>
<item><p>Deve identificar seu aplicativo em sistemas onde esteja instalado e executando.</p></item>
</list>

<p>Garanta que o nome de seu aplicativo é curto – menos de 15 caracteres. Isto garante que será sempre completamente exibido em um ambiente GNOME 3.</p>

<p>Adicionalmente, selecione um nome de aplicativo que seja fácil de entender e comunique a funcionalidade do seu aplicativo. Evite referências que não serão compreendidas ou que não serão familiares à usuários potenciais, como referências culturais obscuras, piadas internas e acrônimos. Ao invés disso, selecione um nome que referencie o que seu aplicativo faz, ou o domínio no qual opera.</p>

</section>

<!-- Possible additional section on defining scope. "A successful application does one thing, and does it well." -->

</page>
