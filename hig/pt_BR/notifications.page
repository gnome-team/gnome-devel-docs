<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="notifications" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Notificações de eventos em todo o sistema</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Notificações</title>

<p>As notificações permitem informar os usuários sobre os eventos quando eles não estão usando seu aplicativo. Eles também fornecem a capacidade de os usuários responderem rapidamente a esses eventos, usando ações de notificação.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>Use notificações para informar o usuário sobre eventos que estarão interessados enquanto não estiverem usando seu aplicativo. Isso pode incluir novas mensagens em aplicativos de mensagens, a conclusão de tarefas de longa duração, lembretes para calendários e assim por diante.</p>

<p>As notificações não devem ser usadas como substituto dos comentários fornecidos pelas janelas do aplicativo, que devem ser capazes de informar o usuário sobre eventos sem a necessidade de notificações.</p>

</section>

<section id="notification-elements">
<title>Elementos de notificação</title>

<p>As notificações no GNOME 3 têm um número de componentes padrão:</p>

<table>
<thead>
<tr>
<td><p>Elemento</p></td><td><p>Descrição</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Ícone de aplicativo</p></td><td><p>Indica qual aplicativo enviou a notificação.</p></td>
</tr>
<tr>
<td><p>Título</p></td><td><p>O cabeçalho da notificação.</p></td>
</tr>
<tr>
<td><p>Corpo</p></td><td><p>Um bloco opcional de texto que fornece detalhes extras sobre a notificação. O corpo da notificação pode incluir vários parágrafos. Por exemplo: um trecho do início de um e-mail.</p></td>
</tr>
<tr>
<td><p>Ação padrão</p></td><td><p>Esta é a ação que é ativada quando a notificação é ativada.</p></td>
</tr>
<tr>
<td><p>Ações</p></td><td><p>Cada notificação pode incluir até três botões.</p></td>
</tr>
</tbody>
</table>

<section id="titles">
<title>Títulos</title>

<p>O título deve fornecer um resumo curto e conciso do evento que desencadeou a notificação. O organismo de notificação nem sempre pode ser visível, por isso é importante garantir que a notificação possa ser entendida apenas pelo título.</p>

</section>

<section id="default-actions">
<title>Ações padrão</title>

<p>A ação padrão deve sempre descartar a notificação e criar uma janela pertencente ao aplicativo que enviou a notificação. Se a notificação se refere a uma parte específica da interface do usuário do seu aplicativo, a ação padrão deve exibir essa parte da interface do usuário. A ação padrão para uma notificação sobre um novo e-mail deve mostrar a mensagem de e-mail relevante quando ativada, por exemplo.</p>

</section>

<section id="actions">
<title>Ações de notificação</title>

<p>Você pode fornecer funcionalidades úteis incorporando botões nas notificações. Isso permite aos usuários responder rápida e facilmente à notificação.</p>

<list>
<item><p>As ações de notificação devem estar relacionadas ao conteúdo da notificação e não devem fornecer ações genéricas para sua aplicação. Isso garante que cada notificação tenha um foco e propósito claros.</p></item>
<item><p>Use apenas ações de notificação quando a funcionalidade que eles fornecem é comumente necessária.</p></item>
<item><p>As ações não devem substituir os controles da interface do usuário em outros lugares – deve ser possível tomar as mesmas ações das janelas do seu aplicativo.</p></item>
<item><p>Não é necessário sempre usar ações de notificação, e muitas notificações não vão exigi-las.</p></item>
<item><p>As ações de notificação não devem duplicar a ação padrão. Por exemplo, uma nova notificação de e-mail não precisa incluir um botão Open, uma vez que a ação padrão já deve executar essa ação.</p></item>
</list>

</section>
</section>

<section id="general-guidance">
<title>Orientação geral</title>

<list>
<item><p>É importante não distrair desnecessariamente os usuários com notificações. Isso pode se tornar irritante e frustrante, e não fará com que os usuários a gostem do seu aplicativo. Portanto, sempre seja crítico ao usar notificações e questione-se se os usuários realmente precisam ser informados sobre os eventos que deseja comunicar.</p></item>
<item><p>Aplicativos que lidam com muitos eventos, como mensagens de e-mail ou de redes sociais, correm o risco especial de distrair os usuários com muitas notificações. Esses aplicativos devem colocar restrições sobre a frequência com que enviam mensagens de notificação. Em vez de mostrar uma notificação para cada nova mensagem, é uma boa ideia para cada notificação fornecer um resumo das novas mensagens.</p></item>
<item><p>As notificações no GNOME 3 persistem após terem sido exibidas inicialmente. Portanto, é importante remover as mensagens de notificação que não são mais relevantes para o usuário.</p>
<list>
<item><p>A janela do seu aplicativo deve fornecer comentários sobre todos os eventos que foram notificados por notificações. Como resultado, quando a janela do seu aplicativo está focada, as mensagens de notificação devem ser tratadas como tendo sido lidas e devem ser removidas.</p></item>
<item><p>Certifique-se de que seu aplicativo exclui as notificações que não são mais válidas. Por exemplo, uma notificação para um aviso meteorológico que foi revogada deve ser removida.</p></item>
</list></item>
</list>

</section>

<section id="api-reference">
<title>Referência de API</title>
<list>
<item><p><link href="https://developer.gnome.org/gio/stable/GNotification.html">GNotification</link></p></item>
</list>
</section>

</page>
