<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="primary-windows" xml:lang="de">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Das oder die Hauptfenster Ihrer Anwendung</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Primäre Fenster</title>

<media type="image" mime="image/svg" src="figures/patterns/primary-windows.svg"/>

<p>Primäre Fenster sind der grundlegende Container der obersten Ebene für die Benutzeroberfläche Ihrer Anwendung. Sie sollten die Kernfunktionalität Ihrer Anwendung präsentieren.</p>

<section id="when-to-use">
<title>Anwendungsfälle</title>

<p>Jede Anwendung sollte ein primäres Fenster anzeigen, wenn der Starter aktiviert wird. Das schließt Anwendungen ein, die hauptsächlich einen Hintergrunddienst bieten.</p>

</section>

<section id="application-types">
<title>Anwendungstypen</title>

<p>Es gibt zwei Hauptmodelle für primäre Fenster:</p>

<section id="single-instance-applications">
<title>Anwendungen mit einer Instanz</title>

<p>Anwendungen mit einer einzelnen Instanz haben ein einzelnes primäres Fenster. Dieses Modell ist für Messaging-Anwendungen üblich, wie E-Mail, Chat oder Kontakte.</p>

</section>

<section id="multiple-instance-applications">
<title>Anwendungen mit mehreren Instanzen</title>

<p>Anwendungen mit mehreren Instanzen können mehrere Anwendungsfenster haben. Typischerweise sind die primären Fenster dabei identisch. Anwendungen mit mehreren Instanzen sind häufig Betrachter oder Editoren, zum Beispiel für Dokumente oder Bilder.</p>

<p>Both single and multiple instance applications can allow multiple content items to be opened, either through the use of <link xref="tabs">tabs</link> or browser-style navigation. However, multiple windows do offer additional capabilities, which include:</p>

<list>
<item><p>Anzeige verschiedener Inhaltsobjekte nebeneinander.</p></item>
<item><p>Platzierung von Inhalten auf verschiedenen Arbeitsflächen.</p></item>
<item><p>Organisieren von Inhalten in verschiedenen Fenstern (falls Reiter verwendet werden).</p></item>
</list>

<section id="parent-child-primary-windows">
<title>Primäre Eltern-/Kind-Fenster</title>

<p>Anwendungen mit mehreren Instanzen haben typischerweise identische primäre Fenster (zum Beispiel mehrere Webbrowser-Fenster). Dies ist jedoch nicht immer der Fall.</p>

<p>Primäre Fenster können eine Eltern-/Kind-Beziehung haben. In diesem Anwendungstyp gibt es immer nur ein Eltern-Fenster. Dieses enthält typischerweise Inhaltsobjekte, die im Eltern-Fenster oder in einem separaten Kind-Fenster geöffnet werden könnten. Dies ermöglicht, dass mehrere Inhaltsobjekte gleichzeitig geöffnet sein können.</p>

<p>Da Kind-Fenster nur über ein Eltern-Fenster geöffnet werden können, sind diese nicht von diesen abhängig, wenn es darum geht, sie geöffnet zu halten: Das Schließen eines Eltern-Fensters bedingt nicht das Schließen aller ihrer Kind-Fenster.</p>

<p>Die <app>Notes</app>-Anwendung von GNOME ist ein gutes Beispiel für primäre Eltern-/Kind-Fenster.</p>

</section>

</section>
</section>

<section id="general-guidelines">
<title>Allgemeine Richtlinien</title>

<list>
<item><p>Ein einzelnes primäres Fenster sollte stets angezeigt werden, wenn Ihre Anwendung gestartet ist.</p></item>
<item><p>Falls Ihr Anwendungsstarter aktiviert wird, während Ihre Anwendung läuft, sollten alle primären Fenster der Anwendung angezeigt werden.</p></item>
<item><p>Primäre Fenster sollten die Hauptfunktionen Ihrer Anwendung bereitstellen. Verlagern Sie keine grundlegende Funktionalität in Dialoge oder sekundäre Fenster.</p></item>
<item><p>Primäre Fenster sollten unabhängig sein. Das bedeutet, dass durch das Schließen eines primären Fensters nicht auch die Schließung eines anderen primären Fensters ausgelöst werden sollte.</p></item>
<item><p>Dialogfenster sollten stets von einem primären Fenster abhängig sein. Erläuterungen hierzu finden Sie auf der Seite zu <link xref="dialogs">Dialogen</link>.</p></item>
<item><p>Die Richtlinien zur <link xref="display-compatibility">Anzeigekompatibilität</link> sind insbesondere für primäre Fenster relevant: Seien Sie vorsichtig, um sicherzustellen, dass diese den Anweisungen für minimale Bildschirmgrößen, der Anzeigeausrichtung und Einrasten im halben Bildschirm folgen.</p></item>
<item><p><gui>Quit</gui> sollte alle primären Fenster schließen.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-Referenz</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkWindow.html">GtkWindow</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">GtkApplicationWindow</link></p></item>
</list>

</section>

</page>
