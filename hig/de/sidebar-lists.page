<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="sidebar-lists" xml:lang="de">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Navigations-Seitenleiste, die eine Liste enthält</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Listen in der Seitenleiste</title>

<media type="image" mime="image/svg" src="figures/patterns/sidebar-list.svg"/>

<p>Eine Seitenleistenliste ermöglicht mehrere Ansichten, zwischen denen gewechselt werden kann. Diese Ansichten können einzelne Inhaltsobjekte oder Gruppen davon enthalten, oder auch mehrere Bedienelemente. Die Seitenleiste teilt das Fenster in zwei, wobei der Inhalt auf der gegenüberliegenden Seite der Seitenleiste angezeigt wird.</p>

<p>Seitenleistenlisten können in primären Fenstern verwendet werden, entweder als permanenter Bestandteil oder als Element, das bei Bedarf angezeigt wird. Sie können auch in Dialogfenstern verwendet werden.</p>

<p>Sidebar lists can be used in conjunction with the <link xref="search">search</link> and <link xref="selection-mode">selection mode</link> design patterns.</p>

<section id="when-to-use">
<title>Anwendungsfälle</title>

<p>Use a sidebar list when it is necessary to expose a larger number of views than can be accommodated by a standard <link xref="view-switchers">view switcher</link>.</p>

<p>Seitenleistenlisten sind auch eine mögliche Alternative zur Navigation im Browser-Stil. Sie haben hier eine Reihe von Vorteilen:</p>

<list>
<item><p>When content items have a narrow width, and don’t require an immersive experience. A sidebar would be inappropriate for browsing videos for this reason, but is well-suited to contacts.</p></item>
<item><p>When content items are dynamic. For messaging applications, where new content items appear or old ones are updated, a sidebar list provides the ability for someone to view one item while simultaneously being aware of updates to the overall message list.</p></item>
<item><p>When it is possible to filter a collection of content, and there are a large number of filters.</p></item>
</list>

<p>Temporary sidebar lists can also be displayed for particular views in your application.</p>

</section>

<section id="guidelines">
<title>Richtlinien</title>

<list>
<item><p>Order the list according to what is most useful for the users of your application. It is often best to place recently updated items at the top of the list.</p></item>
<item><p>Header bar controls which affect the sidebar list should be placed within the list pane section of the header bar. Controls for search and selection should be found above the list.</p></item>
<item><p>Jede Listenzeile kann mehrere Textzeilen und auch Bilder enthalten. Achten Sie jedoch darauf, dass die wichtigsten Informationen dabei nicht verloren gehen und sorgen sie für ein sauberes und attraktives Erscheinungsbild.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-Referenz</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkListBox.html">GtkListBox</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkScrolledWindow.html">GtkScrolledWindow</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkStackSidebar.html">GtkStackSidebar</link></p></item>
</list>
</section>

</page>
