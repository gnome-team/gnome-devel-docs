<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="writing-style" xml:lang="de">

  <info>
    <revision pkgversion="3.15" date="2015-01-14" status="review"/>

    <credit type="author">
      <name>Allan Day</name>
      <email its:translate="no">aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Schreiben von Text für Ihre Benutzeroberfläche, Regeln für Groß- und Kleinschreibung.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Schreibstil</title>

<p>Texte spielen in Benutzeroberflächen eine entscheidende Rolle. Nehmen Sie sich die Zeit und prüfen Sie, ob jeglicher Text klar und deutlich geschrieben und leicht verständlich ist.</p>

<section id="guidelines">
<title>Richtlinien</title>

<p>Ihr Hauptziel sollte es sein, dass Texte leicht zu verstehen und schnell zu lesen sind.</p>

<list>
<item><p>Halten Sie Texte kurz und knapp. So kann ein Benutzer die Information schneller aufnehmen. Dadurch wird auch die Vergrößerung der übersetzten Textversionen verringert. Denken Sie daran, dass in manchen Sprachen Texte 30 Prozent größer werden können im Vergleich zum englischen Original.</p></item>
<item><p>Kürzen Sie Ihren Text nicht so weit, dass seine Bedeutung nicht klar wird. Eine Beschriftung aus drei Wörtern liefert klarere Informationen als ein einziges Wort, das zweideutig oder unklar ist. Versuchen Sie stets, die Bedeutung einer Beschriftung mit so wenig Wörtern wie möglich und so vielen wie nötig auszudrücken.</p></item>
<item><p>Verwenden Sie Wörter, Phrasen und Konzepte, mit denen die Zielgruppe Ihrer Anwendung vertraut ist, und keine Begriffe aus dem zugrunde liegenden System. Das beinhaltet Begriffe, die im Umfeld Ihrer Anwendung üblicherweise verwendet werden. In der Medizin beispielsweise ist der Hefter, der die Patientendaten enthält, eine »Karte«. Daher sollte in einer Anwendung mit Zielgruppe in der Medizin natürlich auch dieser Begriff verwendet werden und nicht etwa »Patienten-Datensatz«.</p></item>
<item><p>Texte sollten in einem neutralen Ton verfasst sein und aus der Richtung des Produkts sprechen. Pronomen wie »you« oder »my« sollten Sie vermeiden, wo immer es möglich ist. Wenn es gar nicht anders geht, dann sollten Sie »your« vor »my« den Vorzug geben.</p></item>
<item><p>Verwenden Sie die GNOME-Standardbegriffe, wenn Sie sich auf Teile der Benutzerschnittstelle beziehen, wie »pointer« oder »window«. Die Richtlinien für Benutzeroberflächen können in diesem Zusammenhang als Referenz dienen.</p></item>
<item><p>Vermeiden Sie Wiederholungen, wenn möglich.</p></item>
<item><p>Sätze sollten nicht aus verschiedenen Bedienelementen konstruiert werden, und jede Beschriftung sollte als in sich abgeschlossen betrachtet werden. Sätze, die über mehrere Bedienelemente verlaufen, ergeben oft keinen Sinn mehr, wenn sie in andere Sprachen übersetzt werden.</p></item>
<item><p>Vermeiden Sie lateinische Abkürzungen wie »ie« oder »eg«, da diese nicht immer problemlos übersetzt und auch von Bildschirmlesern nicht immer sinnvoll erfasst werden können. Verwenden sie stattdessen vollständige Wörter wie »for example«.</p></item>
</list>

</section>

<section id="capitalization">
<title>Großschreibung</title>

<p>In den Benutzeroberflächen in GNOME werden zwei Großschreibungstypen verwendet: Überschrifts-Großschreibung und Satz-Großschreibung.</p>

<section id="header-capitalization">
<title>Überschrifts-Großschreibung</title>

<p>Überschrifts-Großschreibung sollte immer für Überschriften verwendet werden, auch für die Überschriften in der Kopfleiste und den Titeln von Seiten, Reitern und Menüs. Sie sollten diese auch für die Beschriftung von Bedienelementen verwenden, die normalerweise keinen korrekten Satz bilden, wie bei den Beschriftungen von Knöpfen, Schaltern und Menüeinträgen.</p>

<p>Schreiben Sie den Anfangsbuchstaben groß in:</p>

<list>
<item><p>allen Wörtern mit mehr als vier Buchstaben.</p></item>
<item><p>Verben beliebiger Länge, wie »Be«, »Are«, »Is«, »See« und »Add«.</p></item>
<item><p>Das erste und letzte Wort.</p></item>
<item><p>Wörtern mit Bindestrichen, zum Beispiel »Self-Test« oder »Post-Install«.</p></item>
</list>

<p>Beispiele: »Create a Document«, »Find and Replace«, »Document Cannot Be Found«.</p>

</section>

<section id="sentence-capitalisation">
<title>Satz-Großschreibung</title>

<p>Sentence capitalization should be used for labels that form sentences or that run on to other text, including labels for check boxes, radio buttons, sliders, text entry boxes, field labels and combobox labels. It should also be used for explanatory or body text, such as in dialogs or notifications.</p>

<p>Schreiben Sie den Anfangsbuchstaben groß in:</p>

<list>
<item><p>Das erste Wort.</p></item>
<item><p>Alle Wörter, die in Sätzen normalerweise groß geschrieben werden, zum Beispiel Eigennamen.</p></item>
</list>

<p>Zum Beispiel: “The document cannot be found in this location.” “Finding results for London.”</p>

</section>
</section>

<section id="ellipses">
<title>Ellipsen (…)</title>

  <p>Verwenden Sie die Unicode-Ellipsis (…) am Ende einer Beschriftung, wenn weitere Eingaben oder Bestätigungen erforderlich ist, bevor die Aktion ausgeführt werden kann. Beispiele hierfür sind <gui>Save As…</gui>, <gui>Find…</gui> oder <gui>Delete…</gui>.</p>

  <p>Fügen Sie keine Ellipsis zu Beschriftungen wie <gui>Properties</gui> oder <gui>Preferences</gui> hinzu. Obwohl diese Befehle Fenster öffnen, die weitere Funktionalität bereitstellen, spezifiziert diese Beschriftung keine Aktion. Deshalb muss nicht direkt darauf verwiesen werden, dass weitere Eingaben oder Bestätigungen erforderlich sind.</p>

</section>

</page>
