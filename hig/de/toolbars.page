<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="toolbars" xml:lang="de">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/toolbar.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Werkzeugleisten</title>
<p>Eine Werkzeugleiste ist ein Streifen aus Bedienelementen, der bequemen Zugriff auf häufig genutzte Funktionen bietet. Die meisten Werkzeugleisten enthalten nur grafische Knöpfe, aber in komplexeren Situationen können auch andere Bedienelemente sinnvoll sein, zum Beispiel Auswahllisten.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/toolbar.svg"/>

<section id="when-to-use">
<title>Anwendungsfälle</title>

<p>Verwenden Sie eine Werkzeugleiste zum Zugriff auf häufig verwendete Aktionen, Werkzeuge oder Optionen in primären Fenstern. <link xref="header-bars">Kopfleisten</link> erfüllen diese Aufgabe ebenfalls. Eine Werkzeugleiste ist generell nicht notwendig, wenn Sie Kopfleisten verwenden.</p>

<p>While toolbars are a common approach, there are cases where they are not the most effective. Interfaces that focus on direct manipulation, or which make heavy use of progressive disclosure, may provide a better alternative. Each of these approaches requires more time and effort at the design stage, and should only be pursued by those who are confident in pursuing more original design solutions.</p>

</section>

<section id="general-guidelines">
<title>Allgemeine Richtlinien</title>

<list>
<item><p>Stellen Sie nur Bedienelemente für die wichtigsten Funktionen bereit. Zu viele Werkzeugleistenelemente schränken die Funktionalität ein, da sie schwerer zu finden sind. Außerdem verringern zu viele Werkzeugleistenzeilen den Platz auf dem Bildschirm, der für den Rest der Anwendung zur Verfügung steht.</p></item>
<item><p>Bestücken Sie Werkzeugleisten nach den üblichen Konventionen, um eine gewisse Vertrautheit aufzubauen. Beispielsweise wird die Hauptwerkzeugleiste einer Büroanwendung fast immer Knöpfe wie Neu, Öffnen und Speichern an den ersten drei Positionen zeigen. Auf ähnliche Weise werden die ersten Werkzeugleistenknöpfe in einem Browser die Vor- und Zurück-Knöpfe sein.</p></item>
<item><p>Stellen Sie in Ihren Werkzeugleisten nur die am häufigsten genutzten Anwendungsfunktionen bereit. Fügen Sie nicht für jeden Menüeintrag einen Knopf hinzu.</p></item>
<item><p>Wenn Sie eine <link xref="menu-bars">Menüleiste</link> verwenden, stellen Sie sicher, dass dort alle Funktionen enthalten sind, die in der Werkzeugleiste angezeigt werden, entweder direkt (über einen entsprechenden Menüeintrag) oder indirekt (beispielsweise im Dialog <guiseq><gui>Options</gui><gui>Settings</gui></guiseq>).</p></item>
<item><p>Werkzeugleisten sollten keine Knöpfe für <gui>Help</gui>, <gui>Close</gui> oder <gui>Quit</gui> anzeigen, da diese selten verwendet werden und der Platz besser für nützlichere Bedienelemente genutzt werden kann. Analog dazu sollten Sie die Knöpfe für <gui>Undo</gui>, <gui>Redo</gui> und die Standardaktionen der Zwischenablage nur in der Werkzeugleiste anzeigen, wenn es tatsächlich Platz dafür gibt und nützliche, anwendungsspezifische Bedienelemente dafür nicht weichen müssen.</p></item>
<item><p>Werkzeugleisten<link xref="buttons">knöpfe</link> sollten ein Relief haben, und Symbolknöpfe sollten <link xref="icons-and-artwork#color-vs-symbolic">stilisierte Symbole</link> haben.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-Referenz</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkToolbar.html">GtkToolbar</link></p></item>
</list>

</section>

</page>
