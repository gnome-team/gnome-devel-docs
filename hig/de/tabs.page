<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="tabs" xml:lang="de">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/tabs.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Reiter</title>

<p>Reiter unterteilen ein Fenster in mehrere Ansichten. Es gibt zwei Formen, feste und dynamische Reiter.</p>

<p>Feste Reiter stellen einen unveränderlichen Satz vordefinierter Ansichten bereit und kommen vorrangig in Dialogfenstern zum Einsatz. Dynamische Reiter ermöglichen es, dass ein Fenster eine veränderliche Auswahl an Inhalten enthält, wie Seiten, Dokumente oder Bilder. Sie werden vor allem in <link xref="primary-windows">primären Fenstern</link> verwendet, als Teil eines Editors oder Browsers.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/tabs.svg"/>

<section id="when-to-use">
<title>Anwendungsfälle</title>

<p>Verwenden Sie feste Reiter, wenn ein <link xref="dialogs">Dialogfenster</link> zu viele Bedienelemente enthält (oder zu viele Informationen), die nicht komfortabel auf einmal angezeigt werden können.</p>

<p>Dynamische Reiter sind primär sinnvoll für Browser- oder Editor-Anwendungen, in denen der Benutzer verschiedene Orte darstellen oder mehrere Inhalte simultan bearbeiten will.</p>

</section>

<section id="fixed">
<title>Feste Reiter</title>

<list>
<item><p>Verwenden Sie nicht zu viele Reiter. Wenn Sie ohne Scrollen oder Teilen in mehrere Ansichten nicht alle Reiter anzeigen können, sollten Sie vielleicht stattdessen eine Listenansicht verwenden.</p></item>
<item><p>Beschriften Sie Reiter in <link xref="writing-style#capitalization">Überschrifts-Großschreibung</link> und verwenden Sie Substantive statt Verben, zum Beispiel <gui>Font</gui> oder <gui>Alignment</gui>. Versuchen Sie, die Länge der Reiterbeschriftungen so weit wie möglich aneinander anzugleichen.</p></item>
<item><p>Gestalten Sie Reiter nicht so, dass die Änderung von Bedienelementen auf einer Seite die Bedienelemente auf einer anderen Seite beeinflusst. Es ist unwahrscheinlich, dass Benutzer solche Abhängigkeiten entdecken.</p></item>
<item><p>Wenn ein Bedienelement jeden Reiter beeinflusst, platzieren Sie es außerhalb der Reiter.</p></item>
<item><p>Legen Sie bei festen Reitern die Breite jedes Reiters proportional zu dessen Beschriftung fest. Setzen Sie nicht alle Reiter auf die gleiche Breite, da dies visuell schlechter erkennbar ist und die Anzahl der Reiter begrenzt, die ohne Bildlauf angezeigt werden können.</p></item>
</list>

</section>

<section id="dynamic">
<title>Dynamische Reiter</title>

<list>
<item><p>Die Breite von Reitern ist oft beschränkt, so dass Sie sicherstellen sollten, dass die Beschriftungen kurz und prägnant sind und der wichtigste Teil der Beschriftung zuerst angezeigt wird. Das sorgt dafür, dass die Bedeutung einer Beschriftung noch erkennbar ist, wenn ein Teil durch die Unicode-Ellipsis verdeckt wird.</p></item>
<item><p>Wenn sich der Inhalt eines Reiters ändert oder Aufmerksamkeit erfordert, kann ein visueller Hinweis angezeigt werden.</p></item>
<item><p>Stellen Sie für jeden Reiter ein Kontextmenü bereit. Dieses Menü sollte nur Aktionen anbieten, die die Manipulation des Reiters selbst betreffen, wie <gui>Move Left</gui>, <gui>Move Right</gui>, <gui>Move to New Window</gui> und <gui>Close</gui>.</p></item>
<item><p>Falls Reiter ein wichtiger Teil der Anwendung sind, kann ein Knopf zum Öffnen eines neuen Reiters in der Kopfleiste oder Werkzeugleiste platziert werden. Verwenden Sie einen solchen Knopf nicht in Anwendungen, wo nicht ständig Reiter angezeigt werden – Tastenkombinationen und/oder Menüeinträge reichen in solchen Fällen aus.</p></item>
</list>

<section id="keyboard-shortcuts">
<title>Standard-Tastenkürzel</title>

<p>Stellen Sie bei der Verwendung dynamischer Reiter sicher, dass die Standard-Tastenkombinationen unterstützt werden.</p>

<table>
<tr>
<td><p><keyseq><key>Strg</key><key>T</key></keyseq></p></td>
<td><p>Einen neuen Reiter öffnen</p></td>
</tr>
<tr>
<td><p><keyseq><key>Strg</key><key>W</key></keyseq></p></td>
<td><p>Den aktuellen Reiter schließen</p></td>
</tr>
<tr>
<td><p><keyseq><key>Strg</key><key>Bild auf</key></keyseq></p></td>
<td><p>Zum nächsten Reiter wechseln</p></td>
</tr>
<tr>
<td><p><keyseq><key>Strg</key><key>Bild ab</key></keyseq></p></td>
<td><p>Zum vorherigen Reiter wechseln</p></td>
</tr>
</table>

</section>

</section>

<section id="api-reference">
<title>API-Referenz</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkNotebook.html">GtkNotebook</link></p></item>
</list>

</section>
</page>
