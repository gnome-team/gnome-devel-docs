<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="spin-boxes" xml:lang="cs">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/spin-boxes.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Číselníky</title>

<p>Číselník je textové pole, které přijímá rozsah hodnot. Obsahuje dvě tlačítka, pomocí kterých může uživatel zvyšovat nebo snižovat hodnotu v pevných mezích.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/spin-boxes.svg"/>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Číselník použijte, aby uživatel mohl vybrat číselnou hodnotu, ale jen když má tato hodnota přímo význam, nebo je užitečné, aby ji uživatel věděl. Pokud tomu tak není, může být lepší volbou <link xref="sliders">táhlo</link>.</p>

<p>Číselníky používejte jen pro vstup číselných hodnot. Pro nečíselná nastavení místo nich použijte <link xref="lists">seznamy</link> nebo <link xref="drop-down-lists">rozbalovací seznamy</link>.</p>

</section>

<section id="sliders">
<title>Táhla</title>

<p>V některých případech je vhodné propojit číselník s táhlem. Díky takové kombinaci je možné jak ovládat přibližnou hodnotu, tak zadat přesnou hodnotu. Má to ale smysl, jen když je zapotřebí, aby lidé znali konkrétní hodnotu, která se použije. Táhlo použijte když:</p>

<list>
<item><p>Je zapotřebí okamžitá zpětná odezva na změny hodnoty v číselníku (třeba v případě úprav obrázků).</p></item>
<item><p>Je pro uživatele užitečné ovládat rychlost změny hodnoty v reálném čase. Například kvůli sledování projevu změny barvy v okně s živým náhledem podle toho, jak se hýbe s táhly RGB.</p></item>
</list>

</section>

<section id="general-guidelines">
<title>Obecné zásady</title>

<list>
<item><p>Číselník opatřete popiskem umístěným nad něj nebo nalevo od něj a řiďte se přitom pravidly pro psaní velkých písmen ve větách. V popisku nabídněte horkou klávesu, která uživateli umožní číselník přímo zaměřit.</p></item>
<item><p>Obsah v číselníku zarovnávejte doprava, pokud to tedy neodporuje zvyklostem národního prostředí. Je to užitečné v oknech, ve kterých uživatel může porovnávat dvě číselné hodnotu ve stejném sloupci ovládacích prvků. V takové případě pohlídejte, aby i příslušné ovládací prvky byly zarovnané.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSpinButton.html">GtkSpinButton</link></p></item>
</list>

</section>

</page>
