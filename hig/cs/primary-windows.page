<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="primary-windows" xml:lang="cs">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Hlavní okno či okna pro vaši aplikaci</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Hlavní okna</title>

<media type="image" mime="image/svg" src="figures/patterns/primary-windows.svg"/>

<p>Hlavní okno je základní kontejner nejvyšší úrovně pro uživatelské rozhraní vaší aplikace a měl by nabízet základní funkcionalitu vaší aplikace.</p>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Každá aplikace by měla při aktivaci svého spouštěče zobrazit hlavní okno. Týká se to i aplikací, které poskytují převážně služby na pozadí.</p>

</section>

<section id="application-types">
<title>Typy aplikací</title>

<p>Existují dva hlavní modely pro hlavní okna:</p>

<section id="single-instance-applications">
<title>Aplikace s jednou instancí</title>

<p>Aplikace s jednou instancí má jedno hlavní okno. Jedná se o model běžný v komunikačních aplikacích pro účely, jako je e-mail, diskuze nebo kontakty.</p>

</section>

<section id="multiple-instance-applications">
<title>Aplikace s více instancemi</title>

<p>Aplikace s více instancemi mohou mít více hlavních oken. Typicky jsou všechna hlavní okna stejná. K aplikacím s více instancemi patří často prohlížeče a editory, například pro dokumenty nebo obrázky.</p>

<p>Jak aplikace s jednou instancí, tak aplikace s více instancemi mohou umožňovat otevřít více položek obsahu, buď pomocí použití <link xref="tabs">karet</link> nebo pomocí navigace ve stylu prohlížeče. Více oken ale nabízí dodatečné schopnosti, včetně:</p>

<list>
<item><p>Zobrazení několika položek obsahu vedle sebe naráz.</p></item>
<item><p>Umístění obsahu na různé pracovní plochy.</p></item>
<item><p>Uspořádání skupin obsahu do různých oken (pokud používají karty).</p></item>
</list>

<section id="parent-child-primary-windows">
<title>Rodičovská/dceřiná okna</title>

<p>Aplikace s více instancemi má typicky identická hlavní okna (například více oken webového prohlížeče). Ale nemusí tomu tak být ve všech případech.</p>

<p>Hlavní okna mohou mít vztah rodič/potomek. V takovémto typu aplikace je vždy jen jedno rodičovské okno. Typicky obsahuje přehled položek, které mohou být otevřeny v rodičovském okně nebo v oddělených dceřiných oknech. Díky tomu lze otevřít více položek obsahu současně.</p>

<p>I když bylo dceřiné okno otevřeno z rodičovského okna, není na něm závisle v tom, jestli má zůstat otevřeno – zavření rodičovského okna nemá za následek zavření dceřiných oken v aplikaci.</p>

<p>Aplikace <app>Poznámky</app> z GNOME je dobrým příkladem hlavních oken se vztahem rodič/potomek.</p>

</section>

</section>
</section>

<section id="general-guidelines">
<title>Obecné zásady</title>

<list>
<item><p>Když je vaše aplikace spuštěna, mělo by se vždy zobrazit jedno hlavní okno.</p></item>
<item><p>Když je spouštěč vaší aplikace aktivován ve chvíli, kdy aplikace běží, měla by se zobrazit všechna její hlavní okna.</p></item>
<item><p>Hlavní okno by se mělo starat o hlavní funkcionalitu vaší aplikace. Nespoléhejte se s poskytnutím základní funkcionality na dialogová nebo podřízená okna.</p></item>
<item><p>Hlavní okno by mělo být nezávislé – zavření jednoho hlavního okna by nemělo mít za následek, že se zavřou další hlavní okna.</p></item>
<item><p>Dialogová okna by měly být vždy závislá na hlavním okně. Viz pokyny k <link xref="dialogs">dialogovým oknům</link>.</p></item>
<item><p>Pokyny pro <link xref="display-compatibility">kompatibilitu s displejem</link> jsou zvláště důležité pro hlavní okna: dávejte pozor, jestli je zajištěno, aby se řídila radami ohledně minimálních velikostí obrazovky, otočení displeje a přichytávání k polovině obrazovky.</p></item>
<item><p><gui>Quit</gui> (<gui>Ukončit</gui>) by mělo zavřít všechna hlavní okna.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkWindow.html">GtkWindow</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">GtkApplicationWindow</link></p></item>
</list>

</section>

</page>
