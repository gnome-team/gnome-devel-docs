<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="info-bars" xml:lang="cs">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Oznámení události v rámci aplikace</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Informační lišty</title>

<media type="image" mime="image/svg" src="figures/patterns/info-bar.svg"/>

<p>Informační lišta je proužek umístěný nad zobrazením obsahu, přímo pod hlavičkovou lištou nebo pod nástrojovou lištou. Obsahuje text a případně i některé ovládací prvky. Její zobrazení může být buď trvalé nebo se může na přání uživatele skrývat.</p>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Informační lišty můžete použít ke sdělení konkrétního stavu ke konkrétní položce obsahu nebo umístění. Například může informační lišta sdělit, že dokumentu vypršela platnost, nebo že je upravován jiným uživatelem, nebo že služba patřící k odkazu je mimo provoz. V některých situacích může být použita i ke sdělení dodatečných informací, jako jsou rady uživateli.</p>

<p>Protože informační lišty trvale zůstávají, jsou obecně vhodnější pro sdělování průběžného stavu, než pro události (k tomu jsou vhodnější <link xref="notifications">oznámení</link> a <link xref="in-app-notifications">oznámení v aplikaci</link>).</p>

<p>Informační lišty používají ke sdělování informací primárně text a jejich přínos je v tom, že mohou obsahovat jak nadpis, tak delší vysvětlující text. Na druhou stranu zabírají místo a odvádějí pozornost. Pokud stav, o kterém chcete informovat, není kritický, nebo může být sdělen jen jednoduchým textem nebo ikonou, možná byste měli zvážit jiný přístup: text nebo ikona mohou být přidány v uživatelském rozhraní kamkoliv jinam, nebo můžete měnit podobu ovládacích prvků navigace (jako jsou <link xref="view-switchers">přepínače zobrazení</link>, <link xref="tabs">karty</link> nebo <link xref="sidebar-lists">postranní panely</link>).</p>

</section>

<section id="guidelines">
<title>Zásady</title>

<list>
<item><p>Vyvarujte se nadužívání informační lišty. Měla by se v uživatelském rozhraní objevovat spíše výjimečně.</p></item>
<item><p>V jeden okamžik by měla být viditelná jej jedna informační lišta.</p></item>
<item><p>Delší vysvětlení poskytněte, jen když je opravdu zapotřebí. Jednoduchý název je často postačující.</p></item>
<item><p>Obecně řečeno, informační lišta nepotřebuje ikonu.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>

<list>
<item><p><link xref="https://developer.gnome.org/gtk3/stable/GtkInfoBar.html">GtkInfoBar</link></p></item>
</list>

</section>

</page>
