<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="radio-buttons" xml:lang="cs">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/radio-buttons.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Skupinové přepínače</title>

<p>Skupinové přepínače se používají ve skupinách k výběru z navzájem se vylučujících voleb. Z celé skupiny může být vždy vybrán jen jeden přepínač. Stejně jakou u zaškrtávacích políček platí, abyste nepoužívali skupinové přepínače ke spouštění činností.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/radio-buttons.svg"/>

<section id="guidelines">
<title>Zásady</title>

<list>
<item><p>Skupinové přepínače používejte ve skupině minimálně po dvou, nikdy nepoužívejte jen jeden samostatný. K prezentování jediného nastavení použijte zaškrtávací políčko nebo dva skupinové přepínače, každý pro jeden stav.</p></item>
<item><p>Ve skupině může být v kteroukoliv chvíli vybrán právě jen jeden skupinový přepínač. Výjimkou je, když skupina zobrazuje vlastnosti vícenásobného výběru, kdy pak jeden nebo více přepínačů může být v nejednoznačném stavu.</p></item>
<item><p>Kliknutí na skupinový přepínač by nemělo ovlivnit hodnoty žádných jiných ovládacích prvků. Může ale zpřístupnit, znepřístupnit, skrýt nebo zobrazit jiné ovládací prvky.</p></item>
<item><p>Když přepínání skupinového přepínače ovlivňuje přístupnost ostatních ovládacích prvků, umístěte přepínač bezprostředně vlevo od nich. To pomůže rozpoznat, že ovládací prvky jsou závislé na stavu skupinového přepínače.</p></item>
<item><p>Pro popisky skupinových přepínačů používejte pravidla pro <link xref="writing-style#capitalization">velká písmena ve větě</link>, například <gui>Switched movement</gui> (<gui>Přesunout prohozením</gui>). V popisku poskytněte <link xref="keyboard-input#access-keys">horkou klávesu</link>, která umožní uživateli nastavit skupinový přepínač přímo z klávesnice.</p></item>
<item><p>Když skupinový přepínač reprezentuje nastavení ve vícenásobném výběru, ve kterém jej některé objekty mají zapnutý a jiné ne, zobrazte přepínač v nejednoznačném stavu.</p></item>
<item><p>Neumisťujte do jedné skupiny více jak osm skupinových přepínačů. Pokud jich potřebujete více, zvažte použití seznamu s výběrem jedné položky, ale možná byste se spíše měli zamyslet, jak zjednodušit uživatelské rozhraní.</p></item>
<item><p>Pokuste se ve skupině zarovnat skupinové přepínače spíš svisle než vodorovně, protože se tak lépe prochází očima. Vodorovné zarovnání nebo zarovnání do obdélníku použijte jen, když významně zlepší rozvržení okna.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkRadioButton.html">GtkRadioButton</link></p></item>
</list>

</section>
</page>
