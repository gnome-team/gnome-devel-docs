<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="dialogs" xml:lang="cs">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Další okna, která se objevují nad hlavním rodičovským oknem</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Dialogová okna</title>

<p>Dialogové okna jsou podřízená okna, která se objevují nad hlavními (rodičovskými) okny. Používají se k zobrazení dodatečných informací a ovládacích prvků, včetně předvoleb a vlastností, nebo k zobrazení zpráv a dotazů.</p>

<p>GTK poskytuje řadu standardních dialogových oken, která můžete použít, například pro tisk nebo výběr barvy.</p>

<p>Existují tři základní typy dialogových oken.</p>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Dialogová okna jsou běžně známé návrhové vzory a mají zavedené zvyklosti pro různé typy dialogů, které můžete potřebovat použít. Pokyny u jednotlivým typům dialogových oken k tomu poskytují další informace.</p>

<p>I když jsou dialogová okna účinným způsobem, jak dát dohromady doplňující ovládací prvky a informace, mohou být také zdrojem obtěžování uživatele. Proto si vždy položte otázku, jestli je dialogové okno nutné a dělejte věci tak, abyste se vyhnuli situacím, kdy je zapotřebí.</p>

<p>Existuje řada způsobů, jak se vyhnout používání dialogových oken:</p>

<list>
<item><p>Pro nové zprávy, záznamy nebo kontakty používejte vložené uspořádání.</p></item>
<item><p>Oznámení v rámci aplikace jsou alternativou k dialogovým oknům se zprávami.</p></item>
<item><p><link xref="popovers">Rozbalovací dialogy</link> mohou být jedním ze způsobů, jako zobrazit doplňující ovládací prvky a volby méně rušivým stylem.</p></item>
</list>

</section>

<section id="message-dialogs">
<title>Dialogová okna se zprávou</title>

<media type="image" mime="image/svg" src="figures/patterns/message-dialog.svg"/>

<p>Dialogová okna se zprávou jsou nejjednodušším typem dialogových oken. Zobrazují zprávy nebo dotazy spolu s jedním až třemi tlačítky, kterými se dá na zprávu zareagovat. Vždy jsou modální, což znamená že brání v přístupu k jejich rodičovskému oknu. Dialogové okno se zprávou je vhodnou volbou, když je podstatné, aby se uživatel na zprávu podíval a zareagoval na ni.</p>

<section id="message-dialog-examples">
<title>Příklady</title>

<p>Potvrzovací dialogová okna používají dialogové okno se zprávou ke kontrole, nebo potvrzení, že uživatel chce nějakou činnost provést. Mají dvě tlačítka: jedno k potvrzení, že činnosti by se měla provést, a druhé ke zrušení činnosti.</p>

<note style="tip"><p>Tato dialogová okna bývají občas potvrzena nechtěně nebo automaticky a ne vždy zabrání chybnému průběhu. Proto je často lepší místo nich prostě poskytnou funkci pro vrácení následků činnosti zpět.</p></note>

<p>Chybová dialogová okna sdělují uživateli chybovou zprávu. Často obsahují jedno tlačítko, které uživatel potvrdí, že vzal zprávu na vědomí a okno uzavře.</p>

<note style="tip"><p>Chybová dialogová okna jsou obecně poslední možností. Měli byste své aplikace navrhovat tak, aby se chyby nevyskytovaly, a když se něco vyvine špatně, vyřešit to automatizovaně.</p></note>

</section>
</section>

<section id="action-dialogs">
<title>Akční dialogová okna</title>

<media type="image" mime="image/svg" src="figures/patterns/action-dialog.svg"/>

<p>Akční dialogová okna zobrazují volby a informace ke konkrétní činnosti před tím, než je provedena. Mají hlavičku (která obvykle popisuje činnosti) a dvě hlavní tlačítka – jedno, které umožní činnost provést, a druhé, kterým ji můžete zrušit.</p>

<p>V některých případech je zapotřebí, aby uživatel před provedením činnosti nastavil nějaké volby. V takové situaci by potvrzující tlačítko nemělo být přístupné, dokud požadované volby nejsou nastavené.</p>

<section id="action-dialog-examples">
<title>Příklady</title>

<p>Velká část standardních dialogových oken GTK jsou právě akční dialogová okna. Dobrým příkladem je dialogové okno tisku: zobrazí se jako odezva na uživatelův požadavek tisku a nabídne informace a volby pro tisk. Dvě tlačítka v hlavičkové liště dávají na výběr činnost zrušit nebo provést.</p>

</section>
</section>

<section id="presentation-dialogs">
<title>Prezentační dialogová okna</title>

<media type="image" mime="image/svg" src="figures/patterns/presentation-dialog.svg"/>

<p>Prezentační dialogová okna zobrazují informace nebo ovládací prvky. Podobně jako akční dialogová okna mají hlavičkovou lištu a uvedeno, čeho se týkají. Nejsou ale předzvěstí nějaké činnosti a jejich obsah se vztahuje k aplikaci nebo položkám obsahu.</p>

<section id="presentation-dialog-examples">
<title>Příklady</title>

<p>Příkladem prezentačních dialogových oken jsou předvolby a vlastnosti. Oba zobrazují informace a nastavení vztahující se ke konkrétnímu subjektu (buď k aplikaci nebo k položce obsahu). Dialogové okno s vlastnostmi je dobrým příkladem, jak se dají podat doplňující informace, které nejsou v hlavním okně aplikaci zapotřebí pořád.</p>

<note style="tip"><p>Snažte se odolat pokušení ve své aplikaci nabízet dialogové okno s předvolbami. Vždy se ptejte, jestli jsou další nastavení opravdu zapotřebí. Většina lidí se vůbec nebude obtěžovat tím, aby nabízené předvolby zkoumala, a volby nastavení jen dělají aplikaci složitější. Raději věnujte úsilí tomu, aby návrh vaší aplikace fungoval pro všechny, bez potřeby nějaká nastavení měnit.</p></note>

</section>

<section id="instant-and-explicit-apply">
<title>Okamžité a výslovné použití</title>

<p>U prezentačních dialogů se rozlišuje, jestli se nastavení použijí okamžitě, nebo až po výslovném potvrzení. U dialogových oken s okamžitým použitím se podle změn v nastavení nebo hodnotách ihned provádí aktualizace. Oproti tomu u dialogových oken s výslovným použitím najdete tlačítko pro potvrzení použití změn.</p>

<p>Kdekoliv je to možné, byste se měli přiklonit k okamžitému použití. Prezentační dialogové okno s okamžitým použitím má na své hlavičkové liště zavírací tlačítko, podobně jako <link xref="primary-windows">hlavní okno</link>.</p>

<p>Výslovné použití je nutné, jen když změny v dialogovém okně musí být použité naráz, aby se dosáhlo požadovaného chování. Tato dialogová okna obsahují tlačítka <gui>Done</gui> a <gui>Cancel</gui> (<gui>Hotovo</gui> a <gui>Zrušit</gui>). Tlačítko <gui>Cancel</gui> vrátí hodnoty na původní, do stavu před otevřením dialogového okna. <gui>Done</gui> zajistí použití změn a zavře okno.</p>

</section>
</section>

<section id="primary-buttons">
<title>Hlavní tlačítka</title>

<p>Součástí dialogových oken se zprávou a akčních dialogových oken jsou tlačítka, která ovlivňují celé okno. Pořadí těchto tlačítek a jejich popisky jsou klíčovou částí dialogového okna.</p>

<section id="order">
<title>Pořadí</title>

<p>Pokud dialogové okno obsahuje tlačítka pro souhlas a zrušení, vždy se postarejte, aby tlačítko pro zrušení bylo v pořadí první. V národních prostředích s postupem zleva doprava to znamená umístit jej vlevo.</p>

<p>Toto pořadí tlačítek zajistí, že na tlačítko pro zrušení uživatel narazí, a uvědomí si jeho funkci, dříve, než tomu bude s tlačítkem pro potvrzení.</p>

</section>

<section id="labels">
<title>Popisky</title>

<p>Hlavní potvrzovací tlačítko popište konkrétním rozkazovacím slovesem, například: <gui>Save</gui>, <gui>Print</gui>, <gui>Remove</gui>. V českých překladech použijte infinitiv a rozlišujte dokonavost (<gui>Uložit</gui>, <gui>Vytisknout</gui>, <gui>Odstranit</gui>). Je to srozumitelnější, než obecné popisky jako <gui>OK</gui> nebo <gui>Done</gui> (<gui>Budiž</gui> nebo <gui>Hotovo</gui>).</p>

<p>Chybové dialogové okno obvykle obsahuje jedno tlačítko pro zavření dialogového okna. V takovýchto situacích nebývá zapotřebí odkazovat na konkrétní činnosti a jsou dobrou příležitostí pro odlehčení špetkou humoru. <gui>Apology Accepted</gui> nebo <gui>Got It</gui> (<gui>Omluva se přijímá</gui> nebo <gui>Beru to</gui>) jsou dobrými příklady popisků.</p>

</section>

<section id="default-action-and-escape">
<title>Výchozí činnost a zrušení</title>

<p>Přiřaďte klávesu <key>Enter</key> aktivaci hlavního potvrzovacího tlačítka v dialogovém okně (například tlačítku <gui>Print</gui> v dialogovém okně tisku). Nazývá se to výchozí činnost a je vyznačena odlišným vizuálním stylem. Nedělejte ale z tlačítka výchozí, pokud jeho činnost nezle vrátit, je destruktivní nebo jinak nepohodlná pro uživatele. Pokud neexistuje žádné vhodné tlačítko, které by mohlo být výchozí, tak žádné jako výchozí nenastavujte.</p>

<p>Měli byste také zajistit, že klávesa <key>Esc</key> aktivuje tlačítko, které vede ke zrušení nebo zavření, a aby toto tlačítko bylo přítomno. Dialogová okna se zprávou s jedním tlačítkem mohou mít na tlačítko navázánu zároveň klávesu <key>Esc</key> i <key>Enter</key>.</p>

<p>Zmíněné navázání kláves poskytuje předvídatelný a pohodlný způsob, jak pokračovat v tom, co dialogové okno nabízí, nebo jak se vrátit.</p>

</section>

</section>

<section id="general-guidelines">
<title>Obecné zásady</title>

<list>
<item><p>Dialogová okna by se nikdy neměla objevovat neočekávaně a měla by se zobrazit jen jako bezprostřední odezva na záměrnou činnost uživatele.</p></item>
<item><p>Dialogové okno by vždy mělo mít rodičovské okno.</p></item>
<item><p>Když navrhujete obsah okna, řiďte se <link xref="visual-layout">pokyny k rozvržení</link>.</p></item>
<item><p>K rozdělení ovládacích prvků a informací použijte <link xref="view-switchers">přepínače zobrazení</link> nebo <link xref="tabs">karty</link>.</p></item>
<item><p>Vyvarujte se vršení dialogových oken jednoho na druhé. V jednu chvíli by mělo být zobrazeno jen jedno dialogové okno.</p></item>
<item><p>Když otevíráte dialogové okno, dejte na začátku zaměření klávesnice komponentě, u které očekáváte, že ji uživatel použije jako první. Toto zaměření je zvláště důležité pro uživatele, kteří musí používat pro ovládání aplikace klávesnici.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkAboutDialog.html">GtkAboutDialog</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkDialog.html">GtkDialog</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMessageDialog.html">GtkMessageDialog</link></p></item>
</list>
</section>

</page>
