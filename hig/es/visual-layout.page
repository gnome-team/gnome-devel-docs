<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="visual-layout" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Orgnizar elementos dentro de una interfaz de usuario.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Distribución visual</title>

<p>El diseño visual de controles, información y contenido influye en que tan fácil de comprender es su aplicación, así como lo bien que se ve. Es importante reconocer que el diseño visual tiene un fuerte impacto en cuanto trabajo esta implicado en usar una aplicación - diseños pobres resultan en que usuarios tengan que poner un esfuerzo adicional, mientras que buenos diseños requieren menos esfuerzo.</p>

<p>Un buen estilo visual hace que la aplicación sea más atractiva y amable visualmente.</p>

<p>Seguir estos lineamientos de diseño visual le ayudara a producir una aplicación que sea agradable, fácil de comprender y eficiente de usar.</p>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>Un punto de alineación es una línea imaginaria vertical u horizontal a través de su ventana que toca el borde de una o más etiquetas o controles en la ventana. Minimice el número de estas: cuantas menos existan, más limpio y simple aparecerá su diseño, y más fácil de entender será para las personas.</p></item>
<item><p>Alinee el contenido y controles en su diseño con exactitud. El ojo es muy sensible para alinear y desalinear objetos. Si los elementos visuales no se alinean, sera difícil para algunos el analizarlos. Los elementos que no se alinean por completo serán motivo de distracción.</p></item>
<item><p>Sea consistente. Use la misma cantidad de espacio siempre.</p></item>
<item><p>Organice controles e información relacionada en grupos, y use espacio para diferenciarlos. Esto hace una interfaz mucho más fácil de leer y entender.</p>
<list>
<item><p>Como regla de oro, deje espacio entre los elementos en incrementos de 6 píxeles, aumentando hasta que la relación entre los elementos relacionados se vuelva mas distante.</p></item>
<item><p>Entre las etiquetas y componentes asociados, deje 12 píxeles horizontales.</p></item>
<item><p>Para espaciado vertical entre grupos de componentes, 18 píxeles es adecuado.</p></item>
<item><p>Se recomienda un espacio de 18 píxeles entre el contenido de una ventana de diálogo y el borde de la ventana.</p></item>
</list></item>
<item><p>Sangre los elementos 12 píxeles para resaltar la jerarquía y la asociación.</p></item>
<item><p>Evite usar marcos con bordes visibles para separar grupos de controles — use el espaciado y las cabeceras en su lugar.</p></item>
</list>

<p>Espaciado y alineación incorrectos:</p>
<media type="image" mime="image/svg" src="figures/visual-alignment-incorrect.svg"/>

<p>Espaciado y alineación correctos:</p>
<media type="image" mime="image/svg" src="figures/visual-alignment-correct.svg"/>

<p>Espaciado correcto, con unidades (píxeles):</p>
<media type="image" mime="image/svg" src="figures/visual-alignment-spacing.svg"/>

</section>

<section id="label-alignment">
<title>Alineación de las etiquetas</title>

<p>Si es posible, justifique las etiquetas a la derecha (vea el diagrama a continuación). Esto evitará vacíos grandes entre las etiquetas y sus controles asociados. Este tipo de justificación a la derecha no es posible si ha sangrado los controles: en este caso, se debe usar la justificación a la izquierda en su lugar.</p>

<media type="image" mime="image/svg" src="figures/label-alignment.svg"/>

</section>

<section id="hierarchy">
<title>Jerarquia visual</title>

<p>Organice los elementos visuales de arriba a abajo y de izquierda a derecha. Esta es la dirección en que la gente de occidente tiende a leer una interfaz, así los elementos en la parte superior izquierda se encontrarán primero. Este orden da jerarquía a las interfaces: aquellos componentes que se ven primero se percibe que tienen prioridad sobre aquellos que vienen después. Por esta razón, deberá colocar los controles dominantes arriba y a la izquierda de los controles y contenido que ellos afectan. Los encabezados son un patrón de diseño clave en este sentido.</p>

</section>

</page>
