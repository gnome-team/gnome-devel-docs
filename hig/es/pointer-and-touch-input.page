<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pointer-and-touch-input" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Interacción con el ratón, el «touchpad» y la pantalla táctil</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Puntero y entrada táctil</title>

<p>El puntera y la entrada táctil son dos de los principales métodos mediante los que los usuarios interactuarán con su aplicación.</p>

<section id="pointer-input">
<title>Entrada del puntero</title>

<p>Un dispositivo apuntador es cualquier dispositivo de entrada que permita la manipulación de un puntero (normalmente representado por una flecha y, a menudo, llamado «cursor») en la pantalla. Aunque los ratones y los «touchpad» son los más comunes, hay una amplia variedad de dispositivos, incluyendo tabletas gráficas, «track balls», «track points» y «joysticks».</p>

<section id="primary-and-secondary-buttons">
<title>Botones primario y secundario</title>

<p>Los ratones y los «touchpad» tienen a menudo dos botones principales. Uno de ellos actúa como botón primario, y el otro como botón secundario. Normalmente, el botón izquierdo se usa como botón primario y el botón derecho se usa como secundario. Sin embargo, este orden os configurable por el usuario y no se traduce a la entrada de la pantalla táctil. Por lo tanto, esta guía se refiere al botón primario y al botón secundario en lugar del a los botones derecho e izquierdo.</p>

<p>Use la acción primaria para seleccionar elementos y activar controles. La acción secundaria se puede usar para acceder a opciones adicionales, normalmente mediante un menú contextual.</p>

<p>No dependa de la entrada de botones secundarios o adicionales. Además de ser físicamente más difíciles de pulsar, algunos dispositivos apuntadores y muchas tecnologías de asistencia sólo soportan o emulan el botón primario.</p>

<p>Pulsa y mantener se debe usar para simular el botón secundario en los dispositivos apuntadores con un solo botón. Por lo tanto, no use «pulsar y mantener» para otros propósitos.</p>

</section>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>La doble pulsación no se debe usar, ya que no se puede descubrir y se exporta pobremente a la entrada táctil.</p></item>
<item><p>Si el ratón tiene rueda de desplazamiento, ésta debe desplazar la ventana o el control de debajo del puntero, si éste lo soporta. Iniciar el desplazamiento de esta manera no debe mover el foco del teclado de la ventana o del control que se desplaza.</p></item>
<item><p>No requiera el uso de pulsación simultánea de varios botones del ratón a la vez para ninguna operación.</p></item>
<item><p>No requiera el uso de varias pulsaciones (triple o cuádruple) para ninguna operación, a menos que proporcione una alternativa accesible para realizar la misma acción.</p></item>
<item><p>Permita que todas las operaciones hechas con el ratón se puedan cancelar antes de completarlas. Pulsar <key>Esc</key> debe cancelar cualquier operación del ratón en curso, tales como arrastrar y soltar en un gestor de archivos o dibujar una sombra en una aplicación de dibujo.</p></item>
<item><p>No se refiera a botones del ratón en particular en su interfaz a menos que sea absolutamente necesario. No todo el mundo usará el mismo ratón convencional con botones izquierdo, derecho y central, por lo que el texto o los diagramas a los que se refiera pueden ser confusos.</p></item>
</list>

</section>

<section id="mouse-and-keyboard-equivalents">
<title>Equivalentes del ratón y el teclado</title>

<p>Asegúrese de que cada operación de su aplicación se puede hacer con el ratón y con el teclado. Las únicas excepciones son las acciones donde un control motor preciso es una parte esencial de la tarea. Por ejemplo, controlar el movimiento en algunos tipos de juegos de acción o dibujar a mano alzada en una aplicación de edición de imágenes.</p>

<p>Si su aplicación permite seleccionar elementos, las siguientes acciones equivalentes deben estar disponibles.</p>

<table>
<thead>
<tr>
<td><p>Acción</p></td>
<td><p>Ratón</p></td>
<td><p>Teclado</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Abrir un elemento</p></td>
<td><p>Botón primario</p></td>
<td><p><key>Espacio</key></p></td>
</tr>
<tr>
<td><p>Añadir/quitar elemento de la selección</p></td>
<td><p><key>Ctrl</key> y botón primario</p></td>
<td><p><keyseq><key>Ctrl</key><key>Espacio</key></keyseq></p></td>
</tr>
<tr>
<td><p>Extender selección</p></td>
<td><p><key>Mayús</key> y botón primario</p></td>
<td><p><key>Mayús</key> combinada con cualquiera de las siguientes: <key>Espacio</key> <key>Inicio</key> <key>Fin</key> <key>Av Pág</key> <key>Re Pág</key></p></td>
</tr>
<tr>
<td><p>Cambiar la selección</p></td>
<td><p>Botón primario</p></td>
<td><p>Cualquiera de las siguientes: <key>←</key> <key>↑</key> <key>→</key> <key>↓</key> <key>Inicio</key> <key>Fin</key> <key>Re Pág</key> <key>Av Pág</key></p></td>
</tr>
<tr>
<td><p>Seleccionar todo</p></td>
<td><p>Botón primario sobre el primer elemento, luego botón primario y <key>Mayús</key> sobre el último elemento</p></td>
<td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
</tr>
<tr>
<td><p>Deseleccionar todo</p></td>
<td><p>Pulsación primaria en el fondo del contenedor</p></td>
<td><p><keyseq><key>Mayús</key><key>Ctrl</key><key>A</key></keyseq></p></td>
</tr>
</tbody>
</table>

</section>
</section>

<section id="touch-input">
<title>Entrada táctil</title>

<p>Las pantallas táctiles también se están volviendo una parte común del hardware moderno, y las aplicaciones con GTK están pensadas para usarse con hardware que incorpore una pantalla táctil. Por lo tanto, y para ajustarse a la mayor parte de este hardware y a las expectativas de los usuarios, es importante considerar la entrada táctil como parte del diseño de la aplicación.</p>

<section id="application-touch-conventions">
<title>Convenios táctiles en aplicaciones</title>

<p>Usar la entrada táctil de manera consistente con otras aplicaciones permitirá a los usuarios aprender fácilmente cómo usar sus aplicaciones con una pantalla táctil. Se recomiendan los siguientes convenios, cuando apliquen.</p>

<table>
<thead>
<tr>
<td><p>Acción</p></td>
<td><p>Descripción</p></td>
<td><p>Resultado</p></td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="3"><p><em style="strong">Pulsar</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/tap.svg"/></td>
<td><p>Pulse sobre un elemento.</p></td>
<td><p>Acción primaria. El elemento se abre; la foto se muestra a tamaño completo, se lanza la aplicación, la canción empieza a sonar.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Pulsar y mantener</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/tap-and-hold.svg"/></td>
<td><p>Pulse y mantenga durante uno o dos segundos.</p></td>
<td><p>Acción secundaria. Seleccione el elemento y liste las acciones que se pueden realizar.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Arrastrar</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/drag.svg"/></td>
<td><p>Deslice el dedo tocando la superficie.</p></td>
<td><p>Desplaza un área en la pantalla</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Pellizcar o estirar</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/pinch-or-stretch.svg"/></td>
<td><p>Toque la superficie con dos dedos mientras los junta o los separa.</p></td>
<td><p>Cambia el nivel de ampliación de la vista (ej. Mapas, Fotos).</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Doble pulsación</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/double-tap.svg"/></td>
<td><p>Pulse dos veces en una rápida sucesión.</p></td>
<td><p>Ampliación paso a paso.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Descartar</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/flick.svg"/></td>
<td><p>Arrastre muy rápido, perdiendo contacto con la superficie sin disminuir el movimiento.</p></td>
<td><p>Elimina un elemento.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="system-touch-conventions">
<title>Convenios táctiles del sistema</title>

<p>En GNOME 3 hay un número de gestos táctiles reservados para uso del sistema. Estos se deben evitar en las aplicaciones.</p>

<table>
<tr>
<td colspan="3"><p><em style="strong">Arrastrar al borde</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/edge-drag.svg"/></td>
<td><p>Arrastrar el dedo desde el borde de la pantalla</p></td>
<td><p>El borde superior izquierdo abre el menú de la aplicación.</p>
<p>Arrastrar en la parte superior derecha abre el menú de estado del sistema.</p>
<p>El borde izquierdo abre la vista de actividades con la vista de la aplicación visible.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Pellizco con tres dedos</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/3-finger-pinch.svg"/></td>
<td><p>Juntar tres o más dedos mientras se toca la interfaz.</p></td>
<td><p>Abre la vista de actividades.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Arrastrar con cuatro dedos</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/4-finger-drag.svg"/></td>
<td><p>Arrastrar arriba o abajo con cuatro dedos tocando la superficie.</p></td>
<td><p>Cambia de área de trabajo.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Mantener y pulsar con tres dedos</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/3-finger-hold-and-tap.svg"/></td>
<td><p>Mantenga tres dedos sobre la superficie mientras pulsa con el cuarto.</p></td>
<td><p>Cambia de aplicación.</p></td>
</tr>
</table>

</section>
</section>
</page>
