<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" xmlns:itst="http://itstool.org/extensions/" type="topic" id="lists" xml:lang="es">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Filas de información o elementos de contenido.</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Listas</title>

<p>Las listas son un elemento básico de la interfaz de usuario que se pueden utilizar para presentar información, elementos de contenido, o controles. Hay dos tipos principales de lista en GNOME 3: listas estándar y tablas.</p>

<p>Las <link xref="sidebar-lists">listas de barras laterales</link> son un patrón de diseño separado que también hace uso de una lista.</p>

<section id="when-to-use">
<title>Cuándo usarlas</title>

<p>Una lista es una manera adecuada de presentar elementos de contenido que tienen un nombre distinguible como identificador primario. Si el contenido es visual por naturaleza, como fotos o vídeos, puede querer considerar usar una <link xref="grids">rejilla</link> en su lugar.</p>

<p>En algunos casos es ventajoso mostrar tanto una lista como una vista de rejilla para los mismos elementos de contenido. Usada de esta manera, una vista de lista permite mostrar información adicional sobre el contenido mediante columnas de información.</p>

</section>

<section id="standard-lists">
<title>Listas estándar</title>

<p>Las listas estándar son el tipo de lista más habitual, y normalmente se deberían usaren lugar de las tablas.</p>

<p>En las listas estándar, cada fila se divide por separadores, y para cambiar la ordenación se usa un control que está fuera de la propia lista.</p>

<section id="styles">
<title>Estilos</title>

<p>Las listas estándar tienen dos estilos: anchura completa y empotradas. Las listas de anchura completa rellenan el contenedor, mientras que las listas empotradas tienen un margen alrededor.</p>

<p>Las listas de anchura completa son visualmente más sencillas y, por lo tanto, más elegantes. Se deben usar siempre que sea posible. De igual manera, no siempre es posible usar una lista de anchura completa, y hay algunas situaciones en las que las listas empotradas son una mejor opción.</p>

<list>
<item><p>Cuando la lista contiene columnas de información que se debe mantener cerca por motivos de legibilidad y es necesario tener la lista dentro de un contenedor ancho.</p></item>
<item><p>Cuando una ventana contiene varias listas.</p></item>
<item><p>Cuando la lista necesita estar alineada con otros controles dentro de la ventana.</p></item>
</list>

<media type="image" mime="image/svg" src="figures/patterns/list-styles.svg"/>

</section>

<section id="editable-lists">
<title>Listas editables</title>

<p>Las listas editables permiten al usuario añadir o eliminar elementos de la lista (por esta razón, algunas veces se denominan listas de añadir/quitar). Tanto las listas de anchura completa como las empotradas pueden ser editables.</p>

<p>Cada fila contiene un botón de eliminar. Si el número de elementos es pequeño, la fila de la lista final se debe usar como un botón de añadir. Alternativamente, si la lista es larga, el botón de añadir se puede colocar en una <link xref="header-bars">barra de cabecera</link> o en una <link xref="action-bars">barra de acción</link>.</p>

<media type="image" mime="image/svg" src="figures/patterns/editable-lists.svg"/>

</section>
</section>

<section id="tables">
<title>Tablas</title>

<p>Las tablas se pueden usar para listas de varias columnas más complejas, donde ordenar la tabla por varias columnas es habitual. Las cabeceras de las columnas permiten a los usuarios identificar el tipo de información en cada columna y reorganizar la lista de acuerdo al contenido de cada columna.</p>

<p>Al usar encabezados de columnas, indicar el orden usando las flechas en el encabezado.</p>

<table>
<thead>
<tr>
<td><p>Orden</p></td>
<td><p>Dirección de la flecha</p></td>
<td><p>Ejemplo</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Natural</p></td>
<td><p>Abajo</p></td>
<td><p>Alfabético, número más pequeño primero, fecha más antigua primero, elementos marcados primero</p></td>
</tr>
<tr>
<td><p>Invertido</p></td>
<td><p>Arriba</p></td>
<td><p>Alfabético invertido, número más grande primero, fecha más reciente primero, elementos sin marcar primero</p></td>
</tr>
</tbody>

</table>

</section>

<section id="row-behavior">
<title>Comportamiento de la fila</title>

<p>Dependiendo del tipo de lista, las filas tienen diferente comportamiento cuando se pulsa o selecciona alguna con el botón del ratón. En este sentido, hay tres tipos de listas:</p>

<table>
<thead>
<tr>
<td><p>Tipo de lista</p></td>
<td><p>Comportamiento de la fila</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Navegación</p></td>
<td><p>Seleccionar una fila abre el elemento correspondiente, si se esta navegando a una nueva vista o si se está abriendo un cuadro de dialogo. Este patrón es común para listas de elementos de contenido, o para presentar grupos de configuraciones.</p></td>
</tr>
<tr>
<td><p>Seleccionar</p></td>
<td><p>El elemento de la lista se selecciona al pulsar en él. Este enfoque se usa comúnmente para seleccionar una de una serie de opciones de configuración. En el caso del <link xref="drop-down-lists">menú desplegable</link>, una opción se selecciona siempre. La fila seleccionada se indica con una marca.</p></td>
</tr>
<tr>
<td><p itst:context="list-type">Editar</p></td>
<td><p>Seleccionar la fila convierte a la fila en un campo de texto que permite que el elemento se edite.</p></td>
</tr>
</tbody>
</table>

<p>Los estilos de las listas de navegación pueden combinarse con la <link xref="selection-mode">función de selección</link> para permitir a abrir y manipular las filas a la vez. Debería evitarse usar una doble pulsación para abrir elementos de lista, pues es incompatible e irreconocible por la entrada táctil.</p>

</section>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>Distinga los diferentes tipos de información usando ajustes diferentes, <link xref="typography">colores de texto y grosor</link>. Resalte la información más importante y relevante dando a otra información un color y/o grosor más suaves.</p></item>
<item><p>Tenga cuidado de no saturar listas con elementos y columnas diferentes, y asegúrese de usarlas sólo para presentar información esencial.</p></item>
<item><p>Como regla general, evite usar muchas listas en la misma ventana, particularmente en ventanas primarias.</p></item>
<item><p>No use listas con menos de cinco elementos, a menos que el número de elementos pueda incrementar con el tiempo. Para listas de opciones, casillas de selección o botones de selección excluyentes se pueden utilizar como una alternativa en este caso.</p></item>
<item><p>Garantice que las listas están ordenadas de forma que sean útiles para aquellos que las están usando. Por ejemplo, documentos recientes podrían ser más útiles que documentos ordenados alfabéticamente, o contactos que podrían estar en línea ser más interesantes que aquellos que están desconectados.</p></item>
<item><p>Si no usa los iconos en su lista, <link xref="icons-and-artwork">use iconos simbólicos</link>. La menor impacto visual de estos iconos significará que ellos no dominan ni sobrecargan visualmente su lista.</p></item>
<item><p>Si la lista es larga, haga posible su búsqueda usando el <link xref="search">patrón de diseño de búsqueda</link> estándar.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia de la API</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkListBox.html">GtkListBox</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkTreeView.html">GtkTreeView</link></p></item>
</list>
</section>

</page>
