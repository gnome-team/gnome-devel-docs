<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="dialogs" xml:lang="es">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Ventanas secundarias que aparecen sobre las ventanas padre, primarias</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Ventanas de diálogo</title>

<p>Los diálogos son ventanas secundarias que aparecen sobre una ventana padre, primaria. Se utilizan para presentar información adicional o controles, incluyendo las preferencias y propiedades, o para presentar mensajes o preguntas.</p>

<p>GTK proporciona un número de diálogos del almacén que se pueden usar, por ejemplo para la impresión o la selección de color.</p>

<p>Hay tres tipos básicos de diálogos.</p>

<section id="when-to-use">
<title>Cuándo usarlas</title>

<p>Los diálogos son comúnmente un patrón de diseño reconocido, y hay convenios establecidos para los diferentes tipos de diálogos que tal vez quiera usar. Las directrices sobre cada tipo de diálogo proporcionan información adicional sobre esto.</p>

<p>Mientras que los diálogos pueden ser una forma efectiva de divulgar controles adicionales o información, también pueden ser una fuente de interrupción para los usuarios. Por esta razón, pregúntese siempre si es necesario un diálogo, y trabaje para evitar las situaciones en las que se requieren.</p>

<p>Hay muchas formas de evitar el uso de diálogos:</p>

<list>
<item><p>Utilice la redacción en línea para nuevos mensajes, registros o contactos.</p></item>
<item><p>En las aplicaciones, las notificaciones son una alternativa a los mensajes de dialogo.</p></item>
<item><p>Las <link xref="popovers">Ventanas emergentes</link> pueden ser una forma de mostrar controles u opciones adicionales de una manera menos invasiva.</p></item>
</list>

</section>

<section id="message-dialogs">
<title>Mensajes de diálogo</title>

<media type="image" mime="image/svg" src="figures/patterns/message-dialog.svg"/>

<p>Los diálogos de mensajes son el tipo más sencillo de diálogo. Muestran un mensaje o una pregunta, junto con 1-3 botones con los que responder. Son siempre modales, lo que significa que evitan el acceso a la ventana padre. Los diálogos de mensajes son una elección apropiada cuando es imprescindible que el usuario vea y responda a un mensaje.</p>

<section id="message-dialog-examples">
<title>Ejemplos</title>

<p>Los diálogos de confirmación utilizan un mensaje de diálogo para comprobar (o confirmar) que el usuario quiere realizar una acción. Tienen dos botones: uno para confirmar que la acción debe llevarse a cabo y otro para cancelar la acción.</p>

<note style="tip"><p>Los diálogo de confirmación ser reconocerán a menudo accidentalmente o automáticamente y no siempre evitarán que se produzcan errores. A menudo es mejor proporcionar una funcionalidad de deshacer en su lugar.</p></note>

<p>Los diálogos de error presentan al usuario un mensaje de error. A menudo incluyen solo un botón que permite al usuario reconocer y cerrar el diálogo.</p>

<note style="tip"><p>Los diálogos de error son generalmente un último recurso. Debe diseñar su aplicación para que no ocurran errores, y para que se recupere automáticamente si algo sale mal.</p></note>

</section>
</section>

<section id="action-dialogs">
<title>Diálogos de acción</title>

<media type="image" mime="image/svg" src="figures/patterns/action-dialog.svg"/>

<p>Los diálogos de acción presentan opciones e información sobre una acción específica antes de que se lleve a cabo. Tienen un título (que normalmente describe la acción) y dos botones primarios: uno que permite llevar a cabo la acción y uno que la cancela.</p>

<p>A veces, el usuario requiere elegir opciones antes de que una acción pueda llevarse a cabo. En estos casos, el botón de diálogo afirmativo debe ser insensible hasta que se hayan seleccionado las opciones necesarias.</p>

<section id="action-dialog-examples">
<title>Ejemplos</title>

<p>Muchos de los diálogos existentes GTK son diálogo de acción. El diálogo de impresión es un buen ejemplo: se muestra al usuario en respuesta mediante la acción de impresión y presenta información y las opciones para la acción de impresión. Los dos botones de la barra de cabecera permiten que la acción de impresión se cancele o se lleve a cabo.</p>

</section>
</section>

<section id="presentation-dialogs">
<title>Diálogos de presentación</title>

<media type="image" mime="image/svg" src="figures/patterns/presentation-dialog.svg"/>

<p>Los diálogos de presentación muestran información o controles. Como diálogos de acción, tienen una barra de cabecera y un tema. Sin embargo, en lugar de determinar una acción, su contenido es relativo a una aplicación o un elemento de contenido.</p>

<section id="presentation-dialog-examples">
<title>Ejemplos</title>

<p>Las preferencias y propiedades son ejemplos de diálogos de presentación, y ambos presentan información y ajustes en relación a una entidad específica (una aplicación o un elemento de contenido). Los diálogos de propiedades son un buen ejemplo de cómo los diálogos pueden utilizarse para revelar información adicional que no siempre se necesita en la ventana principal de la aplicación.</p>

<note style="tip"><p>Resista la tentación de ofrecer una ventana de preferencia para su aplicación. Siempre cuestione si los ajustes adicionales son realmente necesarios. La mayoría de la gente no se molestará en investigar las preferencias que proporcione, y las opciones de configuración contribuirán a la complejidad de su aplicación. Haga un esfuerzo para asegurar que el diseño de su aplicación funcione para todos sin necesidad de cambiar su configuración.</p></note>

</section>

<section id="instant-and-explicit-apply">
<title>Aplicar instantánea y explícitamente</title>

<p>Los diálogos de presentación son de aplicación instantánea o explícita. En los diálogos de aplicación instantánea, los cambios en la configuración se actualizan automáticamente, mientras que los diálogos de aplicación explícita incluyen un botón para aplicar los cambios.</p>

<p>La aplicación instantánea debe utilizarse siempre que sea posible. Los diálogos de presentación aplicados al instante tienen un botón cerrar en la barra de cabecera, igual que una <link xref="primary-windows">ventana primaria</link>.</p>

<p>La aplicación explícita sólo es necesaria si los cambios en el diálogo se deben aplicar simultáneamente para ver el comportamiento esperado. Los diálogos de aplicación explícita incluyen los botones <gui>Hecho</gui> y <gui>Cancelar</gui> (<gui>Cancelar</gui> restablece todos los valores del diálogo al estado anterior a cuando se abrió y Hecho aplica todos los cambios y cierra la ventana).</p>

</section>
</section>

<section id="primary-buttons">
<title>Botones primarios</title>

<p>Los diálogos de mensaje y acción incluyen botones primarios que afectan a toda la ventana. El orden de estos botones, así como las etiquetas utilizadas, son una parte clave del diálogo.</p>

<section id="order">
<title>Orden</title>

<p>Cuando un diálogo incluye un botón afirmativo y un botón para cancelar, asegúrese siempre de que el botón Cancelar aparece primero, antes de que el botón afirmativo. En posición de izquierda a derecha, esto está a la izquierda.</p>

<p>Este orden de los botones asegura que los usuarios tomen conciencia, y recuerden, la posibilidad de cancelar antes de encontrar el botón afirmativo.</p>

</section>

<section id="labels">
<title>Etiquetas</title>

<p>Etiquete el botón primario afirmativo con un verbo imperativo específico, por ejemplo: <gui>Guardar</gui>, <gui>Imprimir</gui>, <gui>Eliminar</gui>. Esto es más claro que una etiqueta genérica del tipo <gui>Aceptar</gui> o <gui>Hecho</gui>.</p>

<p>Normalmente, los diálogos de error incluyen un único botón que descarta el diálogo. En este caso no es necesario referenciar una acción específica, y puede ser una buena oportunidad para usar el humor. <gui>Disculpa aceptada</gui> o <gui>Lo entiendo</gui> son ejemplos de buenas etiquetas.</p>

</section>

<section id="default-action-and-escape">
<title>Acción predeterminada y escape</title>

<p>Asigne la tecla Intro para activar el primer botón afirmativo en un diálogo (por ejemplo, <gui>Imprimir</gui> en un diálogo de impresión). Esto se denomina acción predeterminada, y se indica visualmente de manera distinta. No cree el botón predeterminado si dicha acción es irreversible, destructiva o de lo contrario será inconveniente para el usuario. Si no existe un botón apropiado para designarlo como botón predeterminado, no establezca ninguno.</p>

<p>También debe asegurarse de que la tecla Esc activa el botón de cancelar o el de cerrar, el que esté presente. Los diálogos de mensajes con un único botón pueden tener las teclas Esc e Intro asociadas al botón.</p>

<p>Asociar las teclas de Intro y Esc de esta forma proporciona una manera predecible y conveniente de continuar por el diálogo o volver atrás.</p>

</section>

</section>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>Las ventanas de diálogo no deben aparecer nunca inesperadamente, y sólo se deben mostrar como una respuesta inmediata a una acción deliberada del usuario.</p></item>
<item><p>Los diálogos deberían siempre tener una ventana padre.</p></item>
<item><p>Siga las <link xref="visual-layout">guías de distribución</link> cuando esté diseñando el contenido de ventanas.</p></item>
<item><p>Use <link xref="view-switchers">conmutadores de vista</link> o <link xref="tabs">pestañas</link> para dividir controles e información.</p></item>
<item><p>Evite apilar ventanas de diálogo unas sobre otras. Sólo se debe mostrar una ventana de diálogo a la vez.</p></item>
<item><p>Cuando abra un diálogo, proporcione el foco inicial del teclado al componente con el que espera que los usuarios operen primero. Este foco es especialmente importante para usuarios que deben usar un teclado para navegar por su aplicación.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia de la API</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkAboutDialog.html">GtkAboutDialog</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkDialog.html">GtkDialog</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMessageDialog.html">GtkMessageDialog</link></p></item>
</list>
</section>

</page>
