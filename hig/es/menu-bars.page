<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="menu-bars" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/menu-bar.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Barras de menú</title>

<p>Una barra de menú incorpora una tira de menús desplegables. Típicamente está ubicada en la parte superior de una ventana primaria, bajo una barra de título de la ventana.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/menu-bar.svg"/>

<section id="when-to-use">
<title>Cuándo usarlas</title>

<p>Las barras de menú incrementan el impacto vertical de una interfaz de usuario de la aplicación, introducen un gran número de puntos conocidos, y funcionan como un conjunto fijo de opciones inflexibles. Por estas razones, las <link xref="header-bars">barras de cabecera</link> y los <link xref="header- bar-menus">menús de barras de cabecera</link> generalmente se colocan en lugar de barras de menú, junto con otros patrones de diseño para mostrar controles bajo demanda, tales como <link xref="selection-mode">modo de selección</link>, <link xref="action-bars">barras de acción</link>, y <link xref="popovers">ventanas emergentes</link>.</p>

<p>Al mismo tiempo, puede ser apropiado para aplicaciones complejas que ya incluyen una barra de menú conservarla. Adicionalmente, algunas plataformas también pueden incorporar espacio para una barra de menú en su entorno de usuario, y un modelo de menú puede ser deseable para para propósitos de integración multiplataforma.</p>

</section>

<section id="standard-menus">
<title>Menús estándar</title>

<p>Esta sección detalla los menús más comunes y los elementos del menú en una barra de menú. Para obtener más detalles sobre los elementos estándar que incluir en cada uno de estos menús, consulte la sección <link xref="keyboard-input#application-shortcuts">entrada de teclado</link>.</p>

<table>
<tr>
<td><p><gui>Archivo</gui></p></td>
<td><p>Comandos que operan en conjunto en el documento actual o elemento actual. Esto es el elemento más a la izquierda en la barra de menú debido a su importancia y frecuencia de uso, y porque es un menú relevante en muchas aplicaciones.</p>
<p>Si su aplicación no trabaja con archivos, nombre este elemento de acuerdo al tipo de objeto que muestre. Por ejemplo, mun reproductor de música podría tener un menú <gui>Música</gui> en lugar de <gui>Archivo</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Editar</gui></p></td>
<td><p>El menú <guimenu>Editar</guimenu> contiene elementos relacionados con la edición del documento tales como manejo del portapapeles, buscar y remplazar, e insertar objetos.</p></td>
</tr>
<tr>
<td><p><gui>Ver</gui></p></td>
<td><p>Incluye elementos que afectan la vista del usuario, como el del documento o página actual, o cómo se presentan los elementos para navegación. No coloque ningún elemento en el menú <gui>Ver</gui> que afecte elementos de contenido.</p></td>
</tr>
<tr>
<td><p><gui>Insertar</gui></p></td>
<td><p>Lista los tipos de objetos que se pueden insertar en el documento actual, tales como imágenes, enlaces o saltos de página. Proporcione este menú sólo si se tienen aproximadamente seis o más tipos de objetos que se puedan insertar; si no es así, ponga elementos individuales para cada tipo en el menú <gui>Editar</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Formato</gui></p></td>
<td><p>Incluye comandos para cambiar la apariencia visual del documento. Por ejemplo, cambiar la tipografía, el color o el interlineado en una selección de texto.</p>
<p>La diferencia entre estos comandos y aquellos los del menú <gui>Ver</gui> es que los cambios hechos con comandos de <gui>Formato</gui> son persistentes y se guardan como parte del documento.</p>
<p>Los elementos del menú Formato son muy específicos de la aplicación.</p></td>
</tr>
<tr>
<td><p><gui>Marcadores</gui></p></td>
<td><p>Proporcione un menú de Marcadores en cualquier aplicación que permita al usuario buscar archivos y carpetas, documentos de ayuda, páginas web o cualquier otro espacio con gran cantidad de información.</p></td>
</tr>
<tr>
<td><p><gui>Ir</gui></p></td>
<td><p>Un menú <gui>Ir</gui> proporciona comandos para navegar rápidamente por un documento o colección de documentos, o un espacio de información como una estructura de carpetas o la web.</p>
<p>Los contenidos del menú pueden variar dependiendo del tipo de aplicación.</p></td>
</tr>
<tr>
<td><p><gui>Ventanas</gui></p></td>
<td><p>Comandos que aplican a todas las ventanas abiertas de la aplicación. También puede etiquetar este menú como <gui>Documentos</gui>, <gui>Búferes</gui> o similar, de acuerdo con el tipo de documento manejado por la aplicación.</p>
<p>Los últimos elementos de este menú son una lista numerada de las ventanas primarias de la aplicación, por ejemplo <gui>1shoppinglist.abw</gui>. Seleccionar uno de estos elementos muestra la correspondiente ventana.</p></td>
</tr>
</table>

</section>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>Normalmente, la barra de menú está visible todo el tiempo y es siempre accesible desde el teclado, por lo que debe hacer que todos los comandos disponibles en su aplicación estén disponibles desde la barra de menú. (Esta guía es única para barras de menú; otros menús no deberían buscar reproducir la funcionalidad que se hace disponible con otros controles)</p></item>
<item><p>Considere los <link xref="application-menus">menús de la aplicación</link> como parte de la barra de menú; no es necesario reproducir los elementos del menú de la aplicación en otros menús.</p></item>
<item><p>No desactive títulos de menús. Permítale al usuario explorarlos, incluso si no hay elementos disponibles en ese momento.</p></item>
<item><p>Los títulos de menú en una barra de menú son una palabra con su primera letra en mayúsculas. No use espacios en un título de menú, esto hace fácil confundirlo con dos títulos separados. No use palabras compuestas (como <gui>OpcionesDeVentana</gui>) o guiones (como <guimenu>Opciones-De-Ventana</guimenu>) para esquivar esta guía.</p></item>
<item><p>No proporcione un mecanismo para esquivar la barra de menú, podría activarse accidentalmente. Algunos usuarios no podrán descubrir como recuperar la barra de menú en este caso.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia de la API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenu.html">GtkMenu</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuBar.html">GtkMenuBar</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuItem.html">GtkMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkRadioMenuItem.html">GtkRadioMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkCheckMenuItem.html">GtkCheckMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSeparatorMenuItem.html">GtkSeparatorMenuItem</link></p></item>
</list>
</section>

</page>
