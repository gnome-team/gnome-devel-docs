<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="toolbars" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/toolbar.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Barras de herramientas</title>
<p>Una barra de herramientas es un tira de controles que permite el acceso conveniente a funciones usadas comúnmente. La mayoría de las barras de herramientas solo contienen botones gráficos, pero en aplicaciones más complejas, otros tipos de controles, tales como listas desplegables, también pueden ser útiles.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/toolbar.svg"/>

<section id="when-to-use">
<title>Cuándo usarlas</title>

<p>Utilice una barra de herramientas para proporcionar acceso a acciones, herramientas u opciones comunes en ventanas principales. Las <link xref="header-bars">barras de encabezado</link> también realizan este rol, y una barra de herramientas generalmente no es necesaria si está usando barras de encabezado.</p>

<p>Aunque las barras de herramientas son un enfoque común, hay casos donde no son los más eficaces. Las interfaces que se enfocan en manipulación directa, o aquellas que hacen uso intensivo de revelación progresiva, pueden ofrecer una mejor alternativa. Cada uno de estos enfoques requieren mas tiempo y esfuerzo en la fase de diseño, y solo deben llevarse acabo por aquellos que confian en alcanzar las soluciones de diseño más originales.</p>

</section>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>Incluya controles solo para las funciones más importantes. Tener demasiados controles en la barra de herramientas reduce su eficiencia, haciéndolos más difícil de encontrar, y demasiadas filas en la barra de herramientas reduce la cantidad de espacio disponible en la pantalla para el resto de la aplicación.</p></item>
<item><p>Utilice convenios en las barras de herramientas para incrementar la familiaridad. Por ejemplo, la barra de herramientas principal en un aplicación de ofimática casi siempre tendrá Nuevo, Abrir y Guardar como sus primeros tres botones en la barra de herramientas. Similarmente, los primeros botones en un navegador deberán ser Atrás y Adelante.</p></item>
<item><p>Coloque solo las funciones de la aplicación mas utilizadas en sus barras de herramientas. No solo agregue botones por cada elemento de menú.</p></item>
<item><p>Si esta usando una <link xref="menu-bars">barra de menú</link>, asegúrese que éste incluya todas las funciones que aparecen en su barra de herramientas, ya sea directamente (es decir, un elemento de menú equivalente) o indirectamente(por ejemplo, en el dialogo de <guiseq><gui>Opciones</gui> de <gui>Configuración</gui></guiseq>.</p></item>
<item><p>Las barras de herramientas no deben incluir los botones de <gui>Ayuda</gui>, <gui>Cerrar</gui> o <gui>Salir</gui>, ya que estos rara vez se usan y el espacio se utiliza mejor para controles más útiles. De igual manera, ponga solo los botones de <gui>Deshacer</gui>, <gui>Rehacer</gui> y las funciones estándar del portapapeles, si hay espacio en la barra de herramientas para hacerlo sin sacrificar lo mas útil, controles específicos de la aplicación.</p></item>
<item><p>Los <link xref="buttons">botones</link> de la barra de herramientas deben tener un relieve, y los iconos de los botones deben usar <link xref="icons-and-artwork#color-vs-symbolic">iconos simbólicos</link>.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia de la API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkToolbar.html">GtkToolbar</link></p></item>
</list>

</section>

</page>
