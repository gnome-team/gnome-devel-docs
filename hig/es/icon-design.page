<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="icon-design" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Seleccionar y crear iconos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Crear iconos</title>

<p>Esta página proporciona pautas para aquellos que crean sus propios iconos, incluidos los que son a todo color (típicamente utilizados para iconos de aplicaciones) e iconos simbólicos (típicamente utilizados para controles de interfaz de usuario). Para obtener una descripción más general de los estilos y el uso de los iconos, incluidos los disponibles, consulte la sección <link xref="icons-and-artwork">iconos y arte</link>.</p>

<section id="full-color-icons">
<title>Iconos a todo color</title>

<media type="image" mime="image/png" src="figures/icons/shading.svg"/>

<p>El estilo de icono a todo color se usa más comúnmente para iconos de aplicaciones. El tamaño nominal de los iconos a todo color es de 128×128px. Sin embargo, debido a que los íconos de las aplicaciones a veces se presentan en resoluciones más bajas, solo deben presentar detalles que sean presentables a una resolución de 64×64px: cualquier cosa más detallada se perdería al filtrar / reducir.</p>

<p>La <link href="https://gitlab.gnome.org/Community/Design/HIG-app-icons/blob/master/template.svg">plantilla de icono a todo color</link> incluye una cuadrícula de 2 píxeles que debería ayudarle a evitar más detalles que sean más finos que el umbral deseado.</p>

<media type="image" mime="image/png" src="figures/icons/sizes.svg"/>

<section id="perspective">
<title>Perspectiva y forma</title>

<p>Los iconos a todo color deben representarse con una vista ortogonal simple y sin perspectiva real o isométrica. Para proporcionar profundidad, se puede aplicar un efecto elevado para imitar el eje Z. Mantenga el efecto sutil. No se recomienda elevar el objeto más de 2 unidades de detalle (4 píxeles nominales).</p>

<p>Para facilitar el reconocimiento, cada icono de la aplicación debe tener una silueta única. Sin embargo, para garantizar el equilibrio visual con otros iconos de aplicaciones, la relación de aspecto no debe ser extrema. Deben evitarse las formas muy estrechas o muy anchas.</p>

<media type="image" mime="image/png" src="figures/icons/aspect-ratio.svg"/>

<p>Una <link href="https://gitlab.gnome.org/Community/Design/HIG-app-icons/raw/master/template.svg">plantilla de cuadrícula</link> está disponible para ayudarle a colocar el contorno del icono. No intente cubrir un área máxima del lienzo: el margen exterior debe dejarse vacío. En algunas circunstancias, se puede extender un pequeño detalle en este espacio de margen.</p>

<media type="image" mime="image/png" src="figures/icons/whitespace.svg"/>

</section>

<section id="shadows">
<title>Sombras</title>

<p>Las sombras se pueden dibujar internamente, dentro de un icono a todo color, con la fuente de luz apuntando directamente desde arriba. Sin embargo, las sombras no deben dibujarse fuera de la silueta principal del icono, ya que se generan mediante programación según el contexto. Cuando los iconos de aplicaciones se presentan sobre un fondo blanco, por ejemplo, se muestra una fuerte sombra para ayudar a definir los contornos.</p>

<media type="image" mime="image/png" src="figures/icons/dropshadow.png"/>

<p>Las versiones de los iconos de la aplicación se pueden generar mediante código para desarrollo o construcciones nocturnas.</p>
</section>

<section id="palette">
<title>Paleta</title>

<p>A continuación está la paleta de colores de iconos de aplicaciones básica de GNOME.</p>

<!-- will chop up into named sections once the colors stabilize -->


<table>
<tr>
<td><p>Azul</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/blue.png"/></td>
</tr>
<tr>
<td><p>Verde</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/green.png"/></td>
</tr>
<tr>
<td><p>Amarillo</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/yellow.png"/></td>
</tr>
<tr>
<td><p>Naranja</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/orange.png"/></td>
</tr>
<tr>
<td><p>Rojo</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/red.png"/></td>
</tr>
<tr>
<td><p>Morado</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/purple.png"/></td>
</tr>
<tr>
<td><p>Marrón</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/brown.png"/></td>
</tr>
<tr>
<td><p>Neutral</p></td>
<td><media type="image" mime="image/svg" src="figures/icons/palette/neutral.png"/></td>
</tr>
</table>


<p>Es libre de usar diferentes sombras de estos colores, dependiendo del efecto de material que quiera. Sin embargo, estos colores primarios son una buena base para empezar. Tiene una paleta de GIMP/Inkscape <link href="https://gitlab.gnome.org/Teams/Design/HIG-app-icons/raw/master/GNOME%20HIG.gpl?inline=false">disponible</link>.</p>

<p>Se recomienda mantener las superficies planas sin sombra, pero se permite el uso de degradados para resaltar superficies dobladas.</p>

</section>
</section>


<section id="custom-symbolic-icons">
<title>Iconos simbólicos</title>

<p>Los iconos simbólicos tienen una forma sencilla y están dibujados en una rejilla de 16×16 píxeles. Se escalan y colorean automáticamente en la propia interfaz de usuario.</p>

<list>
<item><p>Identifique una propiedad simple cuando busque una metáfora apropiada para un icono, y céntrese en la idea que distingue lo que se quiere comunicar. Por ejemplo, cuando se describe una acción que realizar en una imagen, no es necesario repetir la idea de una imagen en cada icono. En su lugar, céntrese en lo que es distinto sobre cada acción (por ejemplo: rotar, etiquetar, alinear).</p></item>
<item><p>Evite usar cualquier perspectiva en los iconos simbólicos, limítese a una vista simple ortogonal.</p></item>
<item><p>Cuando use trazos sin relleno para un contorno, intente evitar las líneas de 1px y haga al menos un trazo de 2px para la característica principal del icono.</p></item>
<item><p>Los iconos simbólicos se colorean en tiempo de ejecución para que coincidan con el contexto, como si fueran fragmentos de texto. Aunque hay maneras de «sombrear» partes de un icono usando opacidad o creando un patrón difuminado o de dos tonos, intente evitarlo siempre que sea posible.</p></item>
<item><p>Cuando una metáfora recae en el espacio negativo, asegúrese de que funcionará con los colores invertidos. Por ejemplo, el resaltado de la lente de una cámara sólo funcionará si es más clara que la lente en sí.</p></item>
</list>

<media type="image" mime="image/svg" src="figures/icons/inverting.svg"/>

<section id="size-and-grid">
<title>Tamaño y cuadrícula</title>

<p>Aunque los iconos son escalables y deben funcionar en cualquier tamaño, el tamaño básico del lienzo es de 16x16 unidades. Puede rellenar todo el lienzo, pero tenga en cuenta que un objeto rectangular completo aparecerá más grande cuando se coloque junto a otros elementos que usen trazos más finos. Intente mantener el contraste del conjunto completo.</p>

</section>

</section>


<!-- graphic assets TODO
<section id="stock-icons">
<title>GNOME Icon Pack</title>

<p>To avoid starting from scratch, we prepared a selection of commonly used icons you can include in your application, or create derivatives of. The set is licensed CC0, so they can be used regardless of your project's license.</p>

</section>
-->

</page>
