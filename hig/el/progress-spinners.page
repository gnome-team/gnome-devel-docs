<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="progress-spinners" xml:lang="el">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2014-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Στρόβιλοι προόδου</title>

<p>Οι στρόβιλοι προόδου είναι συνηθισμένα στοιχεία διεπαφής χρήστη για την υπόδειξη της προόδου σε μια εργασία. Αντίθετα με τις γραμμές προόδου, δείχνουν μόνο ότι η πρόοδος λαμβάνει χώρα και δεν εμφανίζει το τμήμα της εργασίας που έχει ολοκληρωθεί.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

<section id="when-to-use">
<title>Πότε χρησιμοποιούνται</title>

<p>Μια ένδειξη προόδου απαιτείται γενικά όποτε μια λειτουργία παίρνει περισσότερο από περίπου τρία δευτερόλεπτα και είναι αναγκαίο να δείχνει ότι μια λειτουργία λαμβάνει χώρα πραγματικά. Αλλιώς, ο χρήστης μπορεί να μείνει με την αμφιβολία αν έχει συμβεί κάποιο λάθος.</p>

<p>Ταυτόχρονα, οι ενδείξεις προόδου είναι μια δυνητική πηγή αφαίρεσης, ειδικά όταν εμφανίζονται για σύντομες περιόδους χρόνου. Αν μια λειτουργία παίρνει λιγότερο από 3 δευτερόλεπτα, είναι καλύτερα να αποφεύγετε η χρήση ενός στροβίλου προόδου, αφού τα κινούμενα στοιχεία που εμφανίζονται για πολύ λίγο χρονικό διάστημα μπορούν να αποσπάσουν τον χρήστη από τη γενική εμπειρία.</p>

<p>Οι στρόβιλοι προόδου δεν εμφανίζουν γραφικά τον βαθμό προόδου μιας εργασίας και συχνά ταιριάζουν καλύτερα για πιο σύντομες λειτουργίες. Αν η εργασία είναι πιθανό να πάρει περισσότερο από ένα λεπτό, μια <link xref="progress-bars">γραμμή προόδου</link> μπορεί να είναι μια καλύτερη επιλογή.</p>

<p>Το σχήμα των στροβίλων προόδου επηρεάζει επίσης την καταλληλότητα τους για διαφορετικές καταστάσεις. Επειδή είναι αποτελεσματικά σε μικρά μεγέθη, οι στρόβιλοι προόδου μπορούν να ενσωματωθούν εύκολα σε μικρά στοιχεία διεπαφής χρήστη, όπως καταλόγους ή γραμμές κεφαλίδας. Παρομοίως, το σχήμα τους σημαίνει ότι είναι αποτελεσματικά όταν ενσωματώνονται μέσα σε τετράγωνους ή ορθογώνιους περιέκτες. Αφ' ετέρου, αν ο κάθετος χώρος είναι περιορισμένος, μια γραμμή προόδου μπορεί να είναι μια καλύτερη ρύθμιση.</p>

</section>

<section id="general-guidelines">
<title>Γενικές οδηγίες</title>

<list>
<item><p>Αν μια λειτουργία μπορεί να μεταβάλλεται χρονικά, χρησιμοποιήστε ένα όριο χρόνου για να εμφανίσετε μόνο ένας στρόβιλος προόδου αφού έχουν περάσει τρία δευτερόλεπτα. Μια ένδειξη προόδου δεν χρειάζεται για μικρότερους χρόνους.</p></item>
<item><p>Τοποθετήστε στροβίλους προόδου κοντά σε ή μέσα σε στοιχεία διεπαφής χρήστη με τα οποίο συσχετίζεται. Αν ένα κουμπί προκαλεί μια χρονοβόρα λειτουργία, ο στρόβιλος προόδου μπορεί να τοποθετηθεί δίπλα σε αυτό το κουμπί, παραδείγματος χάρη. Όταν φορτώνεται περιεχόμενο, ο στρόβιλος προόδου πρέπει να τοποθετηθεί μέσα στην περιοχή όπου το περιεχόμενο θα εμφανιστεί.</p></item>
<item><p>Generally, only one progress spinner should be displayed at once. Avoid showing a large number of spinners simultaneously — this will often be visually overwhelming.</p></item>
<item><p>Μια ετικέτα μπορεί να εμφανιστεί δίπλα σε έναν στρόβιλο αν βοηθά την διευκρίνηση της εργασίας με την οποία συσχετίζεται ο στρόβιλος.</p></item>
<item><p>If a spinner is displayed for a long time, a label can indicate both the identity of the task and progress through it. This can take the form of a percentage, an indication of the time remaining, or progress through sub-components of the task (e.g. modules downloaded, or pages exported).</p></item>
</list>

</section>

<section id="api-reference">
<title>Αναφορά API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSpinner.html">GtkSpinner</link></p></item>
</list>
</section>

</page>
