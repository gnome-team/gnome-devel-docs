<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="versioning" xml:lang="sv">

  <info>
    <link type="guide" xref="index#maintainer-guidelines"/>

    <credit type="author copyright">
      <name>Philip Withnall</name>
      <email its:translate="no">philip.withnall@collabora.co.uk</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Versionering och utgåvor av bibliotek och program</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  </info>

  <title>Versionering</title>

  <synopsis>
    <title>Sammanfattning</title>

    <p>Versionering för moduler skiljer sig åt för bibliotek och program: bibliotek behöver en libtool-version angiven utöver sin paketversion. Program behöver bara ha en paketversion.</p>

    <list>
      <item><p>
        Libraries and applications have a package version of the form
        <em>major.minor.micro</em>. (<link xref="#package-versioning"/>)
      </p></item>
      <item><p>
        Libraries additionally have a libtool version of the form
        <em>current:revision:age</em>. (<link xref="#libtool-versioning"/>)
      </p></item>
      <item><p>
        Version numbers should be updated for each release (using release and
        post-release increments). (<link xref="#release-process"/>)
      </p></item>
      <item><p>Paketversioner bör öka vid funktionsändringar eller tillägg. (<link xref="#package-versioning"/>)</p></item>
      <item><p>Libtool-versioner bör uppdateras vid ändringar eller tillägg till API:t. (<link xref="#libtool-versioning"/>)</p></item>
      <item><p>
        Even/odd <em>minor</em> package versions can be used respectively for
        stable/unstable releases. (<link xref="#stable-unstable-versions"/>)
      </p></item>
    </list>
  </synopsis>

  <section id="package-versioning">
    <title>Paketversionering</title>

    <p>
      Both libraries and applications have a package version of the form
      <em>major.minor.micro</em>.
    </p>

    <p>
      The package version number is that passed to
      <code>AC_INIT()</code>, and the one which is typically known as the
      project’s version number. For example, the Debian package for a library
      will use the library’s package version (though may also include the major
      version number in the package name to allow for
      <link xref="parallel-installability">parallel installability</link>).
      Package versions are updated by the following rules:
    </p>
    <steps>
      <item><p>
        If breaking <link xref="api-stability">API compatibility</link> in a
        library, or making a large change to an application which affects
        everything (such as a UI redesign), increment major and set minor and
        micro to 0.
      </p></item>
      <item><p>
        Otherwise, if changing or adding a feature, or adding any API,
        increment minor and set micro to 0.
      </p></item>
      <item><p>
        Otherwise (if making a release containing only bug fixes and
        translation updates), increment micro.
      </p></item>
    </steps>

    <p>
      Note that the minor version number should be updated if any API is added.
    </p>
  </section>

  <section id="libtool-versioning">
    <title>Libtool-versionering</title>

    <p>Bibliotek har två versionsnummer: en libtool-version som spårar ABI-bakåtkompatibilitet (se <link xref="api-stability"/>), och en paketversion som spårar funktionsändringar. Dessa ökas vanligen synkroniserat, men bör hållas åtskilda eftersom ABI-bakåtkompatibilitet inte nödvändigtvis är relaterad till funktionsändringar och felkorrigeringar. Vidare har de två versionsnumren olika semantisk mening, och kan inte automatiskt genereras från varandra.</p>

    <p>En bra överblick för libtool-versionering, och skillnaderna mot paketversionering ges i <link href="https://autotools.io/libtool/version.html">Autotools Mythbuster</link>; en annan finns i <link href="http://www.gnu.org/s/libtool/manual/html_node/Updating-version-info.html">libtool-handboken</link>.</p>

    <p>För att uppdatera libtool-versionen, följ algoritmen given i kommentarerna nedan. Detta är en typisk <file>configure.ac</file>-kodsnutt för att konfigurera libtool-versionering:</p>

    <code>
# Before making a release, the LT_VERSION string should be modified. The
# string is of the form c:r:a. Follow these instructions sequentially:
#   1. If the library source code has changed at all since the last update, then
#      increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#   2. If any interfaces have been added, removed, or changed since the last
#      update, increment current, and set revision to 0.
#   3. If any interfaces have been added since the last public release, then
#      increment age.
#   4. If any interfaces have been removed or changed since the last public
#      release, then set age to 0.
AC_SUBST([LT_VERSION],[0:0:0])</code>

    <p>Följande kodsnutt kan användas i <file>Makefile.am</file> för att skicka versionsinformationen till libtool:</p>
    <code>my_library_la_LDFLAGS = -version-info $(LT_VERSION)</code>
  </section>

  <section id="stable-unstable-versions">
    <title>Stabila och instabila paketversioner</title>

    <p>
      Most GNOME modules follow a convention for stable and unstable releases.
      The minor version is even for stable releases and is odd for unstable
      releases.  For example, the 3.20.* versions are stable, but the 3.19.*
      versions are unstable.  The 3.19.* versions can be seen as alpha and beta
      releases of the 3.20 version.
    </p>

    <p>
      A new micro <em>stable</em> version (e.g. 3.20.0 → 3.20.1) doesn’t add
      new features, only translation updates and bug fixes.  On the other hand,
      <em>unstable</em> micro releases (e.g. 3.19.1 → 3.19.2) can add API, or
      change or remove API which was added in a previous micro release in that
      minor series.
    </p>

    <p>libtool-versionen bör endast uppdateras för stabila paketversioner.</p>
  </section>

  <section id="release-process">
    <title>Utgåveprocess</title>

    <p>
      The standard process for making a release of a module increments the
      libtool version (if the module is a library) at the time of release, then
      increments the package version number immediately afterwards (this is
      called a post-release increment).
    </p>

    <p>
      Updating the libtool versions at the time of release means that they are
      only incremented once for all ABI changes in a release. The use of
      post-release increment for package versions means the package version
      number is not outdated (still equal to the previous release) during
      the development cycle.
    </p>

    <p>Utgåveprocessen (baserad på <link href="https://wiki.gnome.org/MaintainersCorner/Releasing">GNOME:s utgåveprocess</link>):</p>
    <steps>
      <item><p>Säkerställ att din kod är à jour: <cmd>git pull</cmd></p></item>

      <item><p>Säkerställ att du inte har några lokala ändringar: <cmd>git status</cmd></p></item>
      <item><p>Om utgåvan är för en stabil paketversion, öka libtool-versionsnumret i <file>configure.ac</file> (om den existerar)</p></item>
      <item><p>Lägg till en post i <file>NEWS</file>-filen</p></item>
      <item>
        <p>Kör <cmd>./autogen.sh &amp;&amp; make &amp;&amp; make install &amp;&amp; make distcheck</cmd> och säkerställ att det lyckas</p>
        <list>
          <item><p>Fixa alla problem som dyker upp, checka in ändringarna, och börja om på steg 3</p></item>
        </list>
      </item>
      <item><p>Om <cmd>make distcheck</cmd> avslutar med ”[arkiv] is ready for distribution”, kör <cmd>git commit -a -m "Release version x.y.z"</cmd> (där ”x.y.z” är paketets versionsnummer)</p></item>
      <item>
        <p>Kör <cmd>git push</cmd></p>
        <list>
          <item><p>
            If that fails due to other commits having been pushed in the
            meantime, run <cmd>git pull</cmd> to merge your commit on the
            branch followed by a second <cmd>git push</cmd>.  This is an
            exception to the GNOME guideline to have a linear Git history
            (<link xref="version-control#use-of-git"/>).  If you prefer to have
            a linear history, you need to restart at step 1.
          </p></item>
        </list>
      </item>
      <item><p>Tagga utgåvan: <cmd>git tag -s x.y.z</cmd> (där ”x.y.z” är paketets versionsnummer)</p></item>
      <item><p>Kör <cmd>git push origin x.y.z</cmd> (där ”x.y.z” är paketets versionsnummer)</p></item>
    </steps>

    <p>Utgåvan är nu klar, och versionsökningen efter utgåvans släppande kan göras:</p>
    <steps>
      <item><p>Öka paketets versionsnummer i <file>configure.ac</file></p></item>
      <item><p>Kör <cmd>git commit -a -m "Post-release version increment"</cmd></p></item>
      <item><p>Kör <cmd>git push</cmd></p></item>
    </steps>

    <p>Paketarkivet som genererats av <cmd>make distcheck</cmd> kan nu skickas till download.gnome.org eller distribueras på andra sätt.</p>
  </section>
</page>
