<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="version-control" xml:lang="sv">

  <info>
    <link type="guide" xref="index#general-guidelines"/>

    <credit type="author copyright">
      <name>Philip Withnall</name>
      <email its:translate="no">philip.withnall@collabora.co.uk</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Versionshantering av källkod med Git</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  </info>

  <title>Versionshantering</title>

  <synopsis>
    <title>Sammanfattning</title>

    <p>git används för versionshantering för alla GNOME-projekt. Denna sida förutsätter god kunskap om git-användning, visst inledande material finns tillgängligt <link href="https://www.atlassian.com/git/tutorials/">här</link>, och en <link href="https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf">fusklapp för git finns här</link>.</p>

    <list>
      <item><p>Gör odelbara, återställningsbara incheckningar. (<link xref="#guidelines-for-making-commits"/>)</p></item>
      <item><p>Inkludera ett fullständigt resonemang i incheckningsmeddelanden, samt länkar till relevanta felrapporter eller specifikationer. (<link xref="#guidelines-for-making-commits"/>)</p></item>
      <item><p>Håll stora ändringar, så som namnändringar, i separata incheckningar. (<link xref="#guidelines-for-making-commits"/>)</p></item>
      <item><p>
        Merge changes from feature branches by rebasing.
        (<link xref="#use-of-git"/>)
      </p></item>
    </list>
  </synopsis>

  <section id="use-of-git">
    <title>Användning av Git</title>

    <p>De flesta GNOME-arkiven följer dessa regler:</p>
    <list>
      <item><p>
        No forced pushes. Except for branches with the <code>wip/</code> prefix
        (work-in-progress), the commits’ history must not be modified, as
        contributors rely on it.
      </p></item>
      <item><p>
        Rebase commits rather than merging, to have a linear history (which is
        easier to follow).
      </p></item>
      <item><p>
        Work on feature branches on GNOME git in <code>wip/</code> branches,
        then rebase on master and fast-forward merge the changes. It is a good
        practice to also add your nickname to the branch name, as
        <code>wip/nickname/feature</code>.
      </p></item>
      <item><p>
        Hide <link href="https://sethrobertson.github.io/GitBestPractices/#sausage">sausage
        making</link> by squashing commits before merging.
      </p></item>
    </list>
  </section>

  <section id="guidelines-for-making-commits">
    <title>Riktlinjer för att göra incheckningar</title>

    <p>
      Commits should be as small as possible, but no smaller. Each commit should
      address a single issue, containing only changes related to that issue. The
      message for each commit should describe the issue, explain what causes it,
      and explain how it has been fixed if it is not obvious. If the commit is
      associated with a bug report, the full URI for the bug report should be
      put on a line by itself at the bottom of the commit message. Similarly,
      the ID for the git commit (from <cmd>git log --oneline</cmd>) should
      be copied into the bug report once the commit has been pushed, so it is
      easy to find one from the other.
    </p>

    <p>
      The changes in each commit should be easy to read. For example, they
      should not unnecessarily change whitespace or indentation. Large,
      mechanical changes, such as renaming a file or function, should be put in
      separate commits from modifications to code inside that file or function,
      so that the latter changes do not get buried and lost in the former.
    </p>

    <p>
      The following principles give the reasoning for all the advice above:
    </p>
    <list>
      <item><p>
        Each commit should take the repository from one working state to
        another, otherwise
        <link href="http://git-scm.com/book/en/v2/Git-Tools-Debugging-with-Git#Binary-Search">bisection</link>
        is impossible.
      </p></item>
      <item><p>
        Each commit should be individually revertable. If it later turns out
        that the commit was a bad idea,
        <cmd>git revert <var>commit ID</var></cmd> should take the repository
        from a working state to another working state.
      </p></item>
      <item><p>
        The reasoning for each commit, and its relationship to external
        resources like specifications and bug reports, should be clear, to the
        extent that commits written by one developer a year in the past should
        still be understandable by a second developer without having to trace
        through the changes and work out what they do.
      </p></item>
      <item><p>Varje incheckning bör skrivas en gång, och vara designad för att läsas många gånger, av många granskare och framtida programmerare.</p></item>
    </list>
  </section>

  <section id="merging-procedure">
    <title>Merging Procedure</title>

    <p>
      To merge a feature branch named <code>my-branch</code> into master, use
      the following commands:
    </p>
    <code mime="application/x-shellscript">
git checkout master
git pull

git checkout wip/<var>min-gren</var>
git rebase --interactive master
# Säkerställ att ombasering lyckades; testa ändringarna

git checkout master
git merge wip/<var>min-gren</var>
git push

# wip/<var>min-gren</var> kan nu tas bort
git push origin :wip/<var>min-gren</var>
git branch -D wip/<var>min-gren</var></code>
  </section>

  <section id="external-links">
    <title>Externa länkar</title>

    <list>
      <item><p><link href="https://sethrobertson.github.io/GitBestPractices/">Bästa tillvägagångssätt för Git</link></p></item>
      <item><p><link href="https://help.github.com/categories/using-git/">Frågor och svar om Git</link></p></item>
      <item><p><link href="https://www.atlassian.com/git/tutorials/">Atlassians git-handledning</link></p></item>
      <item><p><link href="http://git-scm.com/docs/gittutorial">Officiell git-handledning</link></p></item>
      <item><p><link href="https://try.github.io/">Interaktiv git-handledning</link></p></item>
      <item><p><link href="http://www.git-tower.com/learn/">Handledning från git-tower</link></p></item>
    </list>
  </section>
</page>
