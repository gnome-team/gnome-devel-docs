<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2003/XInclude" type="topic" id="databases" xml:lang="cs">

  <info>
    <link type="guide" xref="index#specific-how-tos"/>

    <credit type="author copyright">
      <name>Philip Withnall</name>
      <email its:translate="no">philip.withnall@collabora.co.uk</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Úložiště pro jednoduché trvalé objekty</desc>
  </info>

  <title>Databáze</title>

  <synopsis>
    <title>Shrnutí</title>

    <list>
      <item><p>Databáze používejte ve vhodných případech, tzn. ne pro nastavení (k tomu slouží GSettings). (<link xref="#when-to-use-databases"/>)</p></item>
      <item><p>Volte mezi GOM a GVDB podle toho, jestli je zapotřebí indexování. (<link xref="#when-to-use-databases"/>)</p></item>
      <item><p>Popřemýšlejte o zásadách úklidu databáze, než zařadíte používání GOM. (<link xref="#when-to-use-databases"/>)</p></item>
      <item><p>Zranitelnosti v podobě injektáže SQL se vyhněte pomocí předpřipravených dotazů. (<link xref="#sql-injection"/>)</p></item>
    </list>
  </synopsis>

  <section id="when-to-use-databases">
    <title>Kdy používat databáze</title>

    <p>Data nastavení by měla být uložena v <link href="https://developer.gnome.org/gio/stable/GSettings.html">GSettings</link>. Platí, že data, která musí být trvalá a ovlivňují chování aplikace, jsou data nastavení. Pokud se jedná o záležitost, na kterou se vztahují zásady určené správcem systému (jako třeba nastavení proxy nebo zamykání), jde o data nastavení. Jestliže to ale obsahuje uživatelem vytvořený obsah, nejedná se o data nastavení a nemělo by to být uloženo v GSettings.</p>

    <p>Pro situace, kdy jsou data vysoce strukturovaná, je pro uchování rozumným řešením databáze. V GNOME jsou doporučovány dvě hlavní databáze: GOM a GVDB. GOM ve skutečnosti obaluje SQLite a tudíž implementuje indexaci polí a dotazy ve stylu SQL. GVDB je mnohem jednodušší úložiště objektů, které podporuje rychlou serializaci slovníku objektů na disk.</p>

    <p>GOM by s měl použít, když jsou zapotřebí pokročilé funkce, především indexování. V ostatních případech by mělo být použito GVDB.</p>

    <p>Dříve, než se rozhodnete použít GOM (a tím pádem SQLite), musíte zvážit zásady úklidu databáze (vacuum) a jestli vaše použití bude dobře spolupracovat s úklidovým systémem SQLite. Úklid je v podstatě výraz používaný u SQLite (respektive u databází obecně) pro defragmentaci databáze — pokud databáze není řádně defragmentovaná, degraduje to výkon a značně narůstá její velikost. Více informací se dočtete v <link href="http://blogs.gnome.org/jnelson/2015/01/06/sqlite-vacuum-and-auto_vacuum/">tomto článku</link> na dané téma. Popřemýšlejte o něm, než zvolíte použití GOM.</p>

    <p>GNOME má ještě další databázovou knihovnu: GNOME Data Access (GDA). Je zaměřená na abstraktní přístup k různým typům relačních databází, pro použití například v programech pro administrační práci s databázemi nebo v kancelářských programech. Naopak není vhodná pro ukládání <link href="https://developer.gnome.org/gio/stable/GSettings.html">uživatelských nastavení</link>.</p>
  </section>

  <section id="gom">
    <title>Používání GOM</title>

    <p>Výuka použití GOM přesahuje rámec tohoto dokumentu, ale <link href="https://developer.gnome.org/gom/">je k dispozici referenční příručka</link>.</p>

    <section id="sql-injection">
      <title>Injektáž SQL</title>

      <p>GOM neumožňuje přístup k nízkoúrovňovému dotazovacímu API databáze SQLite. Když jej používáte, <em style="strong">musíte</em> místo přímého sestavení řetězce s SQL, a jeho následného předání ke zpracování, dotazy sestavovat pomocí <link href="https://www.sqlite.org/c3ref/stmt.html">předpřipravených výrazů</link> SQLite a API pro <link href="https://www.sqlite.org/c3ref/bind_blob.html">napojení hodnot</link>. Přímé sestavování řetězců zvyšuje pravděpodobnost útoků typu <link href="https://cs.wikipedia.org/wiki/SQL_injection">injektování SQL</link>, pomocí kterých může útočník získat přístup k libovolným uživatelským datům v databázi.</p>
    </section>
  </section>

  <section id="gvdb">
    <title>Používání GVDB</title>

    <p>GVDB má jednoduché API, které zrcadlí běžné hašovací tabulky. V současnosti je GVDB odstupné jen jako knihovna pro postup „kopírovat a vložit“. Stáhněte si nejnovější kopii kódu z <link href="https://gitlab.gnome.org/GNOME/gvdb">gitu GVDB</link> a nakopírujte ji do svého projektu. Je licencována pod LGPLv2.1+.</p>

    <p>Úplná výuka GVDB překračuje rozsah tohoto dokumentu.</p>
  </section>
</page>
