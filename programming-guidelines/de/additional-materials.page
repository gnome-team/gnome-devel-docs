<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="additional-materials" xml:lang="de">

  <info>
    <link type="guide" xref="index#references"/>
    
    <credit type="author copyright">
      <name>Federico Mena-Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Andere Richtlinien und Bücher über die Organisation von Projekten der Freien Software</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2016, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Stefan Melmuk</mal:name>
      <mal:email>stefan.melmuk@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Zusätzliche Materialien</title>

  <p>Hier sind einige Links zu anderen Materialien, die Sie vielleicht lesen wollen. Sie vermitteln Ihnen einen Einblick in die Arbeitsweise weit verstreuter Teams von Entwicklern freier Software sowie über guten Programmierstil im Allgemeinen.</p>

  <list>
    <item>
      <p><link href="http://producingoss.com/">Producing Open Source Software</link> von Karl Fogel. Dieses wirklich exzellente Buch enthält empfehlenswerte Vorgehensweisen für Projekte freier Software. Dabei geht es um die <em>sozialen Aspekte</em> des Projekts: Wie behandelt man Mitwirkende, wie organisiert und moderiert man Kommunikation, wie geht man mit Non-Profit-Organisationen um? Falls Sie sich schon einmal gefragt haben: »Wie muss ich mit $menschliche_situation im Projekt umgehen?«, dann könnte dieses Buch die Antwort parat haben.</p>
    </item>

    <item>
      <p><link href="http://www.gnu.org/prep/standards/">GNU Coding Standards</link>. Diese alte Dokument enthält viele exzellente Ratschläge. Es behandelt C-Programmierstile, Probleme bei Systemen mit Erweiterungen, Namen für übliche Einstellungen in Befehlszeilen-Programmen, Makefile-Konventionen und einige sehr GNU-spezifische Details wie die Verwendung von Texinfo in Dokumentationen.</p>
    </item>

    <item>
      <p><link href="https://www.kernel.org/doc/Documentation/process/coding-style.rst"> Linux Kernel Coding Style</link>. Erklärt die Gründe für »Tiefe Einrückungen«, Klammernplatzierung, prägnante und eindeutige Benennungen sowie das zentralisierte Verlassen von Funktionen.</p>
    </item>
  </list>
</page>
