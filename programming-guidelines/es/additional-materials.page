<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="additional-materials" xml:lang="es">

  <info>
    <link type="guide" xref="index#references"/>
    
    <credit type="author copyright">
      <name>Federico Mena-Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Otros libros y guías de estilo sobre cómo organizar proyectos de software libre</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2016-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Javier Mazorra</mal:name>
      <mal:email>mazi.debian@gmail.com</mal:email>
      <mal:years>2016, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Material adicional</title>

  <p>Aquí le damos enlaces a otros materiales que puede querer leer. Estos le enseñarán mucho sobre cómo trabajar en grandes equipos distribuidos de desarrolladores de software libre, y sobre el buen estilo de programación en general.</p>

  <list>
    <item>
      <p><link href="http://producingoss.com/">Produducing Open Source Software</link>, de Karl Fogel. Éste realmente es un libro excelente de buenas prácticas que deberían seguir los proyectos de software libre. Esto es sobre <em>aspectos sociales</em> del proyecto: cómo tratar a los contribuidores, cómo organizar y moderar la comunicación, cómo tratar con las fundaciones sin ánimo de lucro. Si se pregunta alguna vez, «¿Cómo debería tratar con $human_situation en el proyecto?», este libro puede proporcionarle la respuesta.</p>
    </item>

    <item>
      <p><link href="http://www.gnu.org/prep/standards/">GNU Coding Standards</link>. Éste es un viejo documento, pero aún tiene muchos consejos excelentes. Habla sobre estilo de programación C, problemas al tratar con sistemas enchufables, nombres de opciones comunes para programas de línea de comandos, convenciones para Makefiles y algunos detalles muy GNU-ish como el uso de Texinfo para la documentación.</p>
    </item>

    <item>
      <p><link href="https://www.kernel.org/doc/Documentation/process/coding-style.rst">Linux Kernel Coding Style</link>. Explica la lógica de «la gran sangría», el lugar de las llaves, denominación concisa pero sin ambigüedades, y la salida centralizada de funciones.</p>
    </item>
  </list>
</page>
