<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2003/XInclude" type="topic" id="gerror" xml:lang="ko">

  <info>
    <link type="guide" xref="index#specific-how-tos"/>

    <credit type="author copyright">
      <name>Philip Withnall</name>
      <email its:translate="no">philip.withnall@collabora.co.uk</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>런타임 오류 처리 및 보고</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016-2018.</mal:years>
    </mal:credit>
  </info>

  <title>GError</title>

  <section id="gerror-usage">
    <title>GError 활용법</title>

    <p>
      <link href="https://developer.gnome.org/glib/stable/glib-Error-Reporting.html"><code>GError</code></link>
      is the standard error reporting mechanism for GLib-using code, and can be
      thought of as a C implementation of an
      <link href="https://en.wikipedia.org/wiki/Exception_handling">exception</link>.
    </p>

    <p>실행 시간에 일어나는 어떤 실패 동작(<link xref="preconditions">프로그래머 잘못</link>이 아님)는 함수에서 반환 처리 전에 함수에 <code>GError**</code> 매개변수를 넣고, 실패를 언급할 때 쓸 만한 해당 GError를 설정하여 처리해야합니다. 프로그래머 오류는 GError로 처리하면 안됩니다. 대신, 단언, 상태-전 또는 상태-후 테스트를 활용하십시오.</p>

    <p>GError는 더 많은 정보를 전달해줄 수 있으며 GLib 도구에서 지원하기에 단순 반환 코드를 취향에 따라 사용해야합니다. 예를 들어, <link xref="introspection">API 인트로스펙션</link> 과정에서 모든 GError 매개 변수를 자동으로 찾아서 다른 언어의 예외로 변환할 수 있습니다.</p>

    <p>라이브러리 코드에서는 콘솔에 경고를 출력하면 안됩니다. GError를 사용하십시오. 그러면 코드를 호출할 때 상위 코드로 오류를 전달할 수 있고, 처리 여부를 결정하거나, 콘솔 출력 여부를 결정할 수 있습니다. 이상적으로, 콘솔에 출력하는 코드는 최상위 프로그램 코드에만 있어야 하며, 라이브러리 코드에 있으면 안됩니다.</p>

    <p><code>GError**</code>를 보유할 수 있는 어떤 함수든, 이 형식의 매개변수를 보유하고, 반환한 GError를 적절하게 반처리해야합니다. 가끔은 <code>GError**</code> 매개변수에 <code>NULL</code> 값을 전달하여 잠재 오류를 무시하는 편이 괜찮을 때가 있습니다.</p>

    <p>GLib API 문서에는 <link href="https://developer.gnome.org/glib/stable/glib-Error-Reporting.html#glib-Error-Reporting.description">GError 활용 완벽 지침서</link>가 있습니다.</p>
  </section>
</page>
