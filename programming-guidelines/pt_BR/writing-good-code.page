<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="writing-good-code" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="index#general-guidelines"/>
    
    <credit type="author copyright">
      <name>Federico Mena-Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Miguel de Icaza</name>
      <email its:translate="no">miguel@gnome.org</email>
    </credit>
    <credit type="author copyright">
      <name>Morten Welinder</name>
      <email its:translate="no">mortenw@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Código bom e legível mantém o projeto manutenível</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>A importância de um código bem escrito</title>

  <p>GNOME é um projeto de software livre muito ambicioso e é composto por muitos pacotes de software que são mais ou menos independentes entre si. Muito do trabalho no GNOME é feito por voluntários: apesar de haver muitas pessoas trabalhando no GNOME em tempo integral ou parcial, voluntários ainda fazem uma grande porcentagem de nossas contribuições. Programadores podem ir e vir todo o tempo e eles serão capazes de dedicar diferentes parcelas de tempo para o projeto do GNOME. As responsabilidades do “mundo real” das pessoas podem alterar e isso refletirá na quantidade de tempo que eles devotam ao GNOME.</p>

  <p>Desenvolvimento de software leva grandes quantidades de tempo e esforços penosos. É por isso que a maioria dos voluntários em tempo parcial não conseguem iniciar grandes projetos por si próprio; é muito mais fácil e recompensador contribuir para projetos existentes, pois isso traz resultados que são imediatamente visíveis e usáveis.</p>

  <p>Portanto, nós concluímos que é muito importante para os projetos existentes torná-los o mais simples possível para que pessoas contribuam para eles. Uma forma de fazer isso é se certificar de que programas são fáceis de ler, entender, modificar e manter.</p>

  <p>Um código bagunçado é difícil de ler e as pessoas podem perder o interesse se elas não puderem decifrar o que o código tenta fazer. Ademais, é importante que os programadores sejam capazes de entender o código rapidamente de forma que eles possam começar a contribuir com correções de erro e aprimoramentos em uma pequena quantidade de tempo. Código-fonte é uma forma de <em>comunicação</em> e é mais para pessoas que para computadores. Assim como alguém não gostaria de ler uma novela com erros de escrita, gramática ruim e pontuação desleixada, programadores devem se esforçar para escrever um bom código que seja fácil de ser entendido e modificado por outros.</p>

  <p>A seguir estão algumas qualidades importantes de um bom código:</p>

  <terms>
    <item>
      <title>Limpeza</title>
      <p>Um código limpo é fácil de ler com esforço mínimo. Isso permite que pessoas comecem a entendê-lo facilmente. Isso inclui o estilo de codificação em si (colocação de chaves, recuo, nomes de variáveis) e o fluxo de controle do código.</p>
    </item>

    <item>
      <title>Consistência</title>
      <p>Um código consistente facilita o entendimento de como um programa funciona. Ao ler um código consistente, forma-se subconscientemente um número de presunções e expectativas sobre como o código funciona, então é mais fácil e seguro fazer modificações nele. Um código que <em>se parece</em> o mesmo em dois locais deve <em>funcionar</em> da mesma forma, também.</p>
    </item>

    <item>
      <title>Extensibilidade</title>
      <p>Um código com propósito geral é mais fácil de reusar e modificar do que um código muito específico com diversas presunções codificadas. Quando alguém deseja adicionar um novo recurso a um programa, obviamente será mais fácil fazer isso se o código foi projetado para ser extensível desde o começo. Um código que não foi escrito dessa forma pode levar pessoas a ter que implementar hacks feios e adicionar recursos.</p>
    </item>

    <item>
      <title>Corretismo</title>
      <p>Finalmente, um código que é projetado para ser correto permite que as pessoas gastem menos tempo se preocupando com erros, e mais tempo melhorando os recursos de um programa. Usuários também apreciam um código correto, já que ninguém gosta de software que trava. Código que é escrito prezando pelo corretismo e segurança (i.e. código que explicitamente tenta se certificar de que o programa permaneça em um estado de consistência) evita muitos tipos de erros bobos.</p>
    </item>
  </terms>

  <section id="book-references">
    <title>Referências literárias</title>

    <list>
      <item><p><link href="http://www.cc2e.com">Code Complete</link>, por Steve McConnell.</p></item>
      <item><p><link href="http://martinfowler.com/books/refactoring.html"> Refactoring: Improving the Design of Existing Code </link>, por Martin Fowler.</p></item>
      <item><p>
        <link href="https://en.wikipedia.org/wiki/Design_Patterns">
          Design Patterns: Elements of Reusable Object-Oriented Software
        </link>, by Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides.
      </p></item>
      <item><p><link href="http://astore.amazon.com/gnomestore-20/detail/020163385X"> Object-Oriented Design Heuristics </link>, por Arthur Riel.</p></item>
    </list>
  </section>
</page>
