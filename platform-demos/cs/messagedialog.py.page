<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="messagedialog.py" xml:lang="cs">
  <info>
    <title type="text">MessageDialog (Python)</title>
    <link type="guide" xref="beginner.py#windows"/>
    <link type="next" xref="gmenu.py"/>
    <revision version="0.1" date="2012-06-11" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Okno se zprávou</desc>
  </info>

  <title>MessageDialog</title>
  <media type="image" mime="image/png" src="media/messagedialog.png"/>
  <p>Dialogové okno se zprávou, které vypíše zprávy v závislosti na vašich volbách do terminálu.</p>

  <links type="section"/>

  <section id="code">
  <title>Kód použitý k vygenerování tohoto příkladu</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    # Konstruktor pro okno (rodičovské okno) s popiskem
    def __init__(self, app):
        Gtk.Window.__init__(self, title="GMenu Example", application=app)
        self.set_default_size(400, 200)
        label = Gtk.Label()
        label.set_text("This application goes boom!")
        self.add(label)

        # Vytvoří message_action (Gio.SimpleAction) pro okno
        message_action = Gio.SimpleAction.new("message", None)
        # Napojí signál z akce na funkci message_cb()
        message_action.connect("activate", self.message_cb)
        # Přidá akci do aplikace
        app.add_action(message_action)

    # Funkce zpětného volání po signál "activate" z message_action
    # v nabídce rodičovského okna
    def message_cb(self, action, parameter):
        # Gtk.MessageDialog
        messagedialog = Gtk.MessageDialog(parent=self,
                                          flags=Gtk.DialogFlags.MODAL,
                                          type=Gtk.MessageType.WARNING,
                                          buttons=Gtk.ButtonsType.OK_CANCEL,
                                          message_format="This action will cause the universe to stop existing.")
        # Napojí signál "response" (od kliknutého tlačítka) na funkci
        # dialog_response()
        messagedialog.connect("response", self.dialog_response)
        # Zobrazí MessageDialog
        messagedialog.show()

    def dialog_response(self, widget, response_id):
        # Když je na tlačítko kliknuto, vrátí OK (-5)
        if response_id == Gtk.ResponseType.OK:
            print("*boom*")
        # Když je na tlačítko kliknuto, vrátí CANCEL (-6)
        elif response_id == Gtk.ResponseType.CANCEL:
            print("good choice")
        # Když je MessageDialog zlikvidován (zmáčknutím ESC)
        elif response_id == Gtk.ResponseType.DELETE_EVENT:
            print("dialog closed or cancelled")
        # Nakonec zlikvidujeme MessageDialog
        widget.destroy()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def quit_cb(self, action, parameter):
        self.quit()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Vytvoří nabídku (Gio.Menu)
        menu = Gio.Menu()
        # Přidá položku nabídky s textem "Message" a akcí "app.message"
        menu.append("Message", "app.message")
        # Přidá položku nabídky s textem "Quit" a akcí "app.quit"
        menu.append("Quit", "app.quit")
        # Nastaví nabídku jako nabídku pro aplikaci
        self.set_app_menu(menu)

        # Nová jednoduchá akce pro aplikaci
        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_cb)
        self.add_action(quit_action)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>

  <section id="methods">
  <title>Užitečné metody pro widget MessageDialog</title>
    <p>Na řádku 18 je signál <code>"activate"</code> napojen na funkci zpětného volání <code>message_cb()</code> pomocí <code><var>widget</var>.connect(<var>signál</var>, <var>funkce zpětného volání</var>)</code>. Podrobnější vysvětlení viz <link xref="signals-callbacks.py"/>.</p>
  <list>
    <item><p>V konstruktoru widgetu <code>MessageDialog</code> bychom mohli nastavit příznak <code>Gtk.DialogFlags.DESTROY_WITH_PARENT</code> (pro likvidaci dialogového okna ve chvíli, kdy je zlikvidováno rodičovské okno) nebo <code>Gtk.DialogFlags.MODAL</code> (žádná komunikace uživatele s ostatními okny aplikace).</p></item>
    <item><p>V konstruktoru widgetu <code>MessageDialog</code> bychom mohli nastavit typ na něco z <code>Gtk.MessageType.INFO</code> (informace), <code>Gtk.MessageType.WARNING</code> (varování), <code>Gtk.MessageType.QUESTION</code> (dotaz), <code>Gtk.MessageType.ERROR</code> (chybové hlášení) nebo <code>Gtk.MessageType.OTHER</code> (jiné), podle toho, jaký typ zprávy chceme.</p></item>
    <item><p>V konstruktoru widgetu <code>MessageDialog</code> bychom mohli nastavit tlačítka, jako jsou standardní <code>Gtk.ButtonsType.NONE</code>, <code>Gtk.ButtonsType.OK</code>, <code>Gtk.ButtonsType.CLOSE</code>, <code>Gtk.ButtonsType.CANCEL</code>, <code>Gtk.ButtonsType.YES_NO</code>, <code>Gtk.ButtonsType.OK_CANCEL</code> nebo libovolné tlačítko pomocí <code>add_button()</code>, jako v <code>Gtk.Dialog</code>.</p></item>
    <item><p>Mohli bychom nahradit výchozí obrázek v <code>MessageDialog</code> jiným obrázkem pomocí</p>
    <code mime="text/x-python">
image = Gtk.Image()
image.set_from_stock(Gtk.STOCK_CAPS_LOCK_WARNING, Gtk.IconSize.DIALOG)
image.show()
messagedialog.set_image(image)</code>
    <p>kde <code>Gtk.STOCK_CAPS_LOCK_WARNING</code> je jeden obrázek ze <link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">standardních položek</link>. Mohli bycho také nastavit libovolný obrázek podobně jako ve widgetu Image pomocí <code>image.set_from_file("název_souboru.png")</code>.</p></item>
    <item><p><code>format_secondary_text("nějaká druhá zpráva")</code> nastaví druhou zprávu. Hlavní text bude tučný.</p></item>
  </list>
  </section>

  <section id="references">
  <title>Odkazy k API</title>
  <p>V této ukázce se používá následující:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkMessageDialog.html">GtkMessageDialog</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkDialog.html">GtkDialog</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
    <item><p><link href="http://developer.gnome.org/gio/stable/GSimpleAction.html">GSimpleAction</link></p></item>
    <item><p><link href="http://developer.gnome.org/gio/unstable/GActionMap.html">GActionMap</link></p></item>
    <item><p><link href="http://developer.gnome.org/gio/stable/GMenu.html">GMenu</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkApplication.html">GtkApplication</link></p></item>
  </list>
  </section>
</page>
