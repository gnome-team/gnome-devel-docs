<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="menubutton.js" xml:lang="cs">
  <info>
  <title type="text">MenuButton (JavaScript)</title>
    <link type="guide" xref="beginner.js#buttons"/>
    <revision version="0.1" date="2012-07-18" status="draft"/>

    <credit type="author copyright">
      <name>Anna Zacchi</name>
      <email its:translate="no">azzurroverde@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Tlačítko, které po kliknutí zobrazí nabídku</desc>
  </info>

  <title>MenuButton</title>
  <media type="image" mime="image/png" src="media/menubutton.png"/>
  <p>Widget <code>GtkMenuButton</code> se používá k zobrazení nabídky po kliknutí na něj. Nabídka může být poskytnuta v podobě <code>GtkMenu</code> nebo jako abstraktní <code>GMenuModel</code>. Tlačítko může obsahovat libovolný platný synovský widget. To znamená, že může obsahovat většinu ostatních standardních potomků <code>GtkWidget</code>. Nejčastěji používaným vloženým widgetem je <code>GtkArrow</code>.</p>

<note><p>Musíte používat GNOME 3.6 nebo novější, aby widget MenuButton fungoval.</p></note>
<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class Application {

    // Vytvoří aplikaci
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.myapp',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

       // Napojí signály "activate" a "startup" k funkcím zpětného volání
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Vytvoří uživatelské rozhraní (v tomto případě jen ApplicationWindow)
    _buildUI() {
        this._window = new Gtk.ApplicationWindow({ application: this.application,
                                                   window_position: Gtk.WindowPosition.CENTER,
                                                   title: "MenuButton Example" });
        this._window.set_default_size(600, 400);
        this.grid = new Gtk.Grid();
        this._window.add(this.grid);


        this._menuButton = new Gtk.MenuButton();
        this.grid.attach(this._menuButton, 0, 0, 1, 1 );
        this.menu = Gtk.Menu.new_from_model(this.menuModel);

        this.menu.show();
        this._menuButton.set_menu_model (this.menuModel);
        this._menuButton.set_size_request(80, 35);
        this._menuButton.show();

        this._window.show_all();
    }

    _showNew() {
        print("You clicked \"New\"");
    }

    _showAbout() {
        print("You clicked \"About\"");
    }

    // Vytvoří položky nabídky a napojí signály na funkce zpětného volání
    _initMenus() {
        let newAction = new Gio.SimpleAction({ name: 'new' });
        newAction.connect('activate', () =&gt; { this._showNew(); });
        this.application.add_action(newAction);

        let aboutAction = new Gio.SimpleAction({ name: 'about' });
        aboutAction.connect('activate', () =&gt; { this._showAbout(); });
        this.application.add_action(aboutAction);

        let quitAction = new Gio.SimpleAction({ name: 'quit' });
        quitAction.connect('activate', () =&gt; { this._window.destroy(); });
         this.application.add_action(quitAction);

        this.menuModel = new Gio.Menu();

        this.menuItemNew = Gio.MenuItem.new("New", 'app.new');
        this.menuItemAbout = Gio.MenuItem.new("About", 'app.about');
        this.fileMenuItem = Gio.MenuItem.new("Other", null);

        this.menuModel.append_item(this.menuItemNew);
        this.menuModel.append_item(this.menuItemAbout);

        // Podnabídka
        this.subMenu = new Gio.Menu();
        this.fileMenuItem.set_submenu(this.subMenu);
        this.menuItemQuit = Gio.MenuItem.new("Quit", 'app.quit');
        this.subMenu.append_item(this.menuItemQuit);
        this.menuModel.append_item(this.fileMenuItem);
    }

    // Funkce zpětného volání pro signál "activate"
    _onActivate() {
        this._window.present();
    }

    // Funkce zpětného volání pro signál "startup"
    _onStartup() {
        //Před voláním _buildUI() musíte zavolat _initMenus()
        this._initMenus();
        this._buildUI();
    }
};

// Spustí aplikaci
let app = new Application();
app.application.run(ARGV);
</code>
<p>V této ukázce se používá následující:</p>
<list>
  <item><p><link href="https://developer.gnome.org/gtk3/unstable/GtkMenuButton.html">MenuButton</link></p></item>
</list>
</page>
