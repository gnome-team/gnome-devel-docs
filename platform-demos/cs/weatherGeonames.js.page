<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="weatherGeonames.js" xml:lang="cs">
  <info>
    <link type="guide" xref="weatherApp.js#main"/>
    <revision version="0.1" date="2012-03-09" status="stub"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc/>
  </info>

  <title>Místní knihovna geoNames</title>
  <synopsis>
    <p>V této část lekce sestrojíme místní knihovnu geoNames používající asynchronní volání. Informace o počasí budeme v tomto příkladu získávat z geonames.org a aplikace používá <link href="http://cs.wikipedia.org/wiki/ICAO_k%C3%B3d_leti%C5%A1t%C4%9B">kódy ICAO</link> pro určení místa v požadavku o počasí. Abyste si mohli napsat a spustit kód příkladu sami, budete potřebovat v počítači nainstalovaný editor pro psaní kódu, terminál a GNOME 3.0 nebo novější. V této lekci budeme postupovat následujícími částmi:</p>

    <list>
      <item><p><link xref="#geonamesimports">Místní knihovna pro získávání počasí</link></p></item>
      <item><p><link xref="#geonamesfunction">Vytvoření funkce GeoNames</link></p></item>
      <item><p><link xref="#geonamesmethods">Metody pro GeoNames</link></p></item>
      <item><p><link xref="#geonames.js">geonames.js </link></p></item>
    </list>
  </synopsis>

  <section id="geonamesimports">
  <title>Místní knihovna pro získávání počasí</title>
  <p>K tomu potřebujeme nový soubor, který bude naší místní knihovnou.</p>
  <code mime="application/javascript" style="numbered">
const Soup = imports.gi.Soup;
const _httpSession = new Soup.SessionAsync();
Soup.Session.prototype.add_feature.call(_httpSession, new Soup.ProxyResolverDefault());
</code>
  <p>Na prvních řádcích naimportujeme a inicializujeme knihovny, které potřebujeme použít v této místní knihovně. Knihovna Soup zpracovává všechny požadavky, které musíme dělat přes HTTP.</p>
  </section>

  <section id="geonamesfunction">
  <title>Vytvoření funkce GeoNames</title>
  <code mime="application/javascript" style="numbered">
function GeoNames(station) {
  this.station = station;
}

GeoNames.prototype = {

}
</code>
  <p>Zde vytvoříme funkci GeoNames, která se nám bude starat o získávání počasí. JavaScript umožňuje vytvořit funkce, které na začátku mají v těle jen základ, který později rozšíříme. Provádíme to mezi složenými závorkami u GeoNames.prototype.</p>
  </section>

  <section id="geonamesmethods">
  <title>Metody pro GeoNames</title>
  <code mime="application/javascript" style="numbered">
getWeather: function(callback) {
    var request = Soup.Message.new('GET', 'http://api.geonames.org/weatherIcaoJSON?ICAO=' + this.station + '&amp;username=demo');
    _httpSession.queue_message(request, function(_httpSession, message) {
      if (message.status_code !== 200) {
        callback(message.status_code, null);
        return;
      }
      var weatherJSON = request.response_body.data;
      var weather = JSON.parse(weatherJSON);
      callback(null, weather);
      });
},

getIcon: function(weather){
    switch (weather.weatherObservation.weatherCondition){
    case "drizzle":
    case "light showers rain":
    case "light rain":
      return "weather-showers-scattered.svg";
    case "rain":
      return "weather-showers.svg";
    case "light snow":
    case "snow grains":
      return "weather-snow.svg";
    }
    switch (weather.weatherObservation.clouds){
      case "few clouds":
      case "scattered clouds":
        return "weather-few-clouds.svg";
      case "clear sky":
        return "weather-clear.svg"
      case "broken clouds":
      case "overcast":
        return "weather-overcast.svg";
    }
    return "weather-fog.svg";
}
</code>
  <p>První metodou pro GeoName je <code>getWeather</code> a druhou <code>getIcon</code>. V <code>getWeather</code> vytvoříme pomocí knihovny soup požadavek http, obsloužíme chyby a zpracujeme informace z odpovědi do formy, kterou chceme používat. V <code>getIcon</code> jednoduše porovnáme výsledky, které jsem dostali od předchozí funkce a podle nich ve výrazu <code>switch</code> získáme ikonu odpovídající aktuálnímu počasí. Nyní, když máme naši místní knihovnu připravenou, je načase ji použít.</p>
  </section>


  <section id="geonames.js">
  <title>geonames.js</title>
  <p>Zde je celý kód pro naši místní knihovnu. Soubor s hlavním programem ji volá asynchronně.</p>
  <code mime="application/javascript" style="numbered">
const Soup = imports.gi.Soup;
const _httpSession = new Soup.SessionAsync();
Soup.Session.prototype.add_feature.call(_httpSession, new Soup.ProxyResolverDefault());

function GeoNames(station) {
  this.station = station;
}

GeoNames.prototype = {
  getWeather: function(callback) {
    var request = Soup.Message.new('GET', 'http://api.geonames.org/weatherIcaoJSON?ICAO=' + this.station + '&amp;username=demo');
    _httpSession.queue_message(request, function(_httpSession, message) {
      if (message.status_code !== 200) {
        callback(message.status_code, null);
        return;
      }
      var weatherJSON = request.response_body.data;
      var weather = JSON.parse(weatherJSON);
      callback(null, weather);
      });
    },

  getIcon: function(weather){
    switch (weather.weatherObservation.weatherCondition){
    case "drizzle":
    case "light showers rain":
    case "light rain":
      return "weather-showers-scattered.svg";
    case "rain":
      return "weather-showers.svg";
    case "light snow":
    case "snow grains":
      return "weather-snow.svg";
    }
    switch (weather.weatherObservation.clouds){
      case "few clouds":
      case "scattered clouds":
        return "weather-few-clouds.svg";
      case "clear sky":
        return "weather-clear.svg"
      case "broken clouds":
      case "overcast":
        return "weather-overcast.svg";
    }
    return "weather-fog.svg";
    }
}
}  </code>
  </section>

</page>
