<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="weatherApp.js" xml:lang="cs">
  <info>
  <title type="text">Aplikace s počasím (JavaScript)</title>
    <link type="guide" xref="js#examples"/>
    <revision version="0.1" date="2012-03-09" status="stub"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>Jak naplánovat aplikaci, která používá asynchronní volání. Asynchronní volání budou předvedena na aplikaci s počasím.</desc>

  </info>

  <title>Aplikace s počasím</title>
  <synopsis>
    <p>V této lekci sestrojíme aplikaci s počasím používající asynchronní volání. Informace o počasí budeme v tomto příkladu získávat z geonames.org a aplikace používá <link href="http://cs.wikipedia.org/wiki/ICAO_k%C3%B3d_leti%C5%A1t%C4%9B">kódy ICAO</link> pro určení místa v požadavku o počasí. Abyste si mohli napsat a spustit kód příkladu sami, budete potřebovat v počítači nainstalovaný editor pro psaní kódu, terminál a GNOME 3.0 nebo novější. V této lekci budeme postupovat následujícími částmi:</p>

    <list>
      <item><p><link xref="#planningUi">Plánování grafického uživatelského rozhraní</link></p></item>
      <item><p><link xref="#asynchronous">Asynchronní volání</link></p></item>
      <item><p><link xref="#main">Soubor s hlavním programem</link></p></item>
      <item><p><link xref="#main">Místní knihovna GeoNames</link></p></item>
      <item><p><link xref="#main">Autotools a ikony</link></p></item>
    </list>
  </synopsis>

  <p>Po přečtení této lekce byste měli na obrazovce dostat toto:</p>
  <media type="image" mime="image/png" src="media/weatherAppJs.png"/>

  <section id="planningUi">
    <title>Plánování grafického uživatelského rozhraní</title>
    <p>Strukturování aplikace v GNOME 3 znamená používat <link href="http://developer.gnome.org/platform-overview/stable/gtk">GTK+</link>. Nejpodstatnější věc, kterou si musíte zapamatovat, je, že hlavní okno může obsahovat jen jeden widget. S tím musíte počítat při návrhu struktury (v tomto příkladu použijeme Gtk.Grid). Vhodným způsobem je vložit do hlavního okna tento box a všechny widgety umístit do něj. Když se podíváte na obrázek budoucí aplikace, je snadnější říci, jaké jsou mezi widgety vztahy. Například Gtk.Grid umisťuje vaše widgety vůči ostatním widgetům, takže od chvíle, kdy je na místě první widget, může být umístění dalších provedeno vůči ostatním widgetům v mřížce.</p>
  </section>
  <section id="asynchronous">
    <title>Asynchronní volání</title>
    <p>V mnoha programovacích jazycích běží všechny operace synchronně – řeknete programu, aby něco udělal a než se může pokračovat, bude se čekat, než to bude úplně hotové. To se ale nehodí pro grafické uživatelské rozhraní, protože celá aplikaci by zamrzla během toho, co by se čekalo na nějakou operaci. Zde pomůže asynchronní provádění. S asynchronními voláními nebude vaše GUI blokováno žádným požadavkem. Asynchronní volání učiní vaši aplikaci pružnější a lépe vybavenou na zpracování situací, kdy volání zabralo více času, než se počítalo, nebo se z nějakého důvodu zaseklo. Asynchronní volání lze použít například u V/V operací souborového systému a pro pomalejší výpočty na pozadí.</p>
    <p>V tomto příkladu musíme získat data z geoname.org. Zatímco se tak bude dít, chceme aby zbytek programu pokračoval. Pokud bychom z geonames.org nedostali žádné informace kvůli problémům s připojením k Internetu a jednalo by se o synchronní aplikaci, nedostali bychom se nikdy do bodu, kdy je správně provedena naše funkce main_quit() a aplikaci by bylo nutné zabít z terminálu.</p>
  </section>
  <section id="main">
    <title>Různé části programu</title>
  </section>
</page>
