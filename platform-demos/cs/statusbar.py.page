<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="statusbar.py" xml:lang="cs">
  <info>
    <title type="text">Statusbar (Python)</title>
    <link type="guide" xref="beginner.py#display-widgets"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="spinner.py"/>    
    <revision version="0.2" date="2012-06-12" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Oznamuje uživateli zprávy s nižší důležitostí</desc>
  </info>

  <title>Statusbar</title>
  <media type="image" mime="image/png" src="media/statusbar.png"/>
  <p>Tato stavová lišta vám sděluje, jestli jste klikli na tlačítko nebo jestli jste zmáčkli klávesu (a kterou).</p>

  <links type="section"/>

  <section id="code">
  <title>Kód použitý k vygenerování tohoto příkladu</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # Okno

    def __init__(self, app):
        Gtk.Window.__init__(self, title="StatusBar Example", application=app)
        self.set_default_size(200, 100)

        # Popisek
        label = Gtk.Label(label="Press any key or ")

        # Tlačítko
        button = Gtk.Button(label="click me.")
        # Napojí signál na zpětné volání
        button.connect("clicked", self.button_clicked_cb)

        # Stavová lišta
        self.statusbar = Gtk.Statusbar()
        # Její id_kontextu - nezobrazuje se v uživatelském rozhraní, ale je
        # potřeba k jednoznačné identifikaci zdroje zprávy
        self.context_id = self.statusbar.get_context_id("example")
        # Vloží zprávu do zásobníku stavové lišty
        self.statusbar.push(
            self.context_id, "Waiting for you to do something...")

        # Mřížka pro připojení widgetů
        grid = Gtk.Grid()
        grid.set_column_spacing(5)
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.attach(label, 0, 0, 1, 1)
        grid.attach_next_to(button, label, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach(self.statusbar, 0, 1, 2, 1)

        # Přidá mřížku do okna
        self.add(grid)

    # Funkce zpětného volání pro kliknutí na tlačítko
    # Když je na tlačítko kliknuto, je událost signalizována stavové liště,
    # do které se vloží nový stav
    def button_clicked_cb(self, button):
        self.statusbar.push(self.context_id, "You clicked the button.")

    # Obsluha události
    def do_key_press_event(self, event):
    # Kterýkoliv signál od klávesnice je předán stavové liště, do
    # které je vložen nový stav se symbolickým názvem zmáčknuté klávesy
        self.statusbar.push(self.context_id, Gdk.keyval_name(event.keyval) +
                            " key was pressed.")
        # Zastaví vysílání signálu
        return True


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  <note><p><code>Gdk.keyval_name(event.keyval)</code> převádí hodnotu klávesy <code>event.keyval</code> na symbolický název. Názvy a odpovídající hodnoty kláves můžete najít <link href="https://gitlab.gnome.org/GNOME/gtk/blob/master/gdk/gdkkeysyms.h">zde</link>, ale například <code>GDK_KEY_BackSpace</code> se změní na řetězec <code>"BackSpace"</code>.</p></note>
  </section>

  <section id="methods">
  <title>Užitečné metody pro widget Statusbar</title>
    <p>Na řádku 17 je signál <code>"clicked"</code> napojen na funkci zpětného volání <code>button_clicked_cb()</code> pomocí <code><var>widget</var>.connect(<var>signál</var>, <var>funkce zpětného volání</var>)</code>. Podrobnější vysvětlení viz <link xref="signals-callbacks.py"/>.</p>
  <list>
    <item><p><code>pop(id_kontextu)</code> odstraní první zprávu ze zásobníku stavové lišty se zadaným <code>id_kontextu</code>.</p></item>
    <item><p><code>remove_all(id_kontextu)</code> odstraní všechny zprávy ze zásobníku stavové lišty se zadaným <code>id_kontextu</code>.</p></item>
    <item><p><code>remove(id_kontextu, id_zprávy)</code> odstraní zprávu se zadaným <code>id_zprávy</code> ze zásobníku stavové lišty se zadaným <code>id_kontextu</code>. Hodnotu <code>id_zprávy</code> vrací funkce <code>push(id_kontextu, "zpráva")</code> při vkládání zprávy do stavové lišty.</p></item>
  </list>
  </section>

  <section id="references">
  <title>Odkazy k API</title>
  <p>V této ukázce se používá následující:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkStatusbar.html">GtkStatusbar</link></p></item>
    <item><p><link href="https://developer.gnome.org/gdk3/stable/gdk3-Keyboard-Handling.html">Gdk - Key Values</link></p></item>
  </list>
  </section>
</page>
