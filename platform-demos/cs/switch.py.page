<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="switch.py" xml:lang="cs">
  <info>
    <title type="text">Switch (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="radiobutton.py"/>
    <revision version="0.1" date="2012-05-24" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Přepínač ve stylu „posuvného vypínače“</desc>
  </info>

  <title>Switch</title>
  <media type="image" mime="image/png" style="floatend" src="media/switch_off.png"/>
  <media type="image" mime="image/png" src="media/switch_on.png"/>

  <p>Přepínač způsobující zobrazení a skrytí textu v záhlaví.</p>

  <links type="section"/>

  <section id="code">
    <title>Kód použitý k vygenerování tohoto příkladu</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Switch Example", application=app)
        self.set_default_size(300, 100)
        self.set_border_width(10)

        # Vypínač
        switch = Gtk.Switch()
        # Nastaví výchozí stav na zapnuto
        switch.set_active(True)
        # Napojí signál notify::active vyslaný vypínačem
        # na funkci zpětného volání activate_cb
        switch.connect("notify::active", self.activate_cb)

        # Popisek
        label = Gtk.Label()
        label.set_text("Title")

        # Mřížka pro umístění widgetů
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.attach(label, 0, 0, 1, 1)
        grid.attach(switch, 1, 0, 1, 1)

        # Přidá mřížku do okna
        self.add(grid)

    # Funkce zpětného volání. Protože signál je notify::active
    # nepotřebujeme argument 'active'
    def activate_cb(self, button, active):
        # Jestliže je vypínače aktivní (tj. zapnutý), nastavíme
        # záhlaví okna na "Switch Example"
        if button.get_active():
            self.set_title("Switch Example")
        # Jinak jej nastavíme na "" (prázdný řetězec)
        else:
            self.set_title("")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>
  <section id="methods">
    <title>Užitečné metody pro widget Switch</title>
    <p>Na řádku 17 je signál <code>"notify::active"</code> napojen na funkci zpětného volání <code>activate_cb()</code> pomocí <code><var>widget</var>.connect(<var>signál</var>, <var>funkce zpětného volání</var>)</code>. Podrobnější vysvětlení viz <link xref="signals-callbacks.py"/>.</p>

  </section>
  <section id="references">
    <title>Odkazy k API</title>
    <p>V této ukázce se používá následující:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkSwitch.html">GtkSwitch</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLabel.html">GtkLabel</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkGrid.html">GtkGrid</link></p></item>
    </list>
  </section>
</page>
