<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="entry.py" xml:lang="sv">
  <info>
    <title type="text">Entry (Python)</title>
    <link type="guide" xref="beginner.py#entry"/>
    <link type="seealso" xref="strings.py"/>
    <link type="next" xref="scale.py"/>
    <revision version="0.2" date="2012-06-23" status="draft"/>

    <credit type="author copyright editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no"/>
      <years>2012</years>
    </credit>

    <credit type="author copyright">
      <name>Sebastian Pölsterl</name>
      <email its:translate="no">sebp@k-d-w.org</email>
      <years>2011</years>
    </credit>
    <desc>Ett textinmatningsfält med en rad</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Entry</title>
  <media type="image" mime="image/png" src="media/entry.png"/>
  <p>Detta program hälsar dig i terminalen med namnet du tillhandahåller.</p>

  <links type="section"/>

  <section id="code">
  <title>Kod som använts för att generera detta exempel</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Vad heter du?", application=app)
        self.set_default_size(300, 100)
        self.set_border_width(10)

        # en enradsinmatning
        name_box = Gtk.Entry()
        # utger en signal när Returknappen trycks ned, ansluten till
        # återanropsfunktionen cb_activate
        name_box.connect("activate", self.cb_activate)

        # lägg till vår Gtk.Entry till fönstret
        self.add(name_box)

    # innehållet i inmatningen används för att skriva i terminalen
    def cb_activate(self, entry):
        # hämta komponentens innehåll
        name = entry.get_text()
        # skriv ut det på ett vänligt sätt i terminalen
        print("Hej " + name + "!")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Användbara metoder för en Entry-komponent</title>
    <p>In line 14 the signal <code>"activate"</code> is connected to the callback function <code>cb_activate()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation. Some of the signals that a Gtk.Entry widget can emit are: <code>"activate"</code> (emitted when the user activates the Entry key); <code>"backspace"</code> (emitted when the user activates the Backspace or Shift-Backspace keys); <code>"copy-clipboard"</code> (Ctrl-c and Ctrl-Insert); <code>"paste-clipboard"</code> (Ctrl-v and Shift-Insert); <code>"delete-from-cursor"</code> (Delete, for deleting a character; Ctrl-Delete, for deleting a word); <code>"icon-press"</code> (emitted when the user clicks an activatable icon); <code>"icon-release"</code> (emitted on the button release from a mouse click over an activatable icon);  <code>"insert-at-cursor"</code> (emitted when the user initiates the insertion of a fixed string at the cursor); <code>"move-cursor"</code> (emitted when the user initiates a cursor movement); <code>"populate-popup"</code> (emitted before showing the context menu of the entry; it can be used to add items to it).</p>
    <list>
      <item><p><code>get_buffer()</code> and <code>set_buffer(buffer)</code>, where <code>buffer</code> is a Gtk.EntryBuffer object, can be used to get and set the buffer for the entry.</p></item>
      <item><p><code>get_text()</code> och <code>set_text("lite text")</code> kan användas för att få samt ställa in innehållet för inmatningsrutan.</p></item>
      <item><p><code>get_text_length()</code> ger textens längd.</p></item>
      <item><p><code>get_text_area()</code> gets the area where the entry's text is drawn.</p></item>
      <item><p>If we set <code>set_visibility(False)</code> the characters in the entry are displayed as the invisible char. This is the best available in the current font, but it can be changed with <code>set_invisible_char(ch)</code>, where <code>ch</code> is a Unicode character. The latter method is reversed by <code>unset_invisible_char()</code>.</p></item>
      <item><p><code>set_max_length(int)</code>, where <code>int</code> is an integer, truncates every entry longer than <code>int</code> to have the desired maximum length.</p></item>
      <item><p>By default, if you press the Entry key the Gtk.Entry emits the signal <code>"activate"</code>. If you would like to activate the default widget for the window (set using <code>set_default(widget)</code> on the window), then use <code>set_activates_default(True)</code>.</p></item>
      <item><p>To set a frame around the entry: <code>set_has_frame(True)</code>.</p></item>
      <item><p><code>set_placeholder_text("lite text")</code> ställer in texten som ska visas i inmatningsrutan då den är tom och ofokuserad.</p></item>
      <item><p><code>set_overwrite_mode(True)</code> and <code>set_overwrite_mode(False)</code> are self-explanatory.</p></item>
      <item><p>Om vi har <code>set_editable(False)</code> så kan användaren inte redigera texten i komponenten.</p></item>
      <item><p><code>set_completion(completion)</code>, where <code>completion</code> is a <link href="http://developer.gnome.org/gtk3/unstable/GtkEntryCompletion.html"><code>Gtk.EntryCompletion</code></link>, sets completion - or disables it if <code>completion</code> is <code>None</code>.</p></item>
      <item><p>An Entry widget can display progress or activity information behind the text. We use <code>set_progress_fraction(fraction)</code>, where <code>fraction</code> is a <code>float</code> between <code>0.0</code> and <code>1.0</code> inclusive, to fill in the given fraction of the bar. We use <code>set_progress_pulse_step()</code> to set the fraction of total entry width to move the progress bouncing block for each call to <code>progress_pulse()</code>. The latter method indicates that some progress is made, and causes the progress indicator of the entry to enter "activity mode", where a block bounces back and forth. Each call to <code>progress_pulse()</code> causes the block to move by a little bit (the amount of movement per pulse is determined, as said before, by <code>set_progress_pulse_step()</code>).</p></item>
      <item><p>An Entry widget can also show icons. These icons can be activatable by clicking, can be set up as drag source and can have tooltips. To add an icon, use <code>set_icon_from_stock(icon_position, stock_id)</code>, or one of <code>set_icon_from_pixbuf(icon_position, pixbuf)</code>, <code>set_icon_from_icon_name(icon_position, icon_name)</code>, where <code>icon_position</code> is one of <code>Gtk.EntryIconPosition.PRIMARY</code> (to set the icon at the beginning of the entry) <code>Gtk.EntryIconPosition.SECONDARY</code> (to set the icon at the end of the entry). To set a tooltip on an icon, use <code>set_icon_tooltip_text("tooltip text")</code> or <code>set_icon_tooltip_markup("tooltip text in Pango markup language")</code>.</p></item>
    </list>
  </section>

  <section id="references">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkEntry.html">GtkEntry</link></p></item>
    </list>
  </section>
</page>
