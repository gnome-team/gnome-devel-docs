<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="grid.js" xml:lang="sv">
  <info>
  <title type="text">Grid (Javascript)</title>
    <link type="guide" xref="beginner.js#layout"/>
    <revision version="0.1" date="2012-05-26" status="draft"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
	<name>Taryn Fox</name>
        <email its:translate="no">jewelfox@fursona.net</email>
    </credit>

    <desc>Packa komponenter i rader och kolumner</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Grid</title>
  <media type="image" mime="image/png" src="media/grid.png"/>
  <p>En knappkomponent ansluten till en förloppsindikator, inuti ett rutnät som hanterar layouten.</p>

<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class GridExample {

    /* Skapa programmet i sig
       Denna standardkod behövs för att bygga alla GTK+-program. */
    constructor() {
        this.application = new Gtk.Application ({
            application_id: 'org.example.jsgrid',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

        // Anslut ”activate”- och ”startup”-signaler till återanropsfunktionerna
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Återanropsfunktion för ”activate”-signal visar fönster när den aktiveras
    _onActivate() {
        this._window.present();
    }

    // Återanropsfunktion för ”startup”-signal initierar menyer och bygger användargränssnittet
    _onStartup() {
        this._buildUI();
    }

    // Bygg programmets användargränssnitt
    _buildUI() {

        // Skapa programfönstret
            this._window = new Gtk.ApplicationWindow  ({ application: this.application,
                                                         window_position: Gtk.WindowPosition.CENTER,
                                                         title: "Grid-exempel"});

        // Skapa rutnätet
        this.Grid = new Gtk.Grid ();

        // Skapa komponenterna i rutnätet
        this.progressBar = new Gtk.ProgressBar ();
        this.Button = new Gtk.Button ({ label: "Knapp" });
        this.Button.connect ("clicked", this._clickHandler.bind(this));

        // Sätt ihop rutnätet
        this._window.add (this.Grid);
        this.Grid.attach (this.Button, 1, 1, 1, 1);
        this.Grid.attach_next_to (this.progressBar, this.Button, Gtk.PositionType.BOTTOM, 1, 1);

                // Visa fönstret och alla barnkomponenter
                this._window.show_all();
    }

    // Här är funktionen som säger vad som händer när knappen klickas på
    _clickHandler() {
        this.progressBar.pulse ();
    }


};

// Kör programmet
let app = new GridExample ();
app.application.run (ARGV);
</code>
<p>I detta exempel använde vi följande:</p>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Button.html">Gtk.Button</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Grid.html">Gtk.Grid</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.ProgressBar.html">Gtk.ProgressBar</link></p></item>
</list>
</page>
