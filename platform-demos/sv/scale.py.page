<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="scale.py" xml:lang="sv">
  <info>
    <title type="text">Scale (Python)</title>
    <link type="guide" xref="beginner.py#entry"/>
    <link type="seealso" xref="grid.py"/>
    <link type="next" xref="textview.py"/>
    <revision version="0.2" date="2012-06-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>En skjutreglagekomponent för att välja ett värde från ett intervall</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Scale</title>
  <media type="image" mime="image/png" src="media/scale.png"/>
  <p>Slide the scales!</p>

  <links type="section"/>

  <section id="code">
    <title>Kod som använts för att generera detta exempel</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Scale Example", application=app)
        self.set_default_size(400, 300)
        self.set_border_width(5)

        # two adjustments (initial value, min value, max value,
        # step increment - press cursor keys to see!,
        # page increment - click around the handle to see!,
        # page size - not used here)
        ad1 = Gtk.Adjustment(0, 0, 100, 5, 10, 0)
        ad2 = Gtk.Adjustment(50, 0, 100, 5, 10, 0)

        # an horizontal scale
        self.h_scale = Gtk.Scale(
            orientation=Gtk.Orientation.HORIZONTAL, adjustment=ad1)
        # of integers (no digits)
        self.h_scale.set_digits(0)
        # that can expand horizontally if there is space in the grid (see
        # below)
        self.h_scale.set_hexpand(True)
        # that is aligned at the top of the space allowed in the grid (see
        # below)
        self.h_scale.set_valign(Gtk.Align.START)

        # we connect the signal "value-changed" emitted by the scale with the callback
        # function scale_moved
        self.h_scale.connect("value-changed", self.scale_moved)

        # a vertical scale
        self.v_scale = Gtk.Scale(
            orientation=Gtk.Orientation.VERTICAL, adjustment=ad2)
        # that can expand vertically if there is space in the grid (see below)
        self.v_scale.set_vexpand(True)

        # we connect the signal "value-changed" emitted by the scale with the callback
        # function scale_moved
        self.v_scale.connect("value-changed", self.scale_moved)

        # a label
        self.label = Gtk.Label()
        self.label.set_text("Move the scale handles...")

        # a grid to attach the widgets
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_column_homogeneous(True)
        grid.attach(self.h_scale, 0, 0, 1, 1)
        grid.attach_next_to(
            self.v_scale, self.h_scale, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach(self.label, 0, 1, 2, 1)

        self.add(grid)

    # any signal from the scales is signaled to the label the text of which is
    # changed
    def scale_moved(self, event):
        self.label.set_text("Horizontal scale is " + str(int(self.h_scale.get_value())) +
                            "; vertical scale is " + str(self.v_scale.get_value()) + ".")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Användbara metoder för en Scale-komponent</title>
    <p>A Gtk.Adjustment is needed to construct the Gtk.Scale. This is the representation of a value with a lower and upper bound, together with step and page increments, and a page size, and it is constructed as <code>Gtk.Adjustment(value, lower, upper, step_increment, page_increment, page_size)</code> where the fields are of type <code>float</code>; <code>step_increment</code> is the increment/decrement that is obtained by using the cursor keys, <code>page_increment</code> the one that is obtained clicking on the scale itself. Note that <code>page_size</code> is not used in this case, it should be set to <code>0</code>.</p>
    <p>På rad 28 ansluts signalen <code>"value-changed"</code> till återanropsfunktionen <code>scale_moved()</code> med <code><var>komponent</var>.connect(<var>signal</var>, <var>återanropsfunktion</var>)</code>. Se <link xref="signals-callbacks.py"/> för en utförligare förklaring.</p>
    <list>
      <item><p><code>get_value()</code> retrieves the current value of the scale; <code>set_value(value)</code> sets it (if the <code>value</code>, of type <code>float</code>, is outside the minimum or maximum range, it will be clamped to fit inside them). These are methods of the class Gtk.Range.</p></item>
      <item><p>Use <code>set_draw_value(False)</code> to avoid displaying the current value as a string next to the slider.</p></item>
      <item><p>To highlight the part of the scale between the origin and the current value:</p>
        <code mime="text/x-python">
self.h_scale.set_restrict_to_fill_level(False)
self.h_scale.set_fill_level(self.h_scale.get_value())
self.h_scale.set_show_fill_level(True)</code>
        <p>in the callback function of the "value-changed" signal, so to have the new filling every time the value is changed. These are methods of the class Gtk.Range.</p>
      </item>
      <item><p><code>add_mark(value, position, markup)</code> adds a mark at the <code>value</code> (<code>float</code> or <code>int</code> if that is the precision of the scale), in <code>position</code> (<code>Gtk.PositionType.LEFT, Gtk.PositionType.RIGHT, Gtk.PositionType.TOP, Gtk.PositionType.BOTTOM</code>) with text <code>Null</code> or <code>markup</code> in the Pango Markup Language. To clear marks, <code>clear_marks()</code>.</p></item>
      <item><p><code>set_digits(digits)</code> sets the precision of the scale at <code>digits</code> digits.</p></item>
    </list>
  </section>

  <section id="references">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkScale.html">GtkScale</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkAdjustment.html">GtkAdjustment</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Standard-Enumerations.html">Standarduppräkningstyper</link></p></item>
    </list>
  </section>
</page>
