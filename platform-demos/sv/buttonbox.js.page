<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="buttonbox.js" xml:lang="sv">
  <info>
    <title type="text">ButtonBox (Javascript)</title>
    <link type="guide" xref="beginner.js#layout"/>
    <link type="seealso" xref="button.js"/>
    <revision version="0.2" date="2013-06-25" status="review"/>

    <credit type="author copyright">
      <name>Meg Ford</name>
      <email its:translate="no">megford@gnome.org</email>
      <years>2013</years>
    </credit>

    <desc>En behållare för att arrangera knappar</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>ButtonBox</title>

  <media type="image" mime="image/png" src="media/buttonbox_calculator.png"/>
  <p>En kalkylator - knapparna infogas i horisontella ButtonBoxar.</p>

  <links type="section"/>

  <section id="code">
    <title>Kod som använts för att generera detta exempel</title>
    <code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;

class ButtonBoxExample {

    // Skapa programmet i sig
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jsbuttonbox'
        });

        // Anslut ”activate”- och ”startup”-signaler till återanropsfunktionerna
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Återanropsfunktion för ”activate”-signal visar fönster när den aktiveras
    _onActivate() {
        this.window.present();
    }

    // Återanropsfunktion för ”startup”-signal bygger användargränssnittet
    _onStartup() {
        this._buildUI();
    }

    // Bygg programmets användargränssnitt
    _buildUI() {
        // Skapa programfönstret
        this.window = new Gtk.ApplicationWindow  ({ application: this.application,
                                                    window_position: Gtk.WindowPosition.CENTER,
                                                    title: "Kalkylator",
                                                    default_width: 350,
                                                    default_height: 200,
                                                    border_width: 10 });
        this.entry = new Gtk.Entry();
        this.entry.set_text('0');
        // högerjusterad text
        this.entry.set_alignment(1);
        // texten i fältet kan inte ändras genom att skriva i det
        this.entry.set_can_focus(false);

        // ett rutnät
        this.grid = new Gtk.Grid();
        this.grid.set_row_spacing(5);
        
        // för att fästa fältet
        this.grid.attach(this.entry, 0, 0, 1, 1);
        
        // etiketterna för knapparna
        this.buttons = [ 7, 8, 9, '/', 4, 5, 6, '*', 1, 2, 3, '-', 'C', 0, '=', '+' ];
        
        // varje rad är en ButtonBox, fäst till rutnätet            
        for (let i = 0; i &lt; 4; i++) {
            this.hbox = Gtk.ButtonBox.new(Gtk.Orientation.HORIZONTAL);
            this.hbox.set_spacing(5);
            this.grid.attach(this.hbox, 0, i + 1, 1, 1);
            // varje ButtonBox har 4 knappar, anslutna till återanropsfunktionen
            for (let j= 0; j &lt; 4; j++) {
                this.button = new Gtk.Button();
                this.buttonLabel = (this.buttons[i * 4 + j].toString());
                this.button.set_label(this.buttonLabel);
                this.button.set_can_focus(false);
                this.button.connect("clicked", this._buttonClicked.bind(this));
                this.hbox.add(this.button);
            }
        }
            
        // några variabler för beräkningarna
        this.firstNumber = 0;
        this.secondNumber = 0;
        this.counter = 0;
        this.operation = "";

        // lägg till rutnätet till fönstret
        this.window.add(this.grid);
        this.window.show_all();
    }

    // återanropsfunktion för alla knapparna
    _buttonClicked(button) {
        this.button = button;
        // för operationerna
        if (this.button.get_label() == '+') {
            this.counter += 1 
            if (this.counter &gt; 1)
                this._doOperation();
            this.entry.set_text('0');
            this.operation = "plus";
        }

        else if (this.button.get_label() == '-') {
            this.counter += 1;
            if (this.counter &gt; 1)
                this._doOperation();
            this.entry.set_text('0');
            this.operation = "minus";
        }

        else if (this.button.get_label() == '*') {
            this.counter += 1; 
            if (this.counter &gt; 1)
                this._doOperation();
            this.entry.set_text('0');
            this.operation = "multiplication";
        }

        else if (this.button.get_label() == '/') {
            this.counter += 1 
            if (this.counter &gt; 1)
                this._doOperation();
            this.entry.set_text('0');
            this.operation = "division";
        }

        // för =
        else if (this.button.get_label() == '=') {
            this._doOperation();
            this.entry.set_text(this.firstNumber.toString());
            this.counter = 1;
        }

        // för avbryt (C)
        else if (this.button.get_label() == 'C') {
            this.firstNumber = 0;
            this.secondNumber = 0;
            this.counter = 0;
            this.entry.set_text('0');
            this.operation = "";
        }

        // för en sifferknapp
        else {
            this.newDigit = parseInt(this.button.get_label());
            if (this.entry.get_text() == "error")
                this.number = 0;
            else
                this.number = parseInt(this.entry.get_text());
            this.number = this.number * 10 + this.newDigit;            
            if (this.counter == 0)
                this.firstNumber = this.number;
            else
                this.secondNumber = this.number;
            this.entry.set_text(this.number.toString());
        }
     }

     _doOperation() {
        if (this.operation == "plus") {
           this.firstNumber += this.secondNumber;
        } else if (this.operation == "minus") {
            this.firstNumber -= this.secondNumber;
        } else if (this.operation == "multiplication") {
            this.firstNumber *= this.secondNumber;
        } else if (this.operation == "division") {
            if (this.secondNumber != 0) {
                this.firstNumber /= this.secondNumber;
            } else {
                this.firstNumber = 0; 
                this.secondNumber = 0;
                this.counter = 0; 
                this.entry.set_text("error");
                this.operation = "";

                return
            }
        } else {
            this.firstNumber = 0;
            this.secondNumber = 0;
            this.counter = 0;
            this.entry.set_text("error");
        }
    }
};

// Kör programmet
let app = new ButtonBoxExample();
app.application.run (ARGV);
</code>
  </section>

  <section id="references">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.ButtonBox.html">GtkButtonBox</link></p></item>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Box.html">GtkBox</link></p></item>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Button.html">GtkButton</link></p></item>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Entry.html">GtkEntry</link></p></item>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Grid.html">GtkGrid</link></p></item>
    </list>
  </section>
</page>
