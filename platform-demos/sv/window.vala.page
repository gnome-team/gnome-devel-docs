<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="window.vala" xml:lang="sv">
  <info>
  <title type="text">Window (Vala)</title>
    <link type="guide" xref="beginner.vala#windows"/>
    <revision version="0.1" date="2012-04-07" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ett toppnivåfönster som kan innehålla andra komponenter</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Window</title>
<table>
  <tr>
    <td>
      <media type="image" mime="image/png" src="media/window.png"/>
      <p>Den enklaste Gtk.Application</p>
    </td>
    <td>
      <p>Använd <link xref="GtkApplicationWindow.vala">ApplicationWindow</link> om du behöver GMenu-stöd.</p>
    </td>
  </tr>
</table>
<code mime="text/x-csharp" style="numbered">/*  Det här är programmet. */
public class Application : Gtk.Application {

	/* Konstruktor */
	public Application () {
		Object (application_id: "org.example.window");
	}

	/* Åsidosätt ”activate”-signalen för GLib.Application,
	 * vilken ärvs av Gtk.Application. */
	public override void activate () {

		var window = new Gtk.Window ();
		window.title = "Välkommen till GNOME";

		/* De följande tre raderna inkluderas här för att visa
		 * dig sätt du kan justera toppnivåfönstret enligt dina
		 * behov. Avkommentera dem för att se vad de gör.
		 */

		//window.border_width = 10;
		//window.set_default_size (350, 70);
		//window.window_position = Gtk.WindowPosition.CENTER;

		/* Lägg till fönstret till detta program. */
		this.add_window (window);

		/* Visa fönstret. */
		window.show ();
	}
}

/* main-funktionen skapar programmet och kör det.*/
int main (string[] args) {
	var app = new Application ();
	return app.run (args);
}
</code>
<p>I detta exempel använde vi följande:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p>Komponenten <link href="http://www.valadoc.org/gtk+-3.0/Gtk.Window.html">Gtk.Window</link></p></item>

  <item><p>Uppräkningstypen <link href="http://www.valadoc.org/gtk+-3.0/Gtk.WindowPosition.html">Gtk.WindowPosition</link></p></item>

  <item><p>Metoden <link href="http://www.valadoc.org/gtk+-3.0/Gtk.Window.set_default_size.html">set_default_size</link></p></item>

  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Container.border_width.html">border_width</link></p></item>

  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Window.window_position.html">window_position</link></p></item>
</list>
</page>
