<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="image-viewer.cpp" xml:lang="sv">

  <info>
    <link type="guide" xref="cpp#examples"/>

    <desc>Lite mer än ett enkelt ”Hej världen”-program med GTKmm.</desc>

    <revision pkgversion="0.1" version="0.1" date="2011-03-18" status="review"/>
    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Johannes Schmid</name>
      <email its:translate="no">jhs@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Bildvisare</title>

<synopsis>
  <p>I denna handledning kommer du lära dig:</p>
  <list>
    <item><p>Några grundläggande koncept i C++/GObject-programmering</p></item>
    <item><p>Hur du skriver ett Gtk-program i C++</p></item>
  </list>
</synopsis>

<media type="image" mime="image/png" src="media/image-viewer.png"/>

<section id="anjuta">
  <title>Skapa ett projekt i Anjuta</title>
  <p>Innan du börjar koda kommer du behöva konfigurera ett nytt projekt i Anjuta. Detta kommer skapa alla filer som du behöver för att bygga och köra koden senare. Det är också användbart för att hålla allting samlat.</p>
  <steps>
    <item>
    <p>Starta Anjuta och klicka på <guiseq><gui>Arkiv</gui><gui>Ny</gui><gui>Projekt</gui></guiseq> för att öppna projektguiden.</p>
    </item>
    <item>
    <p>Välj <gui>GTKmm (enkel)</gui> från fliken <gui>C++</gui>, klicka på <gui>Framåt</gui>, och fyll i dina detaljer på de nästkommande sidorna. Använd <file>image-viewer</file> som projektnamn och katalog.</p>
   	</item>
    <item>
    <p>Säkerställ att <gui>Använd GtkBuilder för användargränssnitt</gui> är inaktiverat då vi kommer skapa användargränssnittet manuellt i denna handledning. Se handledningen <link xref="guitar-tuner.cpp">Gitarrstämmare</link> om du vill lära dig hur du använder gränssnittsbyggaren.</p>
    </item>
    <item>
    <p>Klicka på <gui>Verkställ</gui> så kommer projektet skapas åt dig. Öppna <file>src/main.cc</file> från flikarna <gui>Projekt</gui> eller <gui>Filer</gui>. Du bör se kod som börjar med raderna:</p>
    <code mime="text/x-csrc">
#include &lt;gtkmm.h&gt;
#include &lt;iostream&gt;

#include "config.h"&gt;</code>
    </item>
  </steps>
</section>

<section id="build">
  <title>Bygg koden för första gången</title>
  <p>This is a very basic C++ code setting up GTKmm. More details are given below; skip this list if you understand the basics:</p>
  <list>
  <item>
    <p>The three <code>#include</code> lines at the top include the <code>config</code> (useful autoconf build defines), <code>gtkmm</code> (user interface) and <code>iostream</code> (C++-STL) libraries. Functions from these libraries are used in the rest of the code.</p>
   </item>
   <item>
    <p><code>main</code>-funktionen skapar ett nytt (tomt) fönster och ställer in fönstertiteln.</p>
   </item>
   <item>
    <p>The <code>kit::run()</code> call starts the GTKmm main loop, which runs the user interface and starts listening for events (like clicks and key presses). As we give the window
    as an argument to that function, the application will automatically exit when
    that window is closed.</p>
   </item>
  </list>

  <p>Denna kod är klar att användas, så du kan kompilera den genom att klicka på <guiseq><gui>Bygg</gui><gui>Bygg projekt</gui></guiseq> (eller trycka <keyseq><key>Skift</key><key>F7</key></keyseq>).</p>
  <p>Press <gui>Execute</gui> on the next window that appears to configure a debug build. You only need to do this once, for the first build.</p>
</section>

<section id="ui">
<title>Skapa användargränssnittet</title>
<p>Now we will bring life into the empty window. GTKmm organizes the user interface
with <code>Gtk::Container</code>s that can contain other widgets and even other containers. Here we
will use the simplest available container, a <code>Gtk::Box</code>:</p>
<code mime="text/x-csrc">
int
main (int argc, char *argv[])
{
	Gtk::Main kit(argc, argv);

	Gtk::Window main_win;
	main_win.set_title ("image-viewer-cpp");

	Gtk::Box* box = Gtk::manage(new Gtk::Box());
	box-&gt;set_orientation (Gtk::ORIENTATION_VERTICAL);
	box-&gt;set_spacing(6);
	main_win.add(*box);

	image = Gtk::manage(new Gtk::Image());
	box-&gt;pack_start (*image, true, true);

	Gtk::Button* button = Gtk::manage(new Gtk::Button("Öppna bild…"));
	button-&gt;signal_clicked().connect (
		sigc::ptr_fun(&amp;on_open_image));
	box-&gt;pack_start (*button, false, false);

	main_win.show_all_children();
	kit.run(main_win);

	return 0;
}
</code>
  <steps>
    <item>
    <p>The first lines create the widgets we want to use: a button for opening up an image, the image view widget itself and the box we will use as a container. </p>
    </item>
    <item>
    <p>The calls to <code>pack_start</code> add the two widgets to the box and define their behaviour. The image will
    expand into any available space while the button will just be as big as needed. You will notice that we don't set
    explicit sizes on the widgets. In GTKmm this is usually not needed as it makes it much easier to have a layout that
    looks good in different window sizes. Next, the box is added to the window.</p>
    </item>
    <item>
    <p>We need to define what happens when the user clicks on the button. GTKmm uses the concept of <em>signals</em>. When the button is clicked, it fires the <em>clicked</em> signal, which we can connect to some action. This is done using the <code>signal_clicked().connect</code>
    method which tells GTKmm to call the <code>on_open_image</code> function when the button is clicked. We will define the <em>callback</em> in the next section.</p>
    </item>
    <item>
    <p>The last step is to show all widgets in the window using
    <code>show_all_children()</code>. This is equivalent to using the <code>show()</code>
    method on all our child widgets.</p>
    </item>
  </steps>
</section>

<section id="show">
<title>Visa bilden</title>
<p>We will now define the signal handler for the <em>clicked</em> signal or the
button we mentioned before. Add this code before the <code>main</code>
method.</p>
<code mime="text/x-csrc"><![CDATA[
Gtk::Image* image = 0;

static void
on_open_image ()
{
	Gtk::FileChooserDialog dialog("Open image",
	                              Gtk::FILE_CHOOSER_ACTION_OPEN);
	dialog.add_button (Gtk::Stock::OPEN,
	                   Gtk::RESPONSE_ACCEPT);
	dialog.add_button (Gtk::Stock::CANCEL,
	                   Gtk::RESPONSE_CANCEL);

	Glib::RefPtr<Gtk::FileFilter> filter =
		Gtk::FileFilter::create();
	filter->add_pixbuf_formats();
	filter->set_name("Images");
	dialog.add_filter (filter);

	const int response = dialog.run();
	dialog.hide();

	switch (response)
	{
		case Gtk::RESPONSE_ACCEPT:
			image->set(dialog.get_filename());
			break;
		default:
			break;
	}
}
]]></code>
  <p>Det här är lite mer komplicerat än något som vi försökt så här långt, så låt oss dela upp det:</p>
  <list>
      <item>
      <p>The dialog for choosing the file is created using the
      <code>Gtk::FileChooserDialog</code> constructor. This takes the title and type of the dialog. In our case, it is an <em>Open</em> dialog.</p>
    </item>
    <item>
    <p>The next two lines add an <em>Open</em> and a <em>Close</em> button to the dialog.</p>
    <p>Observera att vi använder <em>standard</em>knappnamn från Gtk, istället för att manuellt skriva in ”Avbryt” eller ”Öppna”. Fördelen med att använda standardnamn är att knappetiketterna redan kommer vara översatta till användarens språk.</p>
    <p>The second argument to the <code>add_button()</code> method is a value to identify
    the clicked button. We use predefined values provided by GTKmm here, too.
    </p>
    </item>
    <item>
    <p>The next two lines restrict the <gui>Open</gui> dialog to only display files which can be opened by <code>Gtk::Image</code>. A filter object is created first; we then add all kinds of files supported by <code>Gdk::Pixbuf</code> (which includes most image formats like PNG and JPEG) to the filter. Finally, we set this filter to be the <gui>Open</gui> dialog's filter.</p>
    <p><code>Glib::RefPtr</code> is a smart pointer used here, that makes sure that the filter is
    destroyed when there is no reference to it anymore.</p>
    </item>
    <item>
    <p><code>dialog.run</code> displays the <gui>Open</gui> dialog. The dialog will wait for the user to choose an image; when they do, <code>dialog.run</code> will return the value <code>Gtk::RESPONSE_ACCEPT</code> (it would return <code>Gtk::RESPONSE_CANCEL</code> if the user clicked <gui>Cancel</gui>). The <code>switch</code> statement tests for this.</p>
    </item>
    <item>
    <p>We hide the <gui>Open</gui> dialog because we don't need it any more. The dialog would be hidden later anyway, as it is only a local variable and is
    destroyed (and therefore hidden) when the scope ends.</p>
    </item>
    <item><p>Assuming that the user did click <gui>Open</gui>, the next line loads the
    file into the <code>Gtk::Image</code> so that it is displayed.</p>
    </item>
  </list>
</section>

<section id="build2">
  <title>Bygg och kör programmet</title>
  <p>All of the code should now be ready to go. Click <guiseq><gui>Build</gui><gui>Build Project</gui></guiseq> to build everything again, and then <guiseq><gui>Run</gui><gui>Execute</gui></guiseq> to start the application.</p>
  <p>If you haven't already done so, choose the <file>Debug/src/image-viewer</file> application in the dialog that appears. Finally, hit <gui>Run</gui> and enjoy!</p>
</section>

<section id="impl">
 <title>Referensimplementation</title>
 <p>Om du stöter på problem med handledningen kan du jämföra din kod med denna <link href="image-viewer/image-viewer.cc">referenskod</link>.</p>
</section>

<section id="next">
  <title>Nästa steg</title>
  <p>Här är några idéer på hur du kan utöka denna enkla demonstration:</p>
  <list>
   <item>
   <p>Låt användaren välja en katalog snarare än en fil, och tillhandahåll kontroller för att gå igenom alla bilderna i en katalog.</p>
   </item>
   <item>
   <p>Tillämpa slumpmässiga filter och effekter till bilden då den läses in och låt användaren spara den ändrade bilden.</p>
   <p><link href="http://www.gegl.org/api.html">GEGL</link> har kraftfulla bildmanipuleringsförmågor.</p>
   </item>
   <item>
   <p>Allow the user to load images from network shares, scanners, and other more complicated sources.</p>
   <p>You can use <link href="http://library.gnome.org/devel/gio/unstable/">GIO</link> to handle network file transfers and the like, and <link href="http://library.gnome.org/devel/gnome-scan/unstable/">GNOME Scan</link> to handle scanning.</p>
   </item>
  </list>
</section>


</page>
