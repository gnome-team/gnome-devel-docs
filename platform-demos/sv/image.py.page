<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="image.py" xml:lang="sv">
  <info>
    <title type="text">Image (Python)</title>
    <link type="guide" xref="beginner.py#display-widgets"/>
    <link type="seealso" xref="properties.py"/>
    <link type="next" xref="strings.py"/>
    <revision version="0.2" date="2012-06-14" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email its:translate="no">sindhus@live.in</email>
      <years>2014</years>
    </credit>

    <desc>En komponent som visar en bild</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Image</title>
  <media type="image" mime="image/png" src="media/image.png"/>
  <p>Denna GtkApplication visar en bildfil från aktuell katalog.</p>

  <note><p>Om bildfilen inte lästes in korrekt kommer bilden innehålla en ”trasig bild”-ikon. <file>filnamn.png</file> behöver vara i aktuell katalog för att denna kod ska fungera.</p></note>

  <links type="section"/>

  <section id="code">
  <title>Kod som använts för att generera detta exempel</title>

  <code mime="text/x-python" style="numbered">
  from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # create a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Välkommen till GNOME", application=app)
        self.set_default_size(300, 300)

        # skapa en bild
        image = Gtk.Image()
        # ställ in bildens innehåll till filen filnamn.png
        image.set_from_file("gnome-image.png")
        # lägg till bilden till fönstret
        self.add(image)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)

  </code>

  <p>Ett annat sätt att få det som vi har i exemplet är att skapa bilden som en instans av en annan klass och lägga till den i instansen av <code>MyWindow</code> i metoden <code>do_activate(self)</code>:</p>

  <code mime="text/x-python">
    # en klass för att skapa ett fönster
    class MyWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        Gtk.Window.__init__(self, title="Välkommen till GNOME", application=app)
        self.set_default_size(300, 300)

    # en klass för att skapa en bild
    class MyImage(Gtk.Image):
    def __init__(self):
        Gtk.Image.__init__(self)
        self.set_from_file("gnome-image.png")

    class MyApplication(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        # skapa en instans av MyWindow
        win = MyWindow(self)
        # skapa en instans av MyImage och lägg till den till fönstret
        win.add(MyImage())
        # visa fönstret och allt som finns i det
        win.show_all()
  </code>

  <note>
    <p>För att använda denna kodsnutt kommer du behöva lägga till koden som importerar <code>Gtk</code> och <code>GdkPixbuf</code> från <code>gi.repository</code> och rader som instansierar <code>MyApplication</code>-fönstret.</p>
  </note>
  </section>

  <section id="methods">
  <title>Användbara metoder för en Image-komponent</title>

  <list>
    <item>
      <p>För att läsa in en bild över ett nätverk, använd <code>set_from_pixbuf(pixbuf)</code>, där <code>pixbuf</code> är en <link href="https://developer.gnome.org/gdk-pixbuf/unstable/index.html"> GdkPixbuf</link>.</p>
      <code mime="text/python">
        from gi.repository import Gtk
        from gi.repository import GdkPixbuf
        import sys

        class MyWindow(Gtk.ApplicationWindow):
            # create a window
            def __init__(self, app):
                Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
                self.set_default_size(300, 300)

                # create a pixbuf from file filename="gnome-image.png", with width=32
                # and height=64 amd boolean preserve_aspect_ratio=False.
                pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("gnome-image.png", 64, 128, False)

                # create an image
                image = Gtk.Image()
                # set the content of the image as the pixbuf
                image.set_from_pixbuf(pixbuf)
                # add the image to the window
                self.add(image)
        </code>
        <p>If <code>preserve_aspect_ratio=True</code> we can use
        <code>new_from_file_at_size(filename, width, height)</code>. If
        <code>width</code> or <code>height</code> is <code>-1</code>, it is
        not constrained.</p>
        <p>For loading from an input stream, see <code>new_from_stream()</code>
        and <code>new_from_stream_at_scale()</code> in the documentation.</p>
    </item>
  </list>
  </section>

  <section id="references">
    <title>API-referenser</title>

    <p>I detta exempel använde vi följande:</p>
    <list>
      <item>
        <p><link href="https://developer.gnome.org/gtk3/unstable/GtkImage.html"> GtkImage</link></p>
      </item>
      <item>
        <p><link href="https://developer.gnome.org/gtk3/unstable/GtkWindow.html"> GtkWindow</link></p>
      </item>
    </list>

  </section>
</page>
