<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="message-board.c" xml:lang="sv">

  <info>
    <title type="text">Anslagstavla (C)</title>
    <link type="guide" xref="c#examples"/>

    <desc>Ett enkelt program som använder WebKitGTK+ och DOM.</desc>

    <revision pkgversion="0.1" version="0.1" date="2010-12-06" status="draft"/>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2010</years>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Anslagstavla</title>

<synopsis>
  <p>I denna handledning kommer du lära dig:</p>
  <list style="compact">
    <item><p>Hur du visar en webbsida med WebKit.</p></item>
    <item><p>Hur du manipulerar en webbsidas innehåll med WebKits DOM-funktioner.</p></item>
  </list>
  <p>Denna handledning antar att du är bekant med programmeringsspråket C och har en grundläggande förståelse av GTK+, inklusive hur du skapar och placerar komponenter och hur du ansluter återanropsfunktioner till signaler. Se <link xref="image-viewer.c"/> för att lära dig grunderna i GTK+.</p>
</synopsis>

<media type="video" mime="video/ogg" src="media/message-board.ogv"/>

<links type="section"/>

<section id="create">
  <title>Skapa ett projekt i Anjuta</title>

  <p>GNOME-plattformen inkluderar WebKitGTK+, byggd på det kraftfulla HTML-ramverket WebKit. WebKit används runt om i GNOME, inte bara för att visa webbsidor på internet, utan också för att skapa rika användargränssnitt som lätt kan stilsättas med CSS.</p>

  <p>I denna handledning kommer du skapa en enkel anslagstavla med WebKit. Anslagstavlan kommer låta dig mata in lite text och lägga till den till en lista med meddelanden i HTML. Innan du börjar behöver du konfigurera ett projekt i Anjuta.</p>

  <steps>
    <item><p>Klicka i Anjuta på <guiseq><gui>Arkiv</gui><gui>Ny</gui><gui>Projekt</gui></guiseq> för att öppna den nya projektguiden.</p></item>
    <item><p>Välj <gui>GTK+ (enkel)</gui> från fliken <gui>C</gui> och klicka på <gui>Fortsätt</gui>.</p></item>
    <item><p>Fyll i dina detaljer på sidan <gui>Grundläggande information</gui>. Använd <input>message-board</input> som projektnamn. Klicka på <gui>Fortsätt</gui>.</p></item>
    <item><p>Inaktivera alternativet <gui>Använd GtkBuilder för användargränssnitt</gui> då denna handledning bygger användargränssnittet manuellt.</p>
    </item>
    <item><p>Du behöver säga till Anjuta att du använder WebKitGTK+ för detta projekt. På sidan <gui>Projektalternativ</gui>, välj <gui>Konfigurera externa paket</gui>. Klicka på <gui>Fortsätt</gui>. På sidan <gui>Konfigurera externa paket</gui>, kryssa i <gui>webkitgtk-3.0</gui>.</p></item>
  </steps>

  <p>Efter att du slutför guiden för nya projekt, öppna filen <file>src/main.js</file> från flikarna <gui>Projekt</gui> eller <gui>Filer</gui>. Anjuta kommer ha fyllt i denna med lite grundläggande GTK+-kod från sina mallar. Eftersom du skapar ett WebKit-projekt behöver du först inkludera WebKit-headerfilerna. Lägg till följande rad efter raden som inkluderar <code>gtk/gtk.h</code>:</p>

  <code>#include &lt;webkit/webkit.h&gt;</code>

  <p>Verify that everything works by building what you have so far.
  Click <guiseq><gui>Build</gui><gui>Build Project</gui></guiseq> or
  just press <keyseq><key>Shift</key><key>F7</key></keyseq>. The first
  time you build, you will be asked for some configure options. Just
  accept the defaults and click <gui>Execute</gui>.</p>

  <p>You should now be able to run the program. Click <guiseq>
  <gui>Run</gui><gui>Execute</gui></guiseq> or just press <key>F3</key>.
  You should see an empty window appear.</p>
</section>

<section id="webview">
  <title>Skapa en layout för ditt fönster och din webbvy</title>

  <p>Now that you can show a window, it's time to start working with WebKit.
  For this tutorial, you'll create a text entry and a web view and pack
  them both into a window. Find the function <code>create_window</code> and
  replace it with the following:</p>

<code style="numbered" mime="text/C"><![CDATA[
static GtkWidget*
create_window (void)
{
    GtkWidget *window, *box, *scroll, *view, *entry;

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size (GTK_WINDOW (window), 400, 400);
    gtk_window_set_title (GTK_WINDOW (window), "Message Board");
    g_signal_connect (window, "delete-event", G_CALLBACK (gtk_main_quit), NULL);

    box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 6);
    gtk_container_set_border_width (GTK_CONTAINER (box), 6);
    gtk_container_add (GTK_CONTAINER (window), box);

    entry = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (box), entry, FALSE, FALSE, 0);

    scroll = gtk_scrolled_window_new (NULL, NULL);
    g_object_set (scroll, "shadow-type", GTK_SHADOW_IN, NULL);
    gtk_box_pack_start (GTK_BOX (box), scroll, TRUE, TRUE, 0);

    view = webkit_web_view_new ();
    gtk_container_add (GTK_CONTAINER (scroll), view);
    webkit_web_view_load_string (WEBKIT_WEB_VIEW (view),
                                 "<html><body></body></html>",
                                 "text/html",
                                 "UTF-8",
                                 NULL);

    gtk_widget_show_all (GTK_WIDGET (box));
    return window;
}
]]></code>

  <p>You first create a <code>GtkWindow</code> object and set its title and
  default size. You also connect the <code>gtk_main_quit</code> function to the
  <code>delete-event</code> signal. The <code>delete-event</code> signal is
  emitted when the window is closed. The <code>gtk_main_quit</code> function is
  part of GTK, and it quits the application.</p>

  <p>You then create a vertical box and add it to the window. A window can only
  hold a single child widget, so you need to use a box to add multiple widgets.
  The second argument to <code>gtk_box_new</code> sets the amount of padding
  (in pixels) between each child, and the next line puts a six-pixel border
  around the entire thing.</p>

  <p>You next create a <code>GtkEntry</code> object and pack it into the box.
  The third and fourth arguments to <code>gtk_box_pack_start</code> specify that
  the entry shouldn't take up any extra space the box has available. The fourth
  argument is the amount of padding you want around the entry. In this case,
  you set the padding to zero, because you're allowing the box to handle all
  the padding.</p>

  <p>Before you add a web view, you have to create a scrolled window to put it
  inside of. The scrolled window will place scrollbars on the right and bottom
  when necessary, and prevent your web view from filling your entire screen.
  This time, you pass <code>TRUE</code> and <code>TRUE</code> to
  <code>gtk_box_pack_start</code> to allow the scrolled window (and thus, the
  web view) to use any extra space available in the box.</p>

  <p>Finally, you create a <code>WebKitWebView</code> and add it to the scrolled
  window. Then load a very basic HTML page into the web view by calling
  <code>webkit_web_view_load_string</code> with the following arguments:</p>

  <terms>
    <item>
      <title><code>WEBKIT_WEB_VIEW (view)</code></title>
      <p>The view itself. Because <code>view</code> is typed as a
      <code>GtkWidget*</code>, you have to use <code>WEBKIT_WEB_VIEW</code>
      to safely cast the object.</p>
    </item>
    <item>
      <title><code>"&lt;html&gt;&lt;body&gt;&lt;/body&gt;&lt;/html&gt;"</code></title>
      <p>Den enklaste HTML-du kan skriva.</p>
    </item>
    <item>
      <title><code>"text/html"</code></title>
      <p>MIME-typen för innehållet du tillhandahöll. I detta fall använder du vanlig HTML.</p>
    </item>
    <item>
      <title><code>"UTF-8"</code></title>
      <p>Teckenkodningen för innehållet du tillhandahöll. Även om du bara använde ASCII-tecken så är det en god idé att ange UTF-8. UTF-8 används som standardkodning för hela GNOME-plattformen.</p>
    </item>
    <item>
      <title><code>NULL</code></title>
      <p>The base URI. You don't need it in this simple example, but you might
      want to provide a <sys>file:</sys> URI if you add images or other features
      where you want to use relative URI references.</p>
    </item>
  </terms>

  <note style="sidebar">
    <p>Every time you add a widget, you have to call <code>gtk_widget_show</code>
    on it for it to be visible. If you call <code>gtk_widget_show_all</code> on
    a container widget like a <code>GtkBox</code>, GTK+ will automatically show
    all the widgets inside the container, to any depth. Sometimes you don't
    want to call <code>gtk_widget_show_all</code>, such as when you want to
    dynamically hide and show some widgets in response to events.</p>
  </note>

  <p>Finally, you have to call <code>gtk_widget_show_all</code> on the box.
  Otherwise, none of the widgets you created will be visible. (The window is
  shown in the <code>main</code> function with <code>gtk_widget_show</code>.)</p>

  <p>Build and run the message board again. You should see a window with a text
  entry and a web view. It doesn't do anything yet because the text entry and
  the web view don't know anything about each other.</p>
</section>

<section id="signals">
  <title>Anslut signaler</title>

  <p>Now you want to make the message board actually <em>do</em> something
  when you enter text into the text entry. To do this, connect a callback
  function to the <code>activate</code> signal of <code>entry</code>. GTK+
  emits the <code>activate</code> signal whenever the user presses
  <key>Enter</key> in the entry. Add the following into <code>create_window</code>,
  anywhere after both <code>entry</code> and <code>view</code> have been defined:</p>

<code><![CDATA[
g_signal_connect (entry, "activate", G_CALLBACK (entry_activate_cb), view);
]]></code>

  <p>You then have to actually define <code>entry_activate_cb</code>. Define
  it as follows, anywhere above <code>create_window</code>:</p>

<code style="numbered"><![CDATA[
static void
entry_activate_cb (GtkEntry *entry, WebKitWebView *view)
{
    WebKitDOMDocument *document;
    WebKitDOMElement *body, *div;

    document = webkit_web_view_get_dom_document (view);
    body = webkit_dom_document_query_selector (document, "body", NULL);
    div = webkit_dom_document_create_element (document, "div", NULL);
    webkit_dom_node_set_text_content (WEBKIT_DOM_NODE (div),
                                      gtk_entry_get_text (entry),
                                      NULL);
    webkit_dom_node_append_child (WEBKIT_DOM_NODE (body),
                                  WEBKIT_DOM_NODE (div),
                                  NULL);
    gtk_entry_set_text (entry, "");
}
]]></code>

  <p>The first thing you do is get a <code>WebKitDOMDocument</code> object
  that represents the HTML document displayed in <code>view</code>. The DOM
  classes and methods in WebKit allow you to inspect and manipulate the HTML
  document, and work very similarly to the DOM APIs you might already know
  from JavaScript.</p>

  <p>Once you have the document, you want to get the <code>body</code> element
  so that you can add <code>div</code> elements to it. The
  <code>webkit_dom_document_query_selector</code> function lets you find an
  element in the document using CSS selectors. This keeps you from having to
  write tedious loops to traverse the document.</p>

  <p>Next, you create a new <code>div</code> element to hold the message. Every
  element you create has to be attached to a document, so the function to create
  an element takes the <code>WebKitDOMDocument</code> as its first arguments.
  You then set the text content of the element to the contents of the text entry.
  Because <code>gtk_entry_get_text</code> returns a <code>const gchar*</code>,
  you don't have to free the result.</p>

  <p>Finally, you append the new <code>div</code> element to the body and
  clear out the text entry so you can type something new. Build and run the
  program again and test it for yourself.</p>
</section>


<section id="css">
  <title>Få det att se ut bättre med CSS</title>

  <p>At this point, your program is completely functional, but not very pretty.
  You can style the message display with CSS, just like you can with any other
  HTML page. There are many ways you could attach some CSS to the page: You
  could add it in the initial HTML document. You could inline it in the
  <code>style</code> attribute of the <code>div</code> elements. You could
  even programmatically construct it using the DOM APIs.</p>

  <p>In this tutorial, you'll attach the CSS using the <code>user-stylesheet-uri</code>
  property of the <code>WebKitWebSetting</code> object attached to your web view.
  In a more complete application, you would want to save and load your HTML file.
  Keeping the style information outside the actual HTML means that you can change
  the styling completely within your application, without having to change users'
  files. You would normally just install a file along with your application, but
  just to keep everything in one file for this demo, we'll use a trick called a
  data URI. First, define the CSS as a static string near the top of your file.</p>

<code><![CDATA[
static const guchar CSS[] =
"body { margin: 0; padding: 0; }\n"
"div { "
" -webkit-border-radius: 2px;"
" background: -webkit-gradient(linear, 0% 100%, 0% 0%,"
" from(#f1f1f1), to(white));"
" border: solid 1px #c6c6c6;"
" -webkit-box-shadow: 0px 0px 2px #c6c6c6;"
" margin: 12px; padding: 6px;"
"}";
]]></code>

  <p>All you have in this example are <code>div</code> elements inside a
  <code>body</code> element. If you created more complicated HTML, you could
  use whatever CSS is necessary. In fact, if you're comfortable with CSS, you
  should trying changing this to something you like better.</p>

  <p>To apply the CSS, you set the <code>user-stylesheet-uri</code> in the
  <code>create_window</code> function, anywhere after <code>view</code> has
  already been defined.</p>

<code><![CDATA[
tmp = g_base64_encode (CSS, strlen((gchar *) CSS));
css = g_strconcat ("data:text/css;charset=utf-8;base64,",
                   tmp, NULL);
g_object_set (webkit_web_view_get_settings (WEBKIT_WEB_VIEW (view)),
              "user-stylesheet-uri", css, NULL);
g_free (css);
g_free (tmp);
]]></code>

  <p>Also, make sure to add variable declarations for <code>tmp</code>
  and <code>css</code> to the top of <code>create_window</code>.</p>

<code>
gchar *tmp, *css;
</code>

 <p>A data URI starts with <sys>data:</sys> and some information about
 the content type and how the data is encoded. The actual data follows after
 a comma, in this case encoded in Base64. Unlike other URI schemes like
 <sys>http:</sys>, <sys>ftp:</sys>, and <sys>file:</sys>, the <sys>data:</sys>
 URI scheme doesn't specify where to find a file to load. Rather, it gives
 the entire contents of the file.</p>

 <p>The code above first encodes your CSS definitions in Base64, then
 combines that with a fixed string to create a data URI. The
 <code>g_strconcat</code> function can take any number of string arguments
 and concatenate them all together, so you have to pass <code>NULL</code>
 as the final argument so it knows when to stop. And don't forget to free
 those temporary strings after you set the stylesheet property.</p>

 <p>Build and run the program again. It should now work exactly the same
 as at the end of the last section, except the messages will be nicely
 styled with a border and a subtle background gradient.</p>
</section>

<section id="more">
  <title>Lär dig mer</title>

  <p>Denna handledning visade dig hur du skapar ett enkelt program med GTK+ och WebKit, inklusive att visa ett dokument och manipulera dess innehåll. För att skapa ett riktigt program vill du förmodligen göra lite mer. Försök lägga till egen funktionalitet. Här är några idéer:</p>

  <list>
    <item><p>Om du är bekväm med CSS kan du försöka ändra stilen för meddelandevisningen. CSS är enkelt att komma igång med, men blir alltmer kraftfullt. Det finns en stor mängd CSS-handledningar på internet, och i princip allt du kan göra på webben kan du göra i detta program.</p></item>

    <item><p>För tillfället förlorar du alla dina meddelanden närhelst du stänger anslagstavlan. Försök att spara HTML-innehållet efter varje post, och läsa in den sparade filen (om den finns) vid uppstart.</p></item>

    <item><p>Om du behåller dina meddelanden länge kommer du börja fundera när du postade dem. Lägg till en tidsstämpel till varje meddelande då det postas. Du kommer troligen vilja skapa några <code>div</code>-barnelement med olika klasser som du kan stilsätta i din CSS.</p>
    </item>

    <item><p>Detta program behåller meddelanden för alltid. Tänk på sätt som du skulle kunna låta användaren ta bort meddelanden. Kanske du vill att meddelanden ska försvinna automatiskt efter att de blivit för gamla, eller efter att det finns ett visst antal meddelanden före dem. Eller så skulle du kunna lägga till en länk i varje meddelande för att ta bort det. Du skulle till och med kunna åsidosätta snabbvalsmenyn då du högerklickar på ett meddelande. Dessa funktioner involverar att utforska WebKits DOM-API mera.</p></item>
  </list>
</section>
</page>
