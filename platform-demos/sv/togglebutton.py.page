<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="togglebutton.py" xml:lang="sv">
  <info>
    <title type="text">ToggleButton (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="switch.py"/>
    <revision version="0.1" date="2012-05-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>En knapp som behåller sitt tillstånd</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>ToggleButton</title>
  <media type="image" mime="image/png" src="media/togglebutton.png"/>
  <p>När denna ToggleButton är i aktivt läge så snurrar väntesnurran.</p>

  <links type="section"/>

  <section id="code">
    <title>Kod som använts för att generera detta exempel</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(
            self, title="ToggleButton-exempel", application=app)
        self.set_default_size(300, 300)
        self.set_border_width(30)

        # en väntesnurreanimering
        self.spinner = Gtk.Spinner()
        # med extra horisontellt utrymme
        self.spinner.set_hexpand(True)
        # med extra vertikalt utrymme
        self.spinner.set_vexpand(True)

        # en växlingsknapp
        button = Gtk.ToggleButton.new_with_label("Starta/stoppa")
        # anslut signalen "toggled" som utges av växlingsknappen
        # när dess tillstånd ändras till återanropsfunktionen toggled_cb
        button.connect("toggled", self.toggled_cb)

        # ett rutnät för att allokera komponenterna
        grid = Gtk.Grid()
        grid.set_row_homogeneous(False)
        grid.set_row_spacing(15)
        grid.attach(self.spinner, 0, 0, 1, 1)
        grid.attach(button, 0, 1, 1, 1)

        # lägg till rutnätet till fönstret
        self.add(grid)

    # återanropsfunktion för signalen "toggled"
    def toggled_cb(self, button):
        # starta väntesnurran om växlingsknappen är aktiv
        if button.get_active():
            self.spinner.start()
        # stoppa den annars
        else:
            self.spinner.stop()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>

  <section id="methods">
    <title>Användbara metoder för en ToggleButton-komponent</title>
    <p>På rad 22 ansluts signalen <code>"toggled"</code> till återanropsfunktionen <code>toggled_cb()</code> med <code><var>komponent</var>.connect(<var>signal</var>, <var>återanropsfunktion</var>)</code>. Se <link xref="signals-callbacks.py"/> för en utförligare förklaring.</p>
  </section>

  <section id="references">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToggleButton.html">GtkToggleButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWidget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkSpinner.html">GtkSpinner</link></p></item>
    </list>
  </section>
</page>
