<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="paned.js" xml:lang="el">
  <info>
    <title type="text">Με φάτνωμα (JavaScript)</title>
    <link type="guide" xref="beginner.js#layout"/>
    <revision version="0.1" date="2013-06-25" status="review"/>

    <credit type="author copyright">
      <name>Meg Ford</name>
      <email its:translate="no">megford@gnome.org</email>
      <years>2013</years>
    </credit>

    <desc>Ένα γραφικό στοιχείο με δύο ρυθμίσιμα φατνώματα</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Σε φάτνωμα</title>
  <media type="image" mime="image/png" src="media/paned.png"/>
  <p>Δύο εικόνες σε δύο ρυθμίσιμα φατνώματα οριζόντια στοιχισμένες.</p>

  <links type="section"/>

  <section id="code">
    <title>Ο χρησιμοποιούμενος κώδικας για παραγωγή αυτού παραδείγματος</title>
    <code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';
const Gtk = imports.gi.Gtk;

class PanedExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({ application_id: 'org.example.panedexample' });

       // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this.window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {
        // Create the application window
        this.window = new Gtk.ApplicationWindow  ({ application: this.application,
                                                    window_position: Gtk.WindowPosition.CENTER,
                                                    title: "Paned Window Example",
                                                    default_width: 450,
                                                    default_height: 350,
                                                    border_width: 10 });

        // a new widget with two adjustable panes,
        // one on the left and one on the right
        this.paned = Gtk.Paned.new(Gtk.Orientation.HORIZONTAL);

        // two images
        this.image1 = new Gtk.Image();
        this.image1.set_from_file("gnome-image.png");
        this.image2 = new Gtk.Image();
        this.image2.set_from_file("tux.png");

        // add the first image to the left pane
        this.paned.add1(this.image1);
        // add the second image to the right pane
        this.paned.add2(this.image2)

        // add the panes to the window
        this.window.add(this.paned)
        this.window.show_all();
    }
};

// Run the application
let app = new PanedExample();
app.application.run (ARGV);
</code>
  </section>

  <section id="references">
    <title>Αναφορές API</title>
    <p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
    <list>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Paned.html">GtkPaned</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/gtk3-Standard-Enumerations.html#GtkOrientation">Standard Enumerations</link></p></item>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Image.html">GtkImage</link></p></item>
    </list>
  </section>
</page>
