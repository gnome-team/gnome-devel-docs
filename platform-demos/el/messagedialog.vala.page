<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="messagedialog.vala" xml:lang="el">
  <info>
  <title type="text">MessageDialog (Vala)</title>
    <link type="guide" xref="beginner.vala#windows"/>
    <revision version="0.1" date="2012-04-07" status="stub"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ένα παράθυρο μηνύματος</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>MessageDialog</title>
  <media type="image" mime="image/png" src="media/messagedialog.png"/>
  <p>Ένας αποκλειστικός διάλογος μηνύματος που μπορεί να προκαλέσει την έκρηξη του κόσμου.</p>

<code mime="text/x-csharp" style="numbered">
//Ένα παράθυρο στην εφαρμογή
public class Window : Gtk.ApplicationWindow {
	public Window (Application app) {
		Object (application: app, title: "Gtk.MessageDialog Example");

		var label = new Gtk.Label ("This application goes boom!");
		this.add (label);

		var message_action = new SimpleAction ("message", null);
		message_action.activate.connect (message);
		this.add_action (message_action);

		this.set_default_size (400, 200);
		this.show_all ();
	}

	void dialog_response (Gtk.Dialog dialog, int response_id) {
		switch (response_id) {
			case Gtk.ResponseType.OK:
				print ("*boom*\n");
				break;
			case Gtk.ResponseType.CANCEL:
				print ("good choice\n");
				break;
			case Gtk.ResponseType.DELETE_EVENT:
				print ("dialog closed or cancelled\n");
				break;
		}
			dialog.destroy();
	}

	void message (SimpleAction simple, Variant? parameter) {
		var messagedialog = new Gtk.MessageDialog (this,
                            Gtk.DialogFlags.MODAL,
                            Gtk.MessageType.WARNING,
                            Gtk.ButtonsType.OK_CANCEL,
                            "This action will cause the universe to stop existing.");

		messagedialog.response.connect (dialog_response);
		messagedialog.show ();
	}
}

//Αυτή είναι η εφαρμογή
public class Application : Gtk.Application {
	protected override void activate () {
		new Window (this);
	}

	protected override void startup () {
		base.startup ();

		var menu = new Menu ();
		menu.append ("Message", "win.message");
		menu.append ("Quit", "app.quit");
		this.app_menu = menu;

		var quit_action = new SimpleAction ("quit", null);
		//quit_action.activate.connect (this.quit);
		this.add_action (quit_action);
	}

	public Application () {
		Object (application_id: "org.example.application");
	}
}

//Η κύρια συνάρτηση δημιουργεί την εφαρμογή και την εκτελεί
int main (string[] args) {
	return new Application ().run (args);
}
</code>
<p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ResponseType.html">Gtk.ResponseType</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.MessageDialog.html">Gtk.MessageDialog</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.DialogFlags.html">Gtk.DialogFlags</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.MessageType.html">Gtk.MessageType</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ButtonsType.html">Gtk.ButtonsType</link></p></item>
</list>
</page>
