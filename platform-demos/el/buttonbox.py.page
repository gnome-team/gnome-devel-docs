<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="buttonbox.py" xml:lang="el">
  <info>
    <title type="text">ButtonBox (Python)</title>
    <link type="guide" xref="beginner.py#layout"/>
    <link type="seealso" xref="button.py"/>
    <link type="next" xref="statusbar.py"/>
    <revision version="0.2" date="2012-08-01" status="stub"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ένας περιέκτης για διευθέτηση κουμπιών</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>ButtonBox</title>

  <media type="image" mime="image/png" src="media/buttonbox_calculator.png"/>
  <p>Μια αριθμομηχανή - τα κουμπιά περικλείονται σε οριζόντια ButtonBoxes.</p>

  <links type="section"/>

  <section id="code">
    <title>Ο χρησιμοποιούμενος κώδικας για παραγωγή αυτού παραδείγματος</title>
    <code mime="text/python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Calculator", application=app)
        self.set_default_size(350, 200)
        self.set_border_width(10)

        # an entry
        self.entry = Gtk.Entry()
        # with an initial text
        self.entry.set_text('0')
        # text aligned on the right
        self.entry.set_alignment(1)
        # the text in the entry cannot be modified writing in it
        self.entry.set_can_focus(False)

        # a grid
        grid = Gtk.Grid()
        grid.set_row_spacing(5)

        # to attach the entry
        grid.attach(self.entry, 0, 0, 1, 1)

        # the labels for the buttons
        buttons = [7, 8, 9, '/',
                   4, 5, 6, '*',
                   1, 2, 3, '-',
                   'C', 0, '=', '+']

        # each row is a ButtonBox, attached to the grid
        for i in range(4):
            hbox = Gtk.ButtonBox.new(Gtk.Orientation.HORIZONTAL)
            hbox.set_spacing(5)
            grid.attach(hbox, 0, i + 1, 1, 1)
            # each ButtonBox has 4 buttons, connected to the callback function
            for j in range(4):
                button = Gtk.Button(label=buttons[i * 4 + j])
                button.set_can_focus(False)
                button.connect("clicked", self.button_clicked)
                hbox.add(button)

        # some variables for the calculations
        self.first_number = 0
        self.second_number = 0
        self.counter = 0
        self.operation = ""

        # add the grid to the window
        self.add(grid)

    # callback function for all the buttons
    def button_clicked(self, button):
        # for the operations
        if button.get_label() == '+':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "plus"
        elif button.get_label() == '-':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "minus"
        elif button.get_label() == '*':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "multiplication"
        elif button.get_label() == '/':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "division"
        # for =
        elif button.get_label() == '=':
            self.do_operation()
            self.entry.set_text(str(self.first_number))
            self.counter = 1
        # for Cancel
        elif button.get_label() == 'C':
            self.first_number = 0
            self.second_number = 0
            self.counter = 0
            self.entry.set_text('')
            self.operation = ""
        # for a digit button
        else:
            new_digit = int(button.get_label())
            if self.entry.get_text() == 'error':
                number = 0
            else:
                number = int(self.entry.get_text())
            number = number * 10 + new_digit
            if self.counter == 0:
                self.first_number = number
            else:
                self.second_number = number
            self.entry.set_text(str(number))

    def do_operation(self):
        if self.operation == "plus":
            self.first_number += self.second_number
        elif self.operation == "minus":
            self.first_number -= self.second_number
        elif self.operation == "multiplication":
            self.first_number *= self.second_number
        elif self.operation == "division":
            try:
                self.first_number /= self.second_number
            except ZeroDivisionError:
                self.first_number = 0
                self.second_number = 0
                self.counter = 0
                self.entry.set_text('error')
                self.operation = ""
                return
        else:
            self.first_number = 0
            self.second_number = 0
            self.counter = 0
            self.entry.set_text('error')


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Χρήσιμες μέθοδοι για ένα γραφικό στοιχείο ButtonBox</title>
    <list>
      <item><p>Η διάταξη του ButtonBox ορίζεται με <code>set_layout(layout)</code>, όπου <code>layout</code> μπορεί να είναι <code>Gtk.ButtonBoxStyle.SPREAD</code> (τα κουμπιά διασπείρονται εξίσου κατά μήκος του πλαισίου), <code>Gtk.ButtonBoxStyle.EDGE</code> (τα κουμπιά τοποθετούνται στις άκρες του πλαισίου), <code>Gtk.ButtonBoxStyle.START</code> (τα κουμπιά ομαδοποιούνται προς την αρχή του πλαισίου), <code>Gtk.ButtonBoxStyle.END</code> (τα κουμπιά ομαδοποιούνται προς το τέλος του πλαισίου), <code>Gtk.ButtonBoxStyle.CENTER</code> (τα κουμπιά κεντράρονται στο πλαίσιο).</p></item>
      <item><p>Ο <code>set_child_secondary(button, is_secondary)</code> ορίζει εάν το <code>button</code> πρέπει να εμφανιστεί σε μια δευτερεύουσα ομάδα θυγατρικών. Μια τυπική χρήση ενός δευτερεύοντος θυγατρικού είναι το βοηθητικό κουμπί σε ένα διάλογο. Αυτή η ομάδα εμφανίζεται μετά τα άλλα θυγατρικά εάν ο τύπος είναι <code>START</code>, <code>SPREAD</code> ή <code>EDGE</code> και πριν τα άλλα θυγατρικά εάν ο τύπος είναι <code>END</code>. Εάν ο τύπος είναι <code>START</code> ή <code>END</code>, τότε τα δευτερεύοντα θυγατρικά στοιχίζονται στην άλλη άκρη του πλαισίου κουμπιού από τα κύρια θυγατρικά. Για τους άλλους τύπους, εμφανίζονται αμέσως δίπλα στα κύρια θυγατρικά.</p></item>
      <item><p>Το <code>set_child_non_homogeneous(button, is_non_homogeneous)</code> ορίζει εάν το θυγατρικό εξαιρείται από το ομογενές μέγεθος. Προεπιλεγμένη τιμή είναι <code>False</code>.</p></item>
      <item><p><code>set_spacing(spacing)</code> ορίζει το διάκενο, σε εικονοστοιχεία, μεταξύ των κουμπιών του πλαισίου.</p></item>
    </list>
  </section>

  <section id="references">
    <title>Αναφορές API</title>
    <p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkButtonBox.html">GtkButtonBox</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkBox.html">GtkBox</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkButton.html">GtkButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkEntry.html">GtkEntry</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkGrid.html">GtkGrid</link></p></item>
    </list>
  </section>
</page>
