<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="signals-callbacks.py" xml:lang="el">

<info>
  <title type="text">Σήματα και επανακλήσεις (Python)</title>
  <link type="guide" xref="beginner.py#theory"/>
  <link type="next" xref="button.py"/>
  <revision version="0.1" date="2012-06-16" status="draft"/>

  <desc>Μια εξήγηση των σημάτων και των επανακλήσεων στο GTK+.</desc>
  <credit type="author copyright">
    <name>Sebastian Pölsterl</name>
    <email its:translate="no">sebp@k-d-w.org</email>
    <years>2011</years>
  </credit>
  <credit type="editor">
    <name>Marta Maria Casetti</name>
    <email its:translate="no">mmcasetti@gmail.com</email>
    <years>2012</years>
  </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Σήματα και επανακλήσεις</title>

<links type="section"/>

<section id="overview">
<title>Επισκόπηση</title>

<p>Όπως τα περισσότερα πακέτα εργαλείων GUI, το GTK+ χρησιμοποιεί ένα πρότυπο προγραμματισμού οδηγούμενο από συμβάντα. Όταν ο χρήστης δεν κάνει τίποτα, το GTK+ κάθεται στον κύριο βρόχο και περιμένει για εισαγωγή. Εάν ο χρήστης εκτελέσει κάποια ενέργεια - ας πούμε, πάτημα ποντικιού - τότε ο κύριος βρόχος "ξυπνά" και διαβιβάζει ένα συμβάν στο GTK+.</p>

<p>Όταν τα γραφικά στοιχεία δέχονται ένα συμβάν, εκπέμπουν συχνά ένα ή περισσότερα σήματα. Τα σήματα ειδοποιούν το πρόγραμμά σας ότι "κάτι σημαντικό συνέβη" καλώντας τις συναρτήσεις που έχετε συνδέσει στο σήμα. Τέτοιες συναρτήσεις είναι γενικά γνωστές ως επανακλήσεις. Όταν οι επανακλήσεις σας καλούνται, θα πάρετε τυπικά κάποια ενέργεια. Αφού τελειώσει η επανάκληση, το GTK+ θα επιστρέψει στον κύριο βρόχο και θα περιμένει περισσότερες εισαγωγές του χρήστη.</p>

<p>Ένα γενικό παράδειγμα είναι: <code>handler_id = widget.connect("event", callback, data)</code>. <code>widget</code> είναι ένα στιγμιότυπο του γραφικού στοιχείου που δημιουργήσαμε νωρίτερα. Κατόπιν ενδιαφερόμαστε για το <code>event</code>. Κάθε γραφικό στοιχείο έχει τα δικά του συγκεκριμένα συμβάντα που μπορούν να συμβούν. Για παράδειγμα, εάν έχετε ένα Gtk.Button συνήθως θέλετε να συνδεθείτε με το "πατημένο" συμβάν: αυτό σημαίνει ότι όταν το κουμπί πατιέται, το σήμα εκδίδεται. Ένα άλλο παράδειγμα είναι το σήμα <code>notify::property</code>: όποτε μια <link xref="properties.py">ιδιότητα</link> τροποποιείται σε ένα GObject, αντί για απλή εκπομπή του σήματος <code>notify</code>, το GObject συσχετίζει ως μια λεπτομέρεια σε αυτή την εκπομπή σήματος το όνομα της τροποποιημένης ιδιότητας. Αυτό επιτρέπει στους πελάτες που επιθυμούν να ειδοποιηθούν για τις αλλαγές σε μόνο μια ιδιότητα να φιλτράρουν τα περισσότερα συμβάντα πριν τα δεχθούν. Τρίτο, το όρισμα επανάκλησης είναι το όνομα της συνάρτησης επανάκλησης, που περιέχει τον κώδικα που εκτελείται όταν τα σήματα του συγκεκριμένου τύπου εκδίδονται. Τελικά, το προαιρετικό όρισμα δεδομένων περιλαμβάνει οποιαδήποτε δεδομένα που θα έπρεπε μα περαστούν όταν το σήμα εκδίδεται.</p>

<p>Η συνάρτηση επιστρέφει έναν αριθμό (the <code>handler_id</code>) που αναγνωρίζει αυτό το συγκεκριμένο ζευγάρι σήματος-επανάκλησης. Αυτός ο αριθμός απαιτείται για αποσύνδεση από ένα τέτοιο σήμα που η συνάρτηση επανάκλησης δεν θα κληθεί κατά τη διάρκεια οποιωνδήποτε μελλοντικών ή τρεχόντων συνεχιζόμενων εκπομπών του σήματος στο οποίο συνδέθηκε, όπως στο <code>widget.disconnect(handler_id)</code>.</p>

</section>

<section id="references">

<title>Αναφορές</title>
<p><link href="http://developer.gnome.org/gobject/stable/signal.html">Σήματα</link> στην τεκμηρίωση GObject</p>
<p><link href="http://python-gtk-3-tutorial.readthedocs.org/en/latest/basics.html">Βασικά - Κύριος βρόχος και σήματα</link> στο μάθημα Python GTK+ 3</p>
</section>


</page>
