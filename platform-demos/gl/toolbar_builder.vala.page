<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="toolbar_builder.vala" xml:lang="gl">
  <info>
  <title type="text">Barra de ferramentas creada usando Glade (Vala)</title>
    <link type="guide" xref="beginner.vala#menu-combo-toolbar"/>
    <link type="seealso" xref="toolbar.vala"/>
    <link type="seealso" xref="grid.vala"/>
    <revision version="0.1" date="2012-05-08" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Unha barra de botóns</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2012-2013.</mal:years>
    </mal:credit>
  </info>

  <title>Barra de ferramentas creada empregando Glade</title>

  <media type="image" mime="image/png" src="media/toolbar.png"/>
  <p>This example is similar to <link xref="toolbar.vala"/>, except we use Glade to create the toolbar in an XML ui file.</p>

  <p>
  To create the toolbar using the <link href="http://glade.gnome.org/">Glade Interface Designer</link>:
  </p>
  <steps>
    <item><p>Open Glade, and save the file as <file>toolbar_builder.ui</file></p>
          <p><media type="image" src="media/glade_ui.png" width="900">
              Screenshot of Glade ui
           </media></p>
    </item>

    <item><p>Under <gui>Containers</gui> on the left hand side, right click on the toolbar icon and select <gui>Add widget as toplevel</gui>.</p>
          <p><media type="image" src="media/glade_select_toolbar.png">
           Screenshot of toolbar icon in Glade ui
          </media></p>
    </item>

    <item><p>Under the <gui>General</gui> tab on the bottom right, change the <gui>Name</gui> to <input>toolbar</input> and <gui>Show Arrow</gui> to <gui>No</gui>.</p>
          <p><media type="image" src="media/glade_toolbar_general.png">
             Screenshot of General tab
          </media></p>
    </item>

    <item><p>Under the <gui>Common</gui> tab, set <gui>Horizontal Expand</gui> to <gui>Yes</gui>.</p>
         <p><media type="image" src="media/glade_toolbar_common.png">
              Screenshot of Common tab
          </media></p>
     </item>

     <item><p>Prema co botón dereito sobre a barra de ferramentas na parte superior dereita e seleccione <gui>Editar</gui>. A xanela <gui>Editor da barra de ferramentas</gui> aparecerá.</p>
         <p><media type="image" src="media/glade_toolbar_edit.png">
             Screenshot of where to right click to edit toolbar.
          </media></p>
   </item>

   <item><p>Queremos engadir 5 ToolButtons: Novo, Abrir, Desfacer, Pantalla completa e Saír da pantalla completa. Primeiro, precisamos engadir o ToolButton Novo.</p>
     <steps>
       <item><p>Baixo a lapela <gui>Xerarquía</gui>, prema <gui>Engadir</gui>.</p></item>
       <item><p>Cambie o nome do TollItem a <input>new_button</input>.</p></item>
       <item><p>Scroll down and set <gui>Is important</gui> to <gui>Yes</gui>.  This will cause the label of the ToolButton to be shown, when you view the toolbar.</p></item>
       <item><p>Escriba o <gui>nome da acción</gui>: <input>app.new</input>.</p></item>
       <item><p>Cambie a <gui>Etiqueta</gui> a <input>Nova</input>.</p></item>
       <item><p>Seleccione o Id de inventario <gui>New</gui> desde o menú despregábel, ou o tipo <input>gtk-new</input>.</p></item>
     </steps>
     <p>Repita os pasos de arriba cos TollButtons que faltan, coas seguintes propiedades:</p>
  <table frame="all" rules="rows">
    <thead>
      <tr>
        <td><p>Nome</p></td>
        <td><p>É importante</p></td>
        <td><p>Nome da acción</p></td>
        <td><p>Etiqueta</p></td>
        <td><p>ID de inventario</p></td>
      </tr>
    </thead>
    <tbody>
    <tr>
      <td><p>open_button</p></td>
      <td><p>Si</p></td>
      <td><p>app.open</p></td>
      <td><p>Open</p></td>
      <td><p>gtk-open</p></td>
    </tr>
    <tr>
      <td><p>undo_button</p></td>
      <td><p>Si</p></td>
      <td><p>win.undo</p></td>
      <td><p>Desfacer</p></td>
      <td><p>gtk-undo</p></td>
    </tr>
    <tr>
      <td><p>fullscreen_button</p></td>
      <td><p>Si</p></td>
      <td><p>win.fullscreen</p></td>
      <td><p>Pantalla completa</p></td>
      <td><p>gtk-fullscreen</p></td>
    </tr>
    <tr>
      <td><p>leave_fullscreen_button</p></td>
      <td><p>Si</p></td>
      <td><p>win.fullscreen</p></td>
      <td><p>Saír do modo de pantalla completa</p></td>
      <td><p>gtk-leave-fullscreen</p></td>
    </tr>
    </tbody>
</table>
          <media type="image" src="media/glade_toolbar_editor.png">

          </media>
    </item>

    <item><p>Pechar o <gui>Editor de barra de ferramentas</gui>.</p>
   </item>

   <item><p>When our program will first start, we don't want the <gui>Leave Fullscreen</gui> ToolButton to be visible, since the application will not be in fullscreen mode.  You can set this in the <gui>Common</gui> tab, by clicking the <gui>Visible</gui> property to <gui>No</gui>.  The ToolButton will still appear in the interface designer, but will behave correctly when the file is loaded into your program code.</p>
          <p><media type="image" src="media/glade_visible_no.png">
                 Setting the visible property to No
          </media></p>
   </item>

    <item><p>Garde o seu traballo e saia de Glade.</p>
   </item>

   <item><p>The XML file created by Glade is shown below. This is the description of the toolbar.  At the time of this writing, the option to add the class Gtk.STYLE_CLASS_PRIMARY_TOOLBAR in the Glade Interface did not exist.  We can manually add this to the XML file.  To do this, add the following XML code at line 9 of <file>toolbar_builder.ui</file>:</p>
   <code><![CDATA[
  <style>
     <class name="primary-toolbar"/>
  </style>
  ]]></code>
  <p>Se non engade isto, o programa funcionará igual. Porén barra de ferramentas resultante semellará un pouco diferente á captura de pantalla da parte superior desta páxina.</p>
   </item>
</steps>
  <code mime="application/xml" style="numbered">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;interface&gt;
  &lt;!-- interface-requires gtk+ 3.0 --&gt;
  &lt;object class="GtkToolbar" id="toolbar"&gt;
    &lt;property name="visible"&gt;True&lt;/property&gt;
    &lt;property name="can_focus"&gt;False&lt;/property&gt;
    &lt;property name="hexpand"&gt;True&lt;/property&gt;
    &lt;property name="show_arrow"&gt;False&lt;/property&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="new_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;app.new&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;New&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-new&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="open_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;app.open&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Open&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-open&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="undo_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;win.undo&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Undo&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-undo&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="fullscreen_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;win.fullscreen&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Fullscreen&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-fullscreen&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="leave_fullscreen_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;win.fullscreen&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Leave Fullscreen&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-leave-fullscreen&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
  &lt;/object&gt;
&lt;/interface&gt;
</code>

  <p>Agora crearemos o código de embaixo, que engade a barra de ferrametnas desde o ficheiro que creamos.</p>
<code mime="text/x-csharp" style="numbered">/* This is the Window */
class MyWindow : Gtk.ApplicationWindow {

	/* Declare these two ToolButtons, as we will get them
	 * from the ui file (see lines 32 and 33), so we can
	 * hide() and show() them as needed.*/
	Gtk.ToolButton fullscreen_button;
	Gtk.ToolButton leave_fullscreen_button;

	/* Constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "Toolbar Example");

		this.set_default_size (400, 200);
		var grid = new Gtk.Grid ();
		this.add (grid);
		grid.show ();

		/* add the toolbar from the ui file */
		var builder = new Gtk.Builder ();
		try {
			builder.add_from_file ("toolbar_builder.ui");
		}
		/* Handle the exception */
		catch (Error e) {
			error ("Unable to load file: %s", e.message);
		}

		grid.attach (builder.get_object ("toolbar") as Gtk.Toolbar, 0, 0, 1, 1);

		/* get these objects from the ui file so we can toggle between them */
		fullscreen_button = builder.get_object ("fullscreen_button") as Gtk.ToolButton;
		leave_fullscreen_button = builder.get_object ("leave_fullscreen_button") as Gtk.ToolButton;

		/* create the "undo" window action action */
		var undo_action = new SimpleAction ("undo", null);
		undo_action.activate.connect (undo_callback);
		this.add_action (undo_action);

		/* create the "fullscreen" window action */
		var fullscreen_action = new SimpleAction ("fullscreen", null);
		fullscreen_action.activate.connect (fullscreen_callback);
		this.add_action (fullscreen_action);
	}

	void undo_callback (SimpleAction simple, Variant? parameter) {
			print ("You clicked \"Undo\".\n");
	}

	void fullscreen_callback (SimpleAction simple, Variant? parameter) {
		if ((this.get_window ().get_state () &amp; Gdk.WindowState.FULLSCREEN) != 0) {
			this.unfullscreen ();
			leave_fullscreen_button.hide ();
			fullscreen_button.show ();
		}
		else {
			this.fullscreen ();
			fullscreen_button.hide ();
			leave_fullscreen_button.show ();
		}
	}
}

/* This is the application */
class MyApplication : Gtk.Application {
	protected override void activate () {
		new MyWindow (this).show ();
	}

	protected override void startup () {
		base.startup ();

		/* Create the "new" action and add it to the app*/
		var new_action = new SimpleAction ("new", null);
		new_action.activate.connect (new_callback);
		this.add_action (new_action);

		/* Create the "open" action, and add it to the app */
		var open_action = new SimpleAction ("open", null);
		open_action.activate.connect (open_callback);
		this.add_action (open_action);

		/* You could also add the action to the app menu
		 * if you wanted to.
		 */
		//var menu = new Menu ();
		//menu.append ("New", "app.new");
		//this.app_menu = menu;
	}

	void new_callback (SimpleAction action, Variant? parameter) {
		print ("You clicked \"New\".\n");
	}

	void open_callback (SimpleAction action, Variant? parameter) {
			print ("You clicked \"Open\".\n");
	}
}

/* The main function creates the application and runs it. */
int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>

<p>Neste exemplo empregaremos o seguinte:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Toolbar.html">Gtk.Toolbar</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ToolButton.html">Gtk.Toolbutton</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Stock.html">Gtk.Stock</link></p></item>
</list>

</page>
