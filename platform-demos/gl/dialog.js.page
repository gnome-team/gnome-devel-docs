<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="dialog.js" xml:lang="gl">
  <info>
  <title type="text">Dialog (JavaScript)</title>
    <link type="guide" xref="beginner.js#windows"/>
    <revision version="0.1" date="2012-05-29" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>A popup window</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2012-2013.</mal:years>
    </mal:credit>
  </info>

  <title>Diálogo</title>
  <media type="image" mime="image/png" src="media/dialog.png"/>
  <p>A customizable popup window, which has a content area and an action area. This example dialog's content area contains a short message, and its action area contains a button which dismisses the dialog.</p>

<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class DialogExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application ({
            application_id: 'org.example.jsdialog',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
            this._window = new Gtk.ApplicationWindow  ({ application: this.application,
                                                         window_position: Gtk.WindowPosition.CENTER,
                                                         title: "Gtk.Dialog Example",
                                                         default_height: 50,
                                                         default_width: 250 });

        // Create a button
        this._button = new Gtk.Button ({label: "Click Me"});
        this._window.add (this._button);

        // Bind it to the function that creates the dialog
        this._button.connect ("clicked", this._createDialog.bind(this));

                // Show the window and all child widgets
                this._window.show_all();
    }

    _createDialog() {

        // Create the dialog
        this._dialog = new Gtk.Dialog ({ transient_for: this._window,
                         modal: true,
                         title: "A Gtk+ dialog" });

        // Create the dialog's content area, which contains a message
        this._contentArea = this._dialog.get_content_area();
        this._message = new Gtk.Label ({label: "This demonstrates a dialog with a label"});
        this._contentArea.add (this._message);

        // Create the dialog's action area, which contains a stock OK button
        this._actionArea = this._dialog.get_action_area();
        this._OKButton = Gtk.Button.new_from_stock (Gtk.STOCK_OK);
        this._actionArea.add (this._OKButton);

        // Connect the button to the function that handles what it does
        this._OKButton.connect ("clicked", this._OKHandler.bind(this));

        this._dialog.show_all();
    }

    _OKHandler(dialog, response_id) {

        // Destroy the dialog
        this._dialog.destroy();
    }

};

// Run the application
let app = new DialogExample ();
app.application.run (ARGV);
</code>
<p>Neste exemplo empregaremos o seguinte:</p>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Dialog.html">Gtk.Dialog</link></p></item>
</list>
</page>
