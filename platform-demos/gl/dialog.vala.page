<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="dialog.vala" xml:lang="gl">
  <info>
  <title type="text">Dialog (Vala)</title>
    <link type="guide" xref="beginner.vala#windows"/>
    <revision version="0.1" date="2012-04-07" status="stub"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A popup window</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2012-2013.</mal:years>
    </mal:credit>
  </info>

  <title>Diálogo</title>
  <media type="image" mime="image/png" src="media/dialog.png"/>
  <p>A dialog with the response signal hooked up to a callback function.</p>

      <code mime="text/x-csharp" style="numbered">
/* A window in the application. */
public class MyWindow : Gtk.ApplicationWindow {

	/* Constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "GNOME Button");

		this.window_position = Gtk.WindowPosition.CENTER;
		this.set_default_size (250,50);

		var button = new Gtk.Button.with_label ("Click Me");

		/* Connect the button's "clicked" signal to
		 * the signal handler (aka. this.callback function).
		 */
		button.clicked.connect (this.on_button_click);

		/* Add the button to this window and show it. */
		this.add (button);
		button.show ();
	}

	/* The signal handler for the buttons 'clicked' signal. */
	void on_button_click (Gtk.Button button) {
		var dialog = new Gtk.Dialog.with_buttons ("A Gtk+ Dialog", this,
                                                          Gtk.DialogFlags.MODAL,
                                                          Gtk.Stock.OK,
                                                          Gtk.ResponseType.OK, null);

		var content_area = dialog.get_content_area ();
		var label = new Gtk.Label ("This demonstrates a dialog with a label");

		content_area.add (label);

		/* Connect the 'response' signal of the dialog
		 * the signal handler.  It is emitted when the dialog's
		 * OK button is clicked.
		 */
		dialog.response.connect (on_response);

		/* Show the dialog and all the widgets. */
		dialog.show_all ();
	}

	/* Signal handler for the 'response' signal of the dialog. */
        void on_response (Gtk.Dialog dialog, int response_id) {

                /* To see the int value of the ResponseType. This is only
		 * for demonstration purposes.*/
                print ("response is %d\n", response_id);

		/* This causes the dialog to be destroyed. */
                dialog.destroy ();
        }

}

/* This is the application. */
public class MyApplication : Gtk.Application {

	/* The constructor of the application. */
	internal MyApplication () {
		Object (application_id: "org.example.MyApplication");
	}

	/* Override the 'activate' signal of GLib.Application. */
	protected override void activate () {

		/* Create a window for the this application and show it. */
		new MyWindow (this).show ();
	}
}

/* The main function creates and runs the application. */
public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>

<p>Neste exemplo empregaremos o seguinte:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Dialog.html">Gtk.Dialog</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Dialog.Dialog.with_buttons.html">Gtk.Dialog.with_buttons</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Label.html">Gtk.Label</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Dialog.get_content_area.html">get_content_area</link></p></item>
  <item><p><link xref="response-type"/></p></item>
</list>
</page>
