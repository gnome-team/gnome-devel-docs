<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="tooltip.js" xml:lang="pt-BR">
  <info>
  <title type="text">Tooltip (JavaScript)</title>
    <link type="guide" xref="beginner.js#misc"/>
    <link type="seealso" xref="toolbar.js"/>
    <revision version="0.1" date="2013-06-25" status="review"/>

    <credit type="author copyright">
      <name>Meg Ford</name>
      <email its:translate="no">megford@gnome.org</email>
      <years>2013</years>
    </credit>

    <desc>Add tips to your widgets</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Tooltip</title>
  <media type="image" mime="image/png" src="media/tooltip.png"/>
  <p>A toolbar with a tooltip (with an image) for a button.</p>
  <note><p>This example builds on the <link xref="toolbar.js">Toolbar</link> example.</p></note>
  <links type="section"/>
    
  <section id="code">
  <title>Code used to generate this example</title>
    <code mime="application/javascript" style="numbered">//!/usr/bin/gjs

imports.gi.versions.Gdk = '3.0';
imports.gi.versions.Gtk = '3.0';

const Gdk = imports.gi.Gdk;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk; 

class TooltipExample {
    // Create the application 
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jstooltip'
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this.window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this.window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            title: "Toolbar with Tooltips Example",
            default_width: 400,
            default_height: 200,
            border_width: 10
        });

        this.grid = new Gtk.Grid();

        this.toolbar = this._createToolbar();
        this.toolbar.set_hexpand(true);
        this.toolbar.show();

        this.grid.attach(this.toolbar, 0, 0, 1, 1);

        this.window.add(this.grid);

        this._newAction = new Gio.SimpleAction({ name: "new" });
        this._newAction.connect("activate", this._newCallback.bind(this));
        this.window.add_action(this._newAction);

        this._openAction = new Gio.SimpleAction({ name: "open" });
        this._openAction.connect("activate", this._openCallback.bind(this));
        this.window.add_action(this._openAction);

        this._undoAction = new Gio.SimpleAction({ name: "undo" });
        this._undoAction.connect("activate", this._undoCallback.bind(this));
        this.window.add_action(this._undoAction);

        this._fullScreenAction = new Gio.SimpleAction({ name: "fullscreenToggle" });
        this._fullScreenAction.connect("activate",
                                       this._fullScreenCallback.bind(this));
        this.window.add_action(this._fullScreenAction);

        this.window.show_all();
    }

    _createToolbar() {
        this.toolbar = new Gtk.Toolbar();
        this.toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR);

        // button for the "new" action
        this.newButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_NEW);
        // with a tooltip with a given text        
        this.newButton.set_tooltip_text("Create a new file");
        this.newButton.set_is_important(true);
        this.toolbar.insert(this.newButton, 0);
        this.newButton.show();
        this.newButton.set_action_name("win.new");

        // button for the "open" action
        this.openButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_OPEN);
        // with a tooltip with a given text in the Pango markup language        
        this.openButton.set_tooltip_markup("Open an &lt;i&gt;existing&lt;/i&gt; file");
        this.openButton.set_is_important(true);
        this.toolbar.insert(this.openButton, 1);
        this.openButton.show();
        this.openButton.set_action_name("win.open");

        // button for the "undo" action
        this.undoButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_UNDO);
        // with a tooltip with an image
        // set true the property "has-tooltip"        
        this.undoButton.set_property("has-tooltip", true);
        // connect to the callback function that for the tooltip
        // with the signal "query-tooltip"
        this.undoButton.connect("query-tooltip", this._undoTooltipCallback.bind(this));
        this.undoButton.set_is_important(true);
        this.toolbar.insert(this.undoButton, 2);
        this.undoButton.show();
        this.undoButton.set_action_name("win.undo");

        // button for the "fullscreen/leave fullscreen" action
        this.fullscreenButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_FULLSCREEN);
        this.fullscreenButton.set_is_important(true);
        this.toolbar.insert(this.fullscreenButton, 3);
        this.fullscreenButton.set_action_name("win.fullscreenToggle");

        return this.toolbar;
    }

    _newCallback(action, parameter) {
        print("You clicked \"New\".");
    }

    _openCallback(action, parameter) {
        print("You clicked \"Open\".");
    }

    // the callback function for the tooltip of the "undo" button
    _undoTooltipCallback(widget, x, y, keyboard_mode, tooltip) {
        // set the text for the tooltip
        tooltip.set_text("Undo your last action");
        // set an icon for the tooltip
        tooltip.set_icon_from_stock(Gtk.STOCK_UNDO, Gtk.IconSize.MENU);
        // show the tooltip
        return true;
    }

    _undoCallback(action, parameter) {
        print("You clicked \"Undo\".");
    }

    _fullScreenCallback() {
        if ((this.window.get_window().get_state() &amp; Gdk.WindowState.FULLSCREEN) != 0 ){
            this.fullscreenButton.set_stock_id(Gtk.STOCK_FULLSCREEN);
            this.fullscreenButton.set_tooltip_text("Make your window fullscreen");
            this.window.unfullscreen();
        } else {
            this.fullscreenButton.set_stock_id(Gtk.STOCK_LEAVE_FULLSCREEN);
            this.fullscreenButton.set_tooltip_text("Leave fullscreen");
            this.window.fullscreen();
        }
    }
};

// Run the application
let app = new TooltipExample ();
app.application.run (ARGV);
</code>
  </section>

  <section id="references">
  <title>Referências da API</title>
    <p>In this sample we used the following:</p>
    <list>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Tooltip.html">GtkTooltip</link></p></item>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Toolbar.html">GtkToolbar</link></p></item>
      <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Widget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/gtk3-Stock-Items.html">Stock Items</link></p></item>
    </list>
  </section>
</page>
