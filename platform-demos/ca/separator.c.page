<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="separator.c" xml:lang="ca">
  <info>
    <title type="text">Separator (C)</title>
    <link type="guide" xref="c#ornaments"/>
    <link type="seealso" xref="grid.c"/>
    <revision version="0.1" date="2013-07-02" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>A separator widget</desc>
  </info>

  <title>Separator</title>

  <media type="image" mime="image/png" src="media/separator.png"/>
  <p>A horizontal and a vertical separator divide some labels.</p>

  <links type="section"/>

  <section id="code">
    <title>Code used to generate this example</title>
    <code mime="text/x-csrc" style="numbered">#include &lt;gtk/gtk.h&gt;

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *grid;
  GtkWidget *window;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *hseparator;
  GtkWidget *vseparator;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Separator Example");

  label1 = gtk_label_new ("Below, a horizontal separator.");
  label2 = gtk_label_new ("On the right, a vertical separator.");
  label3 = gtk_label_new ("On the left, a vertical separator.");

  vseparator = gtk_separator_new (GTK_ORIENTATION_VERTICAL);
  hseparator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);

  grid = gtk_grid_new ();

  gtk_grid_attach (GTK_GRID (grid), label1, 0, 0, 3, 1);
  gtk_grid_attach (GTK_GRID (grid), hseparator, 0, 1, 3, 1);
  gtk_grid_attach (GTK_GRID (grid), label2, 0, 2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), vseparator, 1, 2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), label3, 2, 2, 1, 1);

  gtk_grid_set_column_homogeneous (GTK_GRID (grid), TRUE);

  gtk_container_add (GTK_CONTAINER (window), grid);

  gtk_widget_show_all (window);
}

int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);
  return status;
}
</code>
  </section>

  <section id="reference">
    <title>API Reference</title>
    <p>In this sample we used the following:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkApplicationWindow.html">GtkApplicationWindow</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkSeparator.html">GtkSeparator</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkGrid.html">GtkGrid</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLabel.html">GtkLabel</link></p></item>
    </list>
  </section>

</page>
