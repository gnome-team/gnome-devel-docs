<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="fontchooserwidget.vala" xml:lang="ca">
  <info>
    <title type="text">FontChooserWidget (Vala)</title>
    <link type="guide" xref="beginner.vala#font-selectors"/>
    <revision version="0.2" date="2013-06-19" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antpoolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>A widget to choose a font</desc>
  </info>

  <title>FontChooserWidget</title>

  <media type="image" mime="image/png" src="media/fontchooserwidget.png"/>
  <p>A FontChooserWidget with a lambda callback function.</p>

  <links type="section"/>

  <section id="code">
    <title>Code used to generate this example</title>
    <code mime="text/x-csharp" style="numbered">public class MyWindow : Gtk.ApplicationWindow {
        internal MyWindow (MyApplication app) {
                Object (application: app, title: "FontChooserWidget");

		var font_chooser = new Gtk.FontChooserWidget ();
		font_chooser.set_font ("Sans");
		font_chooser.set_preview_text ("This is an example of preview text!");
		this.add (font_chooser);

		font_chooser.notify["font"].connect (() =&gt; {
			print ("font: %s\n", font_chooser.get_font ().to_string ());
			print ("desc: %s\n", font_chooser.get_font_desc ().to_string ());
			print ("face: %s\n", font_chooser.get_font_face ().get_face_name ());
			print ("size: %d\n", font_chooser.get_font_size ());
			print ("family: %s\n", font_chooser.get_font_family ().get_name ());
			print ("monospace: %s\n\n", font_chooser.get_font_family ().is_monospace ().to_string ());
		});

                this.show_all ();
        }
}

public class MyApplication : Gtk.Application {
        protected override void activate () {
                new MyWindow (this).show ();
        }
}

public int main (string[] args) {
        return new MyApplication ().run (args);
}

</code>
  </section>

  <section id="references">
    <title>API References</title>
    <p>In this sample we used the following:</p>
    <list>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.FontChooserWidget.html">GtkFontChooserWidget</link></p></item>
    </list>
  </section>
</page>
