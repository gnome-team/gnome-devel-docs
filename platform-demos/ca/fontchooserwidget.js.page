<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="fontchooserwidget.js" xml:lang="ca">
  <info>
    <title type="text">FontChooserWidget (JavaScript)</title>
    <link type="guide" xref="beginner.js#font-selectors"/>
    <revision version="0.2" date="2013-06-25" status="review"/>

    <credit type="author copyright">
      <name>Meg Ford</name>
      <email its:translate="no">megford@gnome.org</email>
      <years>2013</years>
    </credit>

    <desc>A widget to choose a font</desc>
  </info>

  <title>FontChooserWidget</title>

  <media type="image" mime="image/png" src="media/fontchooserwidget.png"/>
  <p>A FontChooserWidget with a callback function.</p>

  <links type="section"/>

  <section id="code">
    <title>Code used to generate this example</title>
    <code mime="application/javascript" style="numbered">//!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';
const Gtk = imports.gi.Gtk;

class FontChooserWidgetExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({ application_id: 'org.example.fontchooserwidget' });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this.window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {
        // Create the application window
        this.window = new Gtk.ApplicationWindow  ({ application: this.application,
                                                    window_position: Gtk.WindowPosition.CENTER,
                                                    title: "FontChooserWidget",
                                                    default_width: 200,
                                                    default_height: 200,
                                                    border_width: 10 });

        this.fontChooser = new Gtk.FontChooserWidget();
        // a default font
        this.fontChooser.set_font("Sans");
        // a text to preview the font
        this.fontChooser.set_preview_text("This is an example of preview text!");

        // connect signal from the font chooser to the callback function
        this.fontChooser.connect("notify::font", this._fontCb.bind(this));

        // add the font chooser to the window
        this.window.add(this.fontChooser);
        this.window.show_all();
   }

     // callback function:
     _fontCb() {
        // print in the terminal
        print("You chose the font " + this.fontChooser.get_font());
    }
};

// Run the application
let app = new FontChooserWidgetExample();
app.application.run (ARGV);
</code>
  </section>

  <section id="references">
    <title>API References</title>
    <p>In this sample we used the following:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkFontChooserWidget.html">GtkFontChooserWidget</link></p></item>
    </list>
  </section>
</page>
