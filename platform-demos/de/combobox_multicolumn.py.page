<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="combobox_multicolumn.py" xml:lang="de">
  <info>
    <title type="text">ComboBox (Python)</title>
    <link type="guide" xref="beginner.py#menu-combo-toolbar"/>
    <link type="next" xref="treeview_advanced_liststore.py"/>
    <revision version="0.1" date="2012-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ein Widget, dass zur Auswahl aus einer Liste von Elementen verwendet wird</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>ComboBox (zweispaltig)</title>
  <media type="image" mime="image/png" src="media/combobox_multicolumn.png"/>
  <p>Diese ComboBox gibt Ihre Auswahl auf dem Terminal aus, wenn Sie sie ändern.</p>

  <links type="section"/>

  <section id="code">
    <title>Code, der zum Generieren dieses Beispiels verwendet wurde</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys

actions = [["Select", None],
           ["New", Gtk.STOCK_NEW],
           ["Open", Gtk.STOCK_OPEN],
           ["Save", Gtk.STOCK_SAVE]]


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
        self.set_default_size(200, -1)
        self.set_border_width(10)

        # the data in the model, of type string on two columns
        listmodel = Gtk.ListStore(str, str)
        # append the data
        for i in range(len(actions)):
            listmodel.append(actions[i])

        # a combobox to see the data stored in the model
        combobox = Gtk.ComboBox(model=listmodel)

        # cellrenderers to render the data
        renderer_pixbuf = Gtk.CellRendererPixbuf()
        renderer_text = Gtk.CellRendererText()

        # we pack the cell into the beginning of the combobox, allocating
        # no more space than needed;
        # first the image, then the text;
        # note that it does not matter in which order they are in the model,
        # the visualization is decided by the order of the cellrenderers
        combobox.pack_start(renderer_pixbuf, False)
        combobox.pack_start(renderer_text, False)

        # associate a property of the cellrenderer to a column in the model
        # used by the combobox
        combobox.add_attribute(renderer_text, "text", 0)
        combobox.add_attribute(renderer_pixbuf, "stock_id", 1)

        # the first row is the active one at the beginning
        combobox.set_active(0)

        # connect the signal emitted when a row is selected to the callback
        # function
        combobox.connect("changed", self.on_changed)

        # add the combobox to the window
        self.add(combobox)

    def on_changed(self, combo):
        # if the row selected is not the first one, write on the terminal
        # the value of the first column in the model
        if combo.get_active() != 0:
            print("You chose " + str(actions[combo.get_active()][0]) + "\n")
        return True


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>

  <section id="methods">
    <title>Nützliche Methoden für ein ComboBox-Widget</title>
    <p>The ComboBox widget is designed around a <em>Model/View/Controller</em> design: the <em>Model</em> stores the data; the <em>View</em> gets change notifications and displays the content of the model; the <em>Controller</em>, finally, changes the state of the model and notifies the view of these changes. For more information and for a list of useful methods for ComboBox see <link xref="model-view-controller.py"/>.</p>
    <p>In line 45 the <code>"changed"</code> signal is connected to the callback function <code>on_changed()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>
  </section>

  <section id="references">
    <title>API-Referenzen</title>
    <p>In diesem Beispiel haben wir Folgendes verwendet:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkComboBox.html">GtkComboBox</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkListStore.html">GtkListStore</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkCellRendererText.html">GtkCellRendererText</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkCellRendererPixbuf.html">GtkCellRendererPixbuf</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">Stock Items</link></p></item>
      <item><p><link href="https://gitlab.gnome.org/GNOME/pygobject/blob/master/gi/overrides/Gtk.py">pygobject - Python bindings for GObject Introspection</link></p></item>
    </list>
  </section>
</page>
