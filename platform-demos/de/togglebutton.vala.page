<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="togglebutton.vala" xml:lang="de">
  <info>
  <title type="text">ToggleButton (Vala)</title>
    <link type="guide" xref="beginner.vala#buttons"/>
    <link type="seealso" xref="grid.vala"/>
    <link type="seealso" xref="spinner.vala"/>

    <revision version="0.1" date="2012-05-09" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A button which retains state</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>ToggleButton</title>
  <media type="image" mime="image/png" src="media/togglebutton.png"/>
  <p>When this ToggleButton is in an active state, the spinner spins.</p>

<code mime="text/x-csharp" style="numbered">public class MyWindow : Gtk.ApplicationWindow {

	Gtk.Spinner spinner;

	internal MyWindow (MyApplication app) {
		Object (application: app, title: "ToggleButton Example");

		this.set_default_size (300, 300);
		this.border_width = 30;

		/*Spinner*/
		spinner = new Gtk.Spinner ();
		spinner.set_hexpand (true);
		spinner.set_vexpand (true);

		/*ToggleButton*/
		var togglebutton = new Gtk.ToggleButton.with_label ("Start/Stop");
		togglebutton.toggled.connect (toggled_cb);

		/*Grid*/
		var grid = new Gtk.Grid ();
		grid.set_row_homogeneous (false);
		grid.set_row_spacing (15);
		grid.attach (spinner, 0, 0, 1, 1);
		grid.attach (togglebutton, 0, 1, 1, 1);

		this.add (grid);
	}

	void toggled_cb (Gtk.ToggleButton button) {
		if (button.get_active()) {
			spinner.start ();
		}
		else {
			spinner.stop ();
		}
	}
}

public class MyApplication : Gtk.Application {

	protected override void activate () {

		//Show all the things
		new MyWindow (this).show_all ();
	}

	internal MyApplication () {
		Object (application_id: "org.example.spinner");
	}
}

public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ToggleButton.html">Gtk.ToggleButton</link></p></item>
</list>
</page>
