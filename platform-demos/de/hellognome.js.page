<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="topic" style="task" id="hellognome.js" xml:lang="de">
  <info>
    <link type="guide" xref="beginner.js#tutorials"/>
    <revision version="0.1" date="2012-07-17" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>Ihre erste GNOME-Anwendung!</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>1. Hallo, GNOME!</title>
  <synopsis>
    <p>This tutorial will show you how to write your first GNOME application in JavaScript. You will use JavaScript to write for GNOME the same way you would for the web. Afterwards, you will learn how to use "native" widgets, to write applications that look and feel like other GNOME apps.</p>
    <note style="warning"><p>Have you gotten GNOME installed on your computer, and <link xref="set-up-gedit.js">gedit</link> set up to write code with? You'll want to do these things first.</p></note>
  </synopsis>

  <links type="section"/>

  <section id="webapp">
    <title>Lassen Sie uns mit einer Webseite anfangen</title>

    <p>Here's some basic HTML, CSS, and JavaScript code. Does this look familiar?</p>
    <code mime="application/javascript" style="numbered">
&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;
        &lt;meta charset="utf-8"&gt;
        &lt;title&gt;Hello, GNOME!&lt;/title&gt;

        &lt;!-- Use JavaScript to show a greeting when someone clicks the button --&gt;
        &lt;script type="application/javascript"&gt;
        function greeting () {
            document.getElementById ("greeting").innerHTML = ("O hai!");
        }
        &lt;/script&gt;

        &lt;!-- Very basic CSS style using the GNOME font --&gt;
        &lt;style type="text/css"&gt;
            body {
                font-face: Cantarell, sans-serif;
                text-align: center; }
        &lt;/style&gt;

    &lt;/head&gt;
    &lt;body&gt;
        &lt;br /&gt; &lt;br /&gt;
        &lt;button type="button" onclick="greeting()"&gt;Hello, GNOME!&lt;/button&gt;

        &lt;!-- Empty H1 element gets filled in when the button is clicked --&gt;
        &lt;h1 id="greeting"&gt;&lt;/h1&gt;
    &lt;/body&gt;
&lt;/html&gt;
</code>

    <p>Let's save this as <file>hellognome.html</file>, and see what it looks like when we run it!</p>

    <media type="image" mime="image/png" src="media/hellognomewebapp.png"/>

    <p>You <em>can</em> run the above code by opening <file>hellognome.html</file> in a web browser. But here, we're going to create a GNOME application that runs our web app inside of it, just like you see in the screenshot. You'll be able to resize and maximize the window, and click the X in the corner to close it, just like you'd expect from any other GNOME app. The difference is that this one will run our web code inside of it.</p>
    <p>The best part? We're going to continue to use JavaScript, to write all the parts that make our app work with GNOME. Let's look at the code, and see how it's done!</p>
  </section>

  <section id="window">
    <title>Creating a GNOME window to frame our web app</title>

    <p>First, we need to tell GNOME that this is a JavaScript application, which uses gjs. Gjs is GNOME's way of turning your JavaScript code into instructions it understands, so this line always has to go at the start of your applications.</p>
    <code mime="application/javascript">
#!/usr/bin/gjs
</code>
    <p>Dann sollten wir die Versionen der Bibliotheken festlegen, die wir verwenden werden.</p>
<code mime="application/javascript">
imports.gi.versions.Gtk = '3.0';
imports.gi.versions.WebKit2 = '4.0';
</code>
    <p>After that, we need to tell GNOME which libraries we want to import.</p>
    <code mime="application/javascript">
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Webkit = imports.gi.WebKit2;
</code>
    <p>Just like how add-on libraries like jQuery let us do extra things with JavaScript, each of these libraries gives us new capabilities for our GNOME apps:</p>
    <steps>
      <item><p><file>Gtk</file> is the basic part of any GNOME application, which lets you create windows and widgets and tie them together.</p></item>
      <item><p><file>GLib</file> is a helper library, which lets us do things like tell GNOME where the <file>hellognome.html</file> file we created is.</p></item>
      <item><p>And <file>Webkit</file> is a web rendering engine, which we'll use to basically create a browser window to open our HTML file with.</p></item>
    </steps>

    <p>Nun erstellen wir die Anwendung selbst:</p>
    <code mime="application/javascript">
class HelloGNOME {
</code>
    <p>This will look familiar to you if you've worked with object-oriented JavaScript before. That's right; our whole application is a class called HelloGNOME.</p>

    <code mime="application/javascript">
    // Create the application itself
    constructor() {
        this.application = new Gtk.Application();

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }
</code>
    <p>Here's some code you will more or less copy-and-paste for every JavaScript application you build. It creates a new Application, and then binds its activate and startup signals to functions that make the window show itself and build its user interface, respectively.</p>
    <p>What does that mean? Well, everything in a GNOME application sends out a signal when something important happens. A button might send out the clicked signal when you click on it, for instance. Our job is to connect the signals to functions which handle them, and make the things that we want to have happen occur. We do this using each object's connect method, which takes two arguments: The signal we want to handle, and the bound function, which we have to use to tell connect which function we want to have handle the signal.</p>
    <p>In this case, we want _onActivate to handle the activate signal, and _onStartup to handle the startup signal. _onActivate just tells the window to present itself; so basically, whenever you <keyseq><key>Alt</key> <key>Tab</key></keyseq> to the application it appears, like you would expect it to. _onStartup calls _buildUI, which is the function that creates our user interface and is the next part that we will look at.</p>
    <note style="tip"><p>When you copy and paste the above code for your own applications, be sure to change the class name to a unique one each time.</p></note>
  </section>

  <section id="ui">
    <title>Entwerfen der Benutzeroberfläche unseres Fensters</title>
    <p>In the _buildUI function, we're going to tell GNOME about our window and the things inside it, one at a time. After that, we're going to connect everything together and put it all on display.</p>

    <code mime="application/javascript">
    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow  ({
            application: this.application,
            title: "Welcome to GNOME",
            default_height: 200,
            default_width: 400,
            window_position: Gtk.WindowPosition.CENTER });
</code>

    <p>The first object we create is an ApplicationWindow. It needs a title to go in the title bar, and its application property needs to be the application that we created, above. Beyond that, there are various ways of customizing how it looks, which the <link xref="GtkApplicationWindow.js">ApplicationWindow</link> reference page will go into more detail about. As you can see here, we gave it a default height and width (measured in pixels), and told GNOME we want our window to appear in the center of the screen.</p>
    <code mime="application/javascript">
        // Create a webview to show the web app
        this._webView = new Webkit.WebView ();

        // Put the web app into the webview
        this._webView.load_uri (GLib.filename_to_uri (GLib.get_current_dir() +
            "/hellognome.html", null));
</code>
    <p>Remember how we imported Webkit right at the start? Here we're creating a new instance of a Webkit class called a WebView, which is more or less a browser window you can put inside of your app. After that, we then give it the URI that we want it to load when the application starts up.</p>
    <p>We <em>could</em> just give it a web URI, like <link href="http://gnome.org">http://gnome.org</link>. Instead, here we use a couple of GLib helper functions to tell the WebView where our <file>hellognome.html</file> file is. GLib.get_current_dir returns the directory that our app's running in, and GLib.filename_to_uri turns our file's path and filename into a URI that the WebView's load_uri function understands. (filename_to_uri's second parameter should be null unless you know what it's used for and have a reason for changing it.)</p>
    <code mime="application/javascript">
        // Put the webview into the window
        this._window.add (this._webView);

        // Show the window and all child widgets
        this._window.show_all();
    },

});
</code>
    <p>Each window can hold one, and only one, widget. Normally, we'd use a container widget like a <link xref="grid.js">Grid</link> to put multiple widgets into, then use the window's add function to add the Grid to it. Here, we just need the WebView, so that's all we add to the window. After that, as the last part of the _buildUI function that creates our window, we tell the window to show itself and its contents.</p>
    <code mime="application/javascript">
// Run the application
let app = new HelloGNOME ();
app.application.run (ARGV);
</code>
    <p>Finally, we create a new instance of our HelloGNOME class, and tell GNOME to run it.</p>
  </section>

  <section id="run">
    <title>Ihre GNOME-Anwendung ausführen</title>

    <p>Now that we've created our first GNOME application, it's time to test it out! You don't need to compile your app or install any special software for this; GNOME has gjs built in, to let it run GNOME Shell. Just save <file>hellognome.html</file> and our actual application, <file>hellognome.js</file>, to a directory you can get to with the terminal. (They usually open onto your home directory, the one that's called by your username.) After that, open a terminal, go there, and type:</p>
    <screen> <output style="prompt">$ </output>gjs hellognome.js </screen>
    <p>You should see more or less the same screenshot as before, with a button that you can click to make a short message appear.</p>

    <note style="tip">
        <p>You can use the terminal command</p>
        <screen> <output style="prompt">$ </output>cd <var>(Ordnername)</var> </screen>
        <p>to navigate between directories inside the Terminal, in order to get to where you saved the files. There is also an extension for Nautilus, GNOME's file manager, which lets you right-click anywhere inside it to open a terminal window right there. Check the app you use to install new software (like Add/Remove Programs or the Software Center) for it.</p>
    </note>
  </section>

  <section id="whatsnext">
    <title>Was kommt als Nächstes?</title>

    <p><link xref="02_welcome_to_the_grid.js">Continue on to the next tutorial</link> to learn how to build "native" GNOME applications that look and feel like the others, instead of a webview with HTML code inside. Or take a look at some <link xref="beginner.js#samples">code samples</link>, if you'd like to see example code for each Gtk widget.</p>
    <p>Finally, if you want to just build GNOME applications using JavaScript libraries designed for the web, you can basically stop here and go do that! Check out <link xref="beginner.js#tutorials">the later tutorials</link> if you'd like to see how to create a .desktop file for your application, which will let it appear in your desktop's Activities menu with all your other apps.</p>
  </section>

  <section id="complete">
    <title>Vollständiges Codebeispiel</title>
<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';
imports.gi.versions.WebKit2 = '4.0';

const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Webkit = imports.gi.WebKit2;

class HelloGNOME {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application ();

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow  ({
            application: this.application,
            title: "Welcome to GNOME",
            default_height: 200,
            default_width: 400,
            window_position: Gtk.WindowPosition.CENTER });

        // Create a webview to show the web app
        this._webView = new Webkit.WebView ();

        // Put the web app into the webview
        this._webView.load_uri (GLib.filename_to_uri (GLib.get_current_dir() +
            "/hellognome.html", null));

        // Put the webview into the window
        this._window.add (this._webView);

        // Show the window and all child widgets
        this._window.show_all();
    }

};

// Run the application
let app = new HelloGNOME ();
app.application.run (ARGV);
</code>
  </section>
</page>
