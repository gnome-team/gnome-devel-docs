<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="aboutdialog.vala" xml:lang="de">
  <info>
  <title type="text">AboutDialog (Vala)</title>
    <link type="guide" xref="beginner.vala#windows"/>
    <link type="seealso" xref="button.vala"/>
    <link type="seealso" xref="linkbutton.vala"/>
    <revision version="0.1" date="2012-04-07" status="stub"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email its:translate="no">desrt@desrt.ca</email>
      <years>2012</years>
    </credit>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>


    <desc>Informationen zur Anwendung anzeigen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>AboutDialog</title>
  <media type="image" mime="image/png" src="media/aboutdialog_GMenu.png"/>
  <p>Ein AboutDialog unter Verwendung von Gtk.ApplicationWindow und Menü</p>
  <note><p><em style="bold">Dies funktioniert nur mit Gtk3.4 oder neuer</em></p></note>

<code mime="text/x-csharp" style="numbered">/* A window in the application */
public class Window : Gtk.ApplicationWindow {

	/* The constructor */
	public Window (Application app) {
		Object (application: app, title: "AboutDialog Example");

		var about_action = new SimpleAction ("about", null);

		about_action.activate.connect (this.about_cb);
		this.add_action (about_action);
		this.show_all ();
	}

	/* This is the callback function connected to the 'activate' signal
	 * of the SimpleAction about_action.
	 */
	void about_cb (SimpleAction simple, Variant? parameter) {
		string[] authors = { "GNOME Documentation Team", null };
		string[] documenters = { "GNOME Documentation Team", null };

		Gtk.show_about_dialog (this,
                               "program-name", ("GtkApplication Example"),
                               "copyright", ("Copyright \xc2\xa9 2012 GNOME Documentation Team"),
                               "authors", authors,
                               "documenters", documenters,
                               "website", "http://developer.gnome.org",
                               "website-label", ("GNOME Developer Website"),
                               null);
	}
}

/* This is the Application */
public class Application : Gtk.Application {

	/* Here we override the activate signal of GLib.Application */
	protected override void activate () {
		new Window (this);
	}

	/* Here we override the startup signal of GLib.Application */
	protected override void startup () {

		base.startup ();

		var menu = new Menu ();
		menu.append ("About", "win.about");
		menu.append ("Quit", "app.quit");
		this.app_menu = menu;

		var quit_action = new SimpleAction ("quit", null);
		//quit_action.activate.connect (this.quit);
		this.add_action (quit_action);
	}

	/* The constructor */
	public Application () {
		Object (application_id: "org.example.application");
	}
}

/* main function creates Application and runs it */
int main (string[] args) {
	return new Application ().run (args);
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Window.set_default_size.html">set_default_size</link></p></item>
</list>
</page>
