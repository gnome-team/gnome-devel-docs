<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="aboutdialog.c" xml:lang="de">
  <info>
    <title type="text">AboutDialog (C)</title>
    <link type="guide" xref="c#windows"/>
    <link type="seealso" xref="dialog.c"/>
    <link type="seealso" xref="messagedialog.c"/>
    <revision version="0.2" date="2012-08-07" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Informationen zur Anwendung anzeigen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>AboutDialog</title>

  <media type="image" mime="image/png" src="media/aboutdialog_GMenu.png"/>
  <p>Ein AboutDialog unter Verwendung von Gtk.ApplicationWindow und Menü</p>
  <note><p><em style="bold">Dies funktioniert nur mit Gtk3.4 oder neuer</em></p></note>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;

/* Callback function in which reacts to the "response" signal from the user in
 * the message dialog window.
 * This function is used to destroy the dialog window.
 */
static void
on_close (GtkDialog *dialog,
          gint       response_id,
          gpointer   user_data)
{
  /* This will cause the dialog to be destroyed */
  gtk_widget_destroy (GTK_WIDGET (dialog));
}

/* Callback function for the response signal "activate" related to the SimpleAction
 * "about_action".
 * This function is used to cause the about dialog window to popup.
 */
static void
about_cb (GSimpleAction *simple,
          GVariant      *parameter,
          gpointer       user_data)
{
  GtkWidget *about_dialog;

  about_dialog = gtk_about_dialog_new ();

  /* Lists of authors/ documenters to be used later, they must be initialized
   * in a null terminated array of strings.
   */
  const gchar *authors[] = {"GNOME Documentation Team", NULL};
  const gchar *documenters[] = {"GNOME Documentation Team", NULL};

  /* We fill in the information for the about dialog */
  gtk_about_dialog_set_program_name (GTK_ABOUT_DIALOG (about_dialog), "AboutDialog Example");
  gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG (about_dialog), "Copyright \xc2\xa9 2012 GNOME Documentation Team");
  gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG (about_dialog), authors);
  gtk_about_dialog_set_documenters (GTK_ABOUT_DIALOG (about_dialog), documenters);
  gtk_about_dialog_set_website_label (GTK_ABOUT_DIALOG (about_dialog), "GNOME Developer Website");
  gtk_about_dialog_set_website (GTK_ABOUT_DIALOG (about_dialog), "http://developer.gnome.org");

  /* We do not wish to show the title, which in this case would be
   * "AboutDialog Example". We have to reset the title of the messagedialog
   * window after setting the program name.
   */
  gtk_window_set_title (GTK_WINDOW (about_dialog), "");

  /* To close the aboutdialog when "close" is clicked we connect the response
   * signal to on_close
   */
  g_signal_connect (GTK_DIALOG (about_dialog), "response",
                    G_CALLBACK (on_close), NULL);

  /* Show the about dialog */
  gtk_widget_show (about_dialog);
}

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;

  GSimpleAction *about_action;

  /* Create a window with a title and a default size */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "AboutDialog Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

  /* Create a new simple action, giving it a NULL parameter type. It will
   * always be NULL for actions invoked from a menu. (e.g clicking on an "ok"
   * or "cancel" button)
   */
  about_action = g_simple_action_new ("about", NULL);

  /* Connect the "activate" signal to the appropriate callback function.
   * It will indicate that the action was just activated.
   */
  g_signal_connect (about_action, "activate", G_CALLBACK (about_cb),
                    GTK_WINDOW (window));

  /* Adds the about_action to the overall action map. An Action map is an
   * interface that contains a number of named GAction instances
   * (such as about_action)
   */
  g_action_map_add_action (G_ACTION_MAP (window), G_ACTION (about_action));

  gtk_widget_show_all (window);
}

/* Callback function for the response signal "activate" from the "quit" action
 * found in the function directly below.
 */
static void
quit_cb (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data)
{
  GApplication *application = user_data;

  g_application_quit (application);
}

/* Startup function for the menu we are creating in this sample */
static void
startup (GApplication *app,
         gpointer      user_data)
{
  GMenu *menu;
  GSimpleAction *quit_action;

  /* Initialize the GMenu, and add a menu item with label "About" and action
   * "win.about". Also add another menu item with label "Quit" and action
   * "app.quit"
   */
  menu = g_menu_new ();
  g_menu_append (menu, "About", "win.about");
  g_menu_append (menu, "Quit", "app.quit");

  /* Create a new simple action for the application. (In this case it is the
   * "quit" action.
   */
  quit_action = g_simple_action_new ("quit", NULL);

  /* Ensure that the menu we have just created is set for the overall application */
  gtk_application_set_app_menu (GTK_APPLICATION (app), G_MENU_MODEL (menu));

  g_signal_connect (quit_action,
                    "activate",
                    G_CALLBACK (quit_cb),
                    app);

  g_action_map_add_action (G_ACTION_MAP (app), G_ACTION (quit_action));
}

/* Startup function for the application */
int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_signal_connect (app, "startup", G_CALLBACK (startup), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkAboutDialog.html">GtkAboutDialog</link></p></item>
  <item><p><link href="http://developer.gnome.org/gio/stable/GMenu.html">GMenu</link></p></item>
  <item><p><link href="http://developer.gnome.org/gio/stable/GActionMap.html">GActionMap</link></p></item>
</list>
</page>
