<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="togglebutton.c" xml:lang="de">
  <info>
    <title type="text">ToggleButton (C)</title>
    <link type="guide" xref="c#buttons"/>
    <link type="seealso" xref="grid.c"/>
    <link type="seealso" xref="switch.c"/>


    <revision version="0.1" date="2012-06-12" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A button which retains state</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>ToggleButton</title>

  <media type="image" mime="image/png" src="media/togglebutton.png"/>
  <p>When this ToggleButton is in an active state, the spinner spins.</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;



/*This is the callback function. It is a handler function 
which reacts to the signal. In this case, it will cause the 
spinner to start and stop according to how many times the user 
presses the button.*/
static void
button_toggled_cb (GtkWidget *button,
                   gpointer   user_data)
{
  GtkWidget *spinner = user_data;

  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(button)))
          gtk_spinner_start (GTK_SPINNER (spinner));
  else {
          gtk_spinner_stop (GTK_SPINNER (spinner));
  }
}



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *grid;
  GtkWidget *spinner;

  /*Create a window with a title, border width and a default size*/
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "ToggleButton Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 300, 300);
  gtk_container_set_border_width(GTK_CONTAINER(window), 30);

  /*Create a togglebutton with a label*/
  button = gtk_toggle_button_new_with_label ("Start/Stop");

  /*Create a spinner, with extra horizontal and vertical space*/
  spinner = gtk_spinner_new ();
  gtk_widget_set_hexpand (spinner, TRUE);
  gtk_widget_set_vexpand (spinner, TRUE);

  /*Create a grid and set the row spacing, attach the togglebutton 
  and spinner onto the grid and position them accordingly*/
  grid = gtk_grid_new();
  gtk_grid_set_row_homogeneous (GTK_GRID (grid), FALSE);
  gtk_grid_set_row_spacing (GTK_GRID (grid), 15);
  gtk_grid_attach (GTK_GRID (grid), spinner, 0, 0, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), button, 0, 1, 1, 1);
  
  gtk_container_add (GTK_CONTAINER (window), grid);

  /*Connecting the toggled signal to the callback*/
  g_signal_connect (GTK_TOGGLE_BUTTON (button), "toggled", 
                    G_CALLBACK (button_toggled_cb), spinner);

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkSpinner.html">GtkSpinner</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkToggleButton.html">GtkTogglebutton</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkGrid.html">GtkGrid</link></p></item>
</list>
</page>
