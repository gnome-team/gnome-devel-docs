<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="menubutton.py" xml:lang="de">
  <info>
  <title type="text">MenuButton</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="toolbar.py"/>
    <revision version="0.1" date="2012-08-19" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A widget that shows a menu when clicked on</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>MenuButton</title>
  <media type="image" mime="image/png" src="media/menubutton.png"/>
  <p>The GtkMenuButton widget is used to display a menu when clicked on. This menu can be provided either as a GtkMenu, or an abstract GMenuModel. The GtkMenuButton widget can hold any valid child widget. That is, it can hold almost any other standard GtkWidget. The most commonly used child is the provided GtkArrow.</p>

  <note><p>Sie müssen GNOME 3.6 ausführen, damit der MenuButton funktioniert.</p></note>

  <links type="section"/>
    
  <section id="code">
  <title>Code, der zum Generieren dieses Beispiels verwendet wurde</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Menubutton Example", application=app)
        self.set_default_size(600, 400)

        grid = Gtk.Grid()

        # a menubutton
        menubutton = Gtk.MenuButton()
        menubutton.set_size_request(80, 35)

        grid.attach(menubutton, 0, 0, 1, 1)

        # a menu with two actions
        menumodel = Gio.Menu()
        menumodel.append("New", "app.new")
        menumodel.append("About", "win.about")

        # a submenu with one action for the menu
        submenu = Gio.Menu()
        submenu.append("Quit", "app.quit")
        menumodel.append_submenu("Other", submenu)

        # the menu is set as the menu of the menubutton
        menubutton.set_menu_model(menumodel)

        # the action related to the window (about)
        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.about_callback)
        self.add_action(about_action)

        self.add(grid)

    # callback for "about"
    def about_callback(self, action, parameter):
        print("You clicked \"About\"")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        #  the actions related to the application
        new_action = Gio.SimpleAction.new("new", None)
        new_action.connect("activate", self.new_callback)
        self.add_action(new_action)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_callback)
        self.add_action(quit_action)

    # callback functions for the actions related to the application
    def new_callback(self, action, parameter):
        print("You clicked \"New\"")

    def quit_callback(self, action, parameter):
        print("You clicked \"Quit\"")
        self.quit()

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
  <title>Nützliche Methoden für ein MenuButton-Widget</title>
      <p>In line 33 the signal <code>"activate"</code> from the action <code>about_action</code> is connected to the callback function <code>about_callback()</code> using <code><var>action</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>

    <p>The positioning of the menu is determined by the "direction" property of the menu button and the "halign" or "valign" properties of the menu. For example, when the direction is <code>Gtk.ArrowType.DOWN</code> (other option: <code>UP</code>) and the horizontal alignment is <code>Gtk.Align.START</code> (other options: <code>CENTER</code> and <code>END</code>), the menu will be positioned below the button, with the starting edge (depending on the text direction) of the menu aligned with the starting edge of the button. If there is not enough space below the button, the menu is popped up above the button instead. If the alignment would move part of the menu offscreen, it is 'pushed in'.</p>
    
    <p>In the case of vertical alignment, the possible ArrowType directions are <code>LEFT</code> and <code>RIGHT</code> and the vertical alignment is again <code>START</code>, <code>CENTER</code> or <code>END</code>.</p>
    
    <p><code>set_align_widget(alignment)</code> and <code>set_direction(direction)</code> can be used to set these properties.</p>
  </section>
  
  <section id="references">
  <title>API-Referenzen</title>
    <p>In diesem Beispiel haben wir Folgendes verwendet:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkMenuButton.html">MenuButton</link></p></item>
    </list>
  </section>
</page>
