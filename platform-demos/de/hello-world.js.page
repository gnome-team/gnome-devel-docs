<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="hello-world.js" xml:lang="de">

  <info>
  <title type="text">Hallo Welt (JavaScript)</title>
    <link type="guide" xref="beginner.js#tutorials" group="#first"/>

    <revision version="0.1" date="2013-06-17" status="review"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>

    <desc>Eine grundlegende »Hallo, Welt«-Anwendung</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>How to build, install and create a <file>tar.xz</file> of a Hello World program</title>
    <media type="image" mime="image/png" style="floatend" src="media/hello-world.png"/>
    <synopsis>
      <p>This tutorial will demonstrate how to:</p>
      <list style="numbered">
        <item><p>eine kleine »Hallo, Welt«-Anwendung mit JavaScript und GTK+ erstellen</p></item>
        <item><p>Erstellen der <file>.desktop</file>Datei</p></item>
        <item><p>Das Buildsystem einrichten</p></item>
      </list>
    </synopsis>



  <links type="section"/>

  <section id="HelloWorld"><title>Das Programm erstellen</title>

    <links type="section"/>

    <section id="script"><title>Skript zum Ausführen der Anwendung</title>
      <p>Dies muss die erste Zeile Ihres Skripts sein:</p>
      <code mime="application/javascript">#!/usr/bin/gjs</code>
      <p>It tells the script to use <link href="https://live.gnome.org/Gjs/">Gjs</link>. Gjs is a JavaScript binding for GNOME.</p>
    </section>


    <section id="imports"><title>Zu importierende Bibliotheken</title>
      <code mime="application/javascript">const Lang = imports.lang;

imports.gi.versions.Gtk = '3.0'
const Gtk = imports.gi.Gtk;</code>
      <p>In order for our script to work with GNOME, we need to import GNOME libraries via GObject Introspection. Here we import the language bindings and GTK+, the library which contains the graphical widgets used to make GNOME applications.  </p>
    </section>

    <section id="mainwindow"><title>Erstellen des Hauptfensters der Anwendung</title>
      <code mime="application/javascript">const Application = new Lang.Class({
    //A Class requires an explicit Name parameter. This is the Class Name.
    Name: 'Application',

    //create the application
    _init: function() {
        this.application = new Gtk.Application();

       //connect to 'activate' and 'startup' signals to handlers.
       this.application.connect('activate', Lang.bind(this, this._onActivate));
       this.application.connect('startup', Lang.bind(this, this._onStartup));
    },

    //create the UI
    _buildUI: function() {
        this._window = new Gtk.ApplicationWindow({ application: this.application,
                                                   title: "Hello World!" });
    },

    //handler for 'activate' signal
    _onActivate: function() {
        //show the window and all child widgets
        this._window.show_all();
    },

    //handler for 'startup' signal
    _onStartup: function() {
        this._buildUI();
    }
});
</code>

    <p>GtkApplication initializes GTK+. It also connects the <gui>x</gui> button that's automatically generated along with the window to the "destroy" signal.</p>
    <p>We can start building our first window. We do this by creating a variable called <var>_window</var> and assigning it a new Gtk.ApplicationWindow.</p>
    <p>We give the window a property called <var>title</var>. The title can be any string you want it to be. To be on the safe side, it's best to stick to UTF-8 encoding.</p>
    <p>Now we have a window which has a title and a working "close" button. Let's add the actual "Hello World" text.</p>
    </section>

    <section id="label"><title>Label for the window</title>
      <code mime="application/javascript">// Add a label widget to your window
this.label = new Gtk.Label({ label: "Hello World" });
this._window.add(this.label);
this._window.set_default_size(200, 200);</code>

      <p>A text label is one of the GTK+ widgets we can use, on account of having imported the GTK+ library. To use it, we create a new variable called label, and assign it a new Gtk.Label. Then we give it properties inside the curly braces {}. In this case, we're setting the text that the label will hold. Finally, we create and run the application:</p>

      <code mime="application/javascript">//run the application
let app = new Application();
app.application.run(ARGV);</code>

      <p>Gtk.ApplicationWindow can only hold one widget at a time. To construct more elaborate programs you need to create a holder widget like Gtk.Grid inside the window, and then add all the other widgets to it.</p>
   </section>


    <section id="js"><title>hello-world.js</title>
      <p>Die vollständige Datei:</p>
      <code mime="application/javascript" style="numbered">#!/usr/bin/gjs

const Lang = imports.lang;

imports.gi.versions.Gtk = '3.0'
const Gtk = imports.gi.Gtk;

class Application {

    //create the application
    constructor() {
        this.application = new Gtk.Application();

       //connect to 'activate' and 'startup' signals to handlers.
       this.application.connect('activate', this._onActivate.bind(this));
       this.application.connect('startup', this._onStartup.bind(this));
    }

    //create the UI
    _buildUI() {
        this._window = new Gtk.ApplicationWindow({ application: this.application,
                                                   title: "Hello World!" });
        this._window.set_default_size(200, 200);
        this.label = new Gtk.Label({ label: "Hello World" });
        this._window.add(this.label);
    }

    //handler for 'activate' signal
    _onActivate() {
        //show the window and all child widgets
        this._window.show_all();
    }

    //handler for 'startup' signal
    _onStartup() {
        this._buildUI();
    }
};

//run the application
let app = new Application();
app.application.run(ARGV);
</code>
    </section>

    <section id="terminal"><title>Anwendung im Terminal ausführen</title>
      <p>To run this application, first save it as hello-world.js. Then open Terminal, go to the folder where your application is stored and run:</p>
      <screen><output style="prompt">$ </output><input>gjs hello-world.js</input></screen>
    </section>
  </section>



  <section id="desktop.in"><title>Die Datei <file>.desktop.in</file></title>
      <p>Running applications from the Terminal is useful at the beginning of the application making process. To have fully working <link href="https://developer.gnome.org/integration-guide/stable/mime.html.en">application integration</link> in GNOME 3 requires a desktop launcher. For this you need to create a  <file>.desktop</file> file. The <file>.desktop</file> file describes the application name, the used icon and various integration bits. A deeper insight into the <file>.desktop</file> file can be found <link href="http://developer.gnome.org/desktop-entry-spec/">here</link>. The <file>.desktop.in</file> file will create the <file>.desktop</file>.</p>

  <note>
       <p>Before continuing, resave <file>hello-world.js</file> as <file>hello-world</file>.  Then run this in the command line:</p>
      <screen><output style="prompt">$ </output><input>chmod +x hello-world</input></screen>
  </note>

    <p>The example shows you the minimum requirements for a <code>.desktop.in</code> file.</p>
    <code mime="text/desktop" style="numbered">[Desktop Entry]
Version=1.0
Encoding=UTF-8
Name=Hello World
Comment=Say Hello
Exec=@prefix@/bin/hello-world
Icon=application-default-icon
Terminal=false
Type=Application
StartupNotify=true
Categories=GNOME;GTK;Utility;
</code>

    <p>Save this as <file>hello-world.desktop.in</file>. Now let's go through some parts of the <code>.desktop.in</code> file.</p>
    <terms>
      <item><title>Name</title><p>Der Anwendungsname.</p></item>
      <item><title>Comment</title><p>Eine kurze Beschreibung der Anwendung.</p></item>
      <item><title>Exec</title><p>Specifies a command to execute when you choose the application from the menu. In this example exec just tells where to find the <file>hello-world</file> file and the file takes care of the rest.</p></item>
      <item><title>Terminal</title><p>Gibt an, ob der Befehl im Exec-Schlüssel in einem Terminal ausgeführt wird.</p></item>
    </terms>

    <p>To put your application into the appropriate category, you need to add the necessary categories to the Categories line. More information on the different categories can be found in the <link href="http://standards.freedesktop.org/menu-spec/latest/apa.html">menu specification</link>.</p>
    <p>In this example we use an existing icon. For a custom icon you need to have a .svg file of your icon, stored in <file>/usr/share/icons/hicolor/scalable/apps</file>. Write the name of your icon file to the .desktop.in file, on line 7. More information on icons in: <link href="https://wiki.gnome.org/Initiatives/GnomeGoals/AppIcon">Installing Icons for Themes</link> and <link href="http://freedesktop.org/wiki/Specifications/icon-theme-spec">on freedesktop.org: Specifications/icon-theme-spec</link>.</p>
  </section>

  <section id="autotools"><title>The build system</title>
    <p>To make your application truly a part of the GNOME 3 system you need to install it with the help of autotools. The autotools build will install all the necessary files to all the right places. </p>
    <p>Dazu benötigen Sie die folgenden Dateien:</p>
    <links type="section"/>

      <section id="autogen"><title>autogen.sh</title>
        <code mime="application/x-shellscript" style="numbered">#!/bin/sh

set -e

test -n "$srcdir" || srcdir=`dirname "$0"`
test -n "$srcdir" || srcdir=.

olddir=`pwd`
cd "$srcdir"

# This will run autoconf, automake, etc. for us
autoreconf --force --install

cd "$olddir"

if test -z "$NOCONFIGURE"; then
  "$srcdir"/configure "$@"
fi
</code>

      <p>After the <file>autogen.sh</file> file is ready and saved, run:</p>
      <screen><output style="prompt">$ </output><input>chmod +x autogen.sh</input></screen>
    </section>


    <section id="makefile"><title>Makefile.am</title>
      <code mime="application/x-shellscript" style="numbered"># The actual runnable program is set to the SCRIPTS primitive.
# # Prefix bin_ tells where to copy this
bin_SCRIPTS = hello-world
# # List of files to be distributed
EXTRA_DIST =  \
	$(bin_SCRIPTS)
#
#     # The desktop files
desktopdir = $(datadir)/applications
desktop_DATA = \
	hello-world.desktop
</code>
    </section>


    <section id="configure"><title>configure.ac</title>
      <code mime="application/x-shellscript" style="numbered"># This file is processed by autoconf to create a configure script
AC_INIT([Hello World], 1.0)
AM_INIT_AUTOMAKE([1.10 no-define foreign dist-xz no-dist-gzip])
AC_CONFIG_FILES([Makefile hello-world.desktop])
AC_OUTPUT
</code>
    </section>


    <section id="readme"><title>Lies mich</title>
       <p>Informationen, die Benutzer zuerst lesen sollten. Diese Datei kann leer sein.</p>

       <p>When you have the <file>hello-world</file>, <file>hello-world.desktop.in</file>, <file>Makefile.am</file>, <file>configure.ac</file> and <file>autogen.sh</file> files with correct information and permissions, create a <file>README</file> file with installation instructions. Below is a sample of what suitable README instructions may look like:</p>
      <code mime="text/readme" style="numbered">To build and install this program, run these commands from a terminal:

./autogen.sh --prefix=/home/$USER/.local
make install

-------------
When running the first command $USER will be replaced by your username.

Running the first command above creates the following files:

aclocal.m4
autom4te.cache
config.log
config.status
configure
hello-world.desktop
install-sh
missing
Makefile.in
Makefile

Running "make install", installs the application in /home/your_username/.local/bin
and installs the hello-world.desktop file in /home/your_username/.local/share/applications

You can now run the application by typing "Hello World" in the Overview.

----------------
To uninstall, type:

make uninstall

----------------
To create a tarball type:

make distcheck

This will create hello-world-1.0.tar.xz
</code>
    </section>

    <!-- TODO: How to make a custom icon with autotools -->

  </section>
</page>
