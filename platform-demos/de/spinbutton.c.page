<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="spinbutton.c" xml:lang="de">
  <info>
    <title type="text">SpinButton (C)</title>
    <link type="guide" xref="c#entry"/>
    <link type="seealso" xref="scale.c"/>
    <link type="seealso" xref="label.c"/>
    <link type="seealso" xref="grid.c"/>
    <revision version="0.2" date="2012-07-05" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Retrieve an integer or floating point number</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>SpinButton</title>

   <media type="image" mime="image/png" src="media/spinbutton.png"/>
    <p>Choose a number, by entering it or by clicking on the -/+ buttons!</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;



/* This is the callback function. 
 * It is a handler function which reacts to the signal. 
 * In this case, it will notify the user the value of their spinbutton 
 * as a label.
 */
static void
spin_clicked (GtkSpinButton *spinbutton,
              gpointer       user_data)
{
   GtkWidget *label = user_data;
   gint value = gtk_spin_button_get_value_as_int (spinbutton);

   /* %d - Is used when printing integers.
    * Note: Using g_strdup_printf returns a string that must be freed. 
    * (In which is done below)
    */
   gchar *str = g_strdup_printf ("The number you selected is %d.", value);
   gtk_label_set_text (GTK_LABEL (label), str);

   g_free(str);
}



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  /* Declare variables */
  GtkWidget *window;
  GtkWidget *label;
  GtkWidget *grid;
  GtkWidget *spin_button;
  GtkAdjustment *adjustment;


  /* Create a window with a title, a border width, and a default size */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "SpinButton Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 210, 70);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  /* Create a label to be shown in the window */
  label = gtk_label_new ("Choose a number");

  /* Create an adjustment representing an adjustable bounded value */
  adjustment = gtk_adjustment_new (0, 0, 100, 1, 0, 0);


  /* Create a spin button that is to be as wide as possible */
  spin_button = gtk_spin_button_new (adjustment, 1, 0);
  gtk_widget_set_hexpand (spin_button, TRUE);
  
  /* Connecting the "value-changed" signal for the spinbutton 
   * to the appropriate callback function. 
   */
  g_signal_connect (spin_button, 
                    "value-changed", 
                    G_CALLBACK (spin_clicked), 
                    label);


  /* Create a grid and arrange everything accordingly */
  grid = gtk_grid_new ();
  gtk_grid_set_column_spacing (GTK_GRID (grid), 10);
  gtk_grid_set_column_homogeneous (GTK_GRID (grid), TRUE);
  gtk_grid_attach (GTK_GRID (grid), spin_button, 0, 0, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), label, 0, 1, 1, 1);
  

  gtk_container_add (GTK_CONTAINER (window), grid);

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkSpinButton.html">GtkSpinButton</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkLabel.html">GtkLabel</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkAdjustment.html">GtkAdjustment</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkGrid.html">GtkGrid</link></p></item>
</list>
</page>
