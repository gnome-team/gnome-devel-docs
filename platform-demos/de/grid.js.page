<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="grid.js" xml:lang="de">
  <info>
  <title type="text">Grid (JavaScript)</title>
    <link type="guide" xref="beginner.js#layout"/>
    <revision version="0.1" date="2012-05-26" status="draft"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
	<name>Taryn Fox</name>
        <email its:translate="no">jewelfox@fursona.net</email>
    </credit>

    <desc>Widgets in Zeilen und Spalten anordnen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Grid</title>
  <media type="image" mime="image/png" src="media/grid.png"/>
  <p>Ein Knopf-Widget, das mit einem Fortschrittsbalken innerhalb eines Grids verbunden ist, welches das Layout verarbeitet.</p>

<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class GridExample {

    /* Create the application itself
       This boilerplate code is needed to build any GTK+ application. */
    constructor() {
        this.application = new Gtk.Application ({
            application_id: 'org.example.jsgrid',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal initializes menus and builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
            this._window = new Gtk.ApplicationWindow  ({ application: this.application,
                                                         window_position: Gtk.WindowPosition.CENTER,
                                                         title: "Grid Example"});

        // Create the grid
        this.Grid = new Gtk.Grid ();

        // Create the widgets inside the grid
        this.progressBar = new Gtk.ProgressBar ();
        this.Button = new Gtk.Button ({ label: "Button" });
        this.Button.connect ("clicked", this._clickHandler.bind(this));

        // Assemble the grid
        this._window.add (this.Grid);
        this.Grid.attach (this.Button, 1, 1, 1, 1);
        this.Grid.attach_next_to (this.progressBar, this.Button, Gtk.PositionType.BOTTOM, 1, 1);

                // Show the window and all child widgets
                this._window.show_all();
    }

    // Here's the function that says what happens when the button is clicked
    _clickHandler() {
        this.progressBar.pulse ();
    }


};

// Run the application
let app = new GridExample ();
app.application.run (ARGV);
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Button.html">Gtk.Button</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Grid.html">Gtk.Grid</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.ProgressBar.html">Gtk.ProgressBar</link></p></item>
</list>
</page>
