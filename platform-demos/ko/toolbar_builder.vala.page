<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="toolbar_builder.vala" xml:lang="ko">
  <info>
  <title type="text">글레이드로 만든 도구 모음(Vala)</title>
    <link type="guide" xref="beginner.vala#menu-combo-toolbar"/>
    <link type="seealso" xref="toolbar.vala"/>
    <link type="seealso" xref="grid.vala"/>
    <revision version="0.1" date="2012-05-08" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>단추 표시줄</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>글레이드로 만든 도구 모음</title>

  <media type="image" mime="image/png" src="media/toolbar.png"/>
  <p>이 예제는 XML UI 파일을 사용해 도구 모음을 만들 때 글레이드를 사용할 수 있다는 점은 빼고 <link xref="toolbar.vala"/>와 유사합니다.</p>

  <p><link href="http://glade.gnome.org/">글레이드 인터페이스 디자이너</link>로 도구 모음을 만들려면:</p>
  <steps>
    <item><p>글레이드를 열고 <file>toolbar_builder.ui</file> 파일로 저장하십시오</p>
          <p><media type="image" src="media/glade_ui.png" width="900"> 글레이드 사용자 인터페이스 스크린샷 </media></p>
    </item>

    <item><p>왼편의 <gui>컨테이너</gui>에서, 도구모음 아이콘에 마우스 커서를 올려둔 후 오른쪽 단추를 누르고 <gui>최상위에 위젯 추가</gui>를 선택하십시오.</p>
          <p><media type="image" src="media/glade_select_toolbar.png"> 글레이드 UI 도구 모음 아이콘 스크린샷 </media></p>
    </item>

    <item><p>하단 우측의 <gui>일반</gui> 탭에서 , <gui>이름</gui>을 <input>toolbar</input>로 바꾸고 <gui>Show Arrow</gui>는 <gui>No</gui>로 바꾸십시오.</p>
          <p><media type="image" src="media/glade_toolbar_general.png"> 일반 탭 스크린샷 </media></p>
    </item>

    <item><p><gui>공통 사항</gui> 탭에서, <gui>수평 확장</gui>을 <gui>Yes</gui>로 설정하십시오.</p>
         <p><media type="image" src="media/glade_toolbar_common.png"> 공통 사항 탭 스크린샷 </media></p>
     </item>

     <item><p>우측 상단의 도구 모음에서 마우스 오른쪽 단추를 누른후 <gui>편집</gui>을 선택하십시오. <gui>도구 모음 편집기</gui> 창이 뜹니다.</p>
         <p><media type="image" src="media/glade_toolbar_edit.png"> 도구 모음을 편집할 때 오른쪽 단추를 누를 부분의 스크린샷. </media></p>
   </item>

   <item><p>새 파일, 열기, 실행 취소, 전체 화면, 전체 화면 나가기 도구 단추 5개를 추가하겠습니다. 우선 새 도구 단추를 추가하겠습니다.</p>
     <steps>
       <item><p><gui>계층 구조</gui> 탭에서, <gui>추가</gui>를 누르십시오.</p></item>
       <item><p>ToolItem 이름을 <input>new_button</input>으로 바꾸십시오.</p></item>
       <item><p>스크롤을 내린 후 <gui>Is important</gui>를 <gui>Yes</gui>로 설정하십시오. 이렇게 설정하면, 도구 모음에서 도구 단추 레이블이 나타납니다.</p></item>
       <item><p><gui>동작 이름</gui>을 <input>app.new</input>로 입력하십시오.</p></item>
       <item><p><gui>레이블</gui>을 <input>New</input>로 바꾸십시오.</p></item>
       <item><p>드롭 다운 메뉴에서 <gui>새로 만들기</gui>의 스톡 ID를 선택하거나 <input>gtk-new</input>를 입력하십시오.</p></item>
     </steps>
     <p>나머지 ToolButton에 대해 위 단계를 반복하시고, 다음 속성을 설정하십시오:</p>
  <table frame="all" rules="rows">
    <thead>
      <tr>
        <td><p>Name</p></td>
        <td><p>중요 여부</p></td>
        <td><p>동작 이름</p></td>
        <td><p>Label</p></td>
        <td><p>스톡 ID</p></td>
      </tr>
    </thead>
    <tbody>
    <tr>
      <td><p>open_button</p></td>
      <td><p>예</p></td>
      <td><p>app.open</p></td>
      <td><p>열기</p></td>
      <td><p>gtk-open</p></td>
    </tr>
    <tr>
      <td><p>undo_button</p></td>
      <td><p>예</p></td>
      <td><p>win.undo</p></td>
      <td><p>실행 취소</p></td>
      <td><p>gtk-undo</p></td>
    </tr>
    <tr>
      <td><p>fullscreen_button</p></td>
      <td><p>예</p></td>
      <td><p>win.fullscreen</p></td>
      <td><p>최대 화면</p></td>
      <td><p>gtk-fullscreen</p></td>
    </tr>
    <tr>
      <td><p>leave_fullscreen_button</p></td>
      <td><p>예</p></td>
      <td><p>win.fullscreen</p></td>
      <td><p>전체 화면 나가기</p></td>
      <td><p>gtk-leave-fullscreen</p></td>
    </tr>
    </tbody>
</table>
          <media type="image" src="media/glade_toolbar_editor.png">

          </media>
    </item>

    <item><p><gui>도구 모음 편집기</gui>를 닫으십시오.</p>
   </item>

   <item><p>프로그램을 처음 시작할 때, 프로그램이 전체 화면 모드가 아니기에 <gui>전체 화면 나가기</gui> ToolButton을 나타내고 싶진 않습니다. 이 설정은 <gui>일반</gui>  탭에서 <gui>Visible</gui>을 누르고 <gui>No</gui>로 설정할 수 있습니다. ToolButton은 인터페이스 디자이너에서 여전히 나타나겠지만 프로그램 코드에 파일을 불러올 때, 상태에 따라 올바르게 나타납니다.</p>
          <p><media type="image" src="media/glade_visible_no.png"> visible 속성을 No로 설정 </media></p>
   </item>

    <item><p>작업을 저장하고 글레이드를 닫으십시오.</p>
   </item>

   <item><p>글레이드에서 만든 XML 파일은 아래와 같습니다. 도구 모음의 설명 부분입니다. 이 글을 쓰는 시점에서는 글레이드 인터페이스에서 추가할 Gtk.STYLE_CLASS_PRIMARY_TOOLBAR 옵션 클래스가 없습니다. 이 옵션 클래스를 XML 파일에 직접 추가할 수 있습니다. 이렇게 하려면, 다음 <file>toolbar_builder.ui</file> XML 코드 9번째 줄에  추가하십시오:</p>
   <code>
  &lt;style&gt;
     &lt;class name="primary-toolbar"/&gt;
  &lt;/style&gt;
  </code>
  <p>추가하지 않았더라도 프로그램은 여전히 멀쩡하게 동작합니다. Toolbar의 결과는 이 페이지 상단의 스크린샷보다는 약간 다르게 보일 수도 있습니다.</p>
   </item>
</steps>
  <code mime="application/xml" style="numbered">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;interface&gt;
  &lt;!-- interface-requires gtk+ 3.0 --&gt;
  &lt;object class="GtkToolbar" id="toolbar"&gt;
    &lt;property name="visible"&gt;True&lt;/property&gt;
    &lt;property name="can_focus"&gt;False&lt;/property&gt;
    &lt;property name="hexpand"&gt;True&lt;/property&gt;
    &lt;property name="show_arrow"&gt;False&lt;/property&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="new_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;app.new&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;New&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-new&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="open_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;app.open&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Open&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-open&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="undo_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;win.undo&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Undo&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-undo&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="fullscreen_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="visible"&gt;True&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;win.fullscreen&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Fullscreen&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-fullscreen&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
    &lt;child&gt;
      &lt;object class="GtkToolButton" id="leave_fullscreen_button"&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="can_focus"&gt;False&lt;/property&gt;
        &lt;property name="use_action_appearance"&gt;False&lt;/property&gt;
        &lt;property name="is_important"&gt;True&lt;/property&gt;
        &lt;property name="action_name"&gt;win.fullscreen&lt;/property&gt;
        &lt;property name="label" translatable="yes"&gt;Leave Fullscreen&lt;/property&gt;
        &lt;property name="use_underline"&gt;True&lt;/property&gt;
        &lt;property name="stock_id"&gt;gtk-leave-fullscreen&lt;/property&gt;
      &lt;/object&gt;
      &lt;packing&gt;
        &lt;property name="expand"&gt;False&lt;/property&gt;
        &lt;property name="homogeneous"&gt;True&lt;/property&gt;
      &lt;/packing&gt;
    &lt;/child&gt;
  &lt;/object&gt;
&lt;/interface&gt;
</code>

  <p>이제 앞서 만들었던 파일로 Toolbar를 추가하는 아래 코드를 작성하겠습니다.</p>
<code mime="text/x-csharp" style="numbered">/* This is the Window */
class MyWindow : Gtk.ApplicationWindow {

	/* Declare these two ToolButtons, as we will get them
	 * from the ui file (see lines 32 and 33), so we can
	 * hide() and show() them as needed.*/
	Gtk.ToolButton fullscreen_button;
	Gtk.ToolButton leave_fullscreen_button;

	/* Constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "Toolbar Example");

		this.set_default_size (400, 200);
		var grid = new Gtk.Grid ();
		this.add (grid);
		grid.show ();

		/* add the toolbar from the ui file */
		var builder = new Gtk.Builder ();
		try {
			builder.add_from_file ("toolbar_builder.ui");
		}
		/* Handle the exception */
		catch (Error e) {
			error ("Unable to load file: %s", e.message);
		}

		grid.attach (builder.get_object ("toolbar") as Gtk.Toolbar, 0, 0, 1, 1);

		/* get these objects from the ui file so we can toggle between them */
		fullscreen_button = builder.get_object ("fullscreen_button") as Gtk.ToolButton;
		leave_fullscreen_button = builder.get_object ("leave_fullscreen_button") as Gtk.ToolButton;

		/* create the "undo" window action action */
		var undo_action = new SimpleAction ("undo", null);
		undo_action.activate.connect (undo_callback);
		this.add_action (undo_action);

		/* create the "fullscreen" window action */
		var fullscreen_action = new SimpleAction ("fullscreen", null);
		fullscreen_action.activate.connect (fullscreen_callback);
		this.add_action (fullscreen_action);
	}

	void undo_callback (SimpleAction simple, Variant? parameter) {
			print ("You clicked \"Undo\".\n");
	}

	void fullscreen_callback (SimpleAction simple, Variant? parameter) {
		if ((this.get_window ().get_state () &amp; Gdk.WindowState.FULLSCREEN) != 0) {
			this.unfullscreen ();
			leave_fullscreen_button.hide ();
			fullscreen_button.show ();
		}
		else {
			this.fullscreen ();
			fullscreen_button.hide ();
			leave_fullscreen_button.show ();
		}
	}
}

/* This is the application */
class MyApplication : Gtk.Application {
	protected override void activate () {
		new MyWindow (this).show ();
	}

	protected override void startup () {
		base.startup ();

		/* Create the "new" action and add it to the app*/
		var new_action = new SimpleAction ("new", null);
		new_action.activate.connect (new_callback);
		this.add_action (new_action);

		/* Create the "open" action, and add it to the app */
		var open_action = new SimpleAction ("open", null);
		open_action.activate.connect (open_callback);
		this.add_action (open_action);

		/* You could also add the action to the app menu
		 * if you wanted to.
		 */
		//var menu = new Menu ();
		//menu.append ("New", "app.new");
		//this.app_menu = menu;
	}

	void new_callback (SimpleAction action, Variant? parameter) {
		print ("You clicked \"New\".\n");
	}

	void open_callback (SimpleAction action, Variant? parameter) {
			print ("You clicked \"Open\".\n");
	}
}

/* The main function creates the application and runs it. */
int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>

<p>이 예제는 다음 참고자료가 필요합니다:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Toolbar.html">Gtk.Toolbar</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ToolButton.html">Gtk.Toolbutton</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Stock.html">Gtk.Stock</link></p></item>
</list>

</page>
