<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="checkbutton.js" xml:lang="ko">
  <info>
  <title type="text">CheckButton(JavaScript)</title>
    <link type="guide" xref="beginner.js#buttons"/>
    <revision version="0.1" date="2012-06-12" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>표시를 하거나 지울 수 있는 상자입니다</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>CheckButton</title>
  <media type="image" mime="image/png" src="media/checkbutton.png"/>
  <p>이 프로그램에는 CheckButton이 있습니다. 이 확인 상자의 표시 여부에 따라 창 제목 표시줄에 무언가를 나타냅니다.</p>
  <p>CheckButton에 표시하거나 지울 때, "toggled" 시그널을 보냅니다. 표시해두면 "active" 속성 값이 true가 되고, 그렇지 않으면 false가 됩니다.</p>
    <links type="section"/>

  <section id="imports">
    <title>가져올 라이브러리</title>
    <code mime="application/javascript">
#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
</code>
    <p>이 프로그램을 실행할 때 가져올 라이브러리입니다. 시작 부분에 항상 gjs가 필요함을 알리는 줄을 작성해야 함을 기억하십시오.</p>
    </section>

  <section id="applicationwindow">
    <title>프로그램 창 만들기</title>
    <code mime="application/javascript">
class CheckButtonExample {
    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jscheckbutton',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

    // Connect 'activate' and 'startup' signals to the callback functions
    this.application.connect('activate', this._onActivate.bind(this));
    this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents window when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI ();
    }
</code>
    <p>이 예제의 모든 코드는 CheckButtonExample 클래스에 들어갑니다. 위 코드에서는 위젯과 창이 들어가는 <link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link>을 만듭니다.</p>
    <code mime="application/javascript">
    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            default_height: 100,
            default_width: 300,
            border_width: 10,
            title: "CheckButton Example"});
</code>
    <p>_buildUI 함수는 프로그램 사용자 인터페이스를 구성하는 모든 코드를 넣는 곳입니다. 우선 모든 위젯을 넣을 새 <link href="GtkApplicationWindow.js.page">Gtk.ApplicationWindow</link> 를 만들겠습니다.</p>
  </section>

  <section id="button">
    <title>확인 단추 만들기</title>
    <code mime="application/javascript">
        // Create the check button
        this._button = new Gtk.CheckButton ({label: "Show Title"});
        this._window.add (this._button);

        // Have the check button be checked by default
        this._button.set_active (true);

        // Connect the button to a function that does something when it's toggled
        this._button.connect ("toggled", this._toggledCB.bind(this));
</code>
    <p>이 코드로 자체 확인 단추를 만듭니다. 확인 단추 옆 레이블은 확인 단추의 "label" 속성에 문자열 값을 넣어 만듭니다. 이 확인 단추는 창 제목을 나타내거나 숨긴 상태를 전환하고 프로그램을 시작할 때 창 제목이 들어온 상태여야 하기 때문에, 기본적으로 확인 상자를 표시한 상태로 두려 합니다. 사용자가 상자에 표시했다 지울 때마다 _toggledCB 함수를 호출하겠습니다.</p>
    <code mime="application/javascript">
        // Show the window and all child widgets
        this._window.show_all();
    }
</code>
    <p>이 코드는 창 자체와 모든 하위 위젯의 표시를 지시하여 사용자 인터페이스 구성을 끝냅니다(이 경우 확인 단추만 해당).</p>
  </section>

  <section id="function">
    <title>확인 단추 상태 전환을 처리하는 함수입니다</title>
    <code mime="application/javascript">
    _toggledCB() {

        // Make the window title appear or disappear when the checkbox is toggled
        if (this._button.get_active() == true)
            this._window.set_title ("CheckButton Example");
        else
            this._window.set_title ("");

    }

};
</code>
    <p>CheckButton 표시를 지운 상태로 바꿀 때 창 제목을 사라지게 하려고 합니다. 다시 이걸 표시해두면 창 제목을 다시 나타나게 하려고 합니다. 단추가 활성(표시) 상태인지 여부를 나중에 확인하는 방식으로 처리할 수 있습니다. 이 과정에서 CheckButton의 get_active() 메서드를 호출하는 간단한 if/else 구문을 사용하겠습니다.</p>
    <code mime="application/javascript">
// Run the application
let app = new CheckButtonExample ();
app.application.run (ARGV);
</code>
    <p>마지막으로 완성본 CheckButtonExample 클래스의 새 인스턴스를 만들고 프로그램 실행을 설정하겠습니다.</p>
  </section>

  <section id="complete">
    <title>완전한 코드 예제</title>
<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

class CheckButtonExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jscheckbutton',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents window when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            default_height: 100,
            default_width: 300,
            border_width: 10,
            title: "CheckButton Example"});

        // Create the check button
        this._button = new Gtk.CheckButton ({label: "Show Title"});
        this._window.add (this._button);

        // Have the check button be checked by default
        this._button.set_active (true);

        // Connect the button to a function that does something when it's toggled
        this._button.connect ("toggled", this._toggledCB.bind(this));

        // Show the window and all child widgets
        this._window.show_all();
    }

    _toggledCB() {

        // Make the window title appear or disappear when the checkbox is toggled
        if (this._button.get_active() == true)
            this._window.set_title ("CheckButton Example");
        else
            this._window.set_title ("");

    }

};

// Run the application
let app = new CheckButtonExample ();
app.application.run (ARGV);
</code>
  </section>

  <section id="in-depth">
    <title>자세한 문서</title>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.CheckButton.html">Gtk.CheckButton</link></p></item>
</list>
  </section>
</page>
