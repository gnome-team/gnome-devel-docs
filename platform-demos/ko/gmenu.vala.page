<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="gmenu.vala" xml:lang="ko">
  <info>
  <title type="text">GMenu (Vala)</title>
	  <link type="guide" xref="beginner.vala#menu-combo-toolbar"/>
	  <link type="seealso" xref="aboutdialog.vala"/>
    <revision version="0.1" date="2012-04-07" status="stub"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email its:translate="no">desrt@desrt.ca</email>
      <years>2012</years>
   </credit>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>

    <desc>GMenuModel의 간단한 구현체</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>GMenu</title>
    <media type="image" mime="image/png" height="200" src="media/gmenu.vala.png"/>
    <note style="important">
       <p><em style="strong">이 코드가 동작하려면 GTK+-3.4 이상이 필요합니다</em></p>
    </note>
    <p>간단한 GMenu 및 SimpleAction과 사용하는 GtkApplication</p>

<code mime="text/x-csharp" style="numbered">/* A window in the application. */
public class Window : Gtk.ApplicationWindow {

	/* Constructor */
	public Window (Application app) {
		Object (application: app, title: "Gmenu Example");

		var about_action = new SimpleAction ("about", null);

		/* Connect the 'activate' signal to the
		 * signal handler (aka. callback).
		 */
		about_action.activate.connect (this.about_cb);

		/* Add the action to this window. */
		this.add_action (about_action);

		this.show ();
	}

	/* Signal handler for 'activate' signal of the SimpleAction. */
	void about_cb (SimpleAction simple, Variant? parameter) {
		print ("This does nothing.  It is only a demonstration.\n");
	}
}

/* This is the Application. */
public class Application : Gtk.Application {

	/* Constructor */
	public Application () {
		Object (application_id: "org.example.application");
	}

	/* Override the 'activate' signal of GLib.Application. */
	protected override void activate () {

		/* Create a new window for this application. */
		new Window (this);
	}

	/* Override the 'startup' signal of GLib.Application. */
	protected override void startup () {
		base.startup ();

		var menu = new Menu ();
		menu.append ("About", "win.about");
		menu.append ("Quit", "app.quit");
		this.app_menu = menu;

		var quit_action = new SimpleAction ("quit", null);
		quit_action.activate.connect (this.quit);
		this.add_action (quit_action);
	}
}

/* main function creates Application and runs it. */
int main (string[] args) {
	return new Application ().run (args);
}
</code>
<p>이 예제는 다음 참고자료가 필요합니다:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.valadoc.org/gio-2.0/GLib.Menu.Menu.html">Menu</link></p></item>
  <item><p><link href="http://www.valadoc.org/gio-2.0/GLib.Menu.append.html">append</link></p></item>
  <item><p><link href="http://www.valadoc.org/gio-2.0/GLib.SimpleAction.html">SimpleAction</link></p></item>
  <item><p><link href="http://www.valadoc.org/gio-2.0/GLib.ActionMap.add_action.html">add_action</link></p></item>
</list>
</page>
