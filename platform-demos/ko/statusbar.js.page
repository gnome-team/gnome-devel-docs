<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="statusbar.js" xml:lang="ko">
  <info>
  <title type="text">Statusbar (JavaScript)</title>
    <link type="guide" xref="beginner.js#display-widgets"/>
    <revision version="0.1" date="2012-06-10" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>지정 상태 표시줄에 알림 표시</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Statusbar</title>
  <media type="image" mime="image/png" src="media/statusbar2.png"/>
  <p>This statusbar keeps track of how many times you've clicked a button. Applications like <link href="https://wiki.gnome.org/Apps/Gedit">gedit</link> use statusbars to display information at a glance, and show notifications without interrupting the user.</p>
  <p>상태 표시줄에 밀어넣은 메시지는 스택의 상단에 있으며, 가장 최근에 밀어넣은 순으로 보여주면서 빼낼 수 있습니다. 지정 형식의 모든 메시지를 한번에 빼낼 수 있습니다. 이 예제 프로그램에서는 이런 기능을 보여드립니다.</p>
    <links type="section"/>

  <section id="imports">
    <title>가져올 라이브러리</title>
    <code mime="application/javascript">
#!/usr/bin/gjs

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
</code>
    <p>이 프로그램을 실행할 때 가져올 라이브러리입니다. 시작 부분에 항상 gjs가 필요함을 알리는 줄을 작성해야 함을 기억하십시오.</p>
    </section>

  <section id="applicationwindow">
    <title>프로그램 창 만들기</title>
    <code mime="application/javascript">
const StatusbarExample = new Lang.Class({
    Name: 'Statusbar Example',

    // Create the application itself
    _init: function() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jsstatusbar',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

    // Connect 'activate' and 'startup' signals to the callback functions
    this.application.connect('activate', Lang.bind(this, this._onActivate));
    this.application.connect('startup', Lang.bind(this, this._onStartup));
    },

    // Callback function for 'activate' signal presents window when active
    _onActivate: function() {
        this._window.present();
    },

    // Callback function for 'startup' signal builds the UI
    _onStartup: function() {
        this._buildUI ();
    },
</code>
    <p>이 예제의 모든 코드는 StatusbarExample 클래스에 들어갑니다. 위 코드는 위젯과 창을 넣는 <link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link>을 만듭니다.</p>
    <code mime="application/javascript">
    // Build the application's UI
    _buildUI: function() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            default_height: 120,
            default_width: 300,
            title: "Button Clicker"});

        // Create a paned interface
        this._panes = new Gtk.Paned ({
            orientation: Gtk.Orientation.VERTICAL });
</code>
    <p>_buildUI 함수는 프로그램 사용자 인터페이스를 만들 모든 코드를 넣는 곳입니다. 첫번째 단계에서는 모든 위젯을 넣을 새 <link href="GtkApplicationWindow.js.page">Gtk.ApplicationWindow</link>를 만듭니다. 그 다음 단계에서는 수직 방향 Gtk.Paned 인터페이스를 만들어 창을 두 부분으로 나눕니다. 이 방식으로 상태 표시줄을 다른 프로그램에서 나타나는 것처럼 표시하고, 사용자가 창 크기를 조절하더라도 창 하단에 머물러 있습니다.</p>
  </section>

  <section id="buttons">
    <title>단추 만들기</title>
    <code mime="application/javascript">
        // Create the main button
        this._clickMe = new Gtk.Button ({
            label: "Click Me!" });
        this._clickMe.connect ("clicked", Lang.bind (this, this._clicked));

        // Create the back button
        this._backButton = new Gtk.Button ({
            label: "gtk-go-back",
            use_stock: true });
        this._backButton.connect ("clicked", Lang.bind (this, this._back));

        // Create the clear button
        this._clearButton = new Gtk.Button ({
            label: "gtk-clear",
            use_stock: true });
        this._clearButton.connect ("clicked", Lang.bind (this, this._clear));
</code>
    <p>이 코드는 상태 표시줄에서 새 메시지를 밀어넣고, 마지막 매시지를 빼내며, 기존 모든 메시지를 지울 때 쓸 <link href="button.js.page">Gtk.Buttons</link> 세 개를 만듭니다. "뒤로"와 "지우기" 단추는 그놈에서 지원하는 언어로 이미 자동으로 번역해주는 <link href="https://developer.gnome.org/gtk3/3.4/gtk3-Stock-Items.html">스톡 단추</link> 입니다.</p>

    <code mime="application/javascript">
        // Put the buttons in a grid
        this._grid = new Gtk.Grid ({
            halign: Gtk.Align.CENTER,
            valign: Gtk.Align.CENTER });
        this._grid.attach (this._backButton, 0, 0, 1, 1);
        this._grid.attach_next_to (this._clickMe, this._backButton, Gtk.PositionType.RIGHT, 1, 1);
        this._grid.attach_next_to (this._clearButton, this._clickMe, Gtk.PositionType.RIGHT, 1, 1);

        // Put the grid in a large frame that fills most of the window
        this._topFrame = new Gtk.Frame ({
            border_width: 20,
            height_request: 90,
            width_request: 300});
        this._topFrame.add (this._grid);
</code>
    <p>단추를 모아 정돈하고 순서대로 단추를 붙여넣는데 쓸 <link href="grid.js.page">Gtk.Grid</link>를 만듭니다. 그 다음 대부분 창을  차지하고, 단추 주변에 상당히 많은 여백을 차지하는 <link href="paned.js.page">Gtk.Frame</link>를 만들고 그리드를 프레임에 추가합니다. 참고로 프레임을 Pane 인터페이스에 넣고 ApplicationWindow에 추가해야합니다.</p>
  </section>

  <section id="statusbar">
    <title>상태 표시줄 만들기</title>
    <code mime="application/javascript">
        // Create the statusbar
        this._statusbar = new Gtk.Statusbar();

        // Keep track of the number of times the button has been clicked
        this.Clicks = 0;
        this.ContextID = this._statusbar.get_context_id ("Number of Clicks");

        // Give the statusbar an initial message
        this._statusbar.push (this.ContextID, "Number of clicks: " + this.Clicks);

        // Put the statusbar in its own frame at the bottom
        this._barFrame = new Gtk.Frame ({
            height_request: 30 });
        this._barFrame.add (this._statusbar);
</code>
    <p>Gtk.Statusbar를 만들고 시작할 때 메시지를 밀어넣습니다. 그 다음 창 하단에 좁은 틀을 확보합니다.</p>
    <p>모든 메시지에는 컨텍스트 ID가 필요한데 get_context_id() 함수로 상태 표시줄에서 가져올 수 있는 정수값입니다. 각각의 컨텍스트 ID를 설명할 때 여러분이 활용할 유일한 매개 변수는 문자열 값 뿐입니다. 보통 메시지의 다양한 유형에 대해 새 컨텍스트 ID를 받기에 remove() 함수는 스택에 있는 최근 메시지가 아닌 지정 메시지를 제거할 목적으로 활용할 수 있습니다. 이 부분은 어떤 종류의 메시지에 대한 예제이므로 모든 경우에 대해 한가지만 활용하겠습니다.</p>
    <p>스택에 새 메시지를 밀어넣을때는 push() 함수를 사용합니다. 처음 매개변수는 컨텍스트 ID며 두번째는 메시지입니다.</p>
    <code mime="application/javascript">
        // Assemble the frames into the paned interface
        this._panes.pack1 (this._topFrame, true, false);
        this._panes.pack2 (this._barFrame, false, false);

        // Put the panes into the window
        this._window.add (this._panes);

        // Show the window and all child widgets
        this._window.show_all();
    },
</code>
    <p>이 코드는 하위 창에 프레임을 패킹해넣고 창에 추가한 다음, 창에게 하위 위젯을 나타내도록 지시하여 창 만들기를 끝냅니다.</p>
  </section>

  <section id="functions">
    <title>상태 표시줄을 다루는 함수</title>
    <code mime="application/javascript">
    _clicked: function() {

        // Increment the number of clicks by 1
        this.Clicks++;

        // Update the statusbar with the new number of clicks
        this._statusbar.push (this.ContextID, "Number of clicks: " + this.Clicks);

    },



    _back: function () {

        // If there have been any clicks, decrement by 1 and remove last statusbar update
        if (this.Clicks &gt; 0 ) {
            this.Clicks--;
            this._statusbar.pop (this.ContextID);
        };

    },



    _clear: function () {

        // Reset the number of clicks
        this.Clicks = 0;

        // Wipe out all the messages pushed to the statusbar
        this._statusbar.remove_all (this.ContextID);

        // Reset the statusbar's message
        this._statusbar.push (this.ContextID, "Number of clicks: " + this.Clicks);

    }

});
</code>
    <p>여기서 메시지를 스택에 밀어넣고, 상단의 메시지를 빼내며, context ID를 가진 일부 메시지를 모두 지우는 함수가 있습니다. pop() 함수는 여러분이 빼고자 하는 최근 추가 메시지 형식에 대한 context ID 매개 변수만을 가지며, 설정 형식의 모든 메시지를 스택에서 제거했을 경우는 제외됩니다.</p>
    <code mime="application/javascript">
// Run the application
let app = new StatusbarExample ();
app.application.run (ARGV);
</code>
    <p>마지막으로, StatusbarExample 클래스에서 새 인스턴스를 만들고 프로그램 실행을 설정하겠습니다.</p>
  </section>

  <section id="complete">
    <title>완전한 코드 예제</title>
<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

class StatusbarExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jsstatusbar',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents window when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            default_height: 120,
            default_width: 300,
            title: "Button Clicker"});

        // Create a paned interface
        this._panes = new Gtk.Paned ({
            orientation: Gtk.Orientation.VERTICAL });

        // Create the main button
        this._clickMe = new Gtk.Button ({
            label: "Click Me!" });
        this._clickMe.connect ("clicked", this._clicked.bind(this));

        // Create the back button
        this._backButton = new Gtk.Button ({
            label: "gtk-go-back",
            use_stock: true });
        this._backButton.connect ("clicked", this._back.bind(this));

        // Create the clear button
        this._clearButton = new Gtk.Button ({
            label: "gtk-clear",
            use_stock: true });
        this._clearButton.connect ("clicked", this._clear.bind(this));

        // Put the buttons in a grid
        this._grid = new Gtk.Grid ({
            halign: Gtk.Align.CENTER,
            valign: Gtk.Align.CENTER });
        this._grid.attach (this._backButton, 0, 0, 1, 1);
        this._grid.attach_next_to (this._clickMe, this._backButton, Gtk.PositionType.RIGHT, 1, 1);
        this._grid.attach_next_to (this._clearButton, this._clickMe, Gtk.PositionType.RIGHT, 1, 1);

        // Put the grid in a large frame that fills most of the window
        this._topFrame = new Gtk.Frame ({
            border_width: 20,
            height_request: 90,
            width_request: 300});
        this._topFrame.add (this._grid);

        // Create the statusbar
        this._statusbar = new Gtk.Statusbar();

        // Keep track of the number of times the button has been clicked
        this.Clicks = 0;
        this.ContextID = this._statusbar.get_context_id ("Number of Clicks");

        // Give the statusbar an initial message
        this._statusbar.push (this.ContextID, "Number of clicks: " + this.Clicks);

        // Put the statusbar in its own frame at the bottom
        this._barFrame = new Gtk.Frame ({
            height_request: 30 });
        this._barFrame.add (this._statusbar);

        // Assemble the frames into the paned interface
        this._panes.pack1 (this._topFrame, true, false);
        this._panes.pack2 (this._barFrame, false, false);

        // Put the panes into the window
        this._window.add (this._panes);

        // Show the window and all child widgets
        this._window.show_all();
    }

    _clicked() {

        // Increment the number of clicks by 1
        this.Clicks++;

        // Update the statusbar with the new number of clicks
        this._statusbar.push (this.ContextID, "Number of clicks: " + this.Clicks);
    }

    _back() {

        // If there have been any clicks, decrement by 1 and remove last statusbar update
        if (this.Clicks &gt; 0 ) {
            this.Clicks--;
            this._statusbar.pop (this.ContextID);
        };
    }

    _clear() {

        // Reset the number of clicks
        this.Clicks = 0;

        // Wipe out all the messages pushed to the statusbar
        this._statusbar.remove_all (this.ContextID);

        // Reset the statusbar's message
        this._statusbar.push (this.ContextID, "Number of clicks: " + this.Clicks);
    }
};

// Run the application
let app = new StatusbarExample ();
app.application.run (ARGV);
</code>
  </section>

  <section id="in-depth">
    <title>자세한 문서</title>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Button.html">Gtk.Button</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Frame.html">Gtk.Frame</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Paned.html">Gtk.Paned</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Statusbar.html">Gtk.Statusbar</link></p></item>
</list>
  </section>
</page>
