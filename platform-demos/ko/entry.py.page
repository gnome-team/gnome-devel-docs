<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="entry.py" xml:lang="ko">
  <info>
    <title type="text">Entry(Python)</title>
    <link type="guide" xref="beginner.py#entry"/>
    <link type="seealso" xref="strings.py"/>
    <link type="next" xref="scale.py"/>
    <revision version="0.2" date="2012-06-23" status="draft"/>

    <credit type="author copyright editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no"/>
      <years>2012</years>
    </credit>

    <credit type="author copyright">
      <name>Sebastian Pölsterl</name>
      <email its:translate="no">sebp@k-d-w.org</email>
      <years>2011</years>
    </credit>
    <desc>단일 행 텍스트 항목 필드</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Entry</title>
  <media type="image" mime="image/png" src="media/entry.png"/>
  <p>이 프로그램은 터미널에 입력한 이름과 함께 인사를 출력합니다.</p>

  <links type="section"/>

  <section id="code">
  <title>예제 결과를 만드는 코드</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="What is your name?", application=app)
        self.set_default_size(300, 100)
        self.set_border_width(10)

        # a single line entry
        name_box = Gtk.Entry()
        # emits a signal when the Enter key is pressed, connected to the
        # callback function cb_activate
        name_box.connect("activate", self.cb_activate)

        # add the Gtk.Entry to the window
        self.add(name_box)

    # the content of the entry is used to write in the terminal
    def cb_activate(self, entry):
        # retrieve the content of the widget
        name = entry.get_text()
        # print it in a nice form in the terminal
        print("Hello " + name + "!")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
수
</code>
  </section>

  <section id="methods">
    <title>Entry 위젯에 쓸만한 메서드</title>
    <p>14번째 줄에서 <code>"activate"</code> 시그널은 <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code> 콜백 함수에 연결했습니다. 콜백 함수에 연결했습니다. 더 자세한 설명은 <link xref="signals-callbacks.py"/>를 참조하십시오. Gtk.Entry 위젯에서 나타날 수 있는 일부 시그널은 <code>"activate"</code> (사용자가 Entry 키를 눌렀을 때 나타남), <code>"backspace"</code> (사용자가 Backspace 또는 Shift-Backspace 키를 눌렀을 때 나타남), <code>"copy-clipboard"</code> (Ctrl-c와 Ctrl-Insert키를 눌렀을 때), <code>"paste-clipboard"</code> (Ctrl-v와 Shift-Insert 키를 눌렀을 때), <code>"delete-from-cursor"</code> (문자를 지울 때 Delete 키를 눌렀을 때, 단어를 지울 경우 Ctrl-Delete키를 눌렀을 때) <code>"icon-press"</code> (사용자가 활성화할 수 있는 아이콘을 눌렀을 때 나타남), <code>"icon-release"</code> (사용자가 활성화할 수 있는 아이콘을 누른 상황에서 마우스 커서를 그 위에 두고 단추를 뗐을 때 나타남), <code>"insert-at-cursor"</code> (사용자가 커서 위치에 지정 문자열을 넣기 시작할 때 나타남), <code>"move-cursor"</code> (사용자가 커서 이동을 시작할 때 나타남), <code>"populate-popup"</code> (항목의 단축 메뉴를 나타내기 전에 나타남. 단축 메뉴에 항목을 추가할 때 사용할 수 있습니다)이 있습니다.</p>
    <list>
      <item><p><code>get_buffer()</code>와 <code>set_buffer(buffer)</code>는 항목의 버퍼를 가져오거나 설정할 때 사용할 수 있으며, <code>buffer</code> 자리에는 Gtk.EntryBuffer 객체가 들어갑니다.</p></item>
      <item><p><code>get_text()</code>와 <code>set_text("some text")</code>는 항목의 텍스트 내용을 가져오고 설정할 때 사용할 수 있습니다.</p></item>
      <item><p><code>get_text_length()</code> 메서드는 이름 그대로 동작합니다.</p></item>
      <item><p><code>get_text_area()</code>는 항목 텍스트를 그린 영역(크기)를 가져옵니다.</p></item>
      <item><p><code>set_visibility(False)</code>를 설정하면 항목에 나타난 문자를 숨깁니다. 현재 글꼴로 나타내는게 가장 좋지만, <code>set_invisible_char(ch)</code>로 특정 글자를 숨길 수 있으며, <code>ch</code> 자리에는 유니코드 문자가 들어갑니다. 후자 메서드의 반대 역할을 하는 메서드는 <code>unset_invisbile_char()</code>입니다.</p></item>
      <item><p><code>set_max_length(int)</code> 함수는 항목이 최대 결정 길이 <code>int</code>를 넘으면 자르며, <code>int</code> 값은 정수형입니다.</p></item>
      <item><p>기본적으로 Entry 키를 누르면 <code>"activate"</code> 시그널을 내보냅니다. 창의 기본 위젯으로 활성화하려면(창에서 <code>set_default(widget)</code> 메서드 호출), <code>set_activates_default(True)</code> 메서드를 호출하십시오.</p></item>
      <item><p>항목에 테두리를 설정하려면 <code>set_has_frame(True)</code>를 실행하십시오.</p></item>
      <item><p><code>set_placeholder_text("some text")</code> 메서드는 항목이 비어있으며 포커스를 주지 않았을 경우 나타낼 텍스트를 설정합니다.</p></item>
      <item><p><code>set_overwrite_mode(True)</code>및 <code>set_overwrite_mode(False)</code> 메서드는 이름 그대로 동작합니다.</p></item>
      <item><p><code>set_editable(False)</code> 메서드를 호출하면 사용자가 위젯의 텍스트를 편집할 수 없습니다.</p></item>
      <item><p><code>completion</code>이 <link href="http://developer.gnome.org/gtk3/unstable/GtkEntryCompletion.html"><code>Gtk.EntryCompletion</code></link>인 <code>set_completion(completion)</code> 함수는 <code>completion</code> 값이 <code>None</code>일 경우 completion 값을 설정하거나 비활성화합니다.</p></item>
      <item><p>항목 위젯에는 화면에 나타난 텍스트 이면의 진행 상황 또는 활동 정보를 나타낼 수 있습니다. 진행 표시줄 막대에 비율을 주어 채워넣을 목적으로 <code>set_progress_fraction(fraction)</code> 함수를 사용하며, 여기서 <code>fraction</code> 은 <code>0.0</code> 과 <code>1.0</code> 사이의 <code>float</code> 값입니다. <code>progress_pulse()</code>를 호출할 때마다 진행 표시줄 바운싱 블록을 이동할 전체 항목 너비 비율을 설정할 때 <code>set_progress_pulse_step()</code> 함수를 사용합니다. 후자 함수는 일부 진행 과정을 끝냈고 블록이 앞뒤로 튀는 항목의 진행 상황 표시자가 "활동 모드"로 들어감을 나타냅니다. 각각의 함수는 <code>progress_pulse()</code>를 호출하여블록을 약간씩 움직이게 합니다(앞에서 말한 바와 같이 <code>set_progress_pulse_step()</code> 함수로 주기당 움직이는 정도를 결정합니다).</p></item>
      <item><p>항목 위젯은 아이콘을 나타낼 수도 있습니다. 이 아이콘은 눌렀을 떄 활성화할 수 있는데, 끌어다 놓을 수 있게 할 수 있으며, 풍선 도움말을 설정할 수도 있습니다. 아이콘을 추가하려면 <code>set_icon_from_stock(icon_position, stock_id)</code> 또는 <code>set_icon_from_pixbuf(icon_position, pixbuf)</code>, <code>set_icon_from_icon_name(icon_position, icon_name)</code> 둘 중 하나를 사용하시면 되며 <code>icon_position</code> 에는 <code>Gtk.EntryIconPosition.PRIMARY</code> (항목 시작 부분에 아이콘 위치) <code>Gtk.EntryIconPosition.SECONDARY</code> (항목 뒷 부분에 아이콘 위치) 값 중 하나가 들어갑니다 아이콘에 풍선 도움말을 설정하려면, <code>set_icon_tooltip_text("풍선 도움말 문장")</code> 또는 <code>set_icon_tooltip_markup("팡고 마크업 언어를 사용한 풍선 도움말 문장")</code> 함수를 사용하십시오.</p></item>
    </list>
  </section>

  <section id="references">
    <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkEntry.html">GtkEntry</link></p></item>
    </list>
  </section>
</page>
