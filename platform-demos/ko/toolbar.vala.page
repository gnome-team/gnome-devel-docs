<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="toolbar.vala" xml:lang="ko">
  <info>
  <title type="text">Toolbar (Vala)</title>
    <link type="guide" xref="beginner.vala#menu-combo-toolbar"/>
    <link type="seealso" xref="grid.vala"/>
    <revision version="0.1" date="2012-05-08" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>단추 표시줄</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Toolbar</title>

  <media type="image" mime="image/png" src="media/toolbar.png"/>
  <p>Toolbar에는 텍스트나 스톡 아이콘을 넣을 수 있습니다. 이 예제에서는 스톡 아이콘을 사용하겠습니다. 이 예제에는 전체 화면 기능이 있습니다.</p>
  <p>SimpleAction(창과 앱)을 활용하는 예제입니다. 앱 동작은 앱 메뉴에서 쉽게 추가할 수 있습니다.</p>

<code mime="text/x-csharp" style="numbered">/* This is the Window */
class MyWindow : Gtk.ApplicationWindow {

	/* Instance variables belonging to the window */
	Gtk.Toolbar toolbar;
	Gtk.ToolButton new_button;
	Gtk.ToolButton open_button;
	Gtk.ToolButton undo_button;
	Gtk.ToolButton fullscreen_button;
	Gtk.ToolButton leave_fullscreen_button;

	/* Constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "Toolbar Example");

		this.set_default_size (400, 200);
		var grid = new Gtk.Grid ();
		this.add (grid);
		grid.show ();

		create_toolbar ();
		toolbar.set_hexpand (true);
		grid.attach (toolbar, 0, 0, 1, 1);
		toolbar.show ();

		/* create the "undo" window action action */
		var undo_action = new SimpleAction ("undo", null);
		undo_action.activate.connect (undo_callback);
		this.add_action (undo_action);

		/* create the "fullscreen" window action */
		var fullscreen_action = new SimpleAction ("fullscreen", null);
		fullscreen_action.activate.connect (fullscreen_callback);
		this.add_action (fullscreen_action);
	}

	/* This function creates the toolbar, its  ToolButtons,
	 * and assigns the actions names to the ToolButtons.*/
	void create_toolbar () {
		toolbar = new Gtk.Toolbar ();
		toolbar.get_style_context ().add_class (Gtk.STYLE_CLASS_PRIMARY_TOOLBAR);

		new_button = new Gtk.ToolButton.from_stock (Gtk.Stock.NEW);
		new_button.is_important = true; //decides whether to show the label
		toolbar.add (new_button);
		new_button.show ();
		new_button.action_name = "app.new";

		open_button = new Gtk.ToolButton.from_stock (Gtk.Stock.OPEN);
		open_button.is_important = true;
		toolbar.add (open_button);
		open_button.show ();
		open_button.action_name = "app.open";

		undo_button = new Gtk.ToolButton.from_stock (Gtk.Stock.UNDO);
		undo_button.is_important = true;
		toolbar.add (undo_button);
		undo_button.show ();
		undo_button.action_name = "win.undo";

		fullscreen_button = new Gtk.ToolButton.from_stock (Gtk.Stock.FULLSCREEN);
		fullscreen_button.is_important = true;
		toolbar.add (fullscreen_button);
		fullscreen_button.show ();
		fullscreen_button.action_name = "win.fullscreen";

		leave_fullscreen_button = new Gtk.ToolButton.from_stock (Gtk.Stock.LEAVE_FULLSCREEN)
;
		leave_fullscreen_button.is_important = true;
		toolbar.add (leave_fullscreen_button);

		leave_fullscreen_button.action_name = "win.fullscreen";
	}

	void undo_callback (SimpleAction simple, Variant? parameter) {
			print ("You clicked \"Undo\".\n");
	}

	void fullscreen_callback (SimpleAction simple, Variant? parameter) {
		if ((this.get_window ().get_state () &amp; Gdk.WindowState.FULLSCREEN) != 0) {
			this.unfullscreen ();
			leave_fullscreen_button.hide ();
			fullscreen_button.show ();
		}
		else {
			this.fullscreen ();
			fullscreen_button.hide ();
			leave_fullscreen_button.show ();
		}
	}
}

/* This is the application */
class MyApplication : Gtk.Application {
	protected override void activate () {
		new MyWindow (this).show ();
	}

	protected override void startup () {
		base.startup ();

		/* Create the "new" action and add it to the app*/
		var new_action = new SimpleAction ("new", null);
		new_action.activate.connect (new_callback);
		this.add_action (new_action);

		/* Create the "open" action, and add it to the app */
		var open_action = new SimpleAction ("open", null);
		open_action.activate.connect (open_callback);
		this.add_action (open_action);

		/* You could also add the action to the app menu
		 * if you wanted to.
		 */
		//var menu = new Menu ();
		//menu.append ("New", "app.new");
		//this.app_menu = menu;
	}

	void new_callback (SimpleAction action, Variant? parameter) {
		print ("You clicked \"New\".\n");
	}

	void open_callback (SimpleAction action, Variant? parameter) {
			print ("You clicked \"Open\".\n");
	}
}

/* The main function creates the application and runs it. */
int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>

<p>이 예제는 다음 참고자료가 필요합니다:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Toolbar.html">Gtk.Toolbar</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ToolButton.html">Gtk.Toolbutton</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Stock.html">Gtk.Stock</link></p></item>
</list>

</page>
