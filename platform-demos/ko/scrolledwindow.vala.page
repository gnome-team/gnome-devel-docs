<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="scrolledwindow.vala" xml:lang="ko">
  <info>
  <title type="text">ScrolledWindow (Vala)</title>
    <link type="guide" xref="beginner.vala#scrolling"/>
    <link type="seealso" xref="image.vala"/>
    <revision version="0.1" date="2012-06-08" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>스크롤 표시줄을 하위 위젯에 추가합니다</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>ScrolledWindow</title>
  <media type="image" mime="image/png" src="media/scrolledwindow.png"/>
  <p>스크롤 창에 이미지를 나타냅니다.</p>

<code mime="text/x-csharp" style="numbered">/* This is the application. */
public class MyApplication : Gtk.Application {
	/* Override the 'activate' signal of GLib.Application. */
	protected override void activate () {
		/* Create the window of this application. */
		var window = new Gtk.ApplicationWindow (this);
		window.title = "ScrolledWindow Example";
		window.set_default_size (200, 200);

		var scrolled_window = new Gtk.ScrolledWindow (null, null);
		scrolled_window.set_border_width (10);
		scrolled_window.add_with_viewport (new Gtk.Image.from_file ("gnome-image.png"));
		scrolled_window.set_policy (Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC);

		window.add (scrolled_window);
		window.show_all ();
	}
}

/* main creates and runs the application. */
public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>이 예제는 다음 참고자료가 필요합니다:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ScrolledWindow.html">Gtk.ScrolledWindow</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.PolicyType.html">Gtk.PolicyType</link></p></item>
</list>
</page>
