<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="toolbar.py" xml:lang="ko">
  <info>
    <title type="text">Toolbar (Python)</title>
    <link type="guide" xref="beginner.py#menu-combo-toolbar"/>
    <link type="seealso" xref="grid.py"/>
    <link type="next" xref="tooltip.py"/>
    <revision version="0.1" date="2012-06-05" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>단추 및 기타 위젯 표시줄</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Toolbar</title>

  <media type="image" mime="image/png" src="media/toolbar.png"/>
  <p>(스톡 아이콘에서 온)단추를 Toolbar에 넣은 예제입니다.</p>

  <links type="section"/>

  <section id="code">
    <title>예제 결과를 만드는 코드</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Toolbar Example", application=app)
        self.set_default_size(400, 200)

        # a grid to attach the toolbar
        grid = Gtk.Grid()

        # a toolbar created in the method create_toolbar (see below)
        toolbar = self.create_toolbar()
        # with extra horizontal space
        toolbar.set_hexpand(True)
        # show the toolbar
        toolbar.show()

        # attach the toolbar to the grid
        grid.attach(toolbar, 0, 0, 1, 1)

        # add the grid to the window
        self.add(grid)

        # create the actions that control the window and connect their signal to a
        # callback method (see below):

        # undo
        undo_action = Gio.SimpleAction.new("undo", None)
        undo_action.connect("activate", self.undo_callback)
        self.add_action(undo_action)

        # fullscreen
        fullscreen_action = Gio.SimpleAction.new("fullscreen", None)
        fullscreen_action.connect("activate", self.fullscreen_callback)
        self.add_action(fullscreen_action)

    # a method to create the toolbar
    def create_toolbar(self):
        # a toolbar
        toolbar = Gtk.Toolbar()

        # which is the primary toolbar of the application
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)

        # create a button for the "new" action, with a stock image
        new_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_NEW)
        # label is shown
        new_button.set_is_important(True)
        # insert the button at position in the toolbar
        toolbar.insert(new_button, 0)
        # show the button
        new_button.show()
        # set the name of the action associated with the button.
        # The action controls the application (app)
        new_button.set_action_name("app.new")

        # button for the "open" action
        open_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_OPEN)
        open_button.set_is_important(True)
        toolbar.insert(open_button, 1)
        open_button.show()
        open_button.set_action_name("app.open")

        # button for the "undo" action
        undo_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_UNDO)
        undo_button.set_is_important(True)
        toolbar.insert(undo_button, 2)
        undo_button.show()
        undo_button.set_action_name("win.undo")

        # button for the "fullscreen/leave fullscreen" action
        self.fullscreen_button = Gtk.ToolButton.new_from_stock(
            Gtk.STOCK_FULLSCREEN)
        self.fullscreen_button.set_is_important(True)
        toolbar.insert(self.fullscreen_button, 3)
        self.fullscreen_button.set_action_name("win.fullscreen")

        # return the complete toolbar
        return toolbar

    # callback method for undo
    def undo_callback(self, action, parameter):
        print("You clicked \"Undo\".")

    # callback method for fullscreen / leave fullscreen
    def fullscreen_callback(self, action, parameter):
        # check if the state is the same as Gdk.WindowState.FULLSCREEN, which
        # is a bit flag
        is_fullscreen = self.get_window().get_state(
        ) &amp; Gdk.WindowState.FULLSCREEN != 0
        if not is_fullscreen:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_LEAVE_FULLSCREEN)
            self.fullscreen()
        else:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_FULLSCREEN)
            self.unfullscreen()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # create the actions that control the window and connect their signal to a
        # callback method (see below):

        # new
        new_action = Gio.SimpleAction.new("new", None)
        new_action.connect("activate", self.new_callback)
        app.add_action(new_action)

        # open
        open_action = Gio.SimpleAction.new("open", None)
        open_action.connect("activate", self.open_callback)
        app.add_action(open_action)

    # callback method for new
    def new_callback(self, action, parameter):
        print("You clicked \"New\".")

    # callback method for open
    def open_callback(self, action, parameter):
        print("You clicked \"Open\".")

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Toolbar 위젯에 쓸만한 메서드</title>
    <p>32번째 줄에서 <code>undo_action</code> 동작의 <code>"activate"</code> 시그널은 <code><var>action</var>.connect(<var>signal</var>, <var>callback function</var>)</code> 함수로 <code>undo_callback()</code> 콜백 함수에 연결했습니다. 더 자세한 설명은 <link xref="signals-callbacks.py"/>를 참조하십시오.</p>

    <list>
      <item><p><code>position</code> 위치에 <code>tool_item</code>을 넣으려면 <code>insert(tool_item, position)</code> 함수를 사용하십시오. <code>position</code> 값이 음수면 해당 항목을 도구 모음 마지막에 붙여넣습니다.</p></item>
      <item><p><code>get_item_index(tool_item)</code> 함수는 도구 모음에서 <code>tool_item</code> 항목의 위치를 가져옵니다.</p></item>
      <item><p><code>get_n_items()</code> 함수는 도구 모음의 항목 갯수를 가져옵니다. <code>get_nth_item(position)</code> 함수는 <code>position</code> 위치의 항목을 반환합니다.</p></item>
      <item><p>모든 메뉴 항목에 대해 ToolBar에서 공간을 가지고 있지 않고, <code>set_show_arrow(True)</code> 함수를 실행했다면, 공간이 없는 항목은 오버플로우 메뉴에 나타납니다.</p></item>
      <item><p><code>set_icon_size(icon_size)</code> 함수는 도구 모음의 아이콘 크기를 설정합니다. <code>icon_size</code> 값은 <code>Gtk.IconSize.INVALID, Gtk.IconSize.MENU, Gtk.IconSize.SMALL_TOOLBAR, Gtk.IconSize.LARGE_TOOLBAR, Gtk.IconSize.BUTTON, Gtk.IconSize.DND, Gtk.IconSize.DIALOG</code> 중 하나가 될 수 있습니다. 도구 모음에 지정 목적을 위해서만 사용해야 하며 일반 프로그램 도구 모음에서는 사용자가 원하는 아이콘 크기 설정을 따라야합니다. <code>unset_icon_size()</code> 함수는 <code>set_icon_size(icon_size)</code> 함수로 설정한 항목의 설정을 취소하므로 아이콘 크기를 결정할 때 사용자 설정 값을 사용합니다.</p></item>
      <item><p><code>style</code> 값을 <code>Gtk.ToolbarStyle.ICONS, Gtk.ToolbarStyle.TEXT, Gtk.ToolbarStyle.BOTH, Gtk.ToolbarStyle.BOTH_HORIZ</code> 중 하나로 사용하는 <code>set_style(style)</code> 함수는 도구 모음에 아이콘만 나타낼 지, 텍스트만 나타낼 지, 아니면 둘 다(수직 방향, 수평 방향) 나타낼 지를 설정합니다. 사용자 설정으로 도구 모음 모양새를 결정하게 하고, 도구 모음 모양새 설정을 취소하려면 <code>unset_style()</code> 함수를 사용하십시오.</p></item>
    </list>

  </section>

  <section id="reference">
    <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolbar.html">GtkToolbar</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolButton.html">GtkToolButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolItem.html">GtkToolItem</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">스톡 항목</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkActionable.html">GtkActionable</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWidget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gdk3/unstable/gdk3-Event-Structures.html#GdkEventWindowState">이벤트 구조체</link></p></item>
    </list>
  </section>
</page>
