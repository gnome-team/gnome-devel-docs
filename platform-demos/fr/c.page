<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/experimental/ui/" type="guide" id="c" xml:lang="fr">

  <info>
    <!-- The text title is used on the help.gnome.org -->
    <title type="link">C</title>
    <link type="guide" xref="index#tutorials"/>
    <revision version="3.4" date="2012-04-28" status="stub"/>

    <credit type="editor">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Bastian Ilso</name>
      <email its:translate="no">bastianilso@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tutorials and code samples in C.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Platform Demos in C</title>

  <section id="prerequisites">

    <title>Prerequisites</title>

    <p>To compile these platform demos you will need a C compiler, for example
    <link href="https://gcc.gnu.org/">gcc</link>.
    You will also need GTK+ 3 including headers and pkg-config files.
    Linux distributions usually abstract GTK+ 3 headers and pkg-config files
    into separate packages called <em>gtk3-devel</em>, <em>gtk3-dev</em>
    or similar.</p>

  </section>

  <section id="for-beginners" ui:expanded="false">

    <title>Guidelines for new developers</title>

    <p>Beyond the prerequisites mentioned above, several tools has been
    developed to assist you further in developing your GNOME application.
    GNOME also recommends a specific coding style and conventions which this
    section will elaborate further upon.</p>

    <section id="recommended-tools">
      <title>Recommended tools</title>

      <list>
        <item>
          <p><link href="https://wiki.gnome.org/Apps/Devhelp">DevHelp</link>:
            An API documentation browser for GNOME.</p>
        </item>
        <item>
          <p>A GNOME IDE (Integrated Development Environment).
          An IDE can assist you in writing and debugging C code,
          for example <link href="https://wiki.gnome.org/Apps/Builder">Builder</link>
          or <link href="https://wiki.gnome.org/Apps/Anjuta">Anjuta</link>.</p>
        </item>
        <item>
          <p><link href="http://glade.gnome.org/">Glade</link>:
          A user interface designer for GTK+ Applications.</p>
        </item>
        <item>
          <p>An IRC Client, for example
          <link href="https://wiki.gnome.org/Apps/Polari">Polari</link> or
          <link href="https://wiki.gnome.org/Apps/Empathy">Empathy</link>.
          If you run into problems while following the beginner tutorials,
          join #gnome on <em>irc.gnome.org</em>.</p>
        </item>
      </list>
    </section>

    <section id="coding-style">
      <title>Coding Style</title>

      <p>Most applications in the GNOME project is written in the
      <link href="http://en.wikipedia.org/wiki/Indent_style#GNU_style">GNU style</link>
      of indentation. The tutorials and code examples presented are following
      this convention and you are recommended to do so too.</p>
    </section>

    <section id="executing-code">
      <title>Executing Code</title>

      <p>Pour exécuter les exemples de code :</p>

      <steps>
        <item>
          <p>Copiez et collez le code dans <var>nomdufichier</var>.c</p>
        </item>
        <item>
          <p>Saisissez dans le terminal :</p>
          <screen>gcc <var>filename</var>.c `pkg-config --cflags --libs gtk+-3.0` -o <var>filename</var></screen>
          <screen>./<var>nomdufichier</var></screen>
        </item>
      </steps>

      <note>
        <p>Pour plus d'informations, consultez <link href="http://developer.gnome.org/gtk3/3.4/gtk-compiling.html">Compiler des applications GTK+ sur UNIX</link>.</p>
      </note>
    </section>

  </section>

  <section id="examples">
    <title>Tutoriels</title>
  </section>

<section id="samples">
<title>Widget Code Examples</title>
 <section id="windows" style="2column"><title>Fenêtres</title>
    <p/>
  </section>
  <section id="display-widgets" style="2column"><title>Éléments graphiques d'affichage</title>
  </section>
  <section id="buttons" style="2column"><title>Boutons et inverseurs</title>
  </section>
  <section id="entry" style="2column"><title>Entrée de données numériques et de texte</title>
  </section>
  <section id="multiline" style="2column"><title>Éditeur de texte multiligne</title>
  </section>
  <section id="menu-combo-toolbar" style="2column"><title>Menu, boîte combinée et éléments graphiques de barre d'outils</title>
  </section>
  <section id="treeview" style="2column"><title>Élément graphique arborescent</title>
  </section>
  <section id="selectors"><title>Sélecteurs</title>
    <section id="file-selectors" style="2column"><title>Sélecteurs de fichiers</title>
    </section>
    <section id="font-selectors" style="2column"><title>Sélecteurs de polices</title>
    </section>
    <section id="color-selectors" style="2column"><title>Sélecteurs de couleurs</title>
    </section>
  </section>
  <section id="layout" style="2column"><title>Conteneurs d'agencement</title>
  </section>
  <section id="ornaments" style="2column"><title>Décorations</title>
  </section>
  <section id="scrolling" style="2column"><title>Barre de défilement</title>
  </section>
  <section id="misc" style="2column"><title>Divers</title>
  </section>

</section>

</page>
