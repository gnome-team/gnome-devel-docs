<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="button.py" xml:lang="fr">
  <info>
    <title type="text">Button (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="linkbutton.py"/>
    <revision version="0.2" date="2012-05-05" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Un bouton qui émet un signal quand il est cliqué</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Button</title>

  <media type="image" mime="image/png" src="media/button.png"/>
  <p>Un bouton connecté à une fonction de rappel simple.</p>

  <links type="section"/>

  <section id="code">
    <title>Code utilisé pour générer cet exemple</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GNOME Button", application=app)
        self.set_default_size(250, 50)

        # a button
        button = Gtk.Button()
        # with a label
        button.set_label("Click me")
        # connect the signal "clicked" emitted by the button
        # to the callback function do_clicked
        button.connect("clicked", self.do_clicked)
        # add the button to the window
        self.add(button)

    # callback function connected to the signal "clicked" of the button
    def do_clicked(self, button):
        print("You clicked me!")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>
  
  <section id="methods">
    <title>Méthode utiles pour un composant graphique Button</title>
    <p>In line 16 the <code>"clicked"</code> signal from the button is connected to the callback function <code>do_clicked()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>
    <list>
      <item><p>La méthode <code>set_relief(Gtk.ReliefStyle.NONE)</code> annule le style en relief des bordures du Gtk.Button - à l'opposé, <code>Gtk.ReliefStyle.NORMAL</code> le rétablit.</p></item>
      <item><p>Si l'étiquette du bouton est issue de la <link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">collection d'icônes</link>, la méthode <code>set_use_stock(True)</code> assigne l'étiquette comme étant le nom de l'icône correspondante de la collection.</p></item>
      <item><p>Pour assigner une image (par ex. une image de la collection d'images) au bouton <code>button</code> :</p>
        <code>
image = Gtk.Image()
image.set_from_stock(Gtk.STOCK_ABOUT, Gtk.IconSize.BUTTON)
button.set_image(image)</code>
      <p>Vous ne devez plus assigner d'étiquette au bouton après cela, sinon il affichera l'étiquette au lieu de l'image.</p></item>
      <item><p>La fonction <code>set_focus_on_click(False)</code> empêche le bouton de prendre le focus quand vous cliquez dessus avec la souris. Cela peut être utile dans les barres d'outils, afin que la zone principale de l'application ne perde pas le focus clavier.</p></item>
    </list>
  </section>
  
  <section id="references">
    <title>Références API</title>
    <p>Dans cet exemple, les éléments suivants sont utilisés :</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkButton.html">GtkButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
    </list>
  </section>
</page>
