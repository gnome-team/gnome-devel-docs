<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="signals-callbacks.py" xml:lang="fr">

<info>
  <title type="text">Signals and callbacks (Python)</title>
  <link type="guide" xref="beginner.py#theory"/>
  <link type="next" xref="button.py"/>
  <revision version="0.1" date="2012-06-16" status="draft"/>

  <desc>Une explication des signaux et rappels dans GTK+.</desc>
  <credit type="author copyright">
    <name>Sebastian Pölsterl</name>
    <email its:translate="no">sebp@k-d-w.org</email>
    <years>2011</years>
  </credit>
  <credit type="editor">
    <name>Marta Maria Casetti</name>
    <email its:translate="no">mmcasetti@gmail.com</email>
    <years>2012</years>
  </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Signaux et rappels</title>

<links type="section"/>

<section id="overview">
<title>Présentation</title>

<p>Comme la plupart des boîtes à outils d'interfaces utilisateurs, GTK+ utilise un modèle de programmation événementiel. Lorsque l'utilisateur ne fait rien, GTK+ attend qu'une entrée se fasse dans la boucle principale. Dès que l'utilisateur fait quelque chose (disons un clic de souris), alors la boucle principale « se réveille » et fournit un événement à GTK+.</p>

<p>Lorsque les éléments graphiques reçoivent un événement, il émettent la plupart du temps un ou plusieurs signaux. Ces signaux indiquent à votre programme que « quelque chose d'intéressant vient de se passer » en exécutant les fonctions que vous leur avez connecté. Lorsque vos rappels sont exécutés, vous agissez normalement en conséquence. Après la fin d'un rappel, GTK+ retourne dans la boucle principale et attend d'autres événements.</p>

<p>Voici un exemple générique : <code>handler_id = widget.connect(evenement, rappel, donnees)</code>. <code>widget</code> est un exemple d'élément graphique que nous avons créé un peu plus tôt. Puis vient l'<code>evenement</code> qui nous intéresse. Chaque élément graphique possède ses propres événements pouvant arriver. Par exemple, si vous connectez un Gtk.Button à l'événement « cliqué » : chaque fois que ce bouton est cliqué, le signal est émis. Un autre exemple avec le signal <code>notify::property</code> : chaque fois qu'une <link xref="properties.py">propriété</link> d'un GObject est modifiée, GObject associe comme détail à l'émission de ce signal, le nom de la propriété modifiée au lieu de seulement émettre le signal <code>notify</code>. Cela permet aux clients qui ne veulent être informés des modifications que d'une seule propriété de filtrer la plupart des événements avant de les recevoir. Troisièmement, l'argument de rappel est le nom de la fonction de rappel, qui contient le code en cours d'exécution quand les signaux de ce type spécifique sont émis. Enfin, l'argument optionnel de données inclut toute donnée devant être passée quand le signal est émis.</p>

<p>La fonction renvoie un nombre (le <code>handler_id</code>) qui identifie cette paire particulière de signal-rappel. Ce nombre est nécessaire pour se déconnecter d'un signal de façon à ce que la fonction de rappel ne soit plus appelée, ni par la suite, ni pendant les émissions du signal à laquelle elle est actuellement connectée, comme dans <code>widget.disconnect(handler_id)</code>.</p>

</section>

<section id="references">

<title>Références</title>
<p><link href="http://developer.gnome.org/gobject/stable/signal.html">Les signaux</link> dans la documentation GObject</p>
<p><link href="http://python-gtk-3-tutorial.readthedocs.org/en/latest/basics.html">Basics - Main loop and Signals</link> in Python GTK+ 3 Tutorial</p>
</section>


</page>
