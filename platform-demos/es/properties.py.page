<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="properties.py" xml:lang="es">

<info>
  <title type="text">Properties (Python)</title>
  <link type="guide" xref="beginner.py#theory"/>
  <link type="next" xref="grid.py"/>
  <revision version="0.1" date="2012-06-24" status="draft"/>

  <desc>Una explicación de propiedades, funciones de obtención y asignación.</desc>
  <credit type="author copyright">
    <name>Sebastian Pölsterl</name>
    <email its:translate="no">sebp@k-d-w.org</email>
    <years>2011</years>
  </credit>
  <credit type="editor">
    <name>Marta Maria Casetti</name>
    <email its:translate="no">mmcasetti@gmail.com</email>
    <years>2012</years>
  </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Propiedades</title>

<links type="section"/>

<section id="overview">
<title>Visión general</title>

<p>Las <em>propiedades</em> describen la configuración y el estado de los widgets, y cada widget tiene su conjunto particular de propiedades. Por ejemplo, un widget como un botón o una etiqueta tiene la propiedad <code>label</code> que contiene el texto del widget. Puede especificar el nombre y el valor de cualquier número de propiedades como argumentos de palabras clave cuando cree una instancia de un widget: por ejemplo, para crear una etiqueta alineada a la derecha con el texto «Hello World» y un ángulo de 25 grados, puede usar:</p>
<code>
label = Gtk.Label(label="Hello World", angle=25, halign=Gtk.Align.END)</code>

<p>Alternativamente, puede definir estas propiedades por separado usando el método asociado a ellas.</p>
<code>
label = Gtk.Label()
label.set_label("Hello World")
label.set_angle(25)
label.set_halign(Gtk.Align.END)</code>

<p>Una vez que haya creado esta etiqueta, puede obtener su texto con <code>label.get_label()</code>, y análogamente para las otras propiedades.</p>

<p>En lugar de usar funciones de obtención y asignación, también puede obtener y establecer las propiedades con <code>get_property(<var>"nombre-propiedad"</var>)</code> y <code>set_property("<var>nombre-propiedad</var>")</code>, respectivamente.</p>

</section>
<section id="references">
<title>Referencias</title>

<p><link href="http://python-gtk-3-tutorial.readthedocs.org/en/latest/basics.html">Conceptos básicos sobre propiedades</link> en el tutorial de GTK+3 con Python</p>
</section>

</page>
