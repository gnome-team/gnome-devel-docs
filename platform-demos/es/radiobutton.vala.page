<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="radiobutton.vala" xml:lang="es">
  <info>
  <title type="text">RadioButton (Vala)</title>
    <link type="guide" xref="beginner.vala#buttons"/>
    <link type="seealso" xref="togglebutton.vala"/>
    <link type="seealso" xref="grid.vala"/>

    <revision version="0.1" date="2012-05-09" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Una elección desde varios CheckButton.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>RadioButton</title>
  <media type="image" mime="image/png" src="media/radiobutton.png"/>
  <p>Estos RadioButton informan de su actividad en la terminal.</p>

<code mime="text/x-csharp" style="numbered">public class MyWindow : Gtk.ApplicationWindow {
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "RadioButton Example");

		this.border_width = 20;
		this.set_default_size (250, 100);

		/* We demonstrate 3 different RadioButton creation methods */

		//Create a Radio Button
		var button1 = new Gtk.RadioButton (null);
		button1.set_label ("Button 1");

		//Create a RadioButton with a label, and add it to the same group as button1.
		var button2 = new Gtk.RadioButton.with_label (button1.get_group(),"Button 2");

		//Create a RadioButton with a label, adding it to button1's group.
		var button3 = new Gtk.RadioButton.with_label_from_widget (button1, "Button 3");

		//Attach the buttons to a grid.
		var grid = new Gtk.Grid ();
		grid.attach (button1, 0, 0, 1, 1);
		grid.attach (button2, 0, 1, 1, 1);
		grid.attach (button3, 0, 2, 1, 1);

		//Add the button to the window.
		this.add (grid);

		//Connect the signal handlers (aka. callback functions) to the buttons.
		button1.toggled.connect (button_toggled_cb);
		button2.toggled.connect (button_toggled_cb);
		button3.toggled.connect (button_toggled_cb);
	}

	void button_toggled_cb (Gtk.ToggleButton button)
	{
		var state = "unknown";

		if (button.get_active ())
			state = "on";
		else {
			state = "off";
			print ("\n");
		}
		print (button.get_label() + " was turned " + state + "\n");
	}
}

public class MyApplication : Gtk.Application {
	protected override void activate () {

		//Show all of the things.
		new MyWindow (this).show_all ();
	}

	internal MyApplication () {
		Object (application_id: "org.example.MyApplication");
	}
}

public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>En este ejemplo se usa lo siguiente:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.RadioButton.html">Gtk.RadioButton</link></p></item>
</list>
</page>
