<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="entry.vala" xml:lang="es">
  <info>
  <title type="text">Entry (Vala)</title>
    <link type="guide" xref="beginner.vala#entry"/>
    <revision version="0.1" date="2012-05-03" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Un campo de entrada de texto de una sola línea</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Entry</title>
  <media type="image" mime="image/png" src="media/entry.png"/>
  <p>Esta aplicación le saluda en la terminal.</p>

<code mime="text/x-csharp" style="numbered">/* A window in the application. */
class MyWindow : Gtk.ApplicationWindow {

	/* Constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "What is your name?");

		var name_box = new Gtk.Entry ();

		/* Connect to the signal handler. */
		name_box.activate.connect (this.on_activate);
		name_box.show ();

		this.set_default_size (300, 100);
		this.border_width = 10;

		/* Add the name_box to this window. */
		this.add (name_box);
	}

	/* Signal handler (aka. callback function) for the 'activate'
	 * signal of a Gtk.Entry.
	 */
	void on_activate (Gtk.Entry entry) {
		name = entry.get_text ();
		print ("\nHello " + name + "!\n\n");
	}
}

/* This is the application. */
class MyApplication : Gtk.Application {

	/* Constructor for the application. */
	internal MyApplication () {
		Object (application_id: "org.example.MyApplication");
	}

	/* Override the 'activate' signal of GLib.Application. */
	protected override void activate () {

		/* Create a new window for this application
		 * and show it. */
		new MyWindow (this).show ();
	}

}

/* The main function creates and runs the application. */
int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>En este ejemplo se usa lo siguiente:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Entry.html">Gtk.Entry</link></p></item>
</list>
</page>
