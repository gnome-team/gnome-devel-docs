<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-io" xml:lang="el">
  <info>
    <link type="guide" xref="index" group="io"/>
    <revision version="0.1" date="2012-02-24" status="review"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Πρόσβαση σε δομημένη αποθήκη δεδομένων, κοινόχρηστη δικτύωση και αρχεία.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2010-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Τζένη Πετούμενου</mal:name>
      <mal:email>epetoumenou@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκυδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Αρχεία &amp; πρόσβαση δεδομένων</title>

<list>
 <item>
  <p><em style="strong">Ασύγχρονη ανάγνωση και εγγραφή αρχείων και άλλων ροών</em></p>
 </item>
 <item>
  <p><em style="strong">Αποθήκευση και ανάκτηση μεταδεδομένων εγγράφου</em></p>
 </item>
</list>

<p>Seamlessly access local and remote files using the core GNOME IO libraries.
Make your application responsive by using the extensive support for
asynchronous IO operations. Allow users to find files easily by providing
metadata to describe documents.</p>

<section id="what">
 <title>Τι μπορείτε να κάνετε;</title>

  <p>Για <em style="strong">ασύγχρονη ανάγνωση και εγγραφή αρχείων και άλλων ροών</em>, χρησιμοποιήστε <em style="strong" xref="tech-gio">GIO</em>. Παρέχεται ένα υψηλού επιπέδου API VFS (εικονικό σύστημα αρχείων), καθώς και βοηθήματα όπως εικονίδια και εκκίνηση εφαρμογής</p>

  <p>Χρησιμοποιήστε το <em style="strong" xref="tech-tracker">Tracker</em> για <em style="strong">αποθήκευση και ανάκτηση μεταδεδομένων ενός εγγράφου</em>, καθώς και <em style="strong">δομημένα δεδομένα</em> όπως επαφές.</p>

</section>

<!-- TODO Add link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
  <title>Πραγματικά παραδείγματα</title>

  <p>Μπορείτε να δείτε πολλές πραγματικές εφαρμογές των τεχνολογιών εισόδου/εξόδου του GNOME σε έργα ανοικτού λογισμικού, όπως τα παραδείγματα που δίνονται παρακάτω.</p>
  <list>
    <item>
      <p>Τα <em style="strong">Έγγραφα</em> διευκολύνουν την εύρεση των αρχείων σας με το <em style="strong">Tracker</em>.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Documents">Website</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-documents">Source code</link> )</p>
  </item>
    <item>
      <p>Τα <em style="strong">Αρχεία</em> αξιοποιεί το <em style="strong">GIO</em> για να διευκολύνει τη διαχείριση τοπικών και απομακρυσμένων αρχείων.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Nautilus">Website</link> | <link href="https://wiki.gnome.org/Apps/Nautilus/Screenshots">Screenshots</link> | <link href="https://gitlab.gnome.org/GNOME/nautilus">Source code</link> )</p>
  </item>
 </list>
</section>
</page>
