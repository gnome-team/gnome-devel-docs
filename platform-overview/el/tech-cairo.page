<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-cairo" xml:lang="el">

  <info>
    <link type="guide" xref="tech" group="cairo"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>2Δ, σχεδίαση με βάση διάνυσμα για υψηλής ποιότητας γραφικά</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2010-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Τζένη Πετούμενου</mal:name>
      <mal:email>epetoumenou@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκυδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Cairo</title>

<p>Η Cairo είναι μια βιβλιοθήκη 2Δ γραφικών που περιλαμβάνει μια προηγμένη API για τη σχεδίαση διανυσματικών γραφικών, σύνθεσης εικόνων και εμφάνισης εξομαλυμένου κειμένου. Η Cairo υποστηρίζει πολλαπλές συσκευές εξόδου, συμπεριλαμβανομένου του συστήματος παραθύρου X , τα Microsoft Windows και ενδιάμεσες μνήμης εικόνων, επιτρέποντάς σας να γράφετε διαλειτουργικό κώδικα για τη σχεδίαση γραφικών σε διαφορετικά μέσα.</p>

<p>Το μοντέλο σχεδίασης της Cairo είναι παρόμοιο με αυτά που παρέχονται από τα PostScript και PDF. Το API της Cairo παρέχει τέτοιες λειτουργίες σχεδίασης όπως το βάψιμο και το γέμισμα κυβικών ευλύγιστων καμπυλών Bézier, η σύνθεση εικόνων και η εκτέλεση συγγενικών μετατροπών. Αυτές οι διανυσματικές λειτουργίες επιτρέπουν να έχετε πλούσια, εξομαλυμένα γραφικά.</p>

<p>Το προηγμένο μοντέλο σχεδίασης της Cairo προσφέρει απόδοση υψηλής ποιότητας σε διάφορα μέσα. Χρησιμοποιώντας το ίδιο API μπορείτε να δημιουργήσετε κείμενο και γραφικά οθόνης, να αποδώσετε εικόνες, ή να δημιουργήσετε σαφείς εξόδους κατάλληλους για εκτύπωση.</p>

<p>You should use Cairo whenever you need to draw graphics in your
application beyond the widgets provided by GTK.  Almost all of the drawing
inside GTK is done using Cairo.  Using Cairo for your custom drawing
will allow your application to have high-quality, anti-aliased, and
resolution-independent graphics.</p>

<list style="compact">
  <item><p><link href="http://www.cairographics.org/manual/">Εγχειρίδιο του Cairo</link></p></item>
  <item><p><link href="http://www.cairographics.org">Η ιστοσελίδα του Cairo</link></p></item>
</list>
</page>
