<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gobject" xml:lang="uk">
  <info>
    <link type="guide" xref="tech" group="gobject"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Заснована на C система об'єктів та типів із сигналами та слотами</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>GObject</title>

  <p>GObject є частиною бібліотеки <link xref="tech-glib">GLib</link>, яка надає у ваше розпорядження систему об'єктів і типів для програм мовою C. Хоча у C, як мові програмування, не передбачено об'єктів і класів, GObject уможливлює написання об'єктно-орієнтованих програм мовою C.</p>

  <p>У GObject реалізовано базовий GType — основу усієї системи типів, похідними від якого є інші типи, зокрема цілочисельні та рядкові. Далі, є сам GObject, базовий клас для усіх інших класів. Об'єкти можуть видавати <em>сигнали</em> для сповіщення функції, яка їх викликає, про те, що сталося щось цікаве. Наприклад, об'єкт <code>Button</code> може видати сигнал <code>clicked</code>, який повідомлятиме, що кнопку було натиснуто і відпущено.</p>

  <p>GObject є придатним до <em>інтроспекції</em>, що означає, що ви можете опитати систему GObject щодо типів класів, які визначено, методів, підтримку яких передбачено, їхніх сигналів тощо. На основі цих даних інтроспекції побудовано прив'язки GNOME до мов програмування. Замість написання обгорток для уможливлення викликів програмного інтерфейсу GNOME з інших мов програмування вручну, прив'язки до мов використовують дані інтроспекції з GObject для автоматичного створення цих обгорток.</p>

  <p>Зазвичай, якщо ви програмуєте для GNOME об'єктно-орієнтованою мовою, вам не слід перейматися питаннями самого GObject. Втім, знайомство із поняттями GObject, зокрема сигналами і слотами, спростить ваше життя як програміста GNOME.</p>

  <list style="compact">
    <item><p><link href="http://developer.gnome.org/gobject/stable/">Довідник-підручник з GObject</link></p></item>
  </list>

</page>
