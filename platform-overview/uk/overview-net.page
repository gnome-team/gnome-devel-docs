<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-net" xml:lang="uk">
  <info>
    <link type="guide" xref="index" group="net"/>
    <revision version="0.1" date="2013-08-06" status="review"/>

    <credit type="author copyright">
      <name>Девід Кінг (David King)</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Обмін даними HTTP між клієнтом і сервером, придатні до портування засоби для обробки вхідних і вихідних даних у мережі на основі сокетів та керування пристроями для роботи у мережі.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Низькорівнева робота у мережі</title>

<list>
 <item>
  <p><em style="strong">Створення потужних і гнучких серверів і клієнтів HTTP</em></p>
 </item>
 <item>
  <p><em style="strong">Використання портативних програмних інтерфейсів на основі сокетів у інтерфейсі користувача без блокування</em></p>
 </item>
 <item>
  <p><em style="strong">Виявлення стану з'єднання мережі та керування ним</em></p>
 </item>
</list>

<p>Скористайтеся перевагами портативних програмних інтерфейсів мережі для доступу до служб мережі. Асинхронний підхід до введення-виведення надаватиме можливість користуватися графічним інтерфейсом вашої програми, доки виконуються дії із введення та виведення даних. Виявляйте зміни у стані мережі системи, щоб ваша програма належним чином відповідала на виклики, коли немає доступу до інтернету.</p>

<!-- TODO: Add screenshot.
<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>IMAGE</p>
</media>
-->

<section id="what">
  <title>Що робити?</title>

  <p>Для <em style="strong">асинхронного доступу до низькорівневих програмних інтерфейсів</em> скористайтеся <em style="strong" xref="tech-gio-network">мережевим GIO</em>. Для <em style="strong">роботи з проксі та записами DNS</em> доступний високорівневий інтерфейс, також реалізовано використання <em style="strong">безпечних сокетів (TLS)</em>.</p>

  <p>У GIO є доступним просте спостереження за станом мережі, а <em style="strong" xref="tech-network-manager">NetworkManager</em> надає можливості з <em style="strong">повноцінної підтримки пристроїв мережі</em> та топології мережі.</p>

  <p><em style="strong" xref="tech-soup">Libsoup</em> забезпечує роботу гнучкого інтерфейсу до <em style="strong">серверів і клієнтів HTTP</em>. Надається доступ як до синхронних, так і до асинхронних програмних інтерфейсів.</p>

</section>

<!-- TODO Add link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
  <title>Приклади з реального життя</title>

  <p>Мережеві технології GNOME застосовуються у багатьох проєктах із відкритим кодом. Деякі з прикладів наведено нижче.</p>
  <list>
    <item>
      <p><em style="strong">Тенета</em> — браузер GNOME, у якому для доступу до служб HTTP використовується libsoup.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Web">Сайт</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/raw/master/data/screenshot.png">Знімок вікна</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/">Початковий код</link> )</p>
    </item>
    <item>
      <p><em style="strong">GNOME Shell</em> — видиме для користувача стільничне середовище GNOME, яке використовує NetworkManager для меню стану мережі, включно з керування дротовими, бездротовими мережами, 3G модемами та мережевими системами VPN.</p>
      <p>( <link href="https://wiki.gnome.org/Projects/GnomeShell">Сайт</link> | <link href="http://www.gnome.org/gnome-3/">Знімок вікна</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-shell/">Початковий код</link> )</p>
    </item>
    <!-- TODO: Add low-level GIO network IO example. -->
  </list>
</section>
</page>
