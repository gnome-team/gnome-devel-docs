<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-net" xml:lang="ko">
  <info>
    <link type="guide" xref="index" group="net"/>
    <revision version="0.1" date="2013-08-06" status="review"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>클라이언트/서버 HTTP 통신, 이식성을 갖춘 소켓 기반 네트워크 입출력, 네트워크 장치 관리 기능을 갖춥니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2017, 2018.</mal:years>
    </mal:credit>
  </info>

<title>저수준 네트워크</title>

<list>
 <item>
  <p><em style="strong">강력하고 유연한 HTTP 서버/클라이언트를 만듭니다</em></p>
 </item>
 <item>
  <p><em style="strong">사용자 인터페이스 동작을 멈추게 하지 않는, 이식성을 갖춘 소켓 기반 API를 활용합니다</em></p>
 </item>
 <item>
  <p><em style="strong">네트워크 연결 상태를 확인하고 관리합니다</em></p>
 </item>
</list>

<p>네트워크 서비스에 접근할 때 이식성을 갖춘 네트워크 API의 장점을 취하십시오. 비동기 입출력 처리 과정은 입출력을 처리하는 과정에서도 프로그램의 UI를 반응할 수 있게 합니다. 네트워크 연결이 끊어졌을 때 프로그램에서 제대로 응답하도록 시스템 네트워크의 상태 바뀜을 확인하십시오.</p>

<!-- TODO: Add screenshot.
<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>IMAGE</p>
</media>
-->

<section id="what">
  <title>뭘 할 수 있을까요?</title>

  <p><em style="strong">저수준 네트워크 API에 비동기 방식으로 접근하려면</em>, <em style="strong" xref="tech-gio-network">GIO 네트워크 기술</em>을 활용하십시오. <em style="strong">보안 소켓(TLS)</em>을 활용할 때와 마찬가지로 <em style="strong">프록시와 DNS 레코드를 확인하는 용도</em>의 좀 더 레벨이 높은 API가 있습니다.</p>

  <p>GIO에는 단순 네트워크 상태 감시 수단이 있지만, <em style="strong" xref="tech-network-manager">NetworkManager</em>에 네트워크 토폴로지와 <em style="strong">실질적인 네트워크 장치 지원 기술</em>이 들어있습니다.</p>

  <p><em style="strong" xref="tech-soup">libsoup</em> <em style="strong">HTTP 서버 및 클라이언트</em>에 유연한 인터페이스를 제공합니다. 동기 및 비동기 API를 모두 제공합니다.</p>

</section>

<!-- TODO Add link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
  <title>실제 예제</title>

  <p>아래 보여드리는 예제에서 오픈 소스 프로젝트에 그놈 네트워크 기술을 활용한 여러가지 실제 프로그램을 볼 수 있습니다.</p>
  <list>
    <item>
      <p><em style="strong">웹</em>은 HTTP 서비스에 접근할 때 libsoup를 활용하는 그놈 브라우저입니다.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Web">Website</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/raw/master/data/screenshot.png">Screenshot</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/">Source code</link> )</p>
    </item>
    <item>
      <p><em style="strong">그놈 셸</em>은 사용자가 볼 수 있으며, 네트워크 상태 메뉴에 유무선, 3G 모뎀, VPN 네트워크망 관리 기능을 갖춘 NetworkManager를 띄우는 그놈 데스크톱입니다.</p>
      <p>( <link href="https://wiki.gnome.org/Projects/GnomeShell">Website</link> | <link href="http://www.gnome.org/gnome-3/">Screenshot</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-shell/">Source Code</link> )</p>
    </item>
    <!-- TODO: Add low-level GIO network IO example. -->
  </list>
</section>
</page>
