<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-d-bus" xml:lang="ja">

  <info>
    <link type="guide" xref="tech" group="d-bus"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Inter-process communication bus to provide APIs to other
    processes</desc>
  </info>

<title>D-Bus</title>

<p>D-Bus is a message bus for sending messages between
various applications, the desktop, and low-level components of the system.
D-Bus provides a simple API for sending messages to particular services
and for broadcasting messages to all interested services.  D-Bus enables
different types of applications to communicate and integrate with each
other and with the desktop, providing better interaction and a richer
experience for the user.  </p>

<p>D-Bus provides a session bus and a system bus.  The session bus is
used by applications in a single user session, allowing them to share
data and event notifications and to integrate into the user's desktop.
For example, movie players can send a D-Bus message to prevent the
screensaver from activating while the user is watching a movie.</p>

<p>システムバスはユーザーセッションとは独立して実行される単一のメッセージバスです。システムバスはアプリケーションと通信することにより、それらのアプリケーションがシステムの低レベルな詳細を知ることなくシステムコンポーネントと相互作用することを可能にします。システムバスはユーザーが彼らのシステムで動作することを期待するであろう重要な機能を提供します。たとえば、システムバスはネットワークインターフェースの UP / DOWN や外部デバイスがプラグインされたり、ラップトップのバッテリが低下するのをモニターするのに使用されます。</p>

<p>D-Bus is developed jointly on <link href="http://www.freedesktop.org/">freedesktop.org</link>, so you can
use it with different desktop environments and applications.  Because
D-Bus is a cross-desktop project, you use it to create portable and
versatile software that seamlessly integrates with the user's desktop,
regardless of which desktop it is.</p>

  <p>GNOME provides full support for D-Bus using the GDBus APIs in
  <link xref="tech-gio">GIO</link>.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/gio/stable/">GIO Reference Manual</link></p></item>
  <item><p><link href="http://dbus.freedesktop.org/doc/dbus-tutorial.html">D-Bus Tutorial</link></p></item>
  <item><p><link href="http://dbus.freedesktop.org/doc/dbus-specification.html">D-Bus Specification</link></p></item>
</list>

</page>
