<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-geoclue2" xml:lang="gl">

  <info>
    <link type="guide" xref="tech" group="geoclue2"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Geolocation - finding the user's geographical location</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2009-2020.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leandro Regueiro</mal:name>
      <mal:email>leandro.regueiro@gmail.com</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  </info>

<title>Geoclue2</title>

  <p>
    Geoclue2 provides <em>geolocation</em> services, that is, it lets
    applications find the user's geographical position.  A mapping
    application could use this to present a "where am I" view, for
    example.
  </p>

  <p>
    Geoclue2 intends to be able to provide positioning information
    in several ways:  from the GPS in a WWAN modem, from a Wi-Fi
    access point's data, or from the computer's IP address.
  </p>

  <p>
    Geoclue2 also intends to provide privacy for users who do not want
    to reveal their geographical location.  Only applications allowed
    by the user will be able to get geolocation information.
  </p>

</page>
