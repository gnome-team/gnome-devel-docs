<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gio" xml:lang="gl">

  <info>
    <link type="guide" xref="tech" group="gio"/>
    <revision pkgversion="3.0" date="2011-04-05" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011</years>
    </credit>
    <credit type="copyright editor">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>File and URI handling, asynchronous file operations, volume
    handling</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2009-2020.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leandro Regueiro</mal:name>
      <mal:email>leandro.regueiro@gmail.com</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  </info>

<title>Ficheiros GIO</title>

<p>GIO provides APIs for asynchronously reading and writing files and
other streams. Files are referenced by URIs (uniform resource identifiers), and backends can
provide access to more than just local files. When running under the
GNOME desktop, GIO uses GVFS to allow access to files over SFTP, FTP,
WebDAV, SMB, and other popular protocols. This transparent network
file access is free to all applications using GIO.</p>

<p>As API de ficheiro de GIO están deseñadas para ser usadas en interfaces gráficas baseadas en eventos. O deseño non bloqueante e asíncrono significa que a súa interface de usuario non se bloqueará mentres agarda por un ficheiro. Tamén existen versións síncronas das API, que son en ocasións máis convenientes para o traballo de fíos ou procesos.</p>

<p>GIO tamén fornece rutinas para xestionar unidades e volumes, consulta de tipos de dato e iconas ademais de buscar aplicacións para abrir ficheiros.</p>

<list style="compact">
  <item><p><link href="http://developer.gnome.org/gio/stable/">Manual de referencia de GIO</link></p></item>
</list>

</page>
