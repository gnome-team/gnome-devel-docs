<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-media" xml:lang="cs">
  <info>
    <link type="guide" xref="index" group="media"/>
    <revision version="0.1" date="2013-06-19" status="draft"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Přehrávání a úpravy zvuku a videa v různých formátech, vysílání přes web a podpora webových kamer</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lucas Lommer</mal:name>
      <mal:email>llommer@svn.gnome.org</mal:email>
      <mal:years>2009.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  </info>

<title>Multimédia</title>

<list>
 <item>
  <p><em style="strong">Přehrávání a nahrávání hromady formátů zvuku a videa</em></p>
 </item>
 <item>
  <p><em style="strong">Přístup k webovým kamerám a dalším multimediálním zařízením přípojeným k systému</em></p>
 </item>
 <item>
  <p><em style="strong">Sdílení a vysílání multimédiálního obsahu pomocí vzdálených zařízení</em></p>
 </item>
</list>

<p>Přidejte do své aplikace multimédia, která si uživatelé mohou snadno přehrát. Získávejte obsah a sdílejte jej s dalšími zařízeními, ať už připojenými k systému nebo vzdáleně přes síť. Pokud potřebujete detailnější kontrolu, je k dispozici i podkladové nízkoúrovňové API.</p>

<media type="image" mime="image/png" src="media/totem-screenshot.png" width="65%">
 <p>Videa</p>
</media>

<section id="what">
 <title>Co můžete dělat?</title>

  <p><em style="strong">Multimédia</em> jsou v GNOME postavena na konstrukci <em style="strong"><link xref="tech-gstreamer">GStreamer</link></em>. Jedná se o flexibilní <em>roury</em>, ve kterých mohou být multimédia vytvářena v podobě od jednoduchého přehrávání zvuku a videa až po komplexní nelineární úravy.</p>

  <p>GStreamer používá pro výstup zvuku <em style="strong"><link xref="tech-pulseaudio">PulseAudio</link></em>, takže jej může směřovat na mnoho typů výstupního hardwaru. PulseAudio ovládá i dynamické přepínání výstupů a nastavení hlasitosti odděleně pro jednotlivé aplikace.</p>

  <p>Pro <em style="strong">webové kamery</em> použijte <em style="strong">Cheese</em>. Poskytuje jednoduché rozhraní k webovým kamerám připojeným k systému a snadný způsob, jak přidat výběr avatara do vaší aplikace.</p>

  <p>Ke <em style="strong">sdílení obsahu přes síť</em> do zařízení, jako jsou televize nebo herní konzole, použijte <em style="strong">Rygel</em>. Rygel používá na pozadí <em style="strong"><link xref="tech-gupnp">GUPnP</link></em>, což je nízkoúrovňové API pro přístup k obsahu pomocí protokolů <em style="strong">UPnP</em>.</p>

  <p>Pro jednoduché <em style="strong">zvukové události</em>, jako je zvuk závěrky fotoaparátu při pořízování snímku, použijte knihovnu <em style="strong"><link xref="tech-canberra">libcanberra</link></em>, která implementuje specifikaci zvukových motivů od freedesktop.org.</p>

</section>

<!-- TODO: Link to code examples if they are moved to the platform overview
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples#integration">More...</link></p></item>
 </list>
</section>
-->

<section id="realworld">
 <title>Reálné příklady</title>

  <p>V projektech s otevřeným kódem můžete najít řadu reálných aplikací využívajích multimediální technologie GNOME. K takovýmto aplikacím patří například tyto:</p>
  <list>
    <item>
      <p><em style="strong">Videa</em> je multimediální přehrávač GNOME.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Videos">Webové stránky</link> | <link href="https://gitlab.gnome.org/GNOME/totem/blob/master/data/appdata/ss-videos.png">Snímek obrazovky</link> | <link href="https://gitlab.gnome.org/GNOME/totem/">Zdrojový kód</link> )</p>
    </item>
    <item>
      <p><em style="strong">PiTiVi</em> je nelineární editor videa, který silně využívá GStreamer.</p>
      <p>( <link href="http://www.pitivi.org/">Webové stránky</link> | <link href="http://www.pitivi.org/?go=tour">Snímky obrazovky</link> | <link href="http://www.pitivi.org/?go=download">Zdrojový kód</link> )</p>
    </item>
 </list>
</section>
</page>
