<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-ui" xml:lang="cs">
  <info>
    <link type="guide" xref="index" group="ui"/>
    <revision version="0.1" date="2013-06-19" status="stub"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Standardní prvky, vykreslování a animace v uživatelském rozhraní</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lucas Lommer</mal:name>
      <mal:email>llommer@svn.gnome.org</mal:email>
      <mal:years>2009.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  </info>

<title>Uživatelské rozhraní a grafika</title>

<list>
 <item>
  <p><em style="strong">Používání jediné sady nástrojů pro všechny standardní widgety ve vaší aplikaci</em></p>
 </item>
 <item>
  <p><em style="strong">Vytváření rychlých, vizuálně bohatých grafických rozhraní</em></p>
 </item>
 <item>
  <p><em style="strong">Vysoce kvalitní, vyhlazená a na rozlišení nezávislá grafika</em></p>
 </item>
 <item>
  <p><em style="strong">Snadné přidání webové funkcionality do vašich aplikací</em></p>
 </item>
 <item>
  <p><em style="strong">Přístup k zabudovaným technologiím přístupnosti</em></p>
 </item>
</list>

<p>Používejte mocné základy platformy GNOME k vytváření konzistentního a pružného uživatelského rozhraní. Udělejte své aplikace dostupné pro široké spektrum uživatelů nasazením i pro jiné platformy. Standardní prvky uživatelského rozhraní podporují samy o sobě přístupnost, ale lze ji snadno přidat do libovolného vlastního prvku uživatelského rozhraní.</p>

<section id="what">
 <title>Co můžete dělat?</title>

  <p>Pro aplikace se <em style="strong">standardními ovládacími prvky</em>, které jsou uživatelům dobře známé, použijte <em style="strong"><link xref="tech-gtk">GTK</link></em>. Každá aplikace, která je součástí GNOME používá GTK+, protože díky tomu je jednotná se zbytkem prostředí a má přístup k mnoha widgetům a funkcím, jako je třeba podpora tisku a motivy založné na CSS.</p>

  <p><em style="strong">Animace, efekty a proměnlivý vzhled</em> jsou díky knihovně <em style="strong"><link xref="tech-clutter">Clutter</link></em> snadnou záležitostí, stejně jako podpora dotekového vstupu a gest.</p>

  <p><em style="strong">Vysoce kvalitní, vyhlazenou a na rozlišení nezávislou grafiku ve 2D</em> poskytuje knihovna <em style="strong"><link xref="tech-cairo">Cairo</link></em>. Cairo se používá pro kreslení widgetů v GTK a může být rovněž použita pro výstup do PDF a SVG.</p>

  <p><em style="strong"><link xref="tech-webkit">WebKitGTK</link></em> vám usnadní přidání <em style="strong">webové funkcionality</em> do vaší aplikace, ať už za účelem vykreslení souboru HTML nebo uživatelského rozhraní s plnou podporou HTML5.</p>

  <p>GTK, Clutter a WebKitGTK mají <em style="strong">vestavěnou podporu pro technologie zpřístupnění</em> pomocí <em style="strong"><link xref="tech-atk">ATK</link></em>. Použijte nástroje Orca, Caribou OSK a nástroje přístupnosti vestavěné v GTK+ nebo si sestavte vlastní nástroje nad ATK.</p>

</section>

<!-- TODO Link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
 <title>Reálné příklady</title>

  <p>V projektech s otevřeným kódem můžete najít řadu reálných aplikací využívajích technologie uživatelského rozhraní GNOME. K takovýmto aplikacím patří například tyto:</p>
  <list>
  <item>
    <p><em style="strong">WWW</em> je webový prohlížeč GNOME, který používá GTK a WebKitGTK a plně podporuje přístupnost.</p>
    <p>( <link href="https://wiki.gnome.org/Apps/Web">Webové stránky</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/">Zdrojový kód</link> )</p>
  </item>
  <item>
    <p><em style="strong">MonoDevelop</em> je multiplatformní IDE navržené pro C# a další jazyky .NET. Funguje pod Linuxem, Mac OS X a Windows.</p>
    <p>( <link href="https://www.monodevelop.com/">Webové stránky</link> | <link href="https://www.monodevelop.com/screenshots/">Snímky obrazovky</link> | <link href="https://github.com/mono/monodevelop">Zdrojový kód</link> )</p>
  </item>
  <item>
    <p><em style="strong">Videa</em> je multimediální přehrávat GNOME a k zobrazení videoobsahu využívá Clutter.</p>
    <p>( <link href="https://wiki.gnome.org/Apps/Videos">Webové stránky</link> | <link href="https://gitlab.gnome.org/GNOME/totem/">Zdrojový kód</link> )</p>
  </item>
 </list>
</section>
</page>
