<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tour-glade" xml:lang="de">

  <info>
    <link type="next" xref="tour-gjs"/>
    <revision version="0.1" date="2017-09-16" status="stub"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Gordon Hill</name>
      <email its:translate="no">caseyweederman@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Verwenden Sie Glade zur Erstellung einer UI-Datei.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Erstellen einer Benutzeroberfläche</title>
  <links type="prevnext" style="top"/>
  <links type="series" style="floatend">
    <title>Erste Schritte mit GNOME</title>
  </links>

  <p>Verwenden Sie <link href="http://glade.gnome.org/">Glade</link>, den UI-Editor für GNOME, um eine Benutzeroberfläche zu erstellen. Glade erzeugt XML-Dateien, die die Attribute einer Anwendung beschreiben.</p>

  <p>Wenn Sie eine UI-Datei mit Glade erstellen, ist die angegebene <gui>ID</gui> die Kennung, die Sie später benötigen, um das Objekt aus der UI-Datei zu beziehen.</p>

  <example>
    <p>Erstellen Sie ein neues Fenster und setzen Sie <gui>ID</gui> auf <code>window1</code> und <gui>Titel</gui> auf <code>Hello World</code>:</p>
    <media type="image" src="media/glade-set-values.png">
      <p>Bildschirmfoto der Wertzuweisung in Glade.</p>
    </media>
    <p>Speichern Sie die Datei mit <guiseq><gui>Datei</gui><gui>Speichern unter</gui></guiseq> und nennen Sie sie <file>helloworld.glade</file>.</p>
  </example>
  <links type="prevnext"/>
</page>
