<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-launching-icons" xml:lang="de">

  <info>
    <link type="next" xref="dev-launching-startupnotify"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Installation eines Symbols</title>

  <links type="series" style="floatend">
    <title>Start Ihrer Anwendung</title>
  </links>

  <p>Installieren Sie ein Symbol zur einfachen Identifikation Ihrer Anwendung.</p>

  <p>Wenn in einer *.desktop-Datei ein Symbol benannt wird, muss das zugehörige Bild an einem Standardort installiert sein, der durch die von Freedesktop.org geschaffenen Spezifikationen <link href="http://standards.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html">Icon Theme</link> und <link href="http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html">Icon Naming</link> beschrieben wird. Die Mindestanforderung ist ein Symbol im PNG-Format im Pfad <file><var>$prefix</var>/share/icons/hicolor/48x48/apps</file>, das mindestens 48x48 Pixel groß ist.</p>

  <p>Damit es sowohl unter GNOME als auch auf anderen Plattformen zu anderen Symbolen passt, folgen Sie den <link href="http://tango.freedesktop.org/Tango_Icon_Theme_Guidelines">Richtlinien für Tango-Symbolthemen</link>, wenn Sie Symbole und sonstige künstlerische Teile für eine Anwendung erstellen.</p>

</page>
