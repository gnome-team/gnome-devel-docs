<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-media" xml:lang="de">
  <info>
    <link type="guide" xref="index" group="media"/>
    <revision version="0.1" date="2013-06-19" status="draft"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wiedergabe und Bearbeitung von Audio- und Video-Inhalten in verschiedenen Formaten, Streamen aus dem Netz und Unterstützung für Webcams.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Multimedia</title>

<list>
 <item>
  <p><em style="strong">Eine Vielzahl von Audio- und Video-Formaten wiedergeben und aufzeichnen</em></p>
 </item>
 <item>
  <p><em style="strong">Zugriff auf Webcams und andere am System angeschlossene Geräte</em></p>
 </item>
 <item>
  <p><em style="strong">Medien auf entfernten Geräten freigeben und von dort streamen</em></p>
 </item>
</list>

<p>Fügen Sie Multimedia-Unterstützung zu Ihrer Anwendung hinzu, damit Benutzer ihre Inhalte einfach wiedergeben können. Tauschen Sie Inhalte mit anderen Geräten aus, die am System angeschlossen sind oder auf die über das Netzwerk zugegriffen werden kann. Für mehr Steuerungsmöglichkeiten ist die darunterliegende Low-Level-API ist ebenfalls verfügbar.</p>

<media type="image" mime="image/png" src="media/totem-screenshot.png" width="65%">
 <p>Videos</p>
</media>

<section id="what">
 <title>Was können Sie tun?</title>

  <p><em style="strong">Multimedia</em> in GNOME baut auf dem <em style="strong"><link xref="tech-gstreamer">GStreamer</link></em>-Framework auf. Mit GStreamer können flexible <em>Medien-Weiterleitungen</em> (Pipelines) erstellt werden, von der einfachen Audio- und Video-Wiedergabe bis hin zur komplexen nichtlinearen Bearbeitung.</p>

  <p>GStreamer verwendet <em style="strong"><link xref="tech-pulseaudio">PulseAudio</link></em> für die Klangausgabe. Daher sind als Zielgeräte zahlreiche Hardwaretypen verwendbar. PulseAudio beherrscht auch die dynamische Umschaltung der Ausgabe und die anwendungsspezifische Lautstärkeumschaltung.</p>

  <p>Für <em style="strong">Webcams</em> verwenden Sie <em style="strong">Cheese</em>. Es stellt eine simple Schnittstelle zu den am System angeschlossenen Webcams bereit sowie einen einfachen Weg, einen Avatar-Wähler in Ihre Anwendung einzubauen.</p>

  <p>Mit <em style="strong">Rygel</em> können Sie <em style="strong">Inhalte im Netzwerk freigeben</em>, an Geräte wie beispielsweise Fernseher oder Spielkonsolen. Rygel setzt auf <em style="strong"><link xref="tech-gupnp">GUPnP</link></em> auf, eine Low-Level-API zum Zugriff auf Inhalte über die <em style="strong">UPnP</em>-Protokolle.</p>

  <p>Für einfache <em style="strong">Ereignisklänge</em>, wie das Geräusch eines Kameraverschlusses beim Aufnehmen eines Fotos, können Sie <em style="strong"><link xref="tech-canberra">libcanberra</link></em> verwenden. Diese Bibliothek implementiert die Klangthemen-Spezifikation von Freedesktop.org.</p>

</section>

<!-- TODO: Link to code examples if they are moved to the platform overview
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples#integration">More...</link></p></item>
 </list>
</section>
-->

<section id="realworld">
 <title>Reale Beispiele</title>

  <p>Nachfolgend finden Sie einige Beispiele aus Anwendungen der realen Welt, in denen GNOME-Multimediatechnologien in Projekten der freien Software verwendet werden.</p>
  <list>
    <item>
      <p><em style="strong">Videos</em> ist das Multimedia-Wiedergabeprogramm für GNOME.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Videos">Internetseite</link> | <link href="https://gitlab.gnome.org/GNOME/totem/blob/master/data/appdata/ss-videos.png">Bildschirmfoto</link> | <link href="https://gitlab.gnome.org/GNOME/totem/">Quellcode</link> )</p>
    </item>
    <item>
      <p><em style="strong">PiTiVi</em> ist ein nichtlinearer Videoeditor, der ausgiebig GStreamer nutzt.</p>
      <p>( <link href="http://www.pitivi.org/">Internetseite</link> | <link href="http://www.pitivi.org/?go=tour">Bildschirmfotos</link> | <link href="http://www.pitivi.org/?go=download">Quellcode</link> )</p>
    </item>
 </list>
</section>
</page>
