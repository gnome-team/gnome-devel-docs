<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gtk" xml:lang="de">

  <info>
    <link type="guide" xref="tech" group="gtk"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Werkzeugsatz zum Erstellen von grafischen Benutzeroberflächen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>GTK</title>

<p>GTK ist die primäre Bibliothek in GNOME, die zum Erstellen von Benutzeroberflächen verwendet wird. Der Name steht für »GIMP Tool Kit«, weil es ursprünglich nur für diese Bildbearbeitung entwickelt wurde und später sich davon löste. Es stellt alle in einer grafischen Anwendung üblicherweise verwendeten Bedienelemente bereit, sogenannte <em>Widgets</em>. Die moderne, objektorientierte Schnittstelle ermöglicht Ihnen, attraktive und anspruchsvolle Benutzeroberflächen zu entwickeln, ohne sich mit den systemnahen Details wie dem Zeichnen und dem Zusammenspiel mit Geräten auseinander setzen zu müssen.</p>

<p>Zusätzlich zu grundlegenden Bedienelementen, wie z.B. Knöpfen, Ankreuzfeldern und Textfeldern, bietet GTK auch leistungsfähige Modell-Ansicht-Steuerung (Model-View-Controller, MVC)-Schnittstellen für Baumansichten, mehrzeilige Textfelder sowie Menüleisten- und Werkzeugleistenaktionen.</p>

<p>Bedienelemente werden in GTK unter Verwendung eines <em>Boxenmodells</em> in Fenstern platziert. Programmierer geben lediglich an, wie Bedienelemente miteinander in Containerboxen gepackt werden sollen, statt sie direkt mit absoluten Koordinaten zu positionieren. GTK stellt sicher, dass Fenster groß genug für ihren Inhalt sind und es handhabt automatisch Größenänderungen der Fenster. Für Schriften wie Arabisch und Hebräisch, die von rechts nach links verlaufen, kehrt GTK automatisch die Benutzerschnittstelle um, so dass Bedienelemente die erwartete optische Reihenfolge haben.</p>

<p>GTK ermöglicht Ihnen die Entwicklung eigener Widgets zur Nutzung in Anwendungen. Ähnlich wie native oder »Stock«-Widgets können diese alle Funktionsmerkmale unterstützen, die GTK bietet: Unterstützung für linksläufige Sprachen, barrierefreie Schnittstellen, Tastaturnavigation und automatische Größenanpassung.</p>

<list style="compact">
  <item><p><link href="https://www.gtk.org/">Offizielle Webseite</link></p></item>
  <item><p><link href="https://developer.gnome.org/gnome-devel-demos/stable/">Demo-Tutorial</link></p></item>
  <item><p><link href="https://developer.gnome.org/gtk3/stable/">Referenzhandbuch</link></p></item>
  <item><p><link href="https://gitlab.gnome.org/GNOME/gtk/">Git-Softwarebestand</link></p></item>
  <item><p><link href="https://discourse.gnome.org/">Allgemeine Diskussionen über GTK, wo Sie auch Hilfe erhalten</link></p></item>
</list>
</page>
