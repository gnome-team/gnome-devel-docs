<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-tracker" xml:lang="sv">

  <info>
    <link type="guide" xref="tech" group="tracker"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Lagring och framtagande av dokumentmetadata</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Tracker</title>

  <p>Tracker är en lagringsmotor för RDF (resursdataformat). RDF består av <em>tripplar</em> så som ämne:verb:objekt. Till exempel skulle det kunna finnas standardtripplar för boktitlar och författare, så som <code>Othello:has-author:William Shakespeare</code>. En standardiserad uppsättning tripplar kallas en <em>ontologi</em>.</p>

  <p>Tracker tillhandahåller en lagring för sådana tripplar, och en frågemotor i form av SPARQL-frågespråket.</p>

  <p>GNOME använder Tracker som en lagring för dokumentmetadata. Ett dokuments metadata kan inkludera dess titel, författare, copyright, ändringsdatum samt nyckelord. Alla dessa metadata lagras som RDF-tripplar i Tracker, och efterfrågas med SPARQL av program så som GNOME Dokument. <link href="http://developer.gnome.org/ontology/unstable/">Ontologin som används</link> är baserad på diverse standardiserade underontologier: <link href="http://dublincore.org/">Dublin Core</link> för dokumentmetadata, och <link href="http://nepomuk.kde.org/">NEPOMUK</link> för kommentarer, filer, kontakter och andra objekt.</p>

  <list style="compact">
    <item><p><link href="https://wiki.gnome.org/Projects/Tracker">Trackers webbsida</link></p></item>
    <item><p><link href="https://wiki.gnome.org/Projects/Tracker/Documentation">Tracker-dokumentation</link></p></item>
  </list>

</page>
