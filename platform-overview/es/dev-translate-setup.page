<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-translate-setup" xml:lang="es">

  <info>
    <link type="next" xref="dev-translate-build"/>
    <revision version="0.1" date="2013-06-19" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011-2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Carrión</mal:name>
      <mal:email>mario@monouml.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  </info>

  <title>Marcar cadenas para traducción</title>

  <links type="series" style="floatend">
    <title>Configurar las traducciones</title>
  </links>

  <p>Antes de poder traducir las cadenas de su aplicación, debe extraerlas desde el código fuente.</p>

  <p>Envuelva los mensajes o las <em>cadenas literales</em> de su código con la macro «<code>_()</code>».</p>

  <note>
    <p>En C, esta macro se define en el archivo de cabecera <file>glib/gi18n.h</file>, que se debe incluir al principio del código de su aplicación.</p>
  </note>

  <p>Las cadenas deben quedar de la siguiente manera:</p>
  <code>_("Press a key to continue")</code>

  <p>Esto marca las cadenas como traducibles y en tiempo de ejecución se llama a <app>gettext</app> para sustituir las cadenas traducidas.</p>

</page>
