<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="license" xml:lang="es">

  <info>
    <link type="guide" xref="index" group="license"/>
    <revision pkgversion="3.14" date="2014-05-01" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>¿Qué licencia debe usar para su aplicación?</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011-2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Carrión</mal:name>
      <mal:email>mario@monouml.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  </info>

  <title>Licenciar la aplicación</title>

  <p>Cuando escribe una aplicación o una biblioteca nuevas, debe escoger una licencia para que otros sepan cómo puede usar o reutilizar su trabajo.</p>

  <p>Las bibliotecas que se usan en GNOME se licencian generalmente bajo la <link href="https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html">LGPL 2.1+</link>. de <link href="http://www.gnu.org/">GNU</link>.</p>

  <p>La mayoría de las aplicaciones nuevas de GNOME se licencian bajo la <link href="http://www.gnu.org/licenses/gpl-3.0.html">GPL3+</link>, aunque algunas aplicaciones antiguas están licenciadas bajo la <link href="http://www.gnu.org/licenses/gpl-2.0.html">GPL2+</link>.</p>

  <p>La ayuda del usuario la escribe el equipo de documentación de GNOME bajo la <link href="http://creativecommons.org/licenses/by-sa/3.0/">CC-by-SA 3.0 sin soporte</link>. El equipo de documentación trata de usar esta licencia de manera consistente, ya que permite reutilizar texto de la Wikipedia y de otras muchas fuentes de referencia.</p>

  <p>Las traducciones tienen la misma licencia que las cadenas originales. Por ejemplo, las cadenas de las aplicaciones están licenciadas generalmente bajo la GPL2+ o la GPL3+, mientras que la documentación lo está bajo la CC-by-SA 3.0.</p>

  <p>GNOME no puede ofrecer consejo legal sobre qué licencia debe usar, pero puede leer la información disponible en la <link href="http://opensource.org/licenses">Open Source Initiative</link>, la <link href="http://www.gnu.org/licenses/license-recommendations.html">FSF</link> y la <link href="https://blogs.gnome.org/bolsh/2014/04/17/choosing-a-license/">entrada del blog de Dave Neary sobre cómo elegir una licencia</link>. También puede resultarle de interés la <link href="http://gstreamer.freedesktop.org/documentation/licensing.html">información de licencia de GStreamer</link>, ya que GStreamer usa complementos.</p>
</page>
